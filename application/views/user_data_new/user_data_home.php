            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="jumbotron">
                            <h1 class="display-4">Selamat Datang</h1>
                            <p class="lead">Selamat Datang di Halaman Pendafataran Magang dan Penelitian, Silahkan daftarkan program magang/penelitian kalian ke Pemerintah Kota Malang dengan menekan tombol berikut.</p><hr>
                            <p class="lead">
                                <a class="btn btn-info btn-md" href="<?=base_url();?>pendaftaran/mainform" role="button">Daftar Magang / Penelitian</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- Row --> 

                <div class="row">
                    <div class="col-lg-4">
                        <!-- <div class="col-md-12">
                            <div class="card card-outline-info">
                                <div class="card-header">
                                    <h4 class="m-b-0 text-white">Informasi Pendaftaran Magang/Penelitian</h4></div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2"><button type="button" class="btn btn-info"><i class="mdi mdi-account"></i></button></div>
                                        <div class="col-md-8" style="te"><h4>12 Peserta Magang</h4></div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div> -->

                        <div class="col-lg-12">
                            <div class="card">                                
                                <div class="card-body">
                                    <center><h4 class="card-title">Download Template Proposal</h4></center><br>
                                    <h4>  
                                        <button type="submit" align="center" class="btn btn-rounded btn-block btn-info" style="text-align: center;">
                                            <i class="mdi mdi-content-paste"></i>&nbsp;&nbsp;Template Proposal
                                        </button>
                                    </h4>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-lg-8">
                        <div class="col-md-12">
                            <div class="card card-outline-info">
                                <div class="card-header">
                                    <h4 class="m-b-0 text-white">Informasi Pendaftaran Magang dan Penelitian</h4></div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            
                                                    <ul class="search-listing">
                                                        <li>
                                                            <h3>Proposal Pemohon Magang / Penelitian</h3>
                                                            <p>Pengunggahan dokumen proposal dilakukan pada saat pemohon mengisi lengkap form. Pastikan proposal yang ditujukan kepada opd sudah benar, dan apabila ada kendala dalam pembuatan proposal pemohon dapat mengunduh format proposal pada tab kiri bawah di halaman ini.</p>
                                                        </li>
                                                        <li>
                                                            <h3>Data Form Pemohon</h3>
                                                            <p>Sebelum mengirim data pemohon ke admin, pastikan data yang diisikan sudah benar dan terisi lengkap. Jika ada data yang kurang lengkap atau salah, pemohon bisa melakukan perubahan data pada halaman Kelengkapan Data. Setelah selesai melakukan pengecekan, pemohon bisa mengirim data tersebut dengan menekan tombol Konfirmasi Laporan</p>
                                                        </li>
                                                        <li>
                                                            <h3>Lokasi Magang / Penelitian</h3>
                                                            <p>Pilih lokasi magang yang telah tersedia, pastikan untuk memilih setidaknya satu tempat magang dan tiga tempat magang opsional yang bisa digunakan oleh pemohon.</p>
                                                        </li>
                                                        <li>
                                                            <h3>Anggota Kelompok</h3>
                                                            <p>Untuk pemohon yang melakukan magang / penelitian lebih dari satu orang, dimohon untuk mengisikan tabel nama anggota pada halaman form. Bila tidak ada anggota kelompok tabel tersebut bisa dikosongi</p>
                                                        </li>
                                                        
                                                    </ul>
                                                    
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

     
                
                