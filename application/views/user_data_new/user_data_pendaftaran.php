            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid"><br><br>
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <?php 
                  if(isset($get_verifikation_data["list_vert_all"])){
                    if($get_verifikation_data["list_vert_all"]){

                      // print_r($get_verifikation_data["main_vert_all"]);

                      $html_icon_success = "<i class=\"text-success\"><i class=\"fa fa-check\"></i></i>";
                      $html_icon_fail = "<i class=\"text-danger\"><i class=\"fa fa-times\"></i></i>";

                      $vert_user_html_icon = $html_icon_success;
                      $vert_permohonan_html_icon = $html_icon_success;
                      $vert_delegasi_html_icon = $html_icon_success;
                      $vert_doc_html_icon = $html_icon_success;
                      

                      if($get_verifikation_data["list_vert_all"]["vert_user"] == "0"){
                        $vert_user_html_icon = $html_icon_fail;
                      }

                      if($get_verifikation_data["list_vert_all"]["vert_permohonan"] == "0"){
                        $vert_permohonan_html_icon = $html_icon_fail;
                      }

                      if($get_verifikation_data["list_vert_all"]["vert_delegasi"] == "0"){
                        $vert_delegasi_html_icon = $html_icon_fail;
                      }

                      if($get_verifikation_data["list_vert_all"]["vert_doc"] == "0"){
                        $vert_doc_html_icon = $html_icon_fail;
                      }
                    }
                  }
                ?>
                <div class="row">
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="stickyside">
                            <div class="list-group" id="top-menu">
                                <a href="#1" class="list-group-item" style="color: black;"><i class="mdi mdi-account"></i>Periksa Data Akun &nbsp;<?= $vert_user_html_icon;?></a>
                                <!-- <a href="#2" class="list-group-item" style="color: black;"><i class="mdi mdi-seat-recline-normal"></i> Input Data Status Pekerjaan &nbsp;<i class="text-danger"><i class="fa fa-times"></i></i></a> -->
                                <a href="#3" class="list-group-item" style="color: black;"><i class="mdi mdi-book-open"></i> Input Data Magang/Penelitian &nbsp;<?= $vert_permohonan_html_icon;?></a>
                                <a href="#4" class="list-group-item" style="color: black;"><i class="mdi mdi-account-switch"></i> Input Data Pendelegasian &nbsp;<?= $vert_delegasi_html_icon;?></a>
                                <a href="#5" class="list-group-item" style="color: black;"><i class="mdi mdi-book-multiple-variant"></i> Input Data Dokumen &nbsp;<?= $vert_doc_html_icon;?></a>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="col-md-12" id="1">
                          <div class="ribbon-wrapper card">
                              <div class="ribbon ribbon-bookmark ribbon-info">
                                <i class="mdi mdi-account"></i>&nbsp;&nbsp;&nbsp;Periksa Data Akun
                              </div>
                              <?php
                                $nama_user = "";
                                $nik = "";
                                $tgl_lhr = "";
                                $alamat = "";
                                $email = "";
                                $tlp = "";
                                $alamat_dom = "";
                                $alamat_dom = "";
                                $instansi = "";
                                $Pekerjaan = "";

                                $url_foto_profil =  "";
                                $url_foto_profil_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                
                                if(isset($user)){
                                  if($user){
                                    $nama_user = $user["nama"];
                                    $email = $user["email"];
                                    $alamat_dom = $user["alamat_dom"];
                                    $alamat = $user["alamat"];
                                    $tgl_lhr = $user["tgl_lhr"];
                                    $nik = $user["nik"];
                                    $tlp = $user["tlp"];
                                    $instansi = $user["instansi"];
                                    $pekerjaan = $user["pekerjaan"];

                                    $url_foto_profil =  $user["url_profil"];

                                    if($url_foto_profil != "" && $url_foto_profil != "0"){
                                      $url_foto_profil_fix = base_url()."doc/foto_pemohon/".$url_foto_profil;  
                                    }else {
                                      $url_foto_profil_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }
                                  }
                                }

                                $id_pemohon = "";
                                $id_permohonan = "";
                                $keterangan_permohonan = "";
                                $no_register = "";
                                $tgl_start = "";
                                $tgl_selesai = "";
                                $judul = "";
                                $instansi_penerima = "";
                                $id_bidang = "[]";
                                $dosen_pembimbing = "[]";
                                $anggota = "[]";
                                $status_pendelegasian = "";
                                $url_ktp_pemohon = "";
                                $url_proposal_pemohon = "";
                                $url_sk_pemohon = "";
                                $jabatan_tdd = "";
                                $nama_tdd = "";
                                $no_surat_tdd = "";
                                $tgl_surat_tdd = "";
                                $alamat_dom_del = "";
                                $alamat_ktp_del = "";
                                $nama_del = "";
                                $nik_del = "";
                                $url_foto_del = "";
                                $url_ktp_del = "";
                                $url_srt_del = "";

                                $url_foto_del_fix = "";
                                $url_ktp_del_fix = "";
                                $url_srt_del_fix = "";

                                $url_sk_pemohon_fix = base_url()."assets/core_img/icon_upload_400X650.png";
                                $url_ktp_pemohon_fix = base_url()."assets/core_img/icon_upload_400x253.png"; 

                                $url_foto_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                $url_ktp_del_fix = base_url()."assets/core_img/icon_upload_400x253.png";
                                $url_srt_del_fix = base_url()."assets/core_img/icon_upload_400X650.png";
                                    

                                if(isset($pendaftaran)){
                                  if($pendaftaran){
                                    $id_user = $pendaftaran["id_user"];
                                    $id_pemohon = $pendaftaran["id_pemohon"];
                                    $id_permohonan = $pendaftaran["id_permohonan"];
                                    $keterangan_permohonan = $pendaftaran["keterangan_permohonan"];
                                    $no_register = $pendaftaran["no_register"];
                                    $tgl_start = $pendaftaran["tgl_start"];
                                    $tgl_selesai = $pendaftaran["tgl_selesai"];
                                    $judul = $pendaftaran["judul"];
                                    $instansi_penerima = $pendaftaran["instansi_penerima"];
                                    $id_bidang = $pendaftaran["id_bidang"];
                                    $dosen_pembimbing = $pendaftaran["dosen_pembimbing"];
                                    $anggota = $pendaftaran["anggota"];
                                    $status_pendelegasian = $pendaftaran["status_pendelegasian"];
                                    $url_ktp_pemohon = $pendaftaran["url_ktp_pemohon"];
                                    $url_proposal_pemohon = $pendaftaran["url_proposal_pemohon"];
                                    $url_sk_pemohon = $pendaftaran["url_sk_pemohon"];
                                    $jabatan_tdd = $pendaftaran["jabatan_tdd"];
                                    $nama_tdd = $pendaftaran["nama_tdd"];
                                    $no_surat_tdd = $pendaftaran["no_surat_tdd"];
                                    $tgl_surat_tdd = $pendaftaran["tgl_surat_tdd"];
                                    $alamat_dom_del = $pendaftaran["alamat_dom_del"];
                                    $alamat_ktp_del = $pendaftaran["alamat_ktp_del"];
                                    $nama_del = $pendaftaran["nama_del"];
                                    $nik_del = $pendaftaran["nik_del"];
                                    $url_foto_del = $pendaftaran["url_foto_del"];
                                    $url_ktp_del = $pendaftaran["url_ktp_del"];
                                    $url_srt_del = $pendaftaran["url_srt_del"];

                                    if($url_foto_del != ""){
                                      if(file_exists('./doc/foto_delegasi/'.$url_foto_del)){
                                        $url_foto_del_fix = base_url()."doc/foto_delegasi/".$url_foto_del;
                                      }else{
                                        $url_foto_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                      }
                                    }else {
                                      $url_foto_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }
                                    

                                    if($url_ktp_del != ""){
                                      if(file_exists('./doc/ktp_delegasi/'.$url_ktp_del)){
                                        $url_ktp_del_fix = base_url()."doc/ktp_delegasi/".$url_ktp_del;  
                                      }else{
                                        $url_ktp_del_fix = base_url()."assets/core_img/icon_upload_400x253.png";
                                      }
                                    }else{
                                      $url_ktp_del_fix = base_url()."assets/core_img/icon_upload_400x253.png";  
                                    }
                                    

                                    if($url_srt_del != ""){
                                      if(file_exists('./doc/srt_kuasa/'.$url_srt_del)){
                                        $url_srt_del_fix = base_url()."doc/srt_kuasa/".$url_srt_del; 
                                      }else{
                                        $url_srt_del_fix = base_url()."assets/core_img/icon_upload_400X650.png";
                                      }
                                    }else{
                                      $url_srt_del_fix = base_url()."assets/core_img/icon_upload_400X650.png";
                                    }


                                    if($url_ktp_pemohon != ""){
                                      if(file_exists('./doc/ktp/'.$url_ktp_pemohon)){
                                        $url_ktp_pemohon_fix = base_url()."doc/ktp/".$url_ktp_pemohon;
                                      }else{
                                        $url_ktp_pemohon_fix = base_url()."assets/core_img/icon_upload_400x253.png";
                                      }
                                        
                                    }else {
                                      $url_ktp_pemohon_fix = base_url()."assets/core_img/icon_upload_400x253.png";  
                                    }
                                                                       

                                    if($url_sk_pemohon != ""){
                                      if(file_exists('./doc/sk/'.$url_sk_pemohon)){
                                        $url_sk_pemohon_fix = base_url()."doc/sk/".$url_sk_pemohon; 
                                      }else{
                                        $url_sk_pemohon_fix = base_url()."assets/core_img/icon_upload_400X650.png"; 
                                      }
                                       
                                    }else{
                                      $url_sk_pemohon_fix = base_url()."assets/core_img/icon_upload_400X650.png";  
                                    }
                                      
                                    

                                  }
                                }

                                $response_profil_status = 0;
                                $response_profil_msg = "";

                                $response_profil_email = "";
                                $response_profil_alamat_dom = "";
                                $response_profil_tlp = "";
                                $response_profil_nama_instansi = "";
                                $response_profil_nama_universitas = "";                                
                                $response_profil_jurusan = "";
                                $response_profil_fakultas = "";
                                $response_profil_pekerjaan = "";

                                $response_foto_profil = "";

                                if(isset($_SESSION["response_profil"])){
                                  if(isset($_SESSION["response_profil"]["msg_main"])){
                                    $response_profil_status = $_SESSION["response_profil"]["msg_main"]["status"];
                                    $response_profil_msg = $_SESSION["response_profil"]["msg_main"]["msg"];
                                  }

                                  if(isset($_SESSION["response_profil"]["msg_detail"])){
                                    $response_profil_email      = $_SESSION["response_profil"]["msg_detail"]["email"];
                                    $response_profil_alamat_dom = $_SESSION["response_profil"]["msg_detail"]["alamat_dom"];
                                    $response_profil_tlp        = $_SESSION["response_profil"]["msg_detail"]["tlp"];
                                    $response_profil_nama_instansi    = $_SESSION["response_profil"]["msg_detail"]["nama_instansi"];
                                    $response_profil_nama_universitas = $_SESSION["response_profil"]["msg_detail"]["nama_universitas"];
                                    $response_profil_jurusan    = $_SESSION["response_profil"]["msg_detail"]["jurusan"];
                                    $response_profil_fakultas   = $_SESSION["response_profil"]["msg_detail"]["fakultas"];
                                    $response_profil_pekerjaan  = $_SESSION["response_profil"]["msg_detail"]["pekerjaan"];
                                    $response_foto_profil       = $_SESSION["response_profil"]["msg_detail"]["foto_profil"];
                                  }
                                }
                              ?>
                              <!-- <button type="submit" class="btn btn-rounded btn-block btn-info">Simpan Foto</button> -->
                              <?php echo form_open_multipart("user_new/maindatausernew/update_profil");?>
                              <div class="row"> 
                                                                
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Upload Foto Profil</label>
                                                <center>
                                                    <img width="152px" height="227px" src="<?= $url_foto_profil_fix;?>" id="img_foto" name="img_foto">
                                                    <p id="msg_foto_profil" style="color: red;"><?= $response_foto_profil;?></p>
                                                </center>

                                                <div>
                                                    <input type="file" name="ft_prof" id="ft_prof" class="form-control"/>
                                                    <label class="form-label">Masukan Foto Pribadi <b style="color: red;">* (dengan ukuran 4x6, maksimal file 512 Kb, dengan tipe file .jpg/.png)</b></label><br>
                                                    
                                                </div>
                                                    <script src="<?php print_r(base_url());?>assets/js/jquery-3.3.1.js"></script>
                                                    <script src="<?php print_r(base_url());?>assets/js/jquery.min.js"></script>
                                                    <script>
                                                      function readURL_foto(input) {
                                                        if (input.files && input.files[0]) {
                                                            var reader = new FileReader();
                                                            reader.onload = function(e) {
                                                              $('#img_foto').attr('src', e.target.result);
                                                          }            
                                                          reader.readAsDataURL(input.files[0]);
                                                        }
                                                      }
                                                                           
                                                      $("#ft_prof").change(function() {
                                                        readURL_foto(this);
                                                        var numFiles = $("#ft_prof")[0].files[0].size;
                                                        $("#msg-foto").html("");
                                                        if(numFiles > 200000){
                                                          $("#msg-foto").html("Ukuran File tidak boleh lebih dari 200 Kb");
                                                        }
                                                        // console.log(numFiles);
                                                        // console.log($("#ft_prof")[0].files[0].type);
                                                        // console.log(numFiles);
                                                        // console.log($("#ft_prof")[0].files[0].type);
                                                      });
                                                    </script>
                                            </div>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="row" id="output-edit-profil">
                                                <div class="col-md-6">
                                                     <div class="col-md-12">
                                                        <div class="form-group">
                                                           <label class="form-label"><i class="fe fe-user"></i> Nama Pemohon <b style="color: red;">*</b></label>
                                                           <input type="text" name="nama" id="nama" class="form-control" placeholder="Company" value="<?php print_r($nama_user);?>" readonly=""/>
                                                           
                                                        </div>
                                                     </div>
                                                     <div class="col-md-12">
                                                        <div class="form-group">
                                                           <label class="form-label"><i class="fe fe-credit-card"></i> NIK Pemohon <b style="color: red;">*</b></label>
                                                           <input type="number" name="nik" id="nik" class="form-control" placeholder="357" value="<?php print_r($nik);?>" readonly=""/>
                                                        </div>
                                                     </div>
                                                     <div class="col-md-12">
                                                        <div class="form-group">
                                                           <label class="form-label"><i class="fe fe-calendar"></i> Tanggal Lahir <b style="color: red;">*</b></label>
                                                           <input type="date" name="tgl" id="tgl" class="form-control" placeholder="357" value="<?php print_r($tgl_lhr);?>" readonly=""/>
                                                           <p id="msg_tgl_lhr_profil" style="color: red;"></p>
                                                        </div>
                                                     </div>

                                                     <div class="col-md-12">
                                                        <div class="form-group">
                                                           <label class="form-label"><i class="fe fe-map"></i> Alamat Pemohon (Sesuai KTP)<b style="color: red;">*</b></label>
                                                           <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Company" value="<?php print_r($alamat);?>" readonly=""/>
                                                           <p id="msg_alamat_ktp_profil" style="color: red;"></p>
                                                        </div>
                                                     </div>
                                                </div>

                                                <div class="col-md-6"> 
                                                         <div class="col-md-12">
                                                            <div class="form-group">
                                                               <label class="form-label"><i class="fe fe-mail"></i> Email address <b style="color: red;">*</b></label>
                                                               <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="<?php print_r($email);?>"/>
                                                               <p id="msg_email_profil" style="color: red;"><?= $response_profil_email;?></p>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-12">
                                                            <div class="form-group">
                                                               <label class="form-label"><i class="fe fe-phone"></i> No Telepon Pemohon <b style="color: red;">*</b></label>
                                                               <input type="number" name="tlp" id="tlp" class="form-control" placeholder="Company" value="<?php print_r($tlp);?>"/>
                                                               <p id="msg_tlp_profil" style="color: red;"><?= $response_profil_tlp;?></p>
                                                            </div>
                                                         </div>
                                                         

                                                         <div class="col-md-12">
                                                            <div class="form-group">
                                                               <label class="form-label"><i class="fe fe-map"></i> Alamat Pemohon (Sesuai Domisili)<b style="color: red;">*</b></label>
                                                               <input type="text" name="alamat_dom" id="alamat_dom" class="form-control" placeholder="Company" value="<?php print_r($alamat_dom);?>"/>
                                                               <p id="msg_alamat_dom_profil" style="color: red;"><?= $response_profil_alamat_dom;?></p>
                                                            </div>
                                                         </div>
                                                </div>                    
                                            </div>
                                            
                                        </div>

                                        <div class="col-md-12">
                                            <label class="form-label">Pekerjaan <b style="color: red;">*</b></label>
                                           
                                            <div class="demo-radio-button">
                                                <input id="id_radio1" type="radio" name="corp" value="0" checked="" />
                                                <label for="id_radio1">Pekerja</label>

                                                <input id="id_radio2" type="radio" name="corp" value="1"/>
                                                <label for="id_radio2">Mahasiswa</label>
                                            </div>

                                            <p style="color: red;" id="msg_pekerjaan"><?= $response_profil_pekerjaan;?></p>
                                        </div>
                                        <br><br>
                                        <div id="uinv_choose" class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label"><i class="fe fe-book-open"></i> Nama Universitas / Lembaga / Kampus <b style="color: red;">*</b></label>
                                                        <input type="text" name="uni" id="uni" class="form-control" placeholder="Universitas"/>
                                                        <p style="color: red;" id="msg_uni"><?= $response_profil_nama_universitas;?></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                       <label class="form-label"><i class="fe fe-book"></i> Fakultas <b style="color: red;">*</b></label>
                                                       <input type="text" name="fak" id="fak" class="form-control" placeholder="Fakultas"/>
                                                       <p style="color: red;" id="msg_fakultas"><?= $response_profil_fakultas;?></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                       <label class="form-label"><i class="fe fe-feather"></i>Program Studi <b style="color: red;">*</b></label>
                                                       
                                                       <select class="form-control" id="prodi" name="prodi">
                                                            <option value="SMA">SMA</option>
                                                            <option value="SMK">SMK</option>
                                                            <option value="DIPLOMA">D1/D2/D3/D4</option>
                                                            <option value="SARJANA">Sarjana</option>
                                                            <option value="MAGISTE">Paska Sarjana/Magister</option>
                                                            <option value="DOKTORAL">Doktoral</option>
                                                       </select>
                                                       <p style="color: red;" id="msg_prodi"></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                       <label class="form-label"><i class="fe fe-feather"></i>Jurusan <b style="color: red;">*</b></label>
                                                       <input type="text" name="jurusan" id="jurusan" class="form-control" placeholder="Jurusan" />
                                                       <p style="color: red;" id="msg_jurusan"><?= $response_profil_jurusan;?></p>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                        <div id="perusahan_choose" class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label"><i class="fe fe-book-open"></i> Nama Perusahaan <b style="color: red;">*</b></label>
                                                        <input type="text" name="ins" id="ins" class="form-control" placeholder="Company" />
                                                        <p style="color: red;" id="msg_nama_perus"><?= $response_profil_nama_instansi;?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12" style="text-align: right;"><br>
                                          <button type="submit" id="edit_profil" name="edit_profil" class="btn btn-rounded btn-info">Ubah Profil</button>
                                        </div>

                                        <svg class="circular" viewBox="25 25 50 50" id="loading_profil" hidden="">
                                            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                                        </svg>  
                              </div>
                              </form>
                            </div>
                        </div>

                        <div class="col-md-12">
                          <div class="ribbon-wrapper card">
                              <div class="ribbon ribbon-bookmark ribbon-info" id="3">
                                <i class="mdi mdi-book-open"></i>&nbsp;&nbsp;&nbsp;Input Data Magang/Penelitian
                              </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                           <div class="form-group">
                                              <label class="form-label">Judul / Tema Penelitian <b style="color: red;">*</b></label>
                                              <textarea class="form-control" name="judul" id="judul"><?=$judul;?></textarea>
                                           </div>
                                           <p style="color: red;" id="msg_judul"></p>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="form-label">Permohonan di pergunakan untuk <b style="color: red;">*</b></label><br><br>
                                                <select class="form-control" name="permohonan" id="permohonan">
                                                  <?php
                                                    if($permohonan){
                                                      foreach ($permohonan as $r_permohonan => $v_permohonan) {
                                                        echo "<option value=\"".$v_permohonan->id_permohonan."\">".$v_permohonan->keterangan_permohonan."</option>";
                                                      }
                                                    }
                                                  ?>
                                                </select>
                                            </div>
                                            <p style="color: red;" id="msg_permohonan"></p>
                                        </div>

                                        <div class="col-sm-4" hidden="">
                                            <div class="form-group">
                                                <label class="form-label">Periode Magang (Bulan)<b style="color: red;">*</b></label>
                                                
                                                <input type="number" class="form-control" name="periode" id="periode"/>
                                            </div>
                                            <p style="color: red;" id="msg_periode"></p>
                                        </div>
                                        <br />
                          
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="form-label">Tanggal mulai (Tanggal/Bulan/Tahun) <b style="color: red;">*</b></label>
                                                <input type="date" class="form-control" name="tgl_mulai" id="tgl_mulai" value="<?=$tgl_start;?>">
                                            </div>
                                            <p style="color: red;" id="msg_tgl_st"></p>
                                        </div>
                          
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="form-label">Tanggal selesai (Tanggal/Bulan/Tahun)<b style="color: red;">*</b></label>
                                                <input type="date" class="form-control" name="tgl_selesai" id="tgl_selesai" value="<?=$tgl_selesai;?>"><br /><br />
                                            </div>
                                            <p style="color: red;" id="msg_tgl_fn"></p>
                                        </div>
                          
                                        <div class="col-md-12">
                                            <div class="form-group mb-0">
                                                <label class="form-label">Dosen Pembimbing <b style="color: red;">*</b></label>
                                                <table class="table">
                                                   <thead>
                                                      <tr>
                                                         <th scope="col" width="10%">No</th>
                                                         <th scope="col" width="75%">Nama Dosen Pembimbing</th>
                                                         <th scope="col" width="15%">Aksi</th>
                                                      </tr>
                                                   </thead>
                                                   <tr>
                                                         <th scope="row">#</th>
                                                         <td>
                                                            <input type="text" class="form-control" name="dosen" id="dosen" placeholder="Prof. Dr. Sukonto Legowo, S.ip., MBAH.,"/>
                                                         </td>
                                                         <td>
                                                            <button type="submit" id="add_dosen" name="add_dosen" class="btn btn-success">Tambah</button>
                                                         </td>
                                                   </tr>
                                                   <tbody id="output_dosen">
                                                      
                                                   </tbody>
                                                </table>
                                                <p style="color: red;" id="msg_dosen"></p>
                                                <br /><br />
                                            </div>
                                        </div>
                          
                                        <div class="col-md-12">
                                            <div class="form-group mb-0">
                                                <label class="form-label">Pilih Lokasi Magang / Penelitian <b style="color: red;">*</b></label>
                                                <table class="table">
                                                   <thead>
                                                      <tr>
                                                         <th scope="col" width="10%">No</th>
                                                         <th scope="col" width="75%">Instansi Tempat Magang / Penelitian</th>
                                                         <th scope="col" width="15%">Aksi</th>
                                                      </tr>
                                                   </thead>
                                                   <tr>
                                                         <th scope="row">#</th>
                                                         <td>
                                                            <select class="form-control" name="id_bidang" id="id_bidang">
                                                              <?php 
                                                                  if(isset($tmp_magang_select)){
                                                                      foreach($tmp_magang_select as $val_tmp){
                                                                          echo "<option value=\"".$val_tmp->id_dinas."\">".$val_tmp->nama_dinas."</option>";
                                                                      }
                                                                  }
                                                              ?>
                                                            </select>
                                                         </td>
                                                         <td>
                                                            <button type="submit" id="add_bidang" name="add_bidang" class="btn btn-success">Tambah</button>
                                                         </td>
                                                   </tr>
                                                   <tbody id="output_bidang">
                                                      
                                                   </tbody>
                                                </table>
                                                <p style="color: red;" id="msg_bidang"></p>
                                                <br /><br />
                                            </div>
                                        </div>
                          
        
                                        <div class="col-md-12">
                                            <div class="form-group mb-0">
                                                <label class="form-label">Anggota Kelompok</label>
                                                <table class="table">
                                                   <thead>
                                                      <tr>
                                                         <th scope="col" width="5%">No</th>
                                                         <th scope="col">Nama</th>
                                                         <th scope="col">Nim / No. KTP</th>
                                                         <th scope="col" width="15%">Aksi</th>
                                                      </tr>
                                                   </thead>
                                                      <tr>
                                                         <th scope="row">#</th>
                                                         <td><input type="text" class="form-control" name="nama_kel" id="nama_kel" placeholder="Surya Hanggara"/></td>
                                                         <td><input type="number" class="form-control" name="nik_kel" id="nik_kel" placeholder="357301197XXXXXXX"/></td>
                                                         <td>
                                                            <button type="submit" id="add_anggota" name="add_anggota" class="btn btn-success">Tambah</button>
                                                         </td>
                                                      </tr>
                                                   <tbody id="output_anggota">
                                                      
                                                   </tbody>
                                                </table>
                                                <p style="color: red;" id="msg_anggota"></p>
                                                <br /><br />
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            
                                            <div class="form-group">
                                                <label class="form-label">Cara Mengurus Surat Ijin Meneliti<b style="color: red;">*</b></label>
                                                <div class="demo-radio-button">
                                                    <input id="pengurusan1" type="radio" name="pengurusan" value="0" checked="" />
                                                    <label for="pengurusan1">Mengurus Sendiri</label>&nbsp;&nbsp;

                                                    <input id="pengurusan2" type="radio" name="pengurusan" value="1" />
                                                    <label for="pengurusan2">Mengurus Dengan Mendelegasikan Kepada Seseorang</label><br /><br />
                                                </div>
                                            </div>
                                         <p style="color: red;" id="msg_pengurusan"></p>
                                        </div>

                                        <!-- <div class="col-md-12" style="text-align: right;">
                                            <div class="form-group" >
                                                <button type="submit" id="submit_pemohon" class="btn btn-rounded btn-info" align="right">Konfirmasi Form</button>
                                            </div>
                                        </div> -->
                                    </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                          <div class="ribbon-wrapper card">
                              <div class="ribbon ribbon-bookmark ribbon-info" id="4">
                                <i class="mdi mdi-account-switch"></i>&nbsp;&nbsp;&nbsp;Input Data Pendelegasian
                              </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                           <div class="form-group">
                                              <label class="form-label">NIK<b style="color: red;">*</b></label>
                                              <input class="form-control" type="number" name="nik_del" id="nik_del" value="<?=$nik_del?>" placeholder="NIK Kuasa">
                                           </div>
                                           <p style="color: red;" id="msg_nik_del"></p>
                                        </div>

                                        <div class="col-md-6">
                                           <div class="form-group">
                                              <label class="form-label">Nama<b style="color: red;">*</b></label>
                                              <input class="form-control" type="text" name="nama_del" id="nama_del" value="<?=$nama_del?>" placeholder="Nama Kuasa">
                                           </div>
                                           <p style="color: red;" id="msg_nama_del"></p>
                                        </div>

                                        <div class="col-md-6">
                                           <div class="form-group">
                                              <label class="form-label">Alamat Sesuai KTP<b style="color: red;">*</b></label>
                                              <input class="form-control" type="text" name="alamat_ktp_del" id="alamat_ktp_del" value="<?=$alamat_ktp_del?>" placeholder="Alamat Sesuai KTP Kuasa">
                                           </div>
                                           <p style="color: red;" id="msg_alamat_ktp_del"></p>
                                        </div>

                                        <div class="col-md-6">
                                           <div class="form-group">
                                              <label class="form-label">Alamat Domisili<b style="color: red;">*</b></label>
                                              <input class="form-control" type="text" name="alamat_dom_del" id="alamat_dom_del" value="<?=$alamat_dom_del?>" placeholder="Alamat Domisili Kuasa">
                                           </div>
                                           <p style="color: red;" id="msg_alamat_dom_del"></p>
                                        </div>

                                        <div class="col-md-6">
                                          <div class="row">
                                            <div class="col-md-12">
                                               <div class="form-group">
                                                  <label class="form-label">Foto <b style="color: red;">*</b></label>
                                                  <input class="form-control" type="File" name="foto_del" id="foto_del" value="" placeholder="Alamat Domisili Kuasa">
                                                  <center>
                                                    <br>
                                                    <img width="152px" height="227px" src="<?=$url_foto_del_fix?>" id="img_foto_del" name="img_foto_del">                                         
                                                  </center>
                                                  
                                               </div>
                                               <p style="color: red;" id="msg_foto_del"></p>
                                            </div>
                                            

                                            <div class="col-md-12">
                                               <div class="form-group">
                                                  <label class="form-label">Scan/Foto KTP<b style="color: red;">*</b></label>
                                                  <input class="form-control" type="file" name="ktp_del" id="ktp_del" value="" placeholder="Alamat Domisili Kuasa">
                                                  <center>
                                                    <br>
                                                    <img width="400px" height="253px" src="<?=$url_ktp_del_fix?>" id="img_ktp_del" name="img_ktp_del"><br>
                                                  </center>                                              
                                               </div>
                                               <p style="color: red;" id="msg_ktp_del"></p>
                                            </div>  
                                          </div>
                                        </div>

                                        <div class="col-md-6">
                                           <div class="form-group">
                                              <label class="form-label">Surat Kuasa <b style="color: red;">*</b></label>
                                               <input class="form-control" type="File" name="surat_kuasa" id="surat_kuasa" value="" placeholder="Alamat Domisili Kuasa">
                                              <center>
                                                <br>
                                                <img width="400px" height="650px" src="<?=$url_srt_del_fix?>" id="img_kuasa_del" name="img_kuasa_del">
                                              </center>
                                             
                                           </div>
                                           <p style="color: red;" id="msg_surat_kuasa"></p>
                                        </div>
                                        

                                        <!-- <div class="col-md-12" style="text-align: right;">
                                          <button type="submit" id="submit_delegasi" class="btn btn-rounded btn-info" align="right">Simpan Delegasi</button>
                                        </div> -->

                                        <script>

                                            var files_foto_del = [];
                                            var files_ktp_del = [];
                                            var files_sk_del = [];

                                
                                            //-------------------------------------foto delegasi---------------------------------------
                                            function readURL_foto_del(input) {
                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();
                                                    reader.onload = function(e) {
                                                        $('#img_foto_del').attr('src', e.target.result);
                                                    }
                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            }

                                            $("#foto_del").change(function(e) {
                                                files_foto_del = e.target.files[0];
                                                readURL_foto_del(this);
                                                var numFiles = $("#foto_del")[0].files[0].size;
                                                //alert(numFiles);
                                                $("#msg_foto_del").html("");
                                                if (numFiles > 200000) {
                                                    $("#msg_foto_del").html("Ukuran File tidak boleh lebih dari 200 Kb");
                                                }

                                            });

                                            //-------------------------------------KTP delegasi---------------------------------------

                                            function readURL_ktp_del(input) {
                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();

                                                    reader.onload = function(e) {
                                                        $('#img_ktp_del').attr('src', e.target.result);
                                                    }

                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            }

                                            $("#ktp_del").change(function(e) {
                                                files_ktp_del = e.target.files[0];
                                                readURL_ktp_del(this);
                                                var numFiles = $("#ktp_del")[0].files[0].size;
                                                //alert(numFiles);
                                                $("#msg_ktp_del").html("");
                                                if (numFiles > 512000) {
                                                    $("#msg_ktp_del").html("Ukuran File tidak boleh lebih dari 512 Kb");
                                                }

                                            });

                                            //-------------------------------------kuasa delegasi---------------------------------------

                                            function readURL_kuasa_del(input) {
                                                if (input.files && input.files[0]) {
                                                    var reader = new FileReader();

                                                    reader.onload = function(e) {
                                                        $('#img_kuasa_del').attr('src', e.target.result);
                                                    }

                                                    reader.readAsDataURL(input.files[0]);
                                                }
                                            }

                                            $("#surat_kuasa").change(function(e) {
                                                files_sk_del = e.target.files[0];
                                                readURL_kuasa_del(this);
                                                var numFiles = $("#surat_kuasa")[0].files[0].size;
                                                //alert(numFiles);
                                                $("#msg_surat_kuasa").html("");
                                                if (numFiles > 1024000) {
                                                    $("#msg_surat_kuasa").html("Ukuran File tidak boleh lebih dari 1 Mb");
                                                }

                                            }); 
                                      </script>
                                    </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                          <div class="ribbon-wrapper card">
                              <div class="ribbon ribbon-bookmark ribbon-info" id="5">
                                <i class="mdi mdi-book-multiple-variant"></i>&nbsp;&nbsp;&nbsp;Input Data Dokumen
                              </div>
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="col-md-12">
                                           <div class="form-group">
                                              <label class="form-label"><i class="fe fe-credit-card"></i> Masukan Scan / Foto KTP <b style="color: red;">* (ukuran maksimal file 512 Kb, dengan tipe file .jpg/.png)</b></label>
                                              <input type="file" name="ktp" id="ktp" class="form-control"/>
                                           </div>
                                           <!-- <p style="color: red;" id="msg_ktp_doc">
                                            </p> -->
                                        </div>
                                        <div class="col-md-12" align="center">
                                          <img width="400px" height="253px" id="img_ktp" src="<?= $url_ktp_pemohon_fix?>"/><br /><br />
                                           
                                           <p id="msg-ktp" style="color: red;"></p>
                                        </div>

                                        <div class="col-md-12">
                                           <div class="form-group">
                                              <label class="form-label"><i class="fe fe-file-text"></i> No Surat Pengantar</label>
                                              <input type="text" name="no_surat" id="no_surat" class="form-control" placeholder="PLTK/20209320/01010" value="<?php echo $no_surat_tdd;?>">
                                           </div>
                                           <p id="msg_no_srt_doc" style="color: red;"></p>
                                        </div>
                                        <div class="col-md-12">
                                           <div class="form-group">
                                              <label class="form-label"><i class="fe fe-file-text"></i> Tgl Surat Pengantar</label>
                                              <input type="date" name="tgl_surat" id="tgl_surat" class="form-control" value="<?php echo date("Y-m-d");?>">
                                           </div>
                                           <p id="msg_tgl_srt_doc" style="color: red;"></p>
                                        </div>
                                        <div class="col-md-12">
                                           <div class="form-group">
                                              <label class="form-label"><i class="fe fe-feather"></i> Yang Menandatangani Surat Pengantar</label>
                                              <input type="text" name="nama_tdd" id="nama_tdd" class="form-control" placeholder="Imam Sanusi, S.Kom, M.Kom" value="<?php echo $nama_tdd;?>">
                                              <p id="msg_nama_srt_doc" style="color: red;"></p>
                                           </div>
                                        </div>
                                        <div class="col-md-12">
                                           <div class="form-group">
                                              <label class="form-label"><i class="fe fe-user"></i> Jabatan</label>
                                              <input type="text" name="jabatan" id="jabatan" class="form-control" placeholder="Dekan" value="<?php echo $jabatan_tdd;?>">
                                              <p id="msg_jabatan_srt_doc" style="color: red;"></p>
                                           </div>
                                        </div>
                                        


                                      </div>

                                        

                                        <br>
                                      <div class="col-md-6">
                                        <div class="col-md-12">
                                           <div class="form-group">
                                              <label class="form-label"><i class="fe fe-file"></i> Masukan Proposal Pemohon <b style="color: red;">* (ukuran maksimal file 2 Mb, dengan tipe file .pdf)</b></label>
                                              <input type="file" name="proposal" id="proposal" class="form-control" />
                                              
                                           </div>
                                        </div>
                                        <div class="col-md-12">
                                            <a id="url_proposal" href="<?php echo base_url()."doc/proposal/".$url_proposal_pemohon;?>"><?php echo $url_proposal_pemohon;?></a><br /><br />
                                            <p id="msg_proposal_doc" style="color: red;"></p>
                                        </div>
                                        
                                        <div class="col-md-12">
                                           <div class="form-group">
                                              <label class="form-label"><i class="fe fe-file"></i> Masukan File Surat Pengantar dari Kampus/ Instansi/ Lembaga <b style="color: red;">* (ukuran maksimal file 1 Mb, dengan tipe file .jpg/.png)</b></label>
                                              <input type="file" name="pengantar" id="pengantar" class="form-control" />
                                           </div>
                                           <p style="color: red;"></p>
                                        </div>
                                        <div class="col-md-12">
                                           <img width="350px" height="500px" id="img_sk" src="<?=$url_sk_pemohon_fix;?>"/><br /><br />
                                           <p id="msg-sk" style="color: red;"></p>
                                        </div>
                                      </div>

                                        <script src="<?= base_url();?>assets/js/jquery-3.3.1.js"></script>
                                        <script src="<?= base_url();?>assets/js/jquery.min.js"></script>
                                        
                                         <script>

                                                var files_proposal_pemohon = [];
                                                var files_ktp_pemohon = [];
                                                var files_sk_pemohon = [];

                                                function readURL_ktp(input) {
                                                  if (input.files && input.files[0]) {
                                                    var reader = new FileReader();
                                                
                                                    reader.onload = function(e) {
                                                      $('#img_ktp').attr('src', e.target.result);
                                                    }
                                                
                                                    reader.readAsDataURL(input.files[0]);
                                                  }
                                                }
                                                
                                                
                                                function readURL_sk(input) {
                                                  if (input.files && input.files[0]) {
                                                    var reader = new FileReader();
                                                
                                                    reader.onload = function(e) {
                                                      $('#img_sk').attr('src', e.target.result);
                                                    }
                                                
                                                    reader.readAsDataURL(input.files[0]);
                                                  }
                                                }
                                                
                                                
                                                
                                                $("#ktp").change(function(e) {
                                                  files_ktp_pemohon = e.target.files[0];
                                                  readURL_ktp(this);
                                                  var numFiles = $("#ktp")[0].files[0].size;
                                                  //alert(numFiles);
                                                  $("#msg-ktp").html("");
                                                  if(numFiles > 512000){
                                                    $("#msg-ktp").html("Ukuran File tidak boleh lebih dari 512 Kb");
                                                  }
                                                });
                                                
                                                $("#pengantar").change(function(e) {
                                                  files_sk_pemohon = e.target.files[0];
                                                  readURL_sk(this);
                                                  var numFiles = $("#pengantar")[0].files[0].size;
                                                  //alert(numFiles);
                                                  $("#msg-sk").html("");
                                                  if(numFiles > 1024000){
                                                    $("#msg-sk").html("Ukuran File tidak boleh lebih dari 512 Kb");
                                                  }
                                                  
                                                  
                                                  // console.log(numFiles);
                                                });
                                                
                                                $("#proposal").change(function(e) {
                                                  files_proposal_pemohon = e.target.files[0];
                                                  //readURL_ktp(this);
                                                  var numFiles = $("#proposal")[0].files[0].size;
                                                  //alert(numFiles);
                                                  $("#msg-proposal").html("");
                                                  if(numFiles > 2048000){
                                                    $("#msg-proposal").html("Ukuran File tidak boleh lebih dari 512 Kb");
                                                  }
                                                  
                                                  // console.log(numFiles);
                                                });
                                        </script>
                                      
                                      <br><br><br><br>
                                      <div class="col-md-12" style="text-align: right;">
                                        <hr>
                                        <div class="form-group">
                                          <button type="submit" class="btn btn-rounded btn-info" name="add_doc" id="add_doc" align="right"><h3 style="color: white;">Lanjutkan Pendaftaran</h3></button>

                                          <!-- <button type="submit" class="btn btn-rounded btn-info" name="sa-success" id="sa-warning" align="right">sa-success</button> -->
                                        </div>
                                      </div>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

            <script src="<?php echo base_url()."assets/js/jquery-3.2.1.js";?>"></script>
            <script>
                    
                    $(document).ready(function(){
                      //----------instansi
                      show_pekerjaan();
                      click_pekerjaan();
                      //----------instansi

                      //----------cara_mengurus
                      $("input[name='pengurusan'][value='<?=$status_pendelegasian?>']").prop('checked', true);
                      show_mengurus();
                      //----------cara_mengurus

                      //----------bidang
                      show_tmp();
                      //----------bidang
                      
                      //----------dosen
                      show_dosen();
                      //----------dosen
                      
                      //----------anggota
                      show_anggota();
                      //----------anggota

                      //----------id_permohonan
                      $("#permohonan").val('<?= $id_permohonan;?>');
                      //----------id_permohonan
                      
                      
                    });

//========================================================================================================
//----------------------------------------------Instnasi--------------------------------------------------
//========================================================================================================
                    $("input[name='corp']").click(function(){
                      click_pekerjaan();
                    });

                    function click_pekerjaan(){
                      var val_corp = $("input[name='corp']:checked").val();
                      // console.log(val_corp);
                      if(val_corp == 0){
                            $("#uinv_choose").hide(500);
                            $("#perusahan_choose").show(500);
                        }else{
                            $("#uinv_choose").show(500);
                            $("#perusahan_choose").hide(500);    
                        }
                    }

                    function show_pekerjaan(){
                      $("input[name='corp'][value='<?=$pekerjaan?>']").prop('checked', true);
                      <?php
                        $instansi_x = strpos($instansi, ";");
                        if($instansi_x){
                            $instansi_x = explode(";", $instansi);?>
                            $("#prodi").val("<?=$instansi_x[0]?>");
                            $("#jurusan").val("<?=$instansi_x[1]?>");
                            $("#fak").val("<?=$instansi_x[2]?>");
                            $("#uni").val("<?=$instansi_x[3]?>");
                        <?php 
                        }else{
                            $instansi_x = $instansi;?>
                            $("#ins").val("<?=$instansi_x?>");
                        
                        <?php
                        }
                      ?>
                    }  
//=======================================================================================================
//---------------------------------------------Instnasi--------------------------------------------------
//=======================================================================================================

//========================================================================================================
//-------------------------------------------Cara_Mengurus------------------------------------------------
//========================================================================================================
                    $("input[name='pengurusan']").click(function(){
                      show_mengurus();
                    });

                    function show_mengurus(){
                      var val_pengurusan = $("input[name='pengurusan']:checked").val();
                      // console.log(val_pengurusan);
                      if(val_pengurusan == 0){
                        $("#nik_del").prop("readonly", true);
                        $("#nama_del").prop("readonly", true);
                        $("#alamat_ktp_del").prop("readonly", true);
                        $("#alamat_dom_del").prop("readonly", true);
                        $("#foto_del").prop("readonly", true);
                        $("#ktp_del").prop("readonly", true);
                        $("#surat_kuasa").prop("readonly", true);

                        empty_value_delegasi();
                      }else{

                        $("#nik_del").removeAttr("readonly");
                        $("#nama_del").removeAttr("readonly");
                        $("#alamat_ktp_del").removeAttr("readonly");
                        $("#alamat_dom_del").removeAttr("readonly");
                        $("#foto_del").removeAttr("readonly");
                        $("#ktp_del").removeAttr("readonly");
                        $("#surat_kuasa").removeAttr("readonly");
                      }
                    }  

                    function empty_value_delegasi(){
                        $("#nik_del").val("");
                        $("#nama_del").val("");
                        $("#alamat_ktp_del").val("");
                        $("#alamat_dom_del").val("");

                        $("#foto_del").val("");
                        $("#ktp_del").val("");
                        $("#surat_kuasa").val("");
                        
                        $("#img_foto_del").attr("src", "<?=base_url();?>assets/core_img/icon_upload_152x277.png");
                        $("#img_ktp_del").attr("src", "<?=base_url();?>assets/core_img/icon_upload_400x253.png");
                        $("#img_kuasa_del").attr("src", "<?=base_url();?>assets/core_img/icon_upload_400X650.png");
                    }
//=======================================================================================================
//--------------------------------------------Cara_Mengurus----------------------------------------------
//=======================================================================================================


//=======================================================================================================
//-------------------------------------------Add_Json_Input----------------------------------------------
//=======================================================================================================

  //------------------------------------------Dinas Pilihan------------------------------------------//
                
                    var master_tmp_magang = JSON.parse('<?php echo json_encode($tmp_magang);?>');
                    
                    var list_add_tmp = <?php print_r($id_bidang);?>;
                    
                    function add_tmp(){
                        var val_tmp_magang = $("#id_bidang").val();

                        if(list_add_tmp.length < 4){
                            if(list_add_tmp.indexOf(val_tmp_magang) == -1){
                                list_add_tmp.push(val_tmp_magang);
                                // console.log(list_add_tmp); 
                            }   
                        }
                        show_tmp();
                    }
                    
                    $("#add_bidang").click(function(){
                      // console.log("add_bidang");
                        add_tmp();
                        //sweet();
                    });
                    
                    $("#add_bidang").keypress(function(e){
                        if(e.keyCode == 13){
                            add_tmp();
                        }
                        
                    });
                    
                    function del_tmp(index_start){
                        list_add_tmp.splice(index_start,1);
                        // console.log(list_add_tmp); 
                        show_tmp();
                    }
                    
                    function show_tmp(){
                        var content_tmp = "";
                        for(var i = 0; i<list_add_tmp.length; i++){
                            var no = i+1;
                            content_tmp += "<tr><td>"+no+
                            ". </td><td>"+master_tmp_magang[list_add_tmp[i]]+
                            "</td><td align=\"center\"><a id=\"del_tmp\" onclick=\"del_tmp("+i+")\"><i class=\"text-danger\"><i class=\"fa fa-close\"></i></i></a></td></tr>";
                        }
                        $("#output_bidang").html(content_tmp);
                    }
  
  //------------------------------------------List Anggota-------------------------------------------//
                    
                    var tmp_anggota = [];
                    var list_anggota = <?php print_r($anggota);?>;
                    
                    function add_anggota_f(){
                        var val_nama = $("#nama_kel").val();
                        var val_nik = $("#nik_kel").val();
                                                
                        if(val_nama && val_nik){
                          tmp_anggota = [];
                          tmp_anggota.push(val_nama);
                          tmp_anggota.push(val_nik);
                          
                          list_anggota.push(tmp_anggota);
                          // console.log(list_anggota);
                          $("#nama_kel").val("");
                          $("#nik_kel").val("");
                          show_anggota();
                        }else{
                          alert("input nama dan nomor identitas anggota tidak boleh kosong");
                        }
                        
                    }
                    
                    $("#add_anggota").click(function(){
                        add_anggota_f();
                        //sweet();
                    });
                    
                    $("#add_anggota").keypress(function(e){
                        if(e.keyCode == 13){
                            add_anggota_f();
                        }
                    });
                    
                    function del_anggota(index_start){
                        list_anggota.splice(index_start,1);
                        // console.log(list_anggota); 
                        show_anggota();
                    }
                    
                    function show_anggota(){
                        var content_anggota = "";
                        for(var i = 0; i<list_anggota.length; i++){
                            var no = i+1;
                            content_anggota += "<tr><td>"+no+
                            ". </td><td>"+list_anggota[i][0]+
                            "</td><td>"+list_anggota[i][1]+
                            "</td><td align=\"center\"><a id=\"del_tmp\" onclick=\"del_anggota("+i+")\"><i class=\"text-danger\"><i class=\"fa fa-close\"></i></i></a></td></tr>";
                        }
                        $("#output_anggota").html(content_anggota);
                    }
                    
  //-------------------------------------------List Dosen--------------------------------------------//
                    
                    var list_dosen = <?php print_r($dosen_pembimbing);?>;
                    
                    function add_dosen_f(){
                        var val_dosen = $("#dosen").val();
                        if(list_dosen.length < 2){
                            if(val_dosen != ""){
                                list_dosen.push(val_dosen);
                                $("#dosen").val("");   
                            }
                        }
                        // console.log(list_dosen); 
                        show_dosen();
                    }
                    
                    $("#add_dosen").click(function(){
                        add_dosen_f();
                        //sweet();
                    });
                    
                    $("#add_dosen").keypress(function(e){
                        if(e.keyCode == 13){
                            add_dosen_f();
                        }
                    });
                    
                    $("#dosen").keypress(function(e){
                        if(e.keyCode == 13){
                            add_dosen_f();
                        }
                    });
                    
                    function del_dosen(index_start){
                        list_dosen.splice(index_start,1);
                        // console.log(list_dosen); 
                        show_dosen();
                    }
                    
                    function show_dosen(){
                        var content_dosen = "";
                        for(var i = 0; i<list_dosen.length; i++){
                            var no = i+1;
                            content_dosen += "<tr><td>"+no+
                            "</td><td>"+list_dosen[i]+
                            "</td><td align=\"center\"><a id=\"del_tmp\" onclick=\"del_dosen("+i+")\"><i class=\"text-danger\"><i class=\"fa fa-close\"></i></i></a></td></tr>";
                        }
                        $("#output_dosen").html(content_dosen);
                    }
//=======================================================================================================
//-------------------------------------------Add_Json_Input----------------------------------------------
//=======================================================================================================


//=======================================================================================================
//-------------------------------------------get_file_value----------------------------------------------
//=======================================================================================================
//=======================================================================================================
//-------------------------------------------get_file_value----------------------------------------------
//=======================================================================================================


//=======================================================================================================
//-----------------------------------------Simpan_Pendafatarn--------------------------------------------
//=======================================================================================================

                  var list_text_no_acc_opd = [];

                  function create_text_no_acc_opd(val_tmp){
                    var obj_tmp = {};
                    obj_tmp["status"] = "0";
                    obj_tmp["date_action"] = "-"
                    obj_tmp["text"] = "-";
                    obj_tmp["id_admin_accept"] = "-";

                    list_text_no_acc_opd[val_tmp] = obj_tmp;
                    // console.log(list_text_no_acc_opd);
                  }

                  $("#add_doc").click(function(){
                    $("#add_doc").prop("disabled", true);
                    list_add_tmp.filter(create_text_no_acc_opd);
                    var fixed_text_no_acc_opd = JSON.stringify(list_text_no_acc_opd);

                    // console.log(JSON.parse(json_text_no_acc_opd));
                      var data_main =  new FormData();
                      data_main.append('id_pemohon', "<?php echo $id_pemohon;?>");

                      data_main.append('judul', $("#judul").val());
                      data_main.append('permohonan', $("#permohonan").val());
                      data_main.append('tgl_st',  $("#tgl_mulai").val());
                      data_main.append('tgl_fn', $("#tgl_selesai").val());
                      data_main.append('periode', $("#periode").val());
                      
                      data_main.append('tmp', JSON.stringify(list_add_tmp));
                      data_main.append('anggota', JSON.stringify(list_anggota));
                      data_main.append('dosen', JSON.stringify(list_dosen));

                      data_main.append('text_no_acc_opd', fixed_text_no_acc_opd);
                      data_main.append('pengurusan', $("input[name='pengurusan']:checked").val());

                      data_main.append('nik_del', $("#nik_del").val());
                      data_main.append('nama_del', $("#nama_del").val());
                      data_main.append('alamat_ktp_del', $("#alamat_ktp_del").val());
                      data_main.append('alamat_dom_del', $("#alamat_dom_del").val());

                      data_main.append('no_surat', $("#no_surat").val());
                      data_main.append('tgl_surat', $("#tgl_surat").val());
                      data_main.append('nama_tdd', $("#nama_tdd").val());
                      data_main.append('jabatan', $("#jabatan").val());

                      
                      data_main.append('url_foto_del', files_foto_del);
                      data_main.append('url_ktp_del', files_ktp_del);
                      data_main.append('url_srt_del', files_sk_del);
                      data_main.append('url_ktp_pemohon', files_ktp_pemohon);
                      data_main.append('url_sk_pemohon', files_sk_pemohon);
                      data_main.append('url_proposal_pemohon', files_proposal_pemohon);

                      $.ajax({
                                url: "<?php echo base_url()."user_new/maindatausernew/insert_pemohon";?>", // point to server-side PHP script 
                                dataType: 'html',  // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,                         
                                type: 'post',
                                success: function(res){
                                    response_add_doc(res);
                                    // console.log(res);
                                    //$("#output-edit-profil").html(res);
                                }
                            });

                      // console.log(data_main);

                  });

                  function response_add_doc(res){
                    var data_res = JSON.parse(res);
                    var data_res_detail = data_res.msg_detail;

                    if(data_res.msg_main.status){
              //success process
                      !function($) {
                          "use strict";
                          var SweetAlert = function() {};
                          //examples 
                          SweetAlert.prototype.init = function() {
                        
                          //Warning Message
                         
                              swal({   
                                  title: "Proses Penyimpanan data anda berhasil",   
                                  text: "Silahkan periksa kelengkapan data anda dan lanjutkan proses ke tahap verifikasi data input anda..",   
                                  type: "success",   
                                  showCancelButton: false,   
                                  confirmButtonColor: "#28a745",   
                                  confirmButtonText: "Lanjutkan",   
                                  closeOnConfirm: false 
                              }, function(){
                                window,location.href = "<?=base_url();?>pendaftaran/accept";   
                              });
                                       
                          },
                          //init
                          $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                      }(window.jQuery),

                      //initializing 
                      function($) {
                          "use strict";
                          $.SweetAlert.init()
                      }(window.jQuery);

                      $("#add_doc").removeAttr("disabled", true);
                    }else{
              //fail process
                      !function($) {
                          "use strict";
                          var SweetAlert = function() {};
                          //examples 
                          SweetAlert.prototype.init = function() {
                          //Warning Message
                              swal("Proses Gagal.!!", "Proses Penyimpanan data anda gagal, Silahkan periksa kelengkapan data dan pastikan input data sesuai aturan sistem..", "warning")
                                       
                          },
                          //init
                          $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                      }(window.jQuery),

                      //initializing 
                      function($) {
                          "use strict";
                          $.SweetAlert.init()
                      }(window.jQuery);

                      $("#add_doc").removeAttr("disabled", true);
                    }

                    // $("#msg_foto_profil").html();
                    // $("#msg_tgl_lhr_profil").html();
                    // $("#msg_alamat_ktp_profil").html();
                    // $("#msg_email_profil").html();
                    // $("#msg_tlp_profil").html();
                    // $("#msg_alamat_dom_profil").html();

                    $("#msg_judul").html(data_res_detail.judul);
                    $("#msg_permohonan").html(data_res_detail.permohonan);
                    $("#msg_tgl_st").html(data_res_detail.tgl_st);
                    $("#msg_tgl_fn").html(data_res_detail.tgl_fn);
                    $("#msg_dosen").html(data_res_detail.dosen);
                    $("#msg_bidang").html(data_res_detail.tmp);
                    $("#msg_anggota").html(data_res_detail.anggota);
                    $("#msg_pengurusan").html(data_res_detail.pengurusan);

                    $("#msg_nik_del").html(data_res_detail.nik_del);
                    $("#msg_nama_del").html(data_res_detail.nama_del);
                    $("#msg_alamat_ktp_del").html(data_res_detail.alamat_ktp_del);
                    $("#msg_alamat_dom_del").html(data_res_detail.alamat_dom_del);

                    $("#msg_foto_del").html(data_res_detail.msg_url_foto_del);
                    $("#msg_ktp_del").html(data_res_detail.msg_url_ktp_del);
                    $("#msg_surat_kuasa").html(data_res_detail.msg_url_srt_del);

                    $("#msg-ktp").html(data_res_detail.msg_url_ktp_pemohon);
                    $("#msg_proposal_doc").html(data_res_detail.msg_url_proposal_pemohon);
                    $("#msg-sk").html(data_res_detail.msg_url_sk_pemohon);
                    
                    $("#msg_no_srt_doc").html(data_res_detail.no_surat);
                    // $("#msg_tgl_srt_doc").html(data_res_detail.msg_url_foto_del);
                    $("#msg_nama_srt_doc").html(data_res_detail.nama_tdd);
                    $("#msg_jabatan_srt_doc").html(data_res_detail.jabatan);
                  }
//=======================================================================================================
//-----------------------------------------Simpan_Pendafatarn--------------------------------------------
//=======================================================================================================

                  
            </script>
                