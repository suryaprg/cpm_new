                            <?php

                                $array_of_month = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
                                $tmp_tgl ="";

                                $nama_user = "";
                                $nik = "";
                                $tgl_lhr = "";
                                $alamat = "";
                                $email = "";
                                $tlp = "";
                                $alamat_dom = "";
                                $alamat_dom = "";
                                $instansi = "";
                                $pekerjaan = "";

                                $url_foto_profil =  "";

                                $status_pekerjaan = "";
                                $instansi_pekerjaan = "";

                                $tgl_lhr_fix = "";

                                if(isset($user)){
                                  if($user){
                                    $nama_user = $user["nama"];
                                    $email = $user["email"];
                                    $alamat_dom = $user["alamat_dom"];
                                    $alamat = $user["alamat"];
                                    $tgl_lhr = $user["tgl_lhr"];
                                    $nik = $user["nik"];
                                    $tlp = $user["tlp"];
                                    $instansi = $user["instansi"];
                                    $pekerjaan = $user["pekerjaan"];

                                    $tmp_tgl = explode("-", $tgl_lhr);
                                    $tgl_lhr_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $url_foto_profil =  $user["url_profil"];

                                    if($url_foto_profil != ""){
                                      $url_foto_profil_fix = base_url()."doc/foto_pemohon/".$url_foto_profil;  
                                    }else {
                                      $url_foto_profil_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }

                                    $status_pekerjaan = "Mahasiswa, ";
                                    if($pekerjaan == 0){
                                        $status_pekerjaan = "Bekerja Pada, ";
                                    }

                                    if(strpos($instansi, ";")){
                                        $data_instansi = explode(";", $instansi);
                                        $instansi_pekerjaan = $status_pekerjaan.implode(", ", $data_instansi);
                                    }else{
                                        $instansi_pekerjaan = $status_pekerjaan.$instansi;
                                    }

                                  }
                                }

                                $id_pemohon = "";
                                $id_permohonan = "";
                                $keterangan_permohonan = "";
                                $no_register = "";
                                $tgl_start = "";
                                $tgl_selesai = "";
                                $judul = "";
                                $instansi_penerima = "";
                                $id_bidang = "";
                                $dosen_pembimbing = "";
                                $anggota = "";
                                $status_pendelegasian = "";
                                $url_ktp_pemohon = "";
                                $url_proposal_pemohon = "";
                                $url_sk_pemohon = "";
                                $jabatan_tdd = "";
                                $nama_tdd = "";
                                $no_surat_tdd = "";
                                $tgl_surat_tdd = "";
                                $alamat_dom_del = "";
                                $alamat_ktp_del = "";
                                $nama_del = "";
                                $nik_del = "";
                                $url_foto_del = "";
                                $url_ktp_del = "";
                                $url_srt_del = "";

                                $tgl_start_fix = "";
                                $tgl_selesai_fix = "";
                                $tgl_surat_tdd_fix = "";
                                    

                                if(isset($pendaftaran)){
                                  if($pendaftaran){
                                    $id_user = $pendaftaran["id_user"];
                                    $id_pemohon = $pendaftaran["id_pemohon"];
                                    $id_permohonan = $pendaftaran["id_permohonan"];
                                    $keterangan_permohonan = $pendaftaran["keterangan_permohonan"];
                                    $no_register = $pendaftaran["no_register"];
                                    $tgl_start = $pendaftaran["tgl_start"];
                                    $tgl_selesai = $pendaftaran["tgl_selesai"];
                                    $judul = $pendaftaran["judul"];
                                    $instansi_penerima = $pendaftaran["instansi_penerima"];
                                    $id_bidang = $pendaftaran["id_bidang"];
                                    $dosen_pembimbing = $pendaftaran["dosen_pembimbing"];
                                    $anggota = $pendaftaran["anggota"];
                                    $status_pendelegasian = $pendaftaran["status_pendelegasian"];
                                    $url_ktp_pemohon = $pendaftaran["url_ktp_pemohon"];
                                    $url_proposal_pemohon = $pendaftaran["url_proposal_pemohon"];
                                    $url_sk_pemohon = $pendaftaran["url_sk_pemohon"];
                                    $jabatan_tdd = $pendaftaran["jabatan_tdd"];
                                    $nama_tdd = $pendaftaran["nama_tdd"];
                                    $no_surat_tdd = $pendaftaran["no_surat_tdd"];
                                    $tgl_surat_tdd = $pendaftaran["tgl_surat_tdd"];
                                    $alamat_dom_del = $pendaftaran["alamat_dom_del"];
                                    $alamat_ktp_del = $pendaftaran["alamat_ktp_del"];
                                    $nama_del = $pendaftaran["nama_del"];
                                    $nik_del = $pendaftaran["nik_del"];
                                    $url_foto_del = $pendaftaran["url_foto_del"];
                                    $url_ktp_del = $pendaftaran["url_ktp_del"];
                                    $url_srt_del = $pendaftaran["url_srt_del"];

                                    $tmp_tgl = explode("-", $tgl_start);
                                    $tgl_start_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", $tgl_selesai);
                                    $tgl_selesai_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", $tgl_surat_tdd);
                                    $tgl_surat_tdd_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    if($url_foto_del != ""){
                                      if(file_exists('./doc/foto_delegasi/'.$url_foto_del)){
                                        $url_foto_del_fix = base_url()."doc/foto_delegasi/".$url_foto_del;
                                      }else{
                                        $url_foto_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                      }
                                    }else {
                                      $url_foto_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }
                                    

                                    if($url_ktp_del != ""){
                                      if(file_exists('./doc/ktp_delegasi/'.$url_ktp_del)){
                                        $url_ktp_del_fix = base_url()."doc/ktp_delegasi/".$url_ktp_del;  
                                      }else{
                                        $url_ktp_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                      }
                                    }else{
                                      $url_ktp_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }
                                    

                                    if($url_srt_del != ""){
                                      if(file_exists('./doc/srt_kuasa/'.$url_srt_del)){
                                        $url_srt_del_fix = base_url()."doc/srt_kuasa/".$url_srt_del; 
                                      }else{
                                        $url_srt_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                      }
                                    }else{
                                      $url_srt_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                    }


                                    if($url_ktp_pemohon != ""){
                                      if(file_exists('./doc/ktp/'.$url_ktp_pemohon)){
                                        $url_ktp_pemohon_fix = base_url()."doc/ktp/".$url_ktp_pemohon;
                                      }else{
                                        $url_ktp_pemohon_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                      }
                                        
                                    }else {
                                      $url_ktp_pemohon_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }
                                    

                                    if($url_sk_pemohon != ""){
                                      if(file_exists('./doc/sk/'.$url_sk_pemohon)){
                                        $url_sk_pemohon_fix = base_url()."doc/sk/".$url_sk_pemohon; 
                                      }else{
                                        $url_sk_pemohon_fix = base_url()."assets/core_img/icon_upload_152x277.png"; 
                                      }
                                       
                                    }else{
                                      $url_sk_pemohon_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }

                                  }
                                }

                                if(isset($get_verifikation_data)){
                                    if($get_verifikation_data){
                                        // print_r("<pre>");
                                        // print_r($get_verifikation_data);
                                        $status_hide_profil = "";
                                        $status_hide_pendaftaran = "";
                                        $status_hide_delegasi = "";
                                        $status_hide_dokumen = "";

                                        if($get_verifikation_data["list_vert_all"]["vert_user"]=="0"){
                                            $status_hide_profil = "hidden";
                                        }

                                        if($get_verifikation_data["list_vert_all"]["vert_permohonan"]=="0"){
                                            $status_hide_pendaftaran = "hidden";
                                        }

                                        if($get_verifikation_data["list_vert_all"]["vert_delegasi"]=="0"){
                                            $status_hide_delegasi = "hidden";
                                        }

                                        if($status_pendelegasian == "0"){
                                            $status_hide_delegasi = "hidden";
                                        }

                                        if($get_verifikation_data["list_vert_all"]["vert_doc"]=="0"){
                                            $status_hide_dokumen = "hidden";
                                        }
                                    }
                                }
                              ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Detail Permohonan</h4><br>
                                <h6 class="card-subtitle"></h6>
                                <div id="accordion1" role="tablist" aria-multiselectable="true">
                                    <div class="card m-b-0">
                                        <div class="card-header" role="tab" id="headingOne1">
                                            <h5 class="mb-0">
                                            <a class="link" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-address-book"></i>&nbsp;&nbsp;
                                                Data Pemohon
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1">
                                            <div class="card-body">
                                                <div class="row" <?php print_r($status_hide_profil);?>>
                                                    
                                                    <div class="col-lg-3">
                                                        <dl>
                                                            <dt>Foto Profil</dt>
                                                            <dd><br>
                                                                <img width="152px" height="227px" src="<?= $url_foto_profil_fix;?>" id="img_foto" name="img_foto">
                                                            </dd>
                                                        </dl>
                                                        
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <dl>
                                                            <dt>Nama Pemohon</dt>
                                                            <dd><?= $nama_user;?></dd>

                                                            <dt>Email</dt>
                                                            <dd><?= $email;?></dd>

                                                            <dt>Nomor Telephone</dt>
                                                            <dd><?= $tlp;?></dd>

                                                            <dt>Tanggal Lahir</dt>
                                                            <dd><?= $tgl_lhr_fix;?></dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <dl>

                                                            <dt>Nomor Identitas</dt>
                                                            <dd><?= $nik;?></dd>

                                                            <dt>Alamat Sesuai KTP</dt>
                                                            <dd><?= $alamat;?></dd>

                                                            <dt>Alamat di Malang</dt>
                                                            <dd><?= $alamat_dom;?></dd>

                                                            <dt>Status Pekerjaan</dt>
                                                            <dd><?= $instansi_pekerjaan;?></dd>
                                                        </dl>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="card m-b-0">
                                        <div class="card-header" role="tab" id="headingTwo2">
                                            <h5 class="mb-0">
                                            <a class="collapsed link" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                                <i class="fa fa-id-card"></i>&nbsp;&nbsp;
                                                Data Pendaftaran Magang/Penelitian
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseTwo2" class="collapse show" role="tabpanel" aria-labelledby="headingTwo2">
                                            <div class="card-body">
                                                <div class="row" <?php print_r($status_hide_pendaftaran);?>>
                                                    
                                                    <div class="col-lg-5">
                                                        <dl>
                                                            <dt>Judul</dt>
                                                            <dd><?= $judul;?></dd>

                                                            <dt>Tujuan Permohonan</dt>
                                                            <dd><?= $keterangan_permohonan;?></dd>

                                                            <dt>Tanggal Mulai</dt>
                                                            <dd><?= $tgl_start_fix;?></dd>

                                                            <dt>Tanggal Berakhir</dt>
                                                            <dd><?= $tgl_selesai_fix;?></dd>

                                                            <dt>Dosen Penanggung Jawab</dt>
                                                            <dd>
                                                                <table class="table" border="0">
                                                                    <thead>
                                                                        <tr>
                                                                            <td width="10%"></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="output_dosen">
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </dd>
                                                            
                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <dl>
                                                            <dt>Dinas yang Dipilih</dt>
                                                            <dd>
                                                                <table class="table" border="0">
                                                                    <thead>
                                                                        <tr>
                                                                            <td width="10%"></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="output_bidang">
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </dd>
                                                            

                                                            <dt>Daftar Anggota Kelompok</dt>
                                                            <dd>
                                                                <table class="table" border="0">
                                                                    <thead>
                                                                        <tr>
                                                                            <td width="10%"></td>
                                                                            <td width="45%"></td>
                                                                            <td width="45%"></td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="output_anggota">
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    
                                    <div class="card">
                                        <div class="card-header" role="tab" id="headingThree3">
                                            <h5 class="mb-0">
                                            <a class="collapsed link" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                                                <i class="fa fa-group"></i>&nbsp;&nbsp;
                                                Data Delegasi
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseThree3" class="collapse show" role="tabpanel" aria-labelledby="headingThree3">
                                            <div class="card-body">
                                                <div class="row" <?php print_r($status_hide_delegasi);?>>
                                                    <div class="col-lg-5">
                                                        <dl>
                                                            <dt>Scan/Foto Surat Kuasa</dt>
                                                            <dd>
                                                                <img width="400px" height="650px" src="<?=$url_srt_del_fix?>" id="img_kuasa_del" name="img_kuasa_del">
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <dl>
                                                                    <dt>Scan/Foto KTP</dt>
                                                                    <dd>
                                                                        <img width="400px" height="253px" src="<?=$url_ktp_del_fix?>" id="img_ktp_del" name="img_ktp_del">
                                                                    </dd>                                                     
                                                                </dl>
                                                                <hr>
                                                            </div><br>
                                                            <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-lg-4">
                                                                        <dl>
                                                                            <dt>Scan/Foto Diri</dt>
                                                                            <dd>
                                                                                <img width="152px" height="227px" src="<?=$url_foto_del_fix?>" id="img_foto_del" name="img_foto_del">
                                                                            </dd>
                                                                        </dl>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <br>
                                                                        <dl>
                                                                            <dt>Nama Penerima Kuasa</dt>
                                                                            <dd><?= $nama_del;?></dd>

                                                                            <dt>Nomor Identitas Penerima Kuasa</dt>
                                                                            <dd><?= $nik_del;?></dd>

                                                                            <dt>Alamat Penerima Kuasa (Sesuai KTP)</dt>
                                                                            <dd><?= $alamat_ktp_del;?></dd>

                                                                            <dt>Alamat Penerima Kuasa (Di Malang)</dt>
                                                                            <dd><?= $alamat_dom_del;?></dd>
                                                                        </dl>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-header" role="tab" id="headingThree4">
                                            <h5 class="mb-0">
                                            <a class="collapsed link" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree4" aria-expanded="false" aria-controls="collapseThree4">
                                                <i class="fa fa-briefcase"></i>&nbsp;&nbsp;
                                                Data Kelengkapan Dokumen
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseThree4" class="collapse show" role="tabpanel" aria-labelledby="headingThree4">
                                            <div class="card-body">
                                                <div class="row" <?php print_r($status_hide_dokumen);?>>
                                                    <div class="col-lg-6">
                                                        <dl>
                                                            <dt>Scan/Foto KTP</dt>
                                                            <dd>
                                                                <img width="400px" height="253px" id="img_ktp" src="<?= $url_ktp_pemohon_fix?>"/><br />
                                                            </dd>

                                                            <br>
                                                            <dt>Nama yang Menandatangani Surat Pengantar</dt>
                                                            <dd><?= $nama_tdd;?></dd>

                                                            <dt>Jabatan yang Menandatangani Surat Pengantar</dt>
                                                            <dd><?= $jabatan_tdd;?></dd>

                                                            <dt>Nomor Surat Pengantar</dt>
                                                            <dd><?= $no_surat_tdd;?></dd>

                                                            <dt>Tanggal Surat Pengantar</dt>
                                                            <dd><?= $tgl_surat_tdd_fix;?></dd>

                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <dl>
                                                            <dt>Dokumen PDF</dt>
                                                            <dd>
                                                                <a id="url_proposal" href="<?php echo base_url()."doc/proposal/".$url_proposal_pemohon;?>"><?php echo $url_proposal_pemohon;?></a><br /><br />
                                                            </dd>

                                                            <dt>Scan/Foto Surat Pengantar</dt>
                                                            <dd>
                                                                <img width="350px" height="500px" id="img_sk" src="<?=$url_sk_pemohon_fix;?>"/><br /><br />
                                                            </dd>
                                                            
                                                        </dl>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-1 text-right">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="checkbox" class="check" id="cek_pendaftaran" data-checkbox="icheckbox_square-green" value="1">    
                                                        </div>
                                                        <div class="col-lg-11">
                                                            <label for="cek_pendaftaran">Demikian data pribadi ini saya buat dengan sebenarnya sesuai <a style="color: #1976d2;" href="<?php echo base_url(); ?>home/persyaratan">syarat dan ketentuan </a>, serta apabila ternyata isian yang dibuat tidak benar, maka saya bersedia menanggung akibat hukum yang ditimbulkannya.</label>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="col-lg-12" align="center">
                                                    <br><hr>
                                                    <button type="button" class="btn btn-rounded btn-danger" name="back_daftar" id="back_daftar">Kembali ke Form Pendaftaran</button>
                                                    &nbsp;&nbsp;
                                                    <button type="button" class="btn btn-rounded btn-info" name="add_check" id="add_check" align="right">Lanjutkan Pendaftaran</button>                                       
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

            <script src="<?php echo base_url()."assets/js/jquery-3.2.1.js";?>"></script>
            <script>
                $(document).ready(function(){
                    show_tmp();
                    show_dosen();
                    show_anggota();
                });

                $("#back_daftar").click(function(){
                    window.location.href = "<?php print_r(base_url());?>pendaftaran/mainform";
                });
                //=======================================================================================================
                //------------------------------------------checked_lisence----------------------------------------------
                //=======================================================================================================
                function check_daftar(){
                    if($("#cek_pendaftaran").is(":checked")){
                        $("#add_check").prop("disabled", true);
                            var data_main =  new FormData();
                            data_main.append('id_pemohon', "<?php echo $id_pemohon;?>");
                            data_main.append('cek_pendaftaran', $("#cek_pendaftaran").is(":checked"));

                            $.ajax({
                                    url: "<?php echo base_url()."user_new/maindatausernew/update_status_pendaftaran";?>", // point to server-side PHP script 
                                    dataType: 'html',  // what to expect back from the PHP script, if anything
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    data: data_main,                         
                                    type: 'post',
                                    success: function(res){
                                        response_act_vert(res);
                                        console.log(res);
                                        //$("#output-edit-profil").html(res);
                                    }
                            });

                          console.log(data_main);
                        
                    }else{
                        !function($) {
                          "use strict";
                          var SweetAlert = function() {};
                          //examples 
                          SweetAlert.prototype.init = function() {
                          //Warning Message
                              swal("Proses Gagal.!!", "Untuk melanjutkan pendaftaran silhakan centang pada persyaratan sebagai tanda bahwa anda setuju dan telah memenuhi persyaratan yang telah di tetapkan", "warning")
                                       
                          },
                          //init
                          $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                      }(window.jQuery),

                      //initializing 
                      function($) {
                          "use strict";
                          $.SweetAlert.init()
                      }(window.jQuery);        
                    }
                }

                $("#add_check").click(function(){
                    check_daftar();
                });

                function response_act_vert(res){
                    var data_json = JSON.parse(res);
                    // console.log($data_json);
                    if(data_json.msg_main.status){
                    //success process
                      !function($) {
                          "use strict";
                          var SweetAlert = function() {};
                          //examples 
                          SweetAlert.prototype.init = function() {
                        
                          //Warning Message
                         
                              swal({   
                                  title: "Proses Pendaftaran Berhasil",   
                                  text: "Silahkan tunggu beberapa saat lagi untuk mendapatkan persetujuan admin. Untuk proses selanjutnya silahkan perhatikan pada menu tata cara pendaftaran.",   
                                  type: "success",   
                                  showCancelButton: false,   
                                  confirmButtonColor: "#28a745",   
                                  confirmButtonText: "Lanjutkan",   
                                  closeOnConfirm: false 
                              }, function(){
                                window,location.href = "<?=base_url();?>pendaftaran/home";   
                              });
                                       
                          },
                          //init
                          $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                      }(window.jQuery),

                      //initializing 
                      function($) {
                          "use strict";
                          $.SweetAlert.init()
                      }(window.jQuery);

                      $("#add_check").removeAttr("disabled", true);
                    }else{
                    //success process
                      !function($) {
                          "use strict";
                          var SweetAlert = function() {};
                          //examples 
                          SweetAlert.prototype.init = function() {
                            swal("Proses Pendaftaran Gagal!!", "Silahkan periksa semua input anda. Proses gagal karena terdapat beberapa input yang kurang sesuai..", "warning");
                            //Warning Message                                       
                          },
                          //init
                          $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                      }(window.jQuery),

                      //initializing 
                      function($) {
                          "use strict";
                          $.SweetAlert.init()
                      }(window.jQuery);

                      $("#add_check").removeAttr("disabled", true);
                    }
                }
                    
                //=======================================================================================================
                //------------------------------------------checked_lisence----------------------------------------------
                //=======================================================================================================

                //=======================================================================================================
                //-------------------------------------------Add_Json_Input----------------------------------------------
                //=======================================================================================================

                  //------------------------------------------Dinas Pilihan------------------------------------------//
                                    var master_tmp_magang = JSON.parse('<?php echo json_encode($tmp_magang);?>');
                                    var list_add_tmp = <?php print_r($id_bidang);?>;
                                    
                                    function show_tmp(){
                                        var content_tmp = "";
                                        for(var i = 0; i<list_add_tmp.length; i++){
                                            var no = i+1;
                                            content_tmp += "<tr><td>"+no+
                                            ". </td><td>"+master_tmp_magang[list_add_tmp[i]]+
                                            "</td></tr>";
                                        }
                                        $("#output_bidang").html(content_tmp);
                                    }
                  
                  //------------------------------------------List Anggota-------------------------------------------//
                                    
                                    var list_anggota = <?php print_r($anggota);?>;
                                    
                                    function show_anggota(){
                                        var content_anggota = "";
                                        for(var i = 0; i<list_anggota.length; i++){
                                            var no = i+1;
                                            content_anggota += "<tr><td>"+no+
                                            ". </td><td>"+list_anggota[i][0]+
                                            "</td><td>"+list_anggota[i][1]+
                                            "</td></tr>";
                                        }
                                        $("#output_anggota").html(content_anggota);
                                    }
                  //-------------------------------------------List Dosen--------------------------------------------//
                                    var list_dosen = <?php print_r($dosen_pembimbing);?>;
                                    
                                    function show_dosen(){
                                        var content_dosen = "";
                                        for(var i = 0; i<list_dosen.length; i++){
                                            var no = i+1;
                                            content_dosen += "<tr><td>"+no+
                                            "</td><td>"+list_dosen[i]+
                                            "</td></tr>";
                                        }
                                        $("#output_dosen").html(content_dosen);
                                    }
                //=======================================================================================================
                //-------------------------------------------Add_Json_Input----------------------------------------------
                //=======================================================================================================
            </script>
            