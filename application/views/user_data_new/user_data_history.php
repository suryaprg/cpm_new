                            <?php

                                $array_of_month = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

                                $array_of_dinas = array();
                                if(isset($tmp_magang)){
                                    if($tmp_magang){
                                        $array_of_dinas = $tmp_magang;
                                    }
                                }
                                                                
                            ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
               

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <!-- <div class="card-header">
                                <h4 class="m-b-0 text-white">Daftar Riwayat Pendaftaran Magang</h4>
                            </div>
                             -->
                            <div class="card-body">
                                <h4 class="card-title">Daftar Riwayat Pendaftaran Magang</h4><br>
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="3%">No</th>
                                                <th width="20%">Judul/Tema Magang</th>
                                                <th width="15%">Jenis Permohonan</th>
                                                <th width="12%">Periode Magang</th>
                                                <th width="">Status Magang</th>
                                                <th width="19%">Status Penerimaan</th>
                                                <th width="15%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody><a href="" onclick="post()"></a>

                                            <?php
                                                if(isset($pemohon)){
                                                  if($pemohon){


                                                    $no = 1;
                                                    foreach ($pemohon as $r_pemohon => $v_pemohon) {
                                                        $id_user = $v_pemohon->id_user;
                                                        $id_pemohon = $v_pemohon->id_pemohon;
                                                        $id_permohonan = $v_pemohon->id_permohonan;
                                                        $keterangan_permohonan = $v_pemohon->keterangan_permohonan;
                                                        $jenis_kegiatan = $v_pemohon->jenis_kegiatan;
                                                        $no_register = $v_pemohon->no_register;
                                                        $tgl_start = $v_pemohon->tgl_start;
                                                        $tgl_selesai = $v_pemohon->tgl_selesai;
                                                        $judul = $v_pemohon->judul;
                                                        $instansi_penerima = $v_pemohon->instansi_penerima;
                                                        $lokasi_magang = $v_pemohon->id_bidang;

                                                        $status_pendaftaran = $v_pemohon->status_pendaftaran;
                                                        $status_magang = $v_pemohon->status_magang;
                                                        $status_diterima = $v_pemohon->status_diterima;
                                                        $status_active = $v_pemohon->status_active_mg;
                                                        
                                                        $tmp_tgl = explode("-", $tgl_start);
                                                        $tgl_start_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                        $tmp_tgl = explode("-", $tgl_selesai);
                                                        $tgl_selesai_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];


                                                        $str_status_pendaftaran = "<span class=\"label label-warning\">Permohonan Magang Belum Diresponse</span>";
                                                        $str_status_action = "<center>
                                                            <a class=\"btn btn-warning\" href=\"".base_url()."pendaftaran/mainform"."\" style=\"width: 50px;\"><i class=\"fa fa-pencil\" style=\"color: white;\"></i></a>

                                                            <a class=\"btn btn-danger\" style=\"width: 50px;\" onclick=\"delete_pemohon('".$id_pemohon."')\"><i class=\"fa fa-trash\" style=\"color: white;\"></i></a>
                                                            </center>";

                                                        $str_status_diterima = "<span class=\"label label-warning\">Permohonan Belum Diresponse</span>";
                                                        if($status_diterima == "1"){
                                                            $str_status_diterima = "<span class=\"label label-info\">Permohonan Diterima</span>,
                                                                <br>Oleh: ".$array_of_dinas[$instansi_penerima];
                                                        }elseif ($status_diterima == "2"){
                                                            $str_status_diterima = "<span class=\"label label-danger\">Permohonan Ditolak</span>";
                                                        }

                                                        $str_lokasi_dinas = "";
                                                        $daftar_dinas = json_decode(str_replace("'", "\"", $lokasi_magang));
                                                        // print_r($json_no_acc_opd);
                                                        foreach ($daftar_dinas as $r_dinas => $v_dinas) {
                                                            
                                                            $str_lokasi_dinas .= "- ".$array_of_dinas[$v_dinas]."<br>";
                                                        }   
                                                        

                                                        if($jenis_kegiatan == "1"){
                                                            if($status_pendaftaran == "1"){
                                                                if($status_active == "1") {
                                                                    if($status_magang == "1"){
                                                                        $str_status_pendaftaran = "<span class=\"label label-info\">Permohonan Magang Aktif</span>";
                                                                        if($status_diterima == "0"){
                                                                            $str_status_action = "Mohon Ditunggu Permohonan Saudara Belum Diresponse Oleh Instansi Terkait";
                                                                        }elseif ($status_diterima == "1") {
                                                                            $str_status_action = 
                                                                            "<center><a class=\"btn btn-info\" onclick=\"get_pernyataan('".$id_pemohon."');\" style=\"width: 50px;\"><i class=\"fa fa-file-text\" style=\"color: white;\"></i></a>
                                                                            </center>";
                                                                        }elseif ($status_diterima == "2") {
                                                                            $str_status_action = "Permohonan saudara telah dinonaktifkan karena, Permohonan saudara telah ditolak instansi terkait";
                                                                        }
                                                                        
                                                                    }elseif($status_magang == "2"){
                                                                        $str_status_pendaftaran = "<span class=\"label label-danger\">Permohonan Tidak Aktif</span>";
                                                                        $str_status_action = "Permohonan saudara telah dinonaktifkan karena, Permohonan saudara tidak lolos dalam proses verifikasi data oleh admin BAKESBANGPOL";
                                                                    }elseif ($status_magang == "3") {
                                                                        $str_status_pendaftaran = "<span class=\"label label-primary\">Permohonan Magang Tidak Aktif</span>";
                                                                        $str_status_action = "Status Permohonan Tidak Aktif";
                                                                    }
                                                                }elseif ($status_active == "2"){
                                                                    $str_status_pendaftaran = "<span class=\"label label-danger\">Permohonan Tidak Aktif</span>";
                                                                    $str_status_action = "Status Permohonan Tidak Aktif";
                                                                }
                                                            }

                                                            print_r("<tr>
                                                                        <td>".$no++."</td>
                                                                        <td>".$judul."</td>
                                                                        <td>".$keterangan_permohonan."</td>
                                                                        <td>".$tgl_start_fix." s/d ".$tgl_selesai_fix."</td>
                                                                        <td>".$str_status_pendaftaran."<br>No Register: <br> ".$no_register."</td>
                                                                        <td>".$str_status_diterima."</td>
                                                                        <td>".$str_status_action."</td>
                                                                    </tr>");
                                                        }else{
                                                            if($status_pendaftaran == "1"){
                                                                if($status_active == "1") {
                                                                    if($status_magang == "1"){
                                                                        $str_status_pendaftaran = "<span class=\"label label-info\">Permohonan Penelitian Aktif</span>";
                                                                        $str_status_action = 
                                                                            "<center><a class=\"btn btn-info\" onclick=\"get_pernyataan('".$id_pemohon."');\" style=\"width: 50px;\"><i class=\"fa fa-file-text\" style=\"color: white;\"></i></a>
                                                                            </center>";                                                                        
                                                                    }elseif($status_magang == "2"){
                                                                        $str_status_pendaftaran = "<span class=\"label label-danger\">Permohonan Tidak Aktif</span>";
                                                                        $str_status_action = "Permohonan saudara telah dinonaktifkan karena, Permohonan saudara tidak lolos dalam proses verifikasi data oleh admin BARENLITBANG";
                                                                    }elseif ($status_magang == "3") {
                                                                        $str_status_pendaftaran = "<span class=\"label label-primary\">Permohonan Penelitian Tidak Aktif</span>";
                                                                        $str_status_action = "Status Permohonan Tidak Aktif";
                                                                    }
                                                                }elseif ($status_active == "2"){
                                                                    $str_status_pendaftaran = "<span class=\"label label-danger\">Permohonan Tidak Aktif</span>";
                                                                    $str_status_action = "Status Permohonan Tidak Aktif";
                                                                }
                                                            }

                                                            print_r("<tr>
                                                                        <td>".$no++."</td>
                                                                        <td>".$judul."</td>
                                                                        <td>".$keterangan_permohonan."</td>
                                                                        <td>".$tgl_start_fix." s/d ".$tgl_selesai_fix."</td>
                                                                        <td>".$str_status_pendaftaran."<br>No Register: <br> ".$no_register."</td>
                                                                        <td>".$str_lokasi_dinas."</td>
                                                                        <td>".$str_status_action."</td>
                                                                    </tr>");
                                                        }
                                                    }
                                                    
                                                  }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                    <!-- <a href="" style="width: 50px;"></a> -->
                                    <br><br>
                            </div>
                            <div class="card-body">
                                <div class="text-right">
                                    <label class="form-label">Keterangan Tombol Aksi ==> </label>
                                    <a class="btn btn-info" style="width: 40px;"><i class="fa fa-file-text" style="color: white;"></i></a>
                                    <label class="form-label text-info">Cetak Surat Pernyataan</label>,&nbsp;

                                    <a class="btn btn-warning" style="width: 40px;"><i class="fa fa-pencil" style="color: white;"></i></a>
                                    <label class="form-label text-warning">Ubah Data</label>,&nbsp;

                                    <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash" style="color: white;"></i></a>
                                    <label class="form-label text-danger">Hapus Data</label>,&nbsp;
                                </div>
                            </div>
                        </div>
                    </div>           
                </div>
            </div>

            <script src="<?php echo base_url()."assets/js/jquery-3.2.1.js";?>"></script>
            <script>
            function get_pernyataan(params) {
                var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("action", "<?php echo base_url();?>surat/mainsuratpernyataan/user_surat_pernyataan");
                form.setAttribute("target", "_blank");

                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", "id_pemohon");
                hiddenField.setAttribute("value", params);

                form.appendChild(hiddenField);

                document.body.appendChild(form);
                form.submit();
            }

            function delete_pemohon(id_pemohon) {
                !function($) {
                    "use strict";

                    var SweetAlert = function() {};

                    //examples 
                    SweetAlert.prototype.init = function() {
                        
                        swal({   
                            title: "Konfirmasi Penghapusan Data?",   
                            text: "Apakah anda yakin ingin menghapus permohonan ini ..?",   
                            type: "warning",   
                            showCancelButton: true,   
                            confirmButtonColor: "#DD6B55",   
                            confirmButtonText: "Ya, Hapus.!",   
                            closeOnConfirm: false 
                        }, function(){   
                            var data_main =  new FormData();
                            data_main.append('id_pemohon', id_pemohon);
                            $.ajax({
                                url: "<?php echo base_url()."user_new/maindatausernew/delete_pemohon";?>", // point to server-side PHP script 
                                dataType: 'html',  // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,                         
                                type: 'post',
                                success: function(res){
                                    response_delete_pemohon(res);
                                }
                            });
                        });

                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                //initializing 
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }

            function response_delete_pemohon(res){
                var data_json = JSON.parse(res);
                    var main_msg = data_json.msg_main;
                    var detail_msg = data_json.msg_detail;
                if(main_msg.status){
                    !function($) {
                        "use strict";
                        var SweetAlert = function() {};
                        //examples 
                        SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({   
                                title: "Proses Berhasil.!!",   
                                text: "Data permohonan berhasil diubah ..!",   
                                type: "success",   
                                showCancelButton: false,   
                                confirmButtonColor: "#28a745",   
                                confirmButtonText: "Lanjutkan",   
                                closeOnConfirm: false 
                            }, function(){
                                window.location.href = "<?php echo base_url()."pendaftaran/history";?>";
                            });                              
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                    }(window.jQuery),
                    
                    function($) {
                        "use strict";
                        $.SweetAlert.init()
                    }(window.jQuery);
                }else{
                    !function($) {
                        "use strict";
                        var SweetAlert = function() {};
                        //examples 
                        SweetAlert.prototype.init = function() {
                                                        
                            swal("Proses Gagal.!!", "Data permohonan gagal diubah, coba periksa jaringan dan koneksi anda", "warning");                   
                        },
                                                  
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                    }(window.jQuery),

                    function($) {
                        "use strict";
                        $.SweetAlert.init()
                    }(window.jQuery);
                }
            }
            </script>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

     
                
                