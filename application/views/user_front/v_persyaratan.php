<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>PMP</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style.css">

</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <?php include 'header_main.php';?>

    <!-- ##### Breadcumb Area Start ##### -->

    <!-- ##### About Us Area Start ##### -->
    <section class="about-us-area mt-50 section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="300ms">
                        <h3>PERSYARATAN IJIN PKL/PKN/MAGANG/PRAKTEK PROFESI ANTAR KOTA</h3>
                        <br>
                        <span>Permendagri Nomor 64 Tahun 2011<br>
                        Mekanisme Permohonan Rekomendasi (Pasal 4, 5 dan 6):<br>
                        Pelayanan di mulai hari senin sampai Jumat</span>
                     </div>
                </div>
            </div>
                        <table border="0">
                        <tr><td>
                        <p>1. Membawa surat permohonan penerbitan rekomendasi penelitian yang ditandatangani oleh :<br>
                        
                        <font size="3px">&#8226;</font> Lurah/Kepala Desa tempat domisili peneliti bagi peneliti kemasyarakatan untuk peneliti individu yang tidak berasal dari lembaga pendidikan/perguruan tinggi;<br>
                        <font size="3px">&#8226;</font> Pimpinan yang membidangi penelitian dari lembaga pendidikan/perguruan tinggi ; yang bersangkutan, untuk peneliti yang berasal dari lembaga pendidikan/perguruan tinggi;<br>
                        <font size="3px">&#8226;</font> Pimpinan yang membidangi penelitian dari badan usaha yang bersangkutan, untuk penelitian badan usaha, <br>
                        <font size="3px">&#8226;</font> Pimpinan yang membidangi penelitian dari kementerian/lembaga pemerintah non kementerian yang bersangkutan bertugas, untuk peneliti aparatur pemerintahan; <br>
                        <font size="3px">&#8226;</font> Pimpinan yang membidangi penelitian dari organisasi kemasyarakatan, untuk peneliti organisasi kemasyarakatan; <br>
                        </p>
                        </td></tr>

                        <tr><td>
                        <p>2. Surat permohonan penerbitan rekomendasi penelitian yang diajukan harus melampirkan:<br>
                        
                        <font size="3px">&#8226;</font> Proposal penelitian (latar belakang masalah, maksud dan tujuan, ruang lingkup, jangka waktu penelitian, nama peneliti, sasaran/target penelitian, metode penelitian, lokasi<br>    <font size="3px",color="#ffffff">&#8226;</font> penelitian dan hasil yang diharapkan dari penelitian);<br>
                        <font size="3px">&#8226;</font> Membawa Salinan/foto copy Kartu Tanda Penduduk atau SIM peneliti/penanggung jawab/ketua/ koordinator peneliti;<br>
                        <font size="3px">&#8226;</font> Mengisi surat pernyataan untuk mentaati dan tidak melanggar ketentuan peraturan perundang-undangan yang berlaku; <br>
                        <font size="3px">&#8226;</font> Peneliti badan usaha, organisasi kemasyarakatan atau lembaga nirlaba lainnya harus melampirkan berkas salinan/foto copy akta notaris pendirian badan usaha/organisasi kemasyarakatan/lembaga nirlaba lainnya. <br>
                        </p>
                        </td></tr>
                       		
                        <tr><td>
                        <p>3. Surat permohonan penerbitan rekomendasi diajukan kepada :<br>
                        
                        <font size="3px">&#8226;</font> Menteri melalui Direktur Jenderal Kesatuan Bangsa dan Politik, untuk penelitian lingkup nasional atau lintas provinsi (d/a. Jl. Medan Merdeka Utara 7 Jakpus, email : Kesbangpol-depdagri@yahoo.co.id);<br>    <font size="3px",color="#ffffff">&#8226;</font> Gubernur melalui Badan Kesatuan Bangsa dan Politik Provinsi. untuk penelitian lingkup provinsi dan kabupaten/kota di wilayahnya (d/a. Jl. Putat Indah 1 Surabaya 60189 Telp. 031-5677935, Fax. 031-5663530 email : bakesbangpol@jatimprov.go.id);<br>
                        <font size="3px">&#8226;</font> Bupati/Walikota melalui Badan/Kantor Kesatuan Bangsa dan Politik Kabupaten/Kota (d/a. Jl. A. Yani No.98 Malang 65125 Telp. 0341-491180, Fax 0341-474254);<br>
                        </p>
                        </td></tr>

                       	<tr><td>
                       	<p>4. BIAYA PEMBUATAN SURAT REKOMENDASI SEMUA GRATIS..<br>
                        
                        <font size="3px">   Kewajiban Peneliti (Pasal 15, 16, 17 dan 18)</font><br>    
                        <font size="3px",color="#ffffff">&#8226;</font> Peneliti wajib mentaati dan melakukan ketentuan dalam rekomendasi penelitian, antara lain :
						Peneliti menyampaikan rekomendasi penelitian dari Menteri kepada Gubernur melalui Badan Kesatuan Bangsa dan Politik Provinsi lokasi penelitian, untuk penelitian lintas provinsi, selanjutnya Badan Kesatuan Bangsa dan Politik Provinsi menerbitkan rekomendasi penelitian berdasarkan rekomendasi Menteri.<br>
                        <font size="3px">&#8226;</font> Peneliti menyampaikan rekomendasi penelitian dari Gubernur kepada Bupati/Walikota lokasi penelitian melalui Badan/Kantor Kesatuan Bangsa dan Politik di Kabupaten/Kota, untuk penelitian lintas Kabupaten/Kota, selanjutnya Badan/Kantor Kesatuan Bangsa dan Politik di Kabupaten/Kota menerbitkan rekomendasi penelitian, berdasarkan rekomendasi dari Gubernur.<br>
                        <font size="3px">&#8226;</font> Peneliti menyampaikan rekomendasi penelitian dari Bupati/Walikota kepada Camat, untuk penelitian lintas kecamatan.<br>
                        </p>
                        </td></tr>	
                        </table>

            <div class="row">
                <div class="col-12 col-md-6 wow fadeInUp" data-wow-delay="400ms">
                    <p><h5>Formulir<font color="#32CD32"> Pernyataan </font>untuk <b>PKL/PKN/MAGANG/PRAKTEK PROFESI</b> silahkan Download di bawah sini</h5></p>
                       <a href="<?php echo base_url(); ?>pmp/img/suratpernyataanpkl.pdf" class="btn academy-btn m-2">Download Formulir Pernyataan</a>
                </div>
              
            </div>
              <div class="row">
                <div class="col-12 col-md-6 wow fadeInUp" data-wow-delay="400ms">
                    <p><h5>Formulir <font color="#32CD32"> Permohonan </font>untuk <b>PKL/PKN/MAGANG/PRAKTEK PROFESI</b> silahkan Download di bawah sini</h5></p>
                       <a href="<?php echo base_url(); ?>pmp/img/formulirpendaftaran.pdf" class="btn academy-btn m-2">Download Formulir Permohonan</a>
                </div>
              
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="about-slides owl-carousel mt-100 wow fadeInUp" data-wow-delay="600ms">
                        <img src="<?php echo base_url(); ?>pmp/img/bg-img/info.png" alt="">
                        <img src="<?php echo base_url(); ?>pmp/img/bg-img/info.png" alt="">
                        <img src="<?php echo base_url(); ?>pmp/img/bg-img/info.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="main-footer-area section-padding-100-0">
            <div class="container">
                <div class="row">
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/core_img/logo_magang_penelitian_footer_front.png" alt=""></a>
                            </div>
                            <p>Pendaftaran Calon Peserta Magang dan Penelitian di Pemerintah Kota Malang</p>
                            <div class="footer-social-info">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            
                            <div class="gallery-list d-flex justify-content-between flex-wrap">
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Usefull Links</h6>
                            </div>
                            <nav>
                                <ul class="useful-links">
                                    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/pendaftaran">Pendaftaran</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/form">Cara Isi Form</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/persyaratan">Persyaratan</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/kontak">Kontak</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Kontak</h6>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-placeholder"></i>
                                <p>Alamat : Jl. Ahmad Yani No. 98 Malang</p>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-telephone-1"></i>
                                <p>Telp: 0341 - 491180 <br> Fax: 0341- 474254</p>
                            </div>
                            <div class="single-contact d-flex">
                                <i class="icon-contract"></i>
                                <p>Email     : bkb@malangkota.go.id</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> by Dinas Komunikasi dan Informatika Kota Malang</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->
    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url(); ?>pmp/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url(); ?>pmp/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo base_url(); ?>pmp/js/active.js"></script>
</body>

</html>