<?php
    //print_r("<pre>");
    //print_r($_SESSION);
    
    #create data temporary for session
    // $ex_identitas = "";

    // $ex_nik = "";
    // $ex_sim = "";
    // $ex_nim = "";
    
    // $ex_nama = "";                  
    // $ex_tgl = "";
    // $ex_alamat_ktp = "";
    // $ex_alamat_dom = "";
    // $ex_email = "";
    // $ex_pass = "";
    // $ex_pass_conf = "";
    // $ex_tlp = "";
    // $ex_corp = "";
    // $ex_instansi = "";
    // $ex_uni = "";
    // $ex_jurusan = "";
    // $ex_fak = "";
    // //$ex_prodi = "";
    
    // if(isset($this->session->flashdata("response_post")["ex_post"])){
    //     $ex_identitas = $this->session->flashdata("response_post")["ex_post"]["identitas"];

    //     $ex_nik = $this->session->flashdata("response_post")["ex_post"]["nik"];
    //     $ex_sim = $this->session->flashdata("response_post")["ex_post"]["sim"];
    //     $ex_nim = $this->session->flashdata("response_post")["ex_post"]["nim"];

    //     $ex_nama = $this->session->flashdata("response_post")["ex_post"]["nama_pemohon"];
    //     $ex_tgl = $this->session->flashdata("response_post")["ex_post"]["tgl_lhr"];
        
    //     // $ex_alamat = $this->session->flashdata("response_post")["ex_post"]["alamat"];
        
    //     $ex_alamat_ktp = $this->session->flashdata("response_post")["ex_post"]["alamat_ktp"];
    //     $ex_alamat_dom = $this->session->flashdata("response_post")["ex_post"]["alamat_dom"];
        
    //     $ex_email = $this->session->flashdata("response_post")["ex_post"]["email"];
    //     $ex_pass = $this->session->flashdata("response_post")["ex_post"]["password"];
    //     $ex_pass_conf = $this->session->flashdata("response_post")["ex_post"]["password_confirm"];
    //     $ex_tlp = $this->session->flashdata("response_post")["ex_post"]["no_telp"];
    //     $ex_corp = $this->session->flashdata("response_post")["ex_post"]["corp"];
    //     $ex_instansi = $this->session->flashdata("response_post")["ex_post"]["instansi"];
    //     $ex_uni = $this->session->flashdata("response_post")["ex_post"]["nama_universitas"];
    //     $ex_jurusan = $this->session->flashdata("response_post")["ex_post"]["jurusan"];
    //     $ex_fak = $this->session->flashdata("response_post")["ex_post"]["fakultas"];
        //$ex_prodi = $this->session->flashdata("response_post")["ex_post"]["program_studi"];
    // }
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="description" content="">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Title -->
      <title>PMP</title>
      <!-- Favicon -->
      <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">
      <!-- Core Stylesheet -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style.css">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.js"></script>
   </head>  
   <body>
      <!-- ##### Preloader ##### -->
      <div id="preloader">
         <i class="circle-preloader"></i>
      </div>

      <?php include 'header_main.php';?>
      
      <!-- ##### Breadcumb Area Start ##### -->
      <!-- ##### About Us Area Start ##### -->
      
      
      <section class="about-us-area mt-50 section-padding-100">
        <div class="container" align="center">
          <div class="col-lg-12" align="left">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Form Pendaftaran Magang/Penelitian</h3>
                    <div class="alert alert-success" role="alert" id="div_msg_success"></div>
                    <div class="alert alert-danger" role="alert" id="div_msg_error"></div>
                    <?php 
                    // print_r("<pre>");
                    // print_r($this->session->flashdata("response_register"));
                    // $msg_nama = "";
                    // $msg_tgl = "";
                    // $msg_alamat_ktp = "";
                    // $msg_alamat_dom = "";
                    // $msg_nik = "";
                    // $msg_email = "";
                    // $msg_pass = "";
                    // $msg_pass_conf = "";
                    // $msg_tlp = "";
                    // $msg_corp = "";

                    // $msg_identitas = "";
                    
                    
                    // if(isset($_SESSION["response_register"]["msg_main"])){
                     
                    //     $msg_identitas = strip_tags($this->session->flashdata("response_register")["msg_detail"]["identitas"]);

                    //     $msg_nama = strip_tags($this->session->flashdata("response_register")["msg_detail"]["nama_pemohon"]);
                    //     $msg_tgl = strip_tags($this->session->flashdata("response_register")["msg_detail"]["tgl_lhr"]);
                    //     $msg_alamat_ktp = strip_tags($this->session->flashdata("response_register")["msg_detail"]["alamat_ktp"]);
                    //     $msg_alamat_dom = strip_tags($this->session->flashdata("response_register")["msg_detail"]["alamat_dom"]);
                    //     // $msg_alamat = strip_tags($this->session->flashdata("response_register")["msg_detail"]["alamat"]);
                    //     $msg_nik = strip_tags($this->session->flashdata("response_register")["msg_detail"]["nik"]);
                    //     $msg_email = strip_tags($this->session->flashdata("response_register")["msg_detail"]["email"]);
                    //     $msg_pass = strip_tags($this->session->flashdata("response_register")["msg_detail"]["password"]);
                    //     $msg_pass_conf = strip_tags($this->session->flashdata("response_register")["msg_detail"]["password_confirm"]);
                    //     $msg_tlp = strip_tags($this->session->flashdata("response_register")["msg_detail"]["no_telp"]);
                    //     $msg_corp = strip_tags($this->session->flashdata("response_register")["msg_detail"]["corp"]);
                        
                    //     if($this->session->flashdata("response_register")["msg_main"]["status"] == true){
                    //         echo "<div class=\"alert alert-success\" role=\"alert\">
                    //                 ".$this->session->flashdata("response_register")["msg_main"]["msg"]."
                    //             </div>
                    //             ";
                    //     }else{
                    //         echo "<div class=\"alert alert-danger\" role=\"alert\">
                    //                 ".$this->session->flashdata("response_register")["msg_main"]["msg"]."
                    //             </div>
                    //             ";
                    //     }    
                    // }
                    ?>
                </div>
                <form method="POST" action="<?php echo base_url().'user/registeruser/get_post'; ?>">
                <div class="card-body">
                  <div class="row">
                      <div class="col-lg-6">
                         <div class="form-group">
                            <label class="form-label">Identitas yang Digunakan<b style="color: red;">*</b></label><br>
                             
                                  <input id="id_radio1" type="radio" name="identitas" value="0" checked=""/>NIK &nbsp;&nbsp;
                                  <input id="id_radio2" type="radio" name="identitas" value="1" /> No. SIM &nbsp;&nbsp;
                                  <input id="id_radio3" type="radio" name="identitas" value="2" /> NIM  
                                  
                            <p id="er_identitas" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-6" id="form_nik">
                        <div class="form-group">
                          <label class="form-label">NIK (Nomor Induk Kependudukan)<b style="color: red;">*</b></label>
                          <input type="number" name="nik" id="nik" value="" class="form-control" placeholder="Masukkan NIK Pemohon" autocomplete="off" maxlength="25">
                          <p id="er_nik" style="color: red;"></p>
                        </div>
                      </div>
                      

                      <div class="col-lg-6" id="form_sim">
                        <div class="form-group">
                          <label class="form-label">No. SIM (Nomor Surat Ijin Mengemudi)<b style="color: red;">*</b></label>
                          <input type="number" name="sim" id="sim" value="" class="form-control" placeholder="Masukkan No. SIM Pemohon" autocomplete="off" maxlength="25">
                          <p id="er_sim" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-6" id="form_nim">
                        <div class="form-group">
                          <label class="form-label">NIM (Nomor Induk Mahasiswa)<b style="color: red;">*</b></label>
                          <input type="number" name="nim" id="nim" value="" class="form-control" placeholder="Masukkan NIM Pemohon" autocomplete="off" maxlength="25">
                          <p id="er_nim" style="color: red;"></p>
                        </div>
                      </div>


                      <div class="col-lg-6">
                        <div class="form-group">
                              <label class="form-label">Nama Pemohon<b style="color: red;">*</b></label>
                              <input type="text" name="nama_pemohon" id="nama_pemohon" value="" class="form-control" data-mask="Masukkan Nama Pemohon" data-mask-clearifnotmatch="true" placeholder="Masukkan Nama Pemohon" autocomplete="off" maxlength="100">
                              <p id="er_nama_pemohon" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-label">Tgl. Lahir<b style="color: red;">*</b></label>
                            <input type="date" name="tgl_lhr" id="tgl_lhr" value="" class="form-control" autocomplete="off" maxlength="150">
                            <p id="er_tgl_lhr" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-label">No Telepon<b style="color: red;">*</b></label>
                            <input type="number" name="no_telp" id="no_telp" value="" class="form-control" data-mask="0000-0000" data-mask-clearifnotmatch="true" placeholder="Masukkan No Telepon" autocomplete="off" maxlength="122">
                            <p id="er_tlp" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-label">Email<b style="color: red;">*</b></label>
                            <input type="text" name="email" id="email" value="" class="form-control" data-mask="Masukkan Email Pemohon" data-mask-clearifnotmatch="true" placeholder="Masukkan Email Pemohon" autocomplete="off" maxlength="50">
                            <p id="er_email" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-label">Alamat Sesuai KTP<b style="color: red;">*</b></label>
                            <input type="text" name="alamat_ktp" id="alamat_ktp" value="" class="form-control" data-mask="Masukkan Alamat" data-mask-clearifnotmatch="true" placeholder="Masukkan alamat" autocomplete="off" maxlength="150">
                            <p id="er_alamat_ktp" style="color: red;"></p>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-label">Alamat Domisili<b style="color: red;">*</b></label>
                            <input type="text" name="alamat_dom" id="alamat_dom" value="" class="form-control" data-mask="Masukkan Alamat" data-mask-clearifnotmatch="true" placeholder="Masukkan alamat" autocomplete="off" maxlength="150">
                            <p id="er_alamat_dom" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-label">Password<b style="color: red;">*</b></label>
                            <input type="password" name="password" id="password" value="" class="form-control" maxlength="100" placeholder="Masukkan Password">
                            <p id="er_pass" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-label">Ulangi Password<b style="color: red;">*</b></label>
                            <input type="password" name="password_confirm" id="password_confirm" value="" class="form-control" maxlength="100" placeholder="Masukkan Password">
                            <p id="er_repass" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-12">
                         <div class="form-group">
                            <label class="form-label">Jenis Pekerjaan<b style="color: red;">*</b></label><br>
                             
                                  <input id="id_radio1" type="radio" name="corp" value="0" checked=""/> Umum/Pekerja  &nbsp;&nbsp;
                                  <input id="id_radio2" type="radio" name="corp" value="1" /> Mahasiswa
                            
                            <p id="er_corp" style="color: red;"></p>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="row" id="g_pekerja">
                          
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label">Nama Instansi<b style="color: red;">*</b></label>
                                <input type="text" name="instansi" id="instansi" value="" class="form-control" data-mask="0000-0000" data-mask-clearifnotmatch="true" placeholder="Masukkan Nama Instansi" autocomplete="off" maxlength="100"/><br>
                                <p id="er_instansi" style="color: red;"></p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="row" id="g_mhs">
                          
                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-label"><strong>Nama Universitas</strong></label>
                              <input type="text" name="nama_universitas" id="nama_universitas" value="" class="form-control"  placeholder="Masukkan Nama Universitas" maxlength="250"><br>
                              <p id="er_nama_universitas" style="color: red;"></p>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-label"><strong>Program Studi</strong></label><br>
                                <select class="form-control" id="program_studi" name="program_studi">
                                    <option value="SMA" selected="">SMA</option>
                                    <option value="SMK">SMK</option>
                                    <option value="DIPLOMA">D1/D2/D3/D4</option>
                                    <option value="SARJANA">Sarjana</option>
                                    <option value="MAGISTE">Paska Sarjana/Magister</option>
                                    <option value="DOKTORAL">Doktoral</option>
                                </select>
                                <p id="er_program_studi" style="color: red;"></p>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-label"><strong>Fakultas</strong></label><br>
                              <input type="text" name="fakultas" id="fakultas" value="" class="form-control" data-mask="0000-0000" data-mask-clearifnotmatch="true" placeholder="Masukkan Nama Fakultas" autocomplete="off" maxlength="250"><br />
                              <p id="er_fakultas" style="color: red;"></p>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <label class="form-label"><strong>Jurusan</strong></label><br>
                              <input type="text" name="jurusan" id="jurusan" value="" class="form-control"  placeholder="Masukkan Program studi" maxlength="250"><br>
                              <p id="er_jurusan" style="color: red;"></p>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-lg-12">
                        <div class="row">
                          <div class="col-lg-12">
                            <label class="form-label">Syarat Dan Ketentuan<b style="color: red;">*</b></label>
                          </div>
                          <div class="col-md-1">
                            <center>
                            <input type="checkbox" id="syarat" name="syarat" value="1"></center>
                          </div>
                          <div class="col-lg-11">
                            <label for="syarat">Demikian data pribadi ini saya buat dengan sebenarnya sesuai <a style="color: #1976d2;" href="<?php echo base_url(); ?>home/persyaratan">syarat dan ketentuan </a>, serta apabila ternyata isian yang dibuat tidak benar, maka saya bersedia menanggung akibat hukum yang ditimbulkannya.</label>
                          </div>
                        </div>
                        <!-- <div class="form-group"> -->
                            
                        <!-- </div> -->
                      </div>
                  </div>
                </div>
                <div class="card-footer text-center">
                  <button type="button" id="simpan" class="btn btn-success">Daftar</button>
                </div>
                </form>
            </div>
          </div>
        </div>
      </section>
      <!-- ##### About Us Area End ##### -->
      <!-- ##### Footer Area Start ##### -->
      <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="main-footer-area section-padding-100-0">
            <div class="container">
                <div class="row">
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/core_img/logo_magang_penelitian_footer_front.png" alt=""></a>
                            </div>
                            <p>Pendaftaran Calon Peserta Magang dan Penelitian di Pemerintah Kota Malang</p>
                            <div class="footer-social-info">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            
                            <div class="gallery-list d-flex justify-content-between flex-wrap">
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Usefull Links</h6>
                            </div>
                            <nav>
                                <ul class="useful-links">
                                    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/pendaftaran">Pendaftaran</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/form">Cara Isi Form</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/persyaratan">Persyaratan</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/kontak">Kontak</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Kontak</h6>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-placeholder"></i>
                                <p>Alamat : Jl. Ahmad Yani No. 98 Malang</p>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-telephone-1"></i>
                                <p>Telp: 0341 - 491180 <br> Fax: 0341- 474254</p>
                            </div>
                            <div class="single-contact d-flex">
                                <i class="icon-contract"></i>
                                <p>Email     : bkb@malangkota.go.id</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> by Dinas Komunikasi dan Informatika Kota Malang</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->
      <!-- jQuery-2.2.4 js -->
      <script src="<?php echo base_url(); ?>pmp/js/jquery/jquery-2.2.4.min.js"></script>
      <!-- Popper js -->
      <script src="<?php echo base_url(); ?>pmp/js/bootstrap/popper.min.js"></script>
      <!-- Bootstrap js -->
      <script src="<?php echo base_url(); ?>pmp/js/bootstrap/bootstrap.min.js"></script>
      <!-- All Plugins js -->
      <script src="<?php echo base_url(); ?>pmp/js/plugins/plugins.js"></script>
      <!-- Active js -->
      <script src="<?php echo base_url(); ?>pmp/js/active.js"></script>
      
      
      
      <script type="text/javascript">
      
         $(document).ready(function () {
            show_begin_inst();
            show_begin_indentitas();

            $("#div_msg_error").hide();
            $("#div_msg_success").hide();

            // $("#instansi").val("");
            // $("#nama_universitas").val("");
            // $("#jurusan").val("");
            // $("#fakultas").val("");
            //$("#program_studi").val("");
         });
         
         function show_begin_inst(){
            var radio_val = $("input[name='corp']:checked").val();
            //alert(radio_val);
            if(radio_val == 0){
                $("#g_mhs").hide(500);
                $("#g_pekerja").show(500);
            }else{
                $("#g_mhs").show(500);
                $("#g_pekerja").hide(500);    
            }
         }
         
                    
         $("input[name='corp']").click(function(){
            show_begin_inst(); 
         });

         function show_begin_indentitas(){
            var radio_val = $("input[name='identitas']:checked").val();
            //alert(radio_val);
            if(radio_val == 0){
                $("#form_nik").show(500);
                $("#form_sim").hide(500);
                $("#form_nim").hide(500);
            }else if(radio_val == 1){
                $("#form_nik").hide(500);
                $("#form_sim").show(500);
                $("#form_nim").hide(500);  
            }else {
                $("#form_nik").hide(500);
                $("#form_sim").hide(500);
                $("#form_nim").show(500);
            }
         }

         $("input[name='identitas']").click(function(){
            show_begin_indentitas(); 
         });
         
         //------------------------------------------------------------Functon Run-----------------------------------------------------------

         $("#simpan").click(function(){
             
              if($("#syarat").prop("checked")){
                    $("#simpan").prop("disabled", true);
                    
                    var data_main =  new FormData();
                    data_main.append('identitas', $("input[name='identitas']:checked").val());
                    // console.log($("input[name='identitas']").val());

                    data_main.append('nik', $("#nik").val());
                    data_main.append('sim', $("#sim").val());
                    data_main.append('nim', $("#nim").val());
                    
                    data_main.append('nama_pemohon', $("#nama_pemohon").val());
                    data_main.append('alamat_ktp', $("#alamat_ktp").val()); 
                    data_main.append('alamat_dom', $("#alamat_dom").val());
                    data_main.append('email', $("#email").val());
                    data_main.append('password', $("#password").val());
                    data_main.append('password_confirm', $("#password_confirm").val());
                    data_main.append('no_telp', $("#no_telp").val());
                    data_main.append('tgl_lhr', $("#tgl_lhr").val());

                    data_main.append('corp', $("input[name='corp']:checked").val());
                    
                    data_main.append('instansi', $("#instansi").val());
                    data_main.append('nama_universitas', $("#nama_universitas").val());
                    data_main.append('program_studi', $("#program_studi").val());
                    data_main.append('fakultas', $("#fakultas").val());
                    data_main.append('jurusan', $("#jurusan").val());


                    $.ajax({
                        url: "<?php echo base_url()."/user_new/registeruser/get_post/";?>", // point to server-side PHP script 
                        dataType: 'html',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data_main,                         
                        type: 'post',
                        success: function(res){
                            // console.log(res);
                            show_search(res);
                        }
                    });
              }else {
                var err_message = "Anda tidak dapat melakukan pendaftaran, sebelum anda melengkapi data dan menyetujui persyaratan pendaftaran";
                alert(err_message);
              }
         });

         function show_search(res){
            var data_json = JSON.parse(res);
            console.log(data_json);

            if(data_json.msg_main.status == false){
              alert(data_json.msg_main.msg);
              
              $("#div_msg_error").html(data_json.msg_main.msg);
              $("#div_msg_error").show();
              $("#div_msg_success").hide();

              $("#er_identitas").html(data_json.msg_detail.identitas);
              $("#er_nik").html(data_json.msg_detail.nik);
              $("#er_sim").html(data_json.msg_detail.sim);
              $("#er_nim").html(data_json.msg_detail.nim);
              $("#er_nama_pemohon").html(data_json.msg_detail.nama_pemohon);
              $("#er_tgl_lhr").html(data_json.msg_detail.tgl_lhr);
              $("#er_tlp").html(data_json.msg_detail.no_telp);
              $("#er_email").html(data_json.msg_detail.email);
              $("#er_alamat_ktp").html(data_json.msg_detail.alamat_ktp);
              $("#er_alamat_dom").html(data_json.msg_detail.alamat_dom);
              $("#er_pass").html(data_json.msg_detail.password);
              $("#er_repass").html(data_json.msg_detail.password_confirm);
              $("#er_corp").html(data_json.msg_detail.corp);
              $("#er_instansi").html(data_json.msg_detail.instansi);
              $("#er_nama_universitas").html(data_json.msg_detail.nama_universitas);
              $("#er_program_studi").html(data_json.msg_detail.program_studi);
              $("#er_fakultas").html(data_json.msg_detail.fakultas);

              $("#simpan").removeAttr("disabled", true);
              // $("#er_jurusan").html(data_json.msg_detail);                   

            }else {
              $("#div_msg_success").html(data_json.msg_main.msg);
              $("#div_msg_success").show();
              $("#div_msg_error").hide();

              clear_alert();

              alert(data_json.msg_main.msg);
              $("#simpan").removeAttr("disabled", true);
              window.location.href = "<?= base_url()."/home/login";?>";
            }
         }

         function clear_alert(){
              $("#er_identitas").html("");
              $("#er_nik").html("");
              $("#er_sim").html("");
              $("#er_nim").html("");
              $("#er_nama_pemohon").html("");
              $("#er_tgl_lhr").html("");
              $("#er_tlp").html("");
              $("#er_email").html("");
              $("#er_alamat_ktp").html("");
              $("#er_alamat_dom").html("");
              $("#er_pass").html("");
              $("#er_repass").html("");
              $("#er_corp").html("");
              $("#er_instansi").html("");
              $("#er_nama_universitas").html("");
              $("#er_program_studi").html("");
              $("#er_fakultas").html("");
         }
      </script>
   </body>
</html>