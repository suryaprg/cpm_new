<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>PMP</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style.css">
	
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
      
<script type ="text/javascript"
  <script src="http://code.jquery.com/jquery-latest.js"></script>
             <script type="text/javascript">
                 $(document).ready(function () {
					  $('#div2').hide('konten');
					   $('#div1').hide('konten');
                    $('#id_radio1').click(function () {
                       $('#div2').hide('konten');
                       $('#div1').show('konten');
                });
                $('#id_radio2').click(function () {
                      $('#div1').hide('konten');
                      $('#div2').show('konten');
                 });
               });
</script>
 <script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="nim[]" placeholder="Masukkan NIM" class="form-control name_list" /></td><td><input type="text" name="name[]" placeholder="Nama Mahasiswa" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name')[0].reset();  
                }  
           });  
      });  
 });  
 </script>
 <script>  
 $(document).ready(function(){  
      var i=1;  
      $('#addmahasiswa').click(function(){  
           i++;  
           $('#dynamic_field_mahasiswa').append('<tr id="row'+i+'"><td><input type="text" name="namamahasiswa[]" placeholder="Masukkan Nama Pembimbing" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id_mahasiswa");   
           $('#row'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_nama_mahasiswa').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_nama_mahasiswa')[0].reset();  
                }  
           });  
      });  
 });  
 </script>

</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <?php include 'header_main.php';?>

    <!-- ##### Breadcumb Area Start ##### -->

    <!-- ##### About Us Area Start ##### -->
    <section class="about-us-area mt-50 section-padding-100">
        <div class="container" align="center">
            <div class="col-lg-12" align="left">
            <div class="card">
                    <div class="card-header">
                  <h4 class="card-title" align="center">Cara Pengisian Form</h4>
                </div>
            <div class="card-body">
                           <ul class="list-group card-list-group">
                              <li class="list-group-item py-5">
                                 <div class="media">
                                    <div class="media-object avatar avatar-md mr-4" style="background-image: url(demo/faces/male/16.jpg)"></div>
                                    <div class="media-body">
                                       <div class="media-heading">
                                         <!--  <small class="float-right text-muted">Sudah Lengkap  Belum Lengkap</small> -->
                                          <h5>Nama Pemohon</h5>
                                       </div>
                                       <div>
                                      Masukkan Nama Pemohon, nama yang diisikan merupakan perwakilan dari kelompok magang / penelitian. Nama yang diisikan akan otomatis menjadi ketua kelompok.
                                       </div>
                                       <br>
                                       <div align="center">
                                          <img src="<?php echo base_url(); ?>tabler/dist/demo/form/a.jpg" class="header-brand-img" alt="tabler logo" align="text-center">
                                       </div>
                                    </div>
                                 </div>
                              </li>
                              <li class="list-group-item py-5">
                                 <div class="media">
                                    <div class="media-object avatar avatar-md mr-4" style="background-image: url(demo/faces/male/16.jpg)"></div>
                                    <div class="media-body">
                                       <div class="media-heading">
                                      
                                          <h5>Alamat</h5>
                                       </div>
                                       <div>
                                         Alamat yang diisikan merupakan alamat pemohon, apabila ada Calon Pemohon yang berdomisili diluar malang harap mengisi dengan alamat tempat tinggal / kos di Malang.
                                       </div>
                                        <div align="center">
                                          <img src="<?php echo base_url(); ?>tabler/dist/demo/form/b.jpg" class="header-brand-img" alt="tabler logo" align="text-center">
                                       </div>
                                    </div>
                                 </div>
                              </li>
                              <li class="list-group-item py-5">
                                 <div class="media">
                                    <div class="media-object avatar avatar-md mr-4" style="background-image: url(demo/faces/male/16.jpg)"></div>
                                    <div class="media-body">
                                       <div class="media-heading">
                                         
                                          <h5>NIK</h5>
                                       </div>
                                       <div>
                                          NIK yang diisi harus sesuai dengan KTP milik Calon Pemohon harap mengisi kolom ini dengan benar.
                                       </div>
                                       <br>
                                        <div align="center">
                                          <img src="<?php echo base_url(); ?>tabler/dist/demo/form/c.jpg" class="header-brand-img" alt="tabler logo" align="text-center">
                                       </div>
                                    </div>
                                 </div>
                              </li>
                                   <li class="list-group-item py-5">
                                 <div class="media">
                                    <div class="media-object avatar avatar-md mr-4" style="background-image: url(demo/faces/male/16.jpg)"></div>
                                    <div class="media-body">
                                       <div class="media-heading">
                                          <h5>Email</h5>
                                       </div>
                                       <div>
                                        Sebelum Calon Pemohon dapat Login kedalam sistem, Calon Pemohon diminta untuk melakukan konfirmasi email. Email yang diisi akan menjadi syarat untuk login kedalam sistem, pastikan email yang diisi sudah benar agar Calon Pemohon dapat menerima notifikasi di email. 
                                       </div>
                                       <br>
                                        <div align="center">
                                          <img src="<?php echo base_url(); ?>tabler/dist/demo/form/d.jpg" class="header-brand-img" alt="tabler logo" align="text-center">
                                       </div>
                                    </div>
                                 </div>
                              </li>
                                   <li class="list-group-item py-5">
                                 <div class="media">
                                    <div class="media-object avatar avatar-md mr-4" style="background-image: url(demo/faces/male/16.jpg)"></div>
                                    <div class="media-body">
                                       <div class="media-heading">
                                         
                                          <h5>Password</h5>
                                       </div>
                                       <div>
                                           Masukkan Password Calon Pemohon, kemudian ketikan lagi untuk konfirmasi Password yang telah dimasukkan.
                                       </div>
                                       <br>
                                        <div align="center">
                                          <img src="<?php echo base_url(); ?>tabler/dist/demo/form/e.jpg" class="header-brand-img" alt="tabler logo" align="text-center">
                                       </div>
                                    </div>
                                 </div>
                              </li>
                                   <li class="list-group-item py-5">
                                 <div class="media">
                                    <div class="media-object avatar avatar-md mr-4" style="background-image: url(demo/faces/male/16.jpg)"></div>
                                    <div class="media-body">
                                       <div class="media-heading">
                                         
                                          <h5>No Telepon</h5>
                                       </div>
                                       <div>
                                          Masukkan no telepon Calon Pemohon 
                                       </div>
                                       <br>
                                        <div align="center">
                                          <img src="<?php echo base_url(); ?>tabler/dist/demo/form/f.jpg" class="header-brand-img" alt="tabler logo" align="text-center">
                                       </div>
                                    </div>
                                 </div>
                              </li>
                                  <li class="list-group-item py-5">
                                 <div class="media">
                                    <div class="media-object avatar avatar-md mr-4" style="background-image: url(demo/faces/male/16.jpg)"></div>
                                    <div class="media-body">
                                       <div class="media-heading">
                                         
                                          <h5>Instansi</h5>
                                       </div>
                                       <div>
                                          Masukkan Nama Instansi, Prodi, dan Jurusan
                                       </div>
                                       <br>
                                        <div align="center">
                                          <img src="<?php echo base_url(); ?>tabler/dist/demo/form/i.jpg" class="header-brand-img" alt="tabler logo" align="text-center">
                                       </div>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
       

                </div>  
            <!--       <div class="form-group">
                    <label class="form-label">Telephone with Code Area</label>
                    <input type="text" name="field-name" class="form-control" data-mask="(00) 0000-0000" data-mask-clearifnotmatch="true" autocomplete="off" maxlength="14">
                  </div>
                  <div class="form-group">
                    <label class="form-label">IP Address</label>
                    <input type="text" name="field-name" class="form-control" data-mask="099.099.099.099" data-mask-clearifnotmatch="true" placeholder="000.000.000.000" autocomplete="off" maxlength="15">
                  </div> -->
                </div>
              </div>
          </div>
     
    </section>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="main-footer-area section-padding-100-0">
            <div class="container">
                <div class="row">
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/core_img/logo_magang_penelitian_footer_front.png" alt=""></a>
                            </div>
                            <p>Pendaftaran Calon Peserta Magang dan Penelitian di Pemerintah Kota Malang</p>
                            <div class="footer-social-info">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            
                            <div class="gallery-list d-flex justify-content-between flex-wrap">
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Usefull Links</h6>
                            </div>
                            <nav>
                                <ul class="useful-links">
                                    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/pendaftaran">Pendaftaran</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/form">Cara Isi Form</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/persyaratan">Persyaratan</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/kontak">Kontak</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Kontak</h6>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-placeholder"></i>
                                <p>Alamat : Jl. Ahmad Yani No. 98 Malang</p>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-telephone-1"></i>
                                <p>Telp: 0341 - 491180 <br> Fax: 0341- 474254</p>
                            </div>
                            <div class="single-contact d-flex">
                                <i class="icon-contract"></i>
                                <p>Email     : bkb@malangkota.go.id</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> by Dinas Komunikasi dan Informatika Kota Malang</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url(); ?>pmp/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url(); ?>pmp/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo base_url(); ?>pmp/js/active.js"></script>
</body>

</html>