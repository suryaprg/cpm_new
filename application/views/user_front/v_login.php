<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>PMP</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style.css">
	
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <?php include 'header_main.php';?>

    <!-- ##### About Us Area Start ##### -->
    <section class="about-us-area mt-50 section-padding-100">
        <div class="container" align="center">
            <div class="col-lg-8" align="left">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Login</h3>
                    <?php
                        if(isset($_SESSION["response_register"])){
                            if($this->session->flashdata("response_register")["msg_main"]["status"]){
                                echo "<div class=\"alert alert-primary\" role=\"alert\">
                                    ".$this->session->flashdata("response_register")["msg_main"]["msg"]."
                                </div>
                                ";
                            }
                        }
                        
                        if(isset($_SESSION["response_login"])){
                            if($this->session->flashdata("response_login")["msg_main"]["status"]){
                                echo "<div class=\"alert alert-success\" role=\"alert\">
                                    ".$this->session->flashdata("response_login")["msg_main"]["msg"]."
                                </div>
                                ";
                            }else{
                                echo "<div class=\"alert alert-danger\" role=\"alert\">
                                    ".$this->session->flashdata("response_login")["msg_main"]["msg"]."
                                </div>
                                ";
                            }
                        }

                        if(isset($_SESSION["response_activaiton"])){
                            if($this->session->flashdata("response_activaiton")["msg_main"]["status"]){
                                echo "<div class=\"alert alert-success\" role=\"alert\">
                                    ".$this->session->flashdata("response_activaiton")["msg_main"]["msg"]."
                                </div>
                                ";
                            }else{
                                echo "<div class=\"alert alert-danger\" role=\"alert\">
                                    ".$this->session->flashdata("response_activaiton")["msg_main"]["msg"]."
                                </div>
                                ";
                            }
                        }

                        if(isset($_SESSION["message_response"])){
                            if($this->session->flashdata("message_response")["msg_main"]["status"]){
                                echo "<div class=\"alert alert-success\" role=\"alert\">
                                    ".$this->session->flashdata("message_response")["msg_main"]["msg"]."
                                </div>
                                ";
                            }else{
                                echo "<div class=\"alert alert-danger\" role=\"alert\">
                                    ".$this->session->flashdata("message_response")["msg_main"]["msg"]."
                                </div>
                                ";
                            }
                        }
                    ?>
                    </div>
                    <form action="<?php echo base_url('user_new/loginuser/aksi_login'); ?>" method="post">
                 
                    <div class="card-body">
                        <div class="form-group">
				            <label class="form-label">Email</label>
                            <input type="email" name="email" required=""
                                value="<?php echo set_value('email'); ?>" class="form-control" data-mask="Masukan Nama Pemohon" 
                                data-mask-clearifnotmatch="true" placeholder="Masukkan Email" autocomplete="off" maxlength="40"/>
                            <p style="color: red;"><?= strip_tags($this->session->flashdata("response_login")["msg_detail"]["email"]);?></p>
                        </div>
				        <div class="form-group">
                            <label class="form-label">Password</label>
                            <input type="password" name="password" required=""
                                value="<?php echo set_value('password'); ?>" class="form-control" data-mask="Masukan Alamat"
                                data-mask-clearifnotmatch="true" placeholder="Masukkan Password" autocomplete="off" maxlength="15"/>
                            <p style="color: red;"><?= strip_tags($this->session->flashdata("response_login")["msg_detail"]["password"]);?></p>
                        </div>
                        <div class="card-footer text-center">
                            <button type="submit" class="btn btn-success">Login</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div> 
        </div>
    </section>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="main-footer-area section-padding-100-0">
            <div class="container">
                <div class="row">
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/core_img/logo_magang_penelitian_footer_front.png" alt=""></a>
                            </div>
                            <p>Pendaftaran Calon Peserta Magang dan Penelitian di Pemerintah Kota Malang</p>
                            <div class="footer-social-info">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            
                            <div class="gallery-list d-flex justify-content-between flex-wrap">
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Usefull Links</h6>
                            </div>
                            <nav>
                                <ul class="useful-links">
                                    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/pendaftaran">Pendaftaran</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/form">Cara Isi Form</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/persyaratan">Persyaratan</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/kontak">Kontak</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Kontak</h6>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-placeholder"></i>
                                <p>Alamat : Jl. Ahmad Yani No. 98 Malang</p>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-telephone-1"></i>
                                <p>Telp: 0341 - 491180 <br> Fax: 0341- 474254</p>
                            </div>
                            <div class="single-contact d-flex">
                                <i class="icon-contract"></i>
                                <p>Email     : bkb@malangkota.go.id</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> by Dinas Komunikasi dan Informatika Kota Malang</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url(); ?>pmp/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url(); ?>pmp/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo base_url(); ?>pmp/js/active.js"></script>
</body>

</html>