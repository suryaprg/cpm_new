<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>PMP</title>

    <!-- Favicon -->
	
    <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">

    <!-- Core Stylesheet -->
     <link rel= "stylesheet" href="<?php echo base_url(); ?>pmp/style.css" />

</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <?php include 'header_main.php';?>

    <!-- ##### Banner Home Bakesbang Start ##### -->
    <section class="hero-area">
        <div class="hero-slides owl-carousel">

            <!-- Single Hero Slide -->
            <div class="single-hero-slide bg-img" style="background-image: url(<?php echo base_url(); ?>pmp/img/bg-img/bg-1.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="hero-slides-content">
                                <!-- <h4 data-animation="fadeInUp" data-delay="100ms">Sistem Informasi Pendaftaran Peserta Magang & Penelitian</h4>
                                <h2 data-animation="fadeInUp" data-delay="400ms">Selamat Datang <br>Bakesbangpol Kota Malang</h2>
                                <a href="<?php echo base_url(); ?>user/user/hal_magang" class="btn academy-btn" data-animation="fadeInUp" data-delay="700ms">Pendaftaran</a> -->
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <!-- Single Hero Slide -->
            <div class="single-hero-slide bg-img" style="background-image: url(<?php echo base_url(); ?>pmp/img/bg-img/bg-2.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                             <div class="hero-slides-content">
                                <!-- <h4 data-animation="fadeInUp" data-delay="100ms">Sistem Informasi Pendaftaran Peserta Magang & Penelitian</h4>
                                <h2 data-animation="fadeInUp" data-delay="400ms">Selamat Datang <br>Bakesbangpol Kota Malang</h2>
                                <a href="<?php echo base_url(); ?>user/user/hal_magang" class="btn academy-btn" data-animation="fadeInUp" data-delay="700ms" align="text-center">Pendaftaran</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Banner Home Bakesbang Area End ##### -->


    <!-- ##### Button Pendaftaran Magang atau Penelitian ##### -->
    <!--<div class="testimonials-area section-padding-100 bg-img bg-overlay" style="background-image: url(<?php echo base_url(); ?>pmp/img/bg-img/bg-2.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center mx-auto white wow fadeInUp" data-wow-delay="300ms">
                        <h3>Silahkan Pilih Pendaftaran</h3>
                         <span>Untuk Calon Peserta Magang atau Calon Peserta Penelitian</span>
                    </div>
                </div>
            </div>
           <div class="add-widget">
           <center>
                            <a href="#"><img src="<?php echo base_url(); ?>pmp/img/core-img/1.png" alt=""></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            <a href="#"><img src="<?php echo base_url(); ?>pmp/img/core-img/2.png" alt=""></a>
                        </center></div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- ##### Button Pendaftaran Magang atau Penelitian Area End ##### -->

    <!-- ========== Jumlah Peserta Magang dan Peserta Penelitian ========== -->
                <div class="col-12">
                    <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="300ms">
                        <span></span><br>
                        <h5>Jumlah Peserta Magang dan Peserta Penelitian di Kota Malang</h5>
                    </div>
                </div>

                <div class="col-12">
                    <div class="academy-cool-facts-area mb-50">
                        <div class="row">
                        <div class="col-12 col-sm-6 col-md-3">
                                <div class="single-cool-fact text-center">
                                </div>
                            </div>
                            <!-- Jumlah peserta magang-->
                            <div class="col-12 col-sm-6 col-md-3">
                                <div class="single-cool-fact text-center">
                                    <i class="icon-assistance"></i>
                                    <h3>
                                        <span class="counter">
                                            <?php print_r($magang); ?>
                                        </span>
                                    </h3>
                                    <p>Jumlah Peserta Magang</p>
                                </div>
                            </div>
                                <!-- Jumlah peserta penelitian-->
                            <div class="col-12 col-sm-6 col-md-3">
                                <div class="single-cool-fact text-center">
                                    <i class="icon-assistance"></i>
                                    <h3>
                                        <span class="counter">
                                            <?= $penelitian; ?>
                                        </span>
                                    </h3>
                                    <p>Jumlah Peserta Penelitian</p>
                                </div>
                            </div>
                        </div>
                           
                    </div>
                </div>
    <!-- ##### CTA Area Start ##### -->
    <div class="call-to-action-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cta-content d-flex align-items-center justify-content-between flex-wrap">
                        <h3>Pendaftaran Peserta Magang dan Penelitian</h3>
                        <a href="<?php echo base_url(); ?>home/pendaftaran" class="btn academy-btn">Pendaftaran</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### CTA Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="main-footer-area section-padding-100-0">
            <div class="container">
                <div class="row">
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <a href="#"><img src="<?php echo base_url(); ?>assets/core_img/logo_magang_penelitian_footer_front.png" alt=""></a>
                            </div>
                            <p>Pendaftaran Calon Peserta Magang dan Penelitian di Pemerintah Kota Malang</p>
                            <div class="footer-social-info">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            
                            <div class="gallery-list d-flex justify-content-between flex-wrap">
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Usefull Links</h6>
                            </div>
                            <nav>
                                <ul class="useful-links">
                                    <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/pendaftaran">Pendaftaran</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/form">Cara Isi Form</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/persyaratan">Persyaratan</a></li>
                                    <li><a href="<?php echo base_url(); ?>home/kontak">Kontak</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    
                    <!-- Footer Widget Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="footer-widget mb-100">
                            <div class="widget-title">
                                <h6>Kontak</h6>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-placeholder"></i>
                                <p>Alamat : Jl. Ahmad Yani No. 98 Malang</p>
                            </div>
                            <div class="single-contact d-flex mb-30">
                                <i class="icon-telephone-1"></i>
                                <p>Telp: 0341 - 491180 <br> Fax: 0341- 474254</p>
                            </div>
                            <div class="single-contact d-flex">
                                <i class="icon-contract"></i>
                                <p>Email     : bkb@malangkota.go.id</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> by Dinas Komunikasi dan Informatika Kota Malang</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url(); ?>pmp/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url(); ?>pmp/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo base_url(); ?>pmp/js/active.js"></script>
</body>

</html>