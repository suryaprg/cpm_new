                            <?php

                                $array_of_month = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

                                $array_of_dinas = array();
                                if(isset($tmp_magang)){
                                    if($tmp_magang){
                                        $array_of_dinas = $tmp_magang;
                                    }
                                }
                                
                                $admin_opd = $_SESSION["admin_lv_1"]["id_bidang"];                            
                            ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-body">
                                <h4 class="card-title">Daftar Riwayat Pendaftaran Magang</h4><br>
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="3%">No</th>
                                                <th width="15%">Judul/Tema Magang</th>
                                                <th width="12%">Nama Pemohon & Delegasi</th>
                                                <th width="15%">Status Registrasi</th>
                                                <th width="10%">Jenis Permohonan</th>
                                                <th width="12%">Periode Magang</th>
                                                <th width="26%">Lokasi Magang</th>
                                                <th width="7%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($pemohon)){
                                                  if($pemohon){
                                                    $no = 1;
                                                    foreach ($pemohon as $r_pemohon => $v_pemohon) {
                                                        $id_user = $v_pemohon->id_user;
                                                        $nama_user = $v_pemohon->nama;
                                                        $nama_del = $v_pemohon->nama_del;
                                                        $id_pemohon = $v_pemohon->id_pemohon;
                                                        $id_permohonan = $v_pemohon->id_permohonan;
                                                        $keterangan_permohonan = $v_pemohon->keterangan_permohonan;
                                                        $tgl_start = $v_pemohon->tgl_start;
                                                        $tgl_selesai = $v_pemohon->tgl_selesai;
                                                        $judul = $v_pemohon->judul;
                                                        $lokasi_magang = $v_pemohon->id_bidang;
                                                        $status_pendaftaran = $v_pemohon->status_pendaftaran;
                                                        $status_magang = $v_pemohon->status_magang;
                                                        $status_diterima = $v_pemohon->status_diterima;

                                                        $instansi_penerima = $v_pemohon->instansi_penerima;

                                                        $tgl_diterima = $v_pemohon->tgl_diterima;
                                                        $no_register = $v_pemohon->no_register;

                                                        $text_no_acc_opd = $v_pemohon->text_no_acc_opd;
                                                        $json_no_acc_opd = json_decode(str_replace("'","\"",$text_no_acc_opd));

                                                        $json_lokasi_magang = json_decode(str_replace("'","\"",$lokasi_magang));
                                                        if(in_array($admin_opd,$json_lokasi_magang)){
                                                            if($json_no_acc_opd[$admin_opd]->status != "0"){
                                                                // print_r($json_no_acc_opd[$admin_opd]);
                                                                $str_nama_ = $nama_user;
                                                                $status_delegasi = $v_pemohon->status_pendelegasian;
                                                                if($status_delegasi == "1"){
                                                                    $str_nama_ = $nama_user." (Delegasi: ".$nama_del.")";
                                                                }

                                                                $str_status_diterima = "Permohonan belum diresponse";
                                                                $str_status_pendaftaran = "<span class=\"label label-warning\">Permohonan belum diresponse</span>";
                                                                
                                                                $str_status_pendaftaran = "<span class=\"label label-warning\">Status Magang Belum Disetujui</span>";
                                                                if($status_pendaftaran == "1"){
                                                                    if($status_magang == "1"){
                                                                        $str_status_pendaftaran = "<span class=\"label label-info\">Permohonan Magang Disetujui</span>";
                                                                    }elseif($json_no_acc_opd[$admin_opd]->status == "2"){
                                                                        $str_status_pendaftaran = "<span class=\"label label-danger\">Permohonan Magang Ditolak</span>";
                                                                    }elseif($json_no_acc_opd[$admin_opd]->status == "3"){
                                                                        $str_status_pendaftaran = "<span class=\"label label-primary\">Permohonan Magang Tidak Aktif</span>";
                                                                    }
                                                                }

                                                                $str_status_diterima = "<span class=\"label label-warning\">Belum Diresponse</span>";
                                                                if($json_no_acc_opd[$admin_opd]->status == "1"){
                                                                    $str_status_diterima = "<span class=\"label label-info\">Permohonan Diterima</span>";
                                                                }elseif ($json_no_acc_opd[$admin_opd]->status == "2") {
                                                                    $str_status_diterima = "<span class=\"label label-danger\">Permohonan Ditolak</span>";
                                                                }
                                                                $tmp_tgl = explode("-", $tgl_start);
                                                                $tgl_start_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                                $tmp_tgl = explode("-", $tgl_selesai);
                                                                $tgl_selesai_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                                $tmp_tgl = explode("-", $tgl_diterima);
                                                                $tgl_diterima_fix = explode(" ", $tmp_tgl[2])[0]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                                $tmp_tgl = explode("-", $json_no_acc_opd[$admin_opd]->date_action);
                                                                $tgl_diterima_opd_fix = explode(" ", $tmp_tgl[2])[0]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                                $str_status_action = "
                                                                    <center>
                                                                        &nbsp;<a class=\"btn btn-info\" style=\"width: 50px;\" data-toggle=\"modal\" data-target=\"#modal_detail\" onclick=\"show_detail('".$id_pemohon."')\"><i class=\"fa fa fa-address-book\" style=\"color: white;\"></i></a>
                                                                    </center>";

                                                                $str_lokasi_dinas = "";
                                                                $daftar_dinas = json_decode(str_replace("'", "\"", $lokasi_magang));
                                                                // print_r($daftar_dinas);
                                                                foreach ($daftar_dinas as $r_dinas => $v_dinas) {
                                                                    $str_lokasi_dinas .= "- ".$array_of_dinas[$v_dinas]."<br>";
                                                                }

                                                                $str_status_diterima_fix = $str_status_diterima.", Pada tanggal: ".$tgl_diterima_opd_fix;
                                                                if($instansi_penerima != "0"){
                                                                    $str_status_diterima_fix = $str_status_diterima.",Oleh: ".$array_of_dinas[$instansi_penerima].", Pada tanggal: ".$tgl_diterima_opd_fix;
                                                                }else {
                                                                    $str_status_diterima_fix = $str_status_diterima.", Pada tanggal: ".$tgl_diterima_opd_fix."<br> Keterangan: ".$json_no_acc_opd[$admin_opd]->text;
                                                                }                                    
                                                                print_r("<tr>
                                                                            <td>".$no++."</td>
                                                                            <td>".$judul."</td>
                                                                            <td>".$str_nama_."</td>
                                                                            <td>".$str_status_pendaftaran."<br>No Register: <br> ".$no_register."</td>
                                                                            <td>".$keterangan_permohonan."</td>
                                                                            <td>".$tgl_start_fix."<br>s/d<br>".$tgl_selesai_fix."</td>
                                                                            <td>".$str_status_diterima_fix."</td>
                                                                            <td>".$str_status_action."</td>
                                                                        </tr>");
                                                            }
                                                        }
                                                    }
                                                    
                                                  }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                    <!-- <a href="" style="width: 50px;"></a> -->
                            </div>
                            <div class="card-body">
                                <div class="text-right">
                                    <label class="form-label">Keterangan Tombol Aksi ==> </label>
                                    <a class="btn btn-info" style="width: 40px;"><i class="fa fa-address-book" style="color: white;"></i></a>
                                    <label class="form-label text-info">Detail Permohonan</label>
                                </div>
                            </div>
                        </div>
                    </div>           
                </div>
            </div>

                                <!-- sample modal content -->
                                <div class="modal fade bs-example-modal-lg" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content" style="width: 1000px;">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Detail Permohonan</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div id="slimtest2">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->


            <script>
            $(document).ready(function(){

            });

            function show_detail(id_pemohon) {
                var data_main =  new FormData();
                data_main.append('id_pemohon', id_pemohon);
                $.ajax({
                    url: "<?php echo base_url()."admin_new/adminopd/detail_pemohon_report";?>", // point to server-side PHP script 
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        // console.log(res);
                        $("#slimtest2").html(res);
                    }
                });
            }  

           
            </script>

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

     
                
                