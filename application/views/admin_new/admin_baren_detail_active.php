                            <?php

                                $array_of_month = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
                                $tmp_tgl ="";

                                $id_user = "";
                                $nama_user = "";
                                $nik = "";
                                $tgl_lhr = "";
                                $alamat = "";
                                $email = "";
                                $tlp = "";
                                $alamat_dom = "";
                                $alamat_dom = "";
                                $instansi = "";
                                $pekerjaan = "";

                                $url_foto_profil =  "";

                                $status_pekerjaan = "";
                                $instansi_pekerjaan = "";

                                $tgl_lhr_fix = "";
                                $url_foto_profil_fix = "";

                                if(isset($user)){
                                  if($user){
                                    $nama_user = $user["nama"];
                                    $id_user = $user["id_user"];
                                    $email = $user["email"];
                                    $alamat_dom = $user["alamat_dom"];
                                    $alamat = $user["alamat"];
                                    $tgl_lhr = $user["tgl_lhr"];
                                    $nik = $user["nik"];
                                    $tlp = $user["tlp"];
                                    $instansi = $user["instansi"];
                                    $pekerjaan = $user["pekerjaan"];

                                    $tmp_tgl = explode("-", $tgl_lhr);
                                    $tgl_lhr_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $url_foto_profil =  $user["url_profil"];

                                    if($url_foto_profil != ""){
                                      $url_foto_profil_fix = base_url()."doc/foto_pemohon/".$url_foto_profil;  
                                    }else {
                                      $url_foto_profil_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }

                                    $status_pekerjaan = "Mahasiswa, ";
                                    if($pekerjaan == 0){
                                        $status_pekerjaan = "Bekerja Pada, ";
                                    }

                                    if(strpos($instansi, ";")){
                                        $data_instansi = explode(";", $instansi);
                                        $instansi_pekerjaan = $status_pekerjaan.implode(", ", $data_instansi);
                                    }else{
                                        $instansi_pekerjaan = $status_pekerjaan.$instansi;
                                    }

                                  }
                                }

                                $id_pemohon = "";
                                $id_permohonan = "";
                                $keterangan_permohonan = "";
                                $no_register = "";
                                $tgl_start = "";
                                $tgl_selesai = "";
                                $judul = "";
                                $instansi_penerima = "";
                                $id_bidang = "";
                                $dosen_pembimbing = "";
                                $anggota = "";
                                $status_pendelegasian = "";
                                                                
                                $alamat_dom_del = "";
                                $alamat_ktp_del = "";
                                $nama_del = "";
                                $nik_del = "";

                                $url_ktp_del = "";
                                $url_ktp_pemohon = "";                                

                                $tgl_start_fix = "";
                                $tgl_selesai_fix = "";

                                $status_pendaftaran = "";
                                $status_magang = "";
                                $status_diterima = "";
                                $tgl_daftar = "";
                                $tgl_acc = "";
                                $tgl_diterima = "";
                                $admin_terima = "";
                                $admin_acc = "";
                                $text_no_acc_bakes = "";  
                                $text_no_acc_opd = "";

                                $status_pendaftaran_fix = "";
                                $status_magang_fix = "";
                                $status_diterima_fix = "";

                                $tgl_daftar_fix = "";
                                $tgl_acc_fix = "";
                                $tgl_diterima_fix = "";

                                if(isset($pendaftaran)){
                                  if($pendaftaran){
                                    $id_user = $pendaftaran["id_user"];
                                    $id_pemohon = $pendaftaran["id_pemohon"];
                                    $id_permohonan = $pendaftaran["id_permohonan"];
                                    $keterangan_permohonan = $pendaftaran["keterangan_permohonan"];
                                    $no_register = $pendaftaran["no_register"];
                                    $tgl_start = $pendaftaran["tgl_start"];
                                    $tgl_selesai = $pendaftaran["tgl_selesai"];
                                    $judul = $pendaftaran["judul"];
                                    $instansi_penerima = $pendaftaran["instansi_penerima"];
                                    $id_bidang = $pendaftaran["id_bidang"];
                                    $dosen_pembimbing = $pendaftaran["dosen_pembimbing"];
                                    $anggota = $pendaftaran["anggota"];
                                    $status_pendelegasian = $pendaftaran["status_pendelegasian"];
                                    
                                    $alamat_dom_del = $pendaftaran["alamat_dom_del"];
                                    $alamat_ktp_del = $pendaftaran["alamat_ktp_del"];
                                    $nama_del = $pendaftaran["nama_del"];
                                    $nik_del = $pendaftaran["nik_del"];

                                    $url_ktp_del = $pendaftaran["url_ktp_del"];
                                    $url_ktp_pemohon = $pendaftaran["url_ktp_pemohon"];

                                    $status_pendaftaran = $pendaftaran["status_pendaftaran"];
                                    $status_magang = $pendaftaran["status_magang"];
                                    $status_diterima = $pendaftaran["status_diterima"];
                                    $status_active = $pendaftaran["status_active_mg"];

                                    $tgl_daftar = $pendaftaran["tgl_daftar"];
                                    $tgl_acc = $pendaftaran["tgl_acc"];
                                    $tgl_diterima = $pendaftaran["tgl_diterima"];
                                    $admin_terima = $pendaftaran["admin_terima"];
                                    $admin_acc = $pendaftaran["admin_acc"];
                                    $text_no_acc_bakes = $pendaftaran["text_no_acc_bakes"];                          
                                    $text_no_acc_opd = $pendaftaran["text_no_acc_opd"];


                                    $tmp_tgl = explode("-", $tgl_start);
                                    $tgl_start_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", $tgl_selesai);
                                    $tgl_selesai_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", $tgl_daftar);
                                    $tgl_daftar_fix = explode(" ", $tmp_tgl[2])[0]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", $tgl_acc);
                                    $tgl_acc_fix = explode(" ", $tmp_tgl[2])[0]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", $tgl_diterima);
                                    $tgl_diterima_fix = explode(" ", $tmp_tgl[2])[0]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $status_pendaftaran_fix = "<span class=\"label label-danger\">Belum Mendaftar</span>";
                                    if($status_pendaftaran == "1"){
                                        $status_pendaftaran_fix = "<span class=\"label label-info\">Mendaftar</span> pada tanggal: ".$tgl_daftar_fix;
                                    }

                                    $status_diterima_fix = "<span class=\"label label-warning\">Permohonan belum Diresponse</span>";
                                    if($status_diterima == "1"){
                                        $status_diterima_fix = "<span class=\"label label-info\">Permohonan diterima</span>, Oleh: ".$tmp_magang[$instansi_penerima].", pada tanggal: ".$tgl_diterima_fix;
                                    }elseif ($status_diterima == "2") {
                                        $status_diterima_fix = "<span class=\"label label-danger\">Permohonan ditolak</span> oleh semua Lembaga pada tanggal: ".$tgl_diterima_fix;
                                    }

                                    $status_magang_fix = "<span class=\"label label-warning\">Permohonan belum diresponse</span>";
                                    if($status_magang == "1"){
                                        $status_magang_fix = "<span class=\"label label-info\">Permohonan diterima</span> BARENLITBANG pada tanggal: ".$tgl_acc_fix;
                                    }elseif ($status_magang == "2") {
                                        $status_magang_fix = "<span class=\"label label-danger\">Permohonan ditolak</span> BARENLITBANG pada tanggal: ".$tgl_acc_fix;
                                    }elseif ($status_magang == "3") {
                                        $status_magang_fix = "<span class=\"label label-success\">Permohonan tidak aktif</span>";
                                    }

                                    $str_status_active = "<span class=\"label label-warning\">Status Magang Belum Aktif</span>";
                                    if($status_active == "1"){
                                        $str_status_active = "<span class=\"label label-info\">Status Magang Aktif</span>";
                                    }elseif ($status_active == "2"){
                                        $str_status_active = "<span class=\"label label-danger\">Status Magang Tidak Aktif</span>";
                                    }
                                    
                                    if($url_ktp_del != ""){
                                      if(file_exists('./doc/ktp_delegasi/'.$url_ktp_del)){
                                        $url_ktp_del_fix = base_url()."doc/ktp_delegasi/".$url_ktp_del;  
                                      }else{
                                        $url_ktp_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                      }
                                    }else{
                                      $url_ktp_del_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }
                                    

                                    if($url_ktp_pemohon != ""){
                                      if(file_exists('./doc/ktp/'.$url_ktp_pemohon)){
                                        $url_ktp_pemohon_fix = base_url()."doc/ktp/".$url_ktp_pemohon;
                                      }else{
                                        $url_ktp_pemohon_fix = base_url()."assets/core_img/icon_upload_152x277.png";
                                      }
                                        
                                    }else {
                                      $url_ktp_pemohon_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }
                                    
                                  }
                                }

                                if(isset($get_verifikation_data)){
                                    if($get_verifikation_data){
                                        
                                        $status_hide_profil = "";
                                        $status_hide_pendaftaran = "";
                                        $status_hide_delegasi = "";
                                        $status_hide_dokumen = "";

                                        if($get_verifikation_data["list_vert_all"]["vert_user"]=="0"){
                                            $status_hide_profil = "hidden";
                                        }

                                        if($get_verifikation_data["list_vert_all"]["vert_permohonan"]=="0"){
                                            $status_hide_pendaftaran = "hidden";
                                        }

                                        if($get_verifikation_data["list_vert_all"]["vert_delegasi"]=="0"){
                                            $status_hide_delegasi = "hidden";
                                        }

                                        if($status_pendelegasian == "0"){
                                            $status_hide_delegasi = "hidden";
                                        }

                                        if($get_verifikation_data["list_vert_all"]["vert_doc"]=="0"){
                                            $status_hide_dokumen = "hidden";
                                        }
                                    }
                                }
                              ?>
            <div id="slimtest2">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                                <div id="accordion1" role="tablist" aria-multiselectable="true">
                                        <div class="card-header" role="tab" id="headingThree4">
                                            <h5 class="mb-0">
                                            <a class="collapsed link" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree4" aria-expanded="false" aria-controls="collapseThree4">
                                                <i class="fa fa-briefcase"></i>&nbsp;&nbsp;
                                                Status Permohonan
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseThree4" class="collapse show" role="tabpanel" aria-labelledby="headingThree4">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-5">
                                                        <dl>
                                                            <dt>Status Aktif Magang</dt>
                                                            <dd><?= $str_status_active;?></dd>

                                                            <dt>Status Pendaftaran</dt>
                                                            <dd><?= $status_pendaftaran_fix;?></dd>

                                                            <dt>No Register</dt>
                                                            <dd><?= $no_register;?></dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <dl>
                                                            <dt>Status Permohonan</dt>
                                                            <dd><?= $status_magang_fix;?></dd>

                                                            <dt>Keterangan Penolakan</dt>
                                                            <dd><?= $text_no_acc_bakes;?></dd>

                                                            <!-- <dt>Status Penerimaan</dt>
                                                            <dd><?= $status_diterima_fix;?></dd> -->
                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <hr>
                                                    </div>
                                                    
                                                    <div class="col-lg-12">
                                                        <dl>
                                                            <dt>Status Penerimaan Dinas</dt>
                                                            <dd>
                                                                <table class="table" border="0">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>No</th>
                                                                            <th>Instansi</th>
                                                                            <th>Keterangan</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="output_bidang">
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </dd>
                                                        </dl>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    <br>
                                        <div class="card-header" role="tab" id="headingOne1">
                                            <h5 class="mb-0">
                                            <a class="link" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-address-book"></i>&nbsp;&nbsp;
                                                Data Pemohon
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1">
                                            <div class="card-body">
                                                <div class="row" <?php print_r($status_hide_profil);?>>
                                                    <div class="col-lg-6">
                                                        <dl>
                                                            <dt>Foto Profil</dt>
                                                            <dd>
                                                                <img width="400px" height="253px" id="img_ktp" src="<?= $url_ktp_pemohon_fix?>"/><br />
                                                            </dd>
                                                        </dl>
                                                        
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <dl>
                                                            <dt>Nama Pemohon</dt>
                                                            <dd><?= $nama_user;?></dd>

                                                            <dt>Email</dt>
                                                            <dd><?= $email;?></dd>

                                                            <dt>Nomor Telephone</dt>
                                                            <dd><?= $tlp;?></dd>

                                                            <dt>Tanggal Lahir</dt>
                                                            <dd><?= $tgl_lhr_fix;?></dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <dl>

                                                            <dt>Nomor Identitas</dt>
                                                            <dd><?= $nik;?></dd>

                                                            <dt>Alamat Sesuai KTP</dt>
                                                            <dd><?= $alamat;?></dd>

                                                            <dt>Alamat di Malang</dt>
                                                            <dd><?= $alamat_dom;?></dd>

                                                            <dt>Status Pekerjaan</dt>
                                                            <dd><?= $instansi_pekerjaan;?></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <br>

                                        <div class="card-header" role="tab" id="headingTwo2">
                                            <h5 class="mb-0">
                                            <a class="collapsed link" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                                                <i class="fa fa-id-card"></i>&nbsp;&nbsp;
                                                Data Pendaftaran Magang
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseTwo2" class="collapse show" role="tabpanel" aria-labelledby="headingTwo2">
                                            <div class="card-body">
                                                <div class="row" <?php print_r($status_hide_pendaftaran);?>>
                                                    
                                                    <div class="col-lg-5">
                                                        <dl>
                                                            <dt>Judul</dt>
                                                            <dd><?= $judul;?></dd>

                                                            <dt>Tujuan Permohonan</dt>
                                                            <dd><?= $keterangan_permohonan;?></dd>

                                                            <dt>Tanggal Mulai</dt>
                                                            <dd><?= $tgl_start_fix;?></dd>

                                                            <dt>Tanggal Berakhir</dt>
                                                            <dd><?= $tgl_selesai_fix;?></dd>

                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <dl>
                                                            
                                                            <dt>Dosen Penanggung Jawab</dt>
                                                            <dd>
                                                                <table class="table" border="0">
                                                                    <thead>
                                                                        <tr>
                                                                            <td width="10%"></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="output_dosen">
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </dd>

                                                            <dt>Daftar Anggota Kelompok</dt>
                                                            <dd>
                                                                <table class="table" border="0">
                                                                    <thead>
                                                                        <tr>
                                                                            <td width="10%"></td>
                                                                            <td width="45%"></td>
                                                                            <td width="45%"></td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="output_anggota">
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    <br>
                                        <div class="card-header" role="tab" id="headingThree3">
                                            <h5 class="mb-0">
                                            <a class="collapsed link" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                                                <i class="fa fa-group"></i>&nbsp;&nbsp;
                                                Data Delegasi
                                            </a>
                                          </h5>
                                        </div>
                                        <div id="collapseThree3" class="collapse show" role="tabpanel" aria-labelledby="headingThree3">
                                            <div class="card-body">
                                                <div class="row" <?php print_r($status_hide_delegasi);?>>
                                                    <div class="col-lg-6">
                                                        <dl>
                                                            <dt>Scan/Foto KTP</dt>
                                                                <dd>
                                                                    <img width="400px" height="253px" src="<?=$url_ktp_del_fix?>" id="img_ktp_del" name="img_ktp_del">
                                                                </dd>                                            
                                                        </dl>
                                                    </div>
                                                    <div class="col-lg-6">                                          
                                                        <dl>
                                                            <dt>Nama Penerima Kuasa</dt>
                                                            <dd><?= $nama_del;?></dd>

                                                            <dt>Nomor Identitas Penerima Kuasa</dt>
                                                            <dd><?= $nik_del;?></dd>

                                                            <dt>Alamat Penerima Kuasa (Sesuai KTP)</dt>
                                                            <dd><?= $alamat_ktp_del;?></dd>

                                                            <dt>Alamat Penerima Kuasa (Di Malang)</dt>
                                                            <dd><?= $alamat_dom_del;?></dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <br>                                  
                                </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <td></td>

            <script>

                $(document).ready(function(){
                    show_tmp();
                    show_dosen();
                    show_anggota();
                });
            
            //===============================================================================
            //-------------------------------------------Add_Json_Input----------------------
            //===============================================================================

                  //------------------------------------------Dinas Pilihan------------------------------------------//
                                    var master_tmp_magang = JSON.parse('<?php echo json_encode($tmp_magang);?>');
                                    var list_add_tmp = <?php print_r($id_bidang);?>;
                                    var json_text_no_acc_opd = <?php print_r($text_no_acc_opd);?>;
                                    var array_of_month = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
                                    console.log(json_text_no_acc_opd);

                                    function show_tmp(){
                                        var content_tmp = "";
                                        for(var i = 0; i<list_add_tmp.length; i++){
                                            var no = i+1;

                                            var status_acc_opd = json_text_no_acc_opd[list_add_tmp[i]]["status"];
                                            var str_status_acc_opd = "<span class=\"label label-warning\">Belum diresponse</span>";
                                            // var tgl_acc_opd = "";
                                            var text_acc_opd = json_text_no_acc_opd[list_add_tmp[i]]["text"];

                                            if(status_acc_opd == "1"){
                                                str_status_acc_opd = "<span class=\"label label-info\">Permohonan diterima</span> pada tanggal: "+json_text_no_acc_opd[list_add_tmp[i]].date_action;
                                            }else if(status_acc_opd == "2"){
                                                str_status_acc_opd = "<span class=\"label label-danger\">Permohonan ditolak</span> pada tanggal: "+json_text_no_acc_opd[list_add_tmp[i]].date_action;
                                            }

                                            // console.log(master_tmp_magang[list_add_tmp[i]]);
                                            // console.log(str_status_acc_opd);
                                            // console.log(status_acc_opd);

                                            content_tmp += "<tr><td width=\"5%\">"+no+
                                            ". </td><td width=\"60%\">"+master_tmp_magang[list_add_tmp[i]]+
                                            "</td><td width=\"35%\">"+text_acc_opd+
                                            "</td></tr>";
                                        }
                                        $("#output_bidang").html(content_tmp);
                                    }
                  
                  //------------------------------------------List Anggota-------------------------------------------//
                                    
                                    var list_anggota = <?php print_r($anggota);?>;
                                    
                                    function show_anggota(){
                                        var content_anggota = "";
                                        for(var i = 0; i<list_anggota.length; i++){
                                            var no = i+1;
                                            content_anggota += "<tr><td>"+no+
                                            ". </td><td>"+list_anggota[i][0]+
                                            "</td><td>"+list_anggota[i][1]+
                                            "</td></tr>";
                                        }
                                        $("#output_anggota").html(content_anggota);
                                    }
                  //-------------------------------------------List Dosen--------------------------------------------//
                                    var list_dosen = <?php print_r($dosen_pembimbing);?>;
                                    
                                    function show_dosen(){
                                        var content_dosen = "";
                                        for(var i = 0; i<list_dosen.length; i++){
                                            var no = i+1;
                                            content_dosen += "<tr><td>"+no+
                                            "</td><td>"+list_dosen[i]+
                                            "</td></tr>";
                                        }
                                        $("#output_dosen").html(content_dosen);
                                    }
            //===============================================================================
            //-------------------------------------------Add_Json_Input----------------------
            //===============================================================================
            </script>
            