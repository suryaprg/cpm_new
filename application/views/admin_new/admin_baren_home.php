            <?php
                $pemohon_req_ = 0;
                if(isset($pemohon_req)){
                    $pemohon_req_ = (double) $pemohon_req;
                }

                $pemohon_acc_ = 0;
                if(isset($pemohon_acc)){
                    $pemohon_acc_ = (double) $pemohon_acc;
                }

                $pemohon_rem_ = 0;
                if(isset($pemohon_rem)){
                    $pemohon_rem_ = (double) $pemohon_rem;
                }

                $pemohon_all_ = 0;
                if(isset($pemohon_all)){
                    $pemohon_all_ = (double) $pemohon_all;
                }

                $proc_req = 0;
                if($pemohon_req_ != 0 && $pemohon_all_ != 0){
                    $proc_req = $pemohon_req_/$pemohon_all_*100;
                }

                $proc_acc = 0;
                if($pemohon_acc_ != 0 && $pemohon_all_ != 0){
                    $proc_acc = $pemohon_acc_/$pemohon_all_*100;
                }

                $proc_rem = 0;
                if($pemohon_rem_ != 0 && $pemohon_all_ != 0){
                    $proc_rem = $pemohon_rem_/$pemohon_all_*100;
                }

                $proc_all = 0;
                if($pemohon_all_ != 0 && $pemohon_all_ != 0){
                    $proc_all = $pemohon_all_/$pemohon_all_*100;
                }
            ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-file-import text-warning"></i></h2>
                                    <h3 class=""><?= $pemohon_req_; ?></h3>
                                    <h6 class="card-subtitle">Total Penelitian yang Belum Diresponse</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: <?= $proc_req; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-file-multiple text-info"></i></h2>
                                    <h3 class=""><?= $pemohon_acc_; ?></h3>
                                    <h6 class="card-subtitle">Total Penelitian Disetujui</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: <?= $proc_acc; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-file-excel text-danger"></i></h2>
                                    <h3 class=""><?= $pemohon_rem_; ?></h3>
                                    <h6 class="card-subtitle">Total Penelitian Ditolak</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: <?= $proc_rem; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-folder-multiple text-primary"></i></h2>
                                    <h3 class=""><?= $pemohon_all_; ?></h3>
                                    <h6 class="card-subtitle">Total Pendaftar Penelitian</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-primary" role="progressbar" style="width: <?= $proc_all; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <div class="row">
                    
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Grafik Penelitian</h4><hr>
                                        <div id="bar-chart" style="width:100%; height:400px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row --> 

                <!-- <div class="row">
                    <div class="col-lg-4">
                        <div class="col-md-12">
                            <div class="card card-outline-info">
                                <div class="card-header">
                                    <h4 class="m-b-0 text-white">Informasi Pendaftaran Penelitian</h4></div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2"><button type="button" class="btn btn-info"><i class="mdi mdi-account"></i></button></div>
                                        <div class="col-md-8" style="te"><h4>12 Penelitian Aktif</h4></div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>                                           
                    </div>
                    <div class="col-lg-8">
                        <div class="col-md-12">
                            <div class="card card-outline-info">
                                <div class="card-header">
                                    <h4 class="m-b-0 text-white">Informasi Pendaftaran Penelitian</h4></div>
                                <div class="card-body">
                                    <div class="row">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div> -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- This page plugins -->
            <!-- ============================================================== -->
            <!-- Chart JS -->
            <script src="<?php print_r(base_url());?>admin_template/assets/plugins/echarts/echarts-all.js"></script>
            <script type="text/javascript">
                // ============================================================== 
                // Bar chart option
                // ============================================================== 
                var myChart = echarts.init(document.getElementById('bar-chart'));

                // specify chart configuration item and data
                option = {
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data:['Penelitian']
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            
                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    color: ["#55ce63", "#009efb"],
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            data : ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        // {
                        //     name:'Magang',
                        //     type:'bar',
                        //     data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
                            
                        // },
                        {
                            name:'Penelitian',
                            type:'bar',
                            data:[<?php
                                    if(is_array($pemohon_list)){print_r(implode(",", $pemohon_list));}
                                ?>],
                            
                        }
                    ]
                };


                // use configuration item and data specified to show chart
                myChart.setOption(option, true), $(function() {
                            function resize() {
                                setTimeout(function() {
                                    myChart.resize()
                                }, 100)
                            }
                            $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
                        });
                     
            </script>
            <!-- <script src="<?php print_r(base_url());?>admin_template/assets/plugins/echarts/echarts-init.js"></script> -->

                
                