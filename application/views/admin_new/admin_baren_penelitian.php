                            <?php

                                $array_of_month = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

                                $array_of_dinas = array();
                                if(isset($tmp_magang)){
                                    if($tmp_magang){
                                        $array_of_dinas = $tmp_magang;
                                    }
                                }
                                                                
                            ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-body">
                                <h4 class="card-title">Daftar Riwayat Pendaftaran Magang</h4><br>
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr><center></center>
                                                <th width="3%">No</th>
                                                <th width="22%">Judul/Tema Magang</th>
                                                <th width="15%">Nama User</th>
                                                <th width="15%">Jenis Permohonan</th>
                                                <th width="15%">Periode Magang</th>
                                                <th width="15%">Lokasi Magang</th>
                                                <th width="10%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody><a href="" onclick="post()"></a>

                                            <?php
                                                if(isset($pemohon)){
                                                  if($pemohon){


                                                    $no = 1;
                                                    foreach ($pemohon as $r_pemohon => $v_pemohon) {
                                                        $id_user = $v_pemohon->id_user;
                                                        $nama_user = $v_pemohon->nama;
                                                        $id_pemohon = $v_pemohon->id_pemohon;
                                                        $id_permohonan = $v_pemohon->id_permohonan;
                                                        $keterangan_permohonan = $v_pemohon->keterangan_permohonan;
                                                        $tgl_start = $v_pemohon->tgl_start;
                                                        $tgl_selesai = $v_pemohon->tgl_selesai;
                                                        $judul = $v_pemohon->judul;
                                                        $lokasi_magang = $v_pemohon->id_bidang;
                                                        $status_pendaftaran = $v_pemohon->status_pendaftaran;
                                                        $status_magang = $v_pemohon->status_magang;
                                                        $status_diterima = $v_pemohon->status_diterima;
                                                        
                                                        $tmp_tgl = explode("-", $tgl_start);
                                                        $tgl_start_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                        $tmp_tgl = explode("-", $tgl_selesai);
                                                        $tgl_selesai_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                        $str_status_action = "
                                                            <center>
                                                                &nbsp;<a class=\"btn btn-info\" style=\"width: 50px;\" data-toggle=\"modal\" data-target=\"#modal_detail\" onclick=\"show_detail('".$id_pemohon."')\"><i class=\"fa fa-address-book\" style=\"color: white;\"></i></a>
                                                            </center>";

                                                        $str_lokasi_dinas = "";
                                                        $daftar_dinas = json_decode(str_replace("'", "\"", $lokasi_magang));
                                                        // print_r($daftar_dinas);
                                                        foreach ($daftar_dinas as $r_dinas => $v_dinas) {
                                                            $str_lokasi_dinas .= "- ".$array_of_dinas[$v_dinas]."<br>";
                                                        }                                                
                                                        print_r("<tr>
                                                                    <td>".$no++."</td>
                                                                    <td>".$judul."</td>
                                                                    <td>".$nama_user."</td>
                                                                    <td>".$keterangan_permohonan."</td>
                                                                    <td>".$tgl_start_fix." s/d ".$tgl_selesai_fix."</td>
                                                                    <td>".$str_lokasi_dinas."</td>
                                                                    <td>".$str_status_action."</td>
                                                                </tr>");

                                                    }
                                                    
                                                  }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                    <!-- <a href="" style="width: 50px;"></a> -->
                            </div>

                            <div class="card-body">
                                <div class="text-right">
                                    <label class="form-label">Keterangan Tombol Aksi ==> </label>
                                    <a class="btn btn-info" style="width: 40px;"><i class="fa fa-address-book" style="color: white;"></i></a>
                                    <label class="form-label text-info">Detail Permohonan</label>
                                </div>
                            </div>
                        </div>
                    </div>           
                </div>
            </div>

                                <!-- sample modal content -->
                                <div class="modal fade bs-example-modal-lg" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content" style="width: 1000px;">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Detail Permohonan</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div id="slimtest2">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->


            <script>
            function show_detail(id_pemohon) {
                
                    var data_main =  new FormData();
                    data_main.append('id_pemohon', id_pemohon);
                    $.ajax({
                        url: "<?php echo base_url()."admin_new/Adminbaren/detail_pemohon";?>", // point to server-side PHP script 
                        dataType: 'html',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data_main,                         
                        type: 'post',
                        success: function(res){
                            // console.log(res);
                            // $("#modal_detail").modal('show');
                            $("#slimtest2").html(res);
                        }
                    });
            }


            </script>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

     
                
                