                            <?php
                                $array_of_month = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

                                $array_of_dinas = array();
                                if(isset($tmp_magang)){
                                    if($tmp_magang){
                                        $array_of_dinas = $tmp_magang;
                                    }
                                }

                                $date_st = "";
                                $date_fn = "";
                                $str_kategori = 0;
                                $str_opd = 0;

                                if(isset($post)){
                                    if(!empty($post)){
                                        $date_st = $post["tgl_st"];
                                        $date_fn = $post["tgl_fn"];
                                        $str_kategori = $post["kategori"];
                                        if(isset($post["opd"])){ 
                                            $str_opd = $post["opd"];
                                        }
                                    }
                                }

                            ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <form method="post" action="<?php print_r(base_url().'bakesbangpol/magang_filter');?>">
                            <div class="card-body">
                                <h4 class="card-title">Filter Data Permohonan</h4><hr><br>
                                <?php
                                    // print_r("<pre>");
                                    // print_r($post);
                                    // print_r($filter);
                                    // print_r($pemohon);
                                ?>
                                <div class="form-material">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Kategori</label>
                                                <select class="form-control" id="kategori" name="kategori" placeholder="placeholder">
                                                    <option value="0">Filter Berdasarkan Tanggal Terdaftar</option>
                                                    <option value="1">Filter Berdasarkan Penerimaan OPD</option>
                                                    <option value="2">Filter Berdasarkan Status Aktif</option>
                                                </select>
                                                <!-- <input type="text" > -->
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Tanggal Mulai</label>
                                                <input type="date" class="form-control" id="tgl_st" name="tgl_st" value="<?php print_r($date_st);?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Tanggal Selesai</label>
                                                <input type="date" class="form-control" id="tgl_fn" name="tgl_fn" value="<?php print_r($date_fn);?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group" id="group_opd">
                                                <label>Nama OPD</label>
                                                <select class="form-control" id="opd" name="opd" placeholder="placeholder">
                                                    <?php
                                                        if(isset($str_option)){
                                                            print_r($str_option);
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <br>
                                            <input type="submit" class="btn btn-rounded btn-info float-lg-right" name="btn_filter" id="btn_filter" value="Filter" style="width: 150px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-body">
                                <h4 class="card-title">Data Permohonan</h4><hr><br>
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="3%">No</th>
                                                <th width="20%">Judul/Tema Magang</th>
                                                <th width="12%">Nama Pemohon & Delegasi</th>
                                                <th width="15%">Status Registrasi</th>
                                                <th width="10%">Jenis Permohonan</th>
                                                <th width="11%">Periode Magang</th>
                                                <th width="19%">Instansi Penerima</th>
                                                <th width="10%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($pemohon)){
                                                  if($pemohon){

                                                    $no = 1;
                                                    foreach ($pemohon as $r_pemohon => $v_pemohon) {
                                                        $id_user = $v_pemohon->id_user;
                                                        $nama_user = $v_pemohon->nama;
                                                        $nama_del = $v_pemohon->nama_del;
                                                        $instansi_penerima = $v_pemohon->instansi_penerima;
                                                        $id_pemohon = $v_pemohon->id_pemohon;
                                                        $id_permohonan = $v_pemohon->id_permohonan;
                                                        $keterangan_permohonan = $v_pemohon->keterangan_permohonan;
                                                        $tgl_start = $v_pemohon->tgl_start;
                                                        $tgl_selesai = $v_pemohon->tgl_selesai;
                                                        $judul = $v_pemohon->judul;
                                                        $lokasi_magang = $v_pemohon->id_bidang;
                                                        $status_pendaftaran = $v_pemohon->status_pendaftaran;
                                                        $status_magang = $v_pemohon->status_magang;
                                                        $status_diterima = $v_pemohon->status_diterima;
                                                        $status_active = $v_pemohon->status_active_mg;

                                                        $tgl_diterima = $v_pemohon->tgl_diterima;
                                                        // print_r($status_active);
                                                        $id_bidang = $v_pemohon->id_bidang;

                                                        $no_register = $v_pemohon->no_register;

                                                        $text_no_acc_opd = $v_pemohon->text_no_acc_opd;
                                                        $json_no_acc_opd = json_decode(str_replace("'","\"",$text_no_acc_opd));

                                                        $str_nama_ = $nama_user;
                                                        $status_delegasi = $v_pemohon->status_pendelegasian;
                                                        if($status_delegasi == "1"){
                                                            $str_nama_ = $nama_user." (Delegasi: ".$nama_del.")";
                                                        }

                                                        $str_status_pendaftaran = "<span class=\"label label-warning\">Permohonan belum diresponse</span>";
                                                        
                                                        if($status_pendaftaran == "1"){
                                                            if($status_magang == "1"){
                                                                $str_status_pendaftaran = "<span class=\"label label-info\">Permohonan Magang Disetujui</span>";
                                                                
                                                            }elseif($status_magang == "2"){
                                                                $str_status_pendaftaran = "<span class=\"label label-danger\">Permohonan Magang Ditolak</span>";
                                                                
                                                            }elseif($status_magang == "3"){
                                                                $str_status_pendaftaran = "<span class=\"label label-primary\">Permohonan Magang Tidak Aktif</span>";
                                                            }
                                                        }

                                                        $str_status_active = "<span class=\"label label-warning\">Status Magang Belum Aktif</span>";
                                                        if($status_active == "1"){
                                                            $str_status_active = "<span class=\"label label-info\">Status Magang Aktif</span>";
                                                        }elseif ($status_active == "2"){
                                                            $str_status_active = "<span class=\"label label-danger\">Status Magang Tidak Aktif</span>";
                                                        }

                                                        $str_status_action = "
                                                                <center>
                                                                    <a class=\"btn btn-primary\" style=\"width: 50px;\" data-toggle=\"modal\" data-target=\"#modal_detail\" onclick=\"show_detail('".$id_pemohon."')\"><i class=\"fa fa-indent\" style=\"color: white;\"></i></a>
                                                                </center>";
                                                        
                                                        $tmp_tgl = explode("-", $tgl_start);
                                                        $tgl_start_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                        $tmp_tgl = explode("-", $tgl_selesai);
                                                        $tgl_selesai_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                        $tmp_tgl = explode("-", $tgl_diterima);
                                                        $tgl_diterima_fix = explode(" ", $tmp_tgl[2])[0]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                                        $str_status_diterima = "<span class=\"label label-warning\">Permohonan belum diresponse</span>";
                                                        $str_status_diterima_fix = $str_status_diterima;

                                                        if($status_diterima == "1"){
                                                            $str_status_diterima = "<span class=\"label label-info\">Permohonan Magang Disetujui</span>";
                                                            $str_status_diterima_fix = $str_status_diterima. ", Oleh: ".$array_of_dinas[$instansi_penerima].", Pada: ". $tgl_diterima_fix;
                                                                
                                                        }elseif($status_diterima == "2"){
                                                            $str_status_diterima = "<span class=\"label label-danger\">Permohonan Magang Ditolak</span>";
                                                            $str_status_diterima_fix = $str_status_diterima.", Pada: ". $tgl_diterima_fix;
                                                                
                                                        }

                                                        print_r("<tr>
                                                                    <td>".$no++."</td>
                                                                    <td>".$judul."</td>
                                                                    <td>".$str_nama_."</td>
                                                                    <td>".$str_status_active."<br>No Register: <br> ".$no_register."</td>
                                                                    <td>".$keterangan_permohonan."</td>
                                                                    <td>".$tgl_start_fix."<br> s/d <br>".$tgl_selesai_fix."</td>
                                                                    <td>".$str_status_diterima_fix."</td>
                                                                    <td>".$str_status_action."</td>
                                                                </tr>");

                                                    }
                                                    
                                                  }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                    <!-- <a href="" style="width: 50px;"></a> -->
                            </div>
                            <div class="card-body">
                                <div class="text-right">
                                    <label class="form-label">Keterangan Tombol Aksi ==> </label>
                                    <a class="btn btn-primary" style="width: 40px;"><i class="fa fa-indent" style="color: white;"></i></a>
                                    <label class="form-label text-primary">Detail Permohonan</label>
                                </div>
                            </div>
                        </div>
                    </div>           
                </div>
            </div>

                                <!-- sample modal content -->
                                <div class="modal fade bs-example-modal-lg" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content" style="width: 1000px;">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Detail Permohonan</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div id="slimtest2">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->


            <script>
            $(document).ready(function(){
                $("#kategori").val('<?php print_r($str_kategori);?>');
                $("#opd").val('<?php print_r($str_opd);?>');
                change_opd();
                
            });

            function show_detail(id_pemohon) {
                var data_main =  new FormData();
                data_main.append('id_pemohon', id_pemohon);
                $.ajax({
                    url: "<?php echo base_url()."admin_new/Adminbakes/detail_pemohon_active";?>", // point to server-side PHP script 
                    dataType: 'html',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,                         
                    type: 'post',
                    success: function(res){
                        // console.log(res);
                        $("#slimtest2").html(res);
                    }
                });
            }  

            $("#kategori").change(function(){
                change_opd();

                $("#opd").val("3");
            });

            function change_opd(){
                if($("#kategori").val() != 1){
                    $("#group_opd").hide(100);
                }else{
                    $("#group_opd").show(500);
                }
            }
            </script>

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

     
                
                