            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="row">
                    
                    <div class="col-12 m-t-30">
                        <!-- Card -->
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Tabel Data Permohonan</h4>

                            </div>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-body">
                                        <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_permohonan"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data Permohonan</button>

                                        <div class="table-responsive m-t-40">
                                            <table id="myTable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th width="10%">No. </th>
                                                        <th width="*">Keterangan Permohonan</th>
                                                        <th width="30%">Kategori Permohonan</th>
                                                        <th width="15%">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if(!empty($permohonan)){
                                                            foreach ($permohonan as $r_permohonan => $v_permohonan) {
                                                                $str_jenis = "Penelitian";
                                                                if($v_permohonan->jenis_kegiatan){
                                                                    $str_jenis = "Magang";
                                                                }
                                                                echo "<tr>
                                                                        <td>".($r_permohonan+1)."</td>
                                                                        <td>".$v_permohonan->keterangan_permohonan."</td>
                                                                        <td>".$str_jenis."</td>
                                                                        <td>
                                                                        <center>
                                                                            <button type=\"button\" class=\"btn btn-info\" id=\"up_permohonan\" onclick=\"up_permohonan_fc('".$v_permohonan->id_permohonan."');\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                            <button type=\"button\" class=\"btn btn-danger\" id=\"del_permohonan\" onclick=\"delete_permohonan('".$v_permohonan->id_permohonan."');\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                        </center>
                                                                        </td>
                                                                    </tr>";
                                                            }
                                                        }
                                                    ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>                     
                            </div>
                            <div class="card-body">
                                <div class="text-right">
                                    <label class="form-label">Keterangan Tombol Aksi ==> </label>

                                    <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                                    <label class="form-label text-info">Update Data</label>,&nbsp;
                                    <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                                    <label class="form-label text-danger">Delete Data</label>
                                </div>
                            </div>
                        </div>
                        <!-- Card -->
                    </div>
                </div>
                <!-- End Row -->                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<div class="modal fade" id="insert_permohonan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Permohonan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Keterangan Permohonan :</label>
                        <input type="text" class="form-control" id="keterangan_permohonan" name="keterangan_permohonan" required="">
                        <p id="msg_keterangan_permohonan" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Jenis Kegiatan :</label>
                        <select class="form-control" id="jenis_kegiatan" name="jenis_kegiatan">
                            <option value="0">Kegiatan Penelitian</option>
                            <option value="1">Kegiatan Magang</option>
                        </select>
                        <p id="msg_jenis_kegiatan" style="color: red;"></p>
                    </div>

                </div>
                <div class="modal-footer">
                    <button id="add_permohonan" type="submit" class="btn btn-primary">Simpan</button>
                </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<div class="modal fade" id="update_permohonan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah permohonan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Nama permohonan :</label>
                        <input type="text" class="form-control" id="keterangan_permohonan_up" name="keterangan_permohonan" required="">
                        <p id="_msg_keterangan_permohonan" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Jenis Kegiatan :</label>
                        <select class="form-control" id="jenis_kegiatan_up" name="jenis_kegiatan">
                            <option value="0">Kegiatan Penelitian</option>
                            <option value="1">Kegiatan Magang</option>
                        </select>
                        <p id="_msg_jenis_kegiatan" style="color: red;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="up_permohonan_btn" type="submit" class="btn btn-primary">Simpan</button>
                </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<script type="text/javascript">

    var id_permohonan_gl;

//==========================================================================================//
//----------------------------------------------------Insert_permohonan---------------------//
//==========================================================================================//
    $("#add_permohonan").click(function(){
        var data_main = new FormData();
        data_main.append('keterangan_permohonan', $("#keterangan_permohonan").val());
        data_main.append('jenis_kegiatan', $("#jenis_kegiatan").val());

        $.ajax({
            url: "<?php echo base_url()."admin_super/superadmin/insert_permohonan/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                // console.log(res);
                response_add_permohonan(res);
            }
        });
    });

    function response_add_permohonan(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
        console.log(data_json);
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data permohonan berhasil disimpan ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."super/permohonan";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_keterangan_permohonan").html(detail_msg.keterangan_permohonan);
                        $("#msg_jenis_kegiatan").html(detail_msg.jenis_kegiatan);
                        
                        swal("Proses Gagal.!!", "Data permohonan gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

                
            }
    }
//==========================================================================================//
//----------------------------------------------------Insert_permohonan---------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Get_permohonan------------------------//
//==========================================================================================//
    function up_permohonan_fc(id_permohonan) {
        clear_from_update();

        var data_main = new FormData();
        data_main.append('id_permohonan', id_permohonan);

        $.ajax({
            url: "<?php echo base_url()."admin_super/superadmin/get_permohonan_update/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                // console.log(res);
                set_val_update(res, id_permohonan);
                $("#update_permohonan").modal('show');
            }
        });
    }

    function set_val_update(res, id_permohonan) {
        var res_pemohon = JSON.parse(res);
        id_permohonan_gl = id_permohonan;

        if (res_pemohon.status == true) {
            $("#keterangan_permohonan_up").val(res_pemohon.val_response.keterangan_permohonan);
            $("#jenis_kegiatan_up").val(res_pemohon.val_response.jenis_kegiatan);
        } else {
            clear_from_update();
        }
    }

    function clear_from_update() {
        $("#keterangan_permohonan_up").val("");
        $("#jenis_kegiatan_up").val("");
    }
//==========================================================================================//
//----------------------------------------------------Get_permohonan------------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Update_permohonan---------------------//
//==========================================================================================//
    $("#up_permohonan_btn").click(function(){
        var data_main = new FormData();
        data_main.append('keterangan_permohonan', $("#keterangan_permohonan_up").val());
        data_main.append('jenis_kegiatan', $("#jenis_kegiatan_up").val());

        data_main.append('id_permohonan', id_permohonan_gl);

        $.ajax({
            url: "<?php echo base_url()."admin_super/superadmin/update_permohonan/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_update_permohonan(res);
            }
        });
    });

    function response_update_permohonan(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data permohonan berhasil diubah ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."super/permohonan";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#_msg_keterangan_permohonan").html(detail_msg.keterangan_permohonan);
                        $("#_msg_jenis_kegiatan").html(detail_msg.jenis_kegiatan);
                        
                        swal("Proses Gagal.!!", "Data permohonan gagal diubah, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

                
            }
    }
//==========================================================================================//
//----------------------------------------------------Update_permohonan---------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Delete_permohonan---------------------//
//==========================================================================================//
    function delete_permohonan(id_permohonan){
        !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: false 
                    }, function(){
                        var data_main = new FormData();
                        data_main.append('id_permohonan', id_permohonan);

                        $.ajax({
                            url: "<?php echo base_url()."admin_super/superadmin/delete_permohonan/";?>", // point to server-side PHP script 
                            dataType: 'html', // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,
                            type: 'post',
                            success: function(res) {
                                console.log(res);
                                response_delete_permohonan(res);
                            }
                        });
                    });                              
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),
            
            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
    }

    function response_delete_permohonan(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                swal( {
                    title: "Proses Berhasil.!!",
                    text: "Data permohonan berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href="<?php echo base_url()."super/permohonan";?>";
                });                   
            }else{
                
                swal("Proses Gagal.!!", "Data permohonan gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");                                 
            }
    }
//==========================================================================================//
//----------------------------------------------------Delete_permohonan---------------------//
//==========================================================================================//
    

    
</script>
