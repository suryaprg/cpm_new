            <?php
            //penelitian
                $pemohon_req_p_ = 0;
                if(isset($pemohon_req_p)){
                    $pemohon_req_p_ = (double) $pemohon_req_p;
                }

                $pemohon_acc_p_ = 0;
                if(isset($pemohon_acc_p)){
                    $pemohon_acc_p_ = (double) $pemohon_acc_p;
                }

                $pemohon_rem_p_ = 0;
                if(isset($pemohon_rem_p)){
                    $pemohon_rem_p_ = (double) $pemohon_rem_p;
                }

                $pemohon_all_p_ = 0;
                if(isset($pemohon_all_p)){
                    $pemohon_all_p_ = (double) $pemohon_all_p;
                }

                $proc_req_p = 0;
                if($pemohon_req_p_ != 0 && $pemohon_all_p_ != 0){
                    $proc_req_p = $pemohon_req_p_/$pemohon_all_p_*100;
                }

                $proc_acc_p = 0;
                if($pemohon_acc_p_ != 0 && $pemohon_all_p_ != 0){
                    $proc_acc_p = $pemohon_acc_p_/$pemohon_all_p_*100;
                }

                $proc_rem_p = 0;
                if($pemohon_rem_p_ != 0 && $pemohon_all_p_ != 0){
                    $proc_rem_p = $pemohon_rem_p_/$pemohon_all_p_*100;
                }

                $proc_all_p = 0;
                if($pemohon_all_p_ != 0 && $pemohon_all_p_ != 0){
                    $proc_all_p = $pemohon_all_p_/$pemohon_all_p_*100;
                }

            //magang
                $pemohon_req_m_ = 0;
                if(isset($pemohon_req_m)){
                    $pemohon_req_m_ = (double) $pemohon_req_m;
                }

                $pemohon_acc_m_ = 0;
                if(isset($pemohon_acc_m)){
                    $pemohon_acc_m_ = (double) $pemohon_acc_m;
                }

                $pemohon_rem_m_ = 0;
                if(isset($pemohon_rem_m)){
                    $pemohon_rem_m_ = (double) $pemohon_rem_m;
                }

                $pemohon_all_m_ = 0;
                if(isset($pemohon_all_m)){
                    $pemohon_all_m_ = (double) $pemohon_all_m;
                }

                $proc_req_m = 0;
                if($pemohon_req_m_ != 0 && $pemohon_all_m_ != 0){
                    $proc_req_m = $pemohon_req_m_/$pemohon_all_m_*100;
                }

                $proc_acc_m = 0;
                if($pemohon_acc_m_ != 0 && $pemohon_all_m_ != 0){
                    $proc_acc_m = $pemohon_acc_m_/$pemohon_all_m_*100;
                }

                $proc_rem_m = 0;
                if($pemohon_rem_m_ != 0 && $pemohon_all_m_ != 0){
                    $proc_rem_m = $pemohon_rem_m_/$pemohon_all_m_*100;
                }

                $proc_all_m = 0;
                if($pemohon_all_m_ != 0 && $pemohon_all_m_ != 0){
                    $proc_all_m = $pemohon_all_m_/$pemohon_all_m_*100;
                }
            ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-file-import text-info"></i></h2>
                                    <h3 class=""><?= $pemohon_acc_m_; ?></h3>
                                    <h6 class="card-subtitle">Total Magang ACC</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: <?= $proc_acc_m; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-file-multiple text-success"></i></h2>
                                    <h3 class=""><?= $pemohon_all_m_; ?></h3>
                                    <h6 class="card-subtitle">Total Pendaftar Magang</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: <?= $proc_all_m; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-file-excel text-info"></i></h2>
                                    <h3 class=""><?= $pemohon_acc_p_; ?></h3>
                                    <h6 class="card-subtitle">Total Penelitian ACC</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: <?= $proc_all_p; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-folder-multiple text-success"></i></h2>
                                    <h3 class=""><?= $pemohon_all_p_; ?></h3>
                                    <h6 class="card-subtitle">Total Pendaftar Penelitian</h6></div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: <?= $proc_all_p; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Row -->
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Grafik Magang dan Penelitian</h4><hr>
                                        <div id="bar-chart" style="width:100%; height:400px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                           
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

            <script src="<?php print_r(base_url());?>admin_template/assets/plugins/echarts/echarts-all.js"></script>
            <script type="text/javascript">
                // ============================================================== 
                // Bar chart option
                // ============================================================== 
                var myChart = echarts.init(document.getElementById('bar-chart'));

                // specify chart configuration item and data
                option = {
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data:['Magang','Penelitian']
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            
                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    color: ["#55ce63", "#009efb"],
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            data : ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        {
                            name:'Magang',
                            type:'bar',
                            data:[<?php
                                    if(is_array($pemohon_list_m)){print_r(implode(",", $pemohon_list_m));}
                                ?>],
                            
                        },
                        {
                            name:'Penelitian',
                            type:'bar',
                            data:[<?php
                                    if(is_array($pemohon_list_p)){print_r(implode(",", $pemohon_list_p));}
                                ?>],
                            
                        }
                    ]
                };


                // use configuration item and data specified to show chart
                myChart.setOption(option, true), $(function() {
                            function resize() {
                                setTimeout(function() {
                                    myChart.resize()
                                }, 100)
                            }
                            $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
                        });
                     
            </script>