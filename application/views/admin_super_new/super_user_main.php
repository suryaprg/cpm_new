
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="row">
                    
                    <div class="col-12 m-t-30">
                        <!-- Card -->
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Tabel Data User</h4>

                            </div>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive m-t-40">
                                            <table id="myTable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th data-sortable="true" width="5%"><center><strong>No.</th>
                                                        <th data-sortable="true" width="20%"><strong>Nama</th>
                                                        <th data-sortable="true" width="20%"><strong>Email</th>
                                                        <th data-sortable="true" width="10%"><strong>Pekerjaan</th>
                                                        <th data-sortable="true" width="15%"><strong>Instansi</th>
                                                        <th data-sortable="true" width="15%"><strong>Status Aktif</th>
                                                        <th ><center><strong><i class='fe fe-mail'> </i>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no =1;
                                                    if(isset($user)){
                                                        foreach ($user as $val_user){
                                                            $status = "<span class=\"label label-danger\">Tidak Aktif</span>";
                                                            if($val_user->status_active == "1"){
                                                                $status = "<span class=\"label label-info\">Aktif</span>";
                                                            }
                                                            
                                                            $pekerjaan = "Pekerja";
                                                            if($val_user->pekerjaan == "0"){
                                                                $pekerjaan = "Mahasiswa";
                                                            }
                                                            
                                                            $instansi = $val_user->instansi;
                                                            if(strpos($val_user->instansi,";")){
                                                                $ex_instansi = explode(";",$val_user->instansi);
                                                                $instansi = $ex_instansi[1].", ".$ex_instansi[0].", ".$ex_instansi[2].", ".$ex_instansi[3];
                                                            }
                                                            
                                                            echo "<tr>
                                                                    <td>".$no."</td>
                                                                    <td>".$val_user->nama."</td>
                                                                    <td>".$val_user->email."</td>
                                                                    <td>".$pekerjaan."</td>
                                                                    <td>".$instansi."</td>
                                                                    <td>".$status."</td>
                                                                                                               
                                                                    <td>
                                                                        <center>
                                                                        <button href=\"#\" onclick=\"detail_user('".$val_user->id_user."')\" class=\"btn btn-success\" style=\"width: 40px;\"><i class='fa fa-info'></i></button>
                                                                        &nbsp;&nbsp;
                                                                        <button href=\"#\" class='btn btn-danger' id='delete_user' onclick=\"delete_user('".$val_user->id_user."')\" style=\"width: 40px;\"><i class='fa fa-trash'></i></button>
                                                                        </center>
                                                                    </td>
                                                                </tr>";
                                                                
                                                            $no++;
                                                        }    
                                                    }
                                                    ?>                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>                     
                            </div>
                            <div class="card-body">
                                <div class="text-right">
                                    <label class="form-label">Keterangan Tombol Aksi ==> </label>

                                    <a class="btn btn-success" style="width: 40px;"><i class="fa fa-info" style="color: white;"></i></a>
                                    <label class="form-label text-success">Detail Data</label>,&nbsp;
                                    <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                                    <label class="form-label text-danger">Delete Data</label>
                                </div>
                            </div>
                        </div>
                        <!-- Card -->
                    </div>
                </div>
                <!-- End Row -->                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<!-- sample modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" id="detail_user">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Detail User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" id="output-detail">
                <!-- content -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- <div class="card card-outline-info"> -->
                        <div class="card-body">
                            <form action="#">
                                <div class="form-body">
                                    <div class="row p-t-20">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">User Name</label>
                                                <p id="out_username"></p>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group has-danger">
                                                <label class="control-label">Nama</label>
                                                <p id="out_nama"></p>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-success">
                                                <label class="control-label">Alamat</label>
                                                <p id="out_alamat"></p>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">NIK</label>
                                                <p id="out_nik"></p>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Pekerjaan</label>
                                                <p id="out_pekerjaan"></p>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Instansi</label>
                                                <p id="out_instansi"></p>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Status Aktif</label>
                                                <p id="out_status"></p>
                                            </div>
                                        </div>

                                        <!--/span-->
                                    </div>

                                </div>

                            </form>
                        </div>
                        <!-- </div> -->
                    </div>
                </div>
                <!-- Row -->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
                                
<script type = "text/javascript">
//==========================================================================================//
//----------------------------------------------------Get_User_Detail-----------------------//
//==========================================================================================//
    function detail_user(id_user) {
        clear_detail_user();
        // console.log(id_user);

        var data_main = new FormData();
        data_main.append('id_user', id_user);

        $.ajax({
            url: "<?php echo base_url()."admin_super/superadmin/detail_user/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                // console.log(res);
                set_val_user(res);
                $("#detail_user").modal('show');
            }
        });
    }

    function set_val_user(res) {
        var res_pemohon = JSON.parse(res);
        // console.log(res_pemohon.val_response.id_dinas);

        if (res_pemohon.status == true) {
            // console.log("res_pemohon.val_response.id_dinas");
            var instansi = res_pemohon.val_response.instansi;
            var str_ins = instansi;

            if(res_pemohon.val_response.instansi.indexOf(";") != -1){
                instansi = res_pemohon.val_response.instansi.split(";");
                str_ins = instansi[1] + " - " + instansi[3];
            }

            var pekerjaan = "Bekerja";
            if (res_pemohon.val_response.pekerjaan) {
                pekerjaan = "Mahasiswa";
            }

            var status_active = "<span class=\"label label-danger\">Tidak Aktif</span>";
            if (res_pemohon.val_response.status_active) {
                status_active = "<span class=\"label label-info\">Aktif</span>";
            }

            $("#out_username").html(res_pemohon.val_response.email);
            $("#out_nama").html(res_pemohon.val_response.nama);
            $("#out_alamat").html(res_pemohon.val_response.alamat);
            $("#out_nik").html(res_pemohon.val_response.nik);
            $("#out_pekerjaan").html(pekerjaan);
            $("#out_instansi").html(str_ins);
            $("#out_status").html(status_active);

        } else {
            // console.log("gak");
            clear_detail_user();
        }
    }

    function clear_detail_user() {
        $("#out_username").html("");
        $("#out_nama").html("");
        $("#out_alamat").html("");
        $("#out_nik").html("");
        $("#out_pekerjaan").html("");
        $("#out_instansi").html("");
        $("#out_status").html("");
    }
//==========================================================================================//
//----------------------------------------------------Get_User_Detail-----------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Delete_User---------------------------//
//==========================================================================================//
    function delete_user(id_user){
        !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: false 
                    }, function(){
                        var data_main = new FormData();
                        data_main.append('id_user', id_user);

                        $.ajax({
                            url: "<?php echo base_url()."admin_super/superadmin/delete_user/";?>", // point to server-side PHP script 
                            dataType: 'html', // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,
                            type: 'post',
                            success: function(res) {
                                console.log(res);
                                response_delete_user(res);
                            }
                        });
                    });                              
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),
            
            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
    }

    function response_delete_user(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                swal( {
                    title: "Proses Berhasil.!!",
                    text: "Data user berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href="<?php echo base_url()."super/user";?>";
                });                   
            }else{
                
                swal("Proses Gagal.!!", "Data user gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");                                 
            }
    }
//==========================================================================================//
//----------------------------------------------------Delete_User---------------------------//
//==========================================================================================//
</script>