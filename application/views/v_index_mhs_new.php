
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">
    <title>Admin Press Admin Template - The Ultimate Bootstrap 4 Admin Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>admin_template/horizontal/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>admin_template/horizontal/css/colors/blue.css" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/skins/all.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<?php
    if(isset($_SESSION["user_bangkes"])){
        $nama_admin = $this->session->userdata("user_bangkes")["nama"];
        $email_admin = $this->session->userdata("user_bangkes")["email"];
        $url_profil = $this->session->userdata("user_bangkes")["url_profil"];
    }
?>
<body class="fix-header fix-sidebar card-no-border logo-center">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /></svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html"></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>

                        <!-- dark Logo text -->
                        <img src="<?php print_r(base_url());?>assets/core_img/logo_magang_penelitian_header_admin.png" alt="homepage" class="dark-logo"/>
                
                    </ul>

                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pemohon, <?php echo $nama_admin;?></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php print_r(base_url().'doc/foto_pemohon/'.$url_profil);?>"></div>
                                            <div class="u-text">
                                                <h4><?php echo $nama_admin;?></h4>
                                                <!-- <p class="text-muted">varun@gmail.com</p></div> -->
                                        </div>
                                    </li>
                                    <li><a href="<?php echo base_url()."pendaftaran/logout"?>"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>

                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PERSONAL</li>
                        <li class="three-column"> <a class="has-arrow waves-effect waves-dark" href="<?=base_url();?>pendaftaran/home" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Halaman Utama</span></a>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?=base_url();?>pendaftaran/mainform" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Pendaftaran</span></a>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="<?=base_url();?>pendaftaran/history" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">Riwayat Pendaftaran</span></a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">

            <?php
                if(isset($page)){
                    if ($page) {
                        switch ($page) {
                            case 'home': include 'user_data_new/user_data_home.php';
                                break;

                            case 'pendaftaran': include 'user_data_new/user_data_pendaftaran.php';
                                break;

                            case 'fixed_pendaftaran': include 'user_data_new/user_data_fixed.php';
                                break;

                            case 'history': include 'user_data_new/user_data_history.php';
                                break;
                            
                            default:
                                # code...
                                break;
                        }
                    }
                }
            ?>
            
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> © 2019, Dinas Komunikasi dan Informatika Kota Malang </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!--Custom JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--sparkline JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--morris JavaScript -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/raphael/raphael-min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/morrisjs/morris.min.js"></script>
    <!-- Chart JS -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/dashboard1.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script> -->

    <!-- icheck -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/icheck.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/icheck.init.js"></script>

    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/horizontal/js/jasny-bootstrap.js"></script>


    <script>
    // This is for the sticky sidebar    
    $(".stickyside").stick_in_parent({
        offset_top: 100
    });
    $('.stickyside a').click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 100
        }, 500);
        return false;
    });
    // This is auto select left sidebar
    // Cache selectors
    // Cache selectors
    var lastId,
        topMenu = $(".stickyside"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function() {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
        });

    // Bind click handler to menu items


    // Bind to scroll
    $(window).scroll(function() {
        // Get container scroll position
        var fromTop = $(this).scrollTop() + topMenuHeight - 250;

        // Get id of current scroll item
        var cur = scrollItems.map(function() {
            if ($(this).offset().top < fromTop)
                return this;
        });
        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                .removeClass("active")
                .filter("[href='#" + id + "']").addClass("active");
        }
    });
        
    </script>

    <!-- This is data table -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>


    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>