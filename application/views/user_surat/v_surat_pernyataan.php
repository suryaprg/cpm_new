<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>PMP</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style.css">
    <link href="pmp/style.css" rel='stylesheet' type='text/css' media="all" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/inputcss.css">

</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcumb Area Start ##### -->

    <!-- ##### About Us Area Start ##### -->
    <section class="about-us-area mt-50 section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="50ms">
                        <h3><u>SURAT PERNYATAAN</u></h3>
                    </div>
                </div>
            </div>
            <h6><font size "12px"> Saya bertanda tangan di bawah ini   :</font></h6>
            
            <?php
                $nama = "";
                $nomor_identitas = "";
                $usia = "";
                $pekerjaan = "";
                $lokasi_penelitan = "";
                $judul = "";
                
                if(!empty($pemohon)){
                    $nama = $pemohon["nama"];
                    $nomor_identitas = $pemohon["nik"];
                    $lokasi_penelitan = $pemohon["id_bidang"];
                    $judul = $pemohon["judul"];
                    
                    $tgl = $pemohon["tgl_lhr"];
                    $usia = date("Y-m-d") - $tgl;
                    
                    if($pemohon["pekerjaan"] == 0){
                        $pekerjaan = "Pekerja";    
                    }else{
                        $pekerjaan = "Mahasiswa";
                    }
                }
            ?>
            
            <table width=0%>
                <tr>
                    <td width="5%">1. </td>
                    <td width="0"><font size="3px">Nama</font></td>
                    <td width="2%">&nbsp;:&nbsp;</td>
                    <td><?= ucwords($nama);?></td>
                </tr>
                <tr>
                    <td>2. </td>
                    <td><font size="3px">Nomor Identitas</font></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td width="50%"><?= $nomor_identitas;?></td>
                </tr>
                <tr>
                    <td>3. </td>
                    <td><font size="3px">Usia</font></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td width="50%"><?= $usia;?></td>
                </tr>    
                <tr>
                    <td>4. </td>
                    <td><font size="3px">Pekerjaan</font></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td width="50%"><?= $pekerjaan; ?></td>
                </tr>
            </table>
            <br />
            <font size="3px"><b>Menerapkan dengan sesungguhnya bahwa saya bermaksud untuk melaksanakan penelitian di : </b><br></font> (Lokasi/Tempat Penelitian/ Survei/ Pengambilan Data/ Obeservasi)
            <br /><br />
            <table width="100%">
                <?php 
                $tmp = strpos($lokasi_penelitan, ";");
                if($tmp){
                    $data_tmp = explode(";",$lokasi_penelitan);
                    $no = 1;
                    foreach($data_tmp as $val_tmp){
                        echo "<tr>
                                <td width=\"3%\">".$no.".&nbsp;</td>
                                <td width=\"95%\">".$dinas[$val_tmp]."</td>
                            </tr>";
                        $no++;
                    }
                }else{
                    $no = 1;
                    echo "<tr>
                                <td width=\"3%\">".$no.".&nbsp;</td>
                                <td width=\"95%\">".$dinas[$lokasi_penelitan]."</td>
                            </tr>";
                }
                ?>
                
                <!--<tr>
                    <td width="80%">
                        <div id="signin-agile">
                            <input type="text" name="user" placeholder="Tempat Penelitian" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'tempat_Penelitian';}" required="required">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="80%">
                        <div id="signin-agile">
                            <input type="text" name="user" placeholder="Survei" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Survei';}" required="required">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="80%">
                        <div id="signin-agile">
                            <input type="text" name="user" placeholder="Pengembalian Data" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'pengembalian_Data';}" required="required">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="80%">
                        <div id="signin-agile">
                            <input type="text" name="user" placeholder="Observasi" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Observasi';}" required="required">
                        </div>
                    </td>
                </tr>-->
            </table>
            <br /><br />
            <font size="3px"><b>dengan Judul/Tema : </b></font>
            <br /><br />
            <table width="100%">
                <tr>
                    <td width="80%">
                        <?= $judul;?>
                    </td>
                </tr>
            </table>
            <br /><br />

            <font size="3px"><b>Maksud dan tujuan penelitian sebagaimana tercantum dalam proposal penelitian yang telah diserahkan kepada Bakesbangpol Kota Malang.</b><br></font>
            <font size="3px"><b>Sehubungan dengan hal tersebut, saya enyatakan dengan sesunguhnya bahwa saya :</b><br></font>
            <table border="0"> <b>                  
                        <tr><td><font size="3px">1. </td><td>Tidak akan melakukan penelitian yang tidak sesuai atau ada kaitannya dengan judul maksud dan tujuan kegiatan sebagaimana tercantum dalam proposal penelitian; </td></tr></font>
                        <tr><td><font size="3px">2. </td><td>Sanggup menjaga perilaku dan mentaati tata tertib yang berlaku pada Lokasi tersebut di atas;</td></tr></font> 
                        <tr><td><font size="3px">3  </td><td>Wajib melaporkan hasil Penelitian dan sejenisnya kepada Walikota Malang melalui Bakesbangpol Kota Malang;</td></tr></font>
                        <tr><td><font size="3px">4. </td><td>Sanggup mentaati ketentuan peraturan perundang-undangan.</td></tr></font>
                        </b></table><br />
            <font size="3px"><b>Demikian surat ernyataan ini saya buat dengan sesungguhnya dan apabila dalam pelaksanaan penelitian saya terbukti melakukan pelanggaran terhadap pernyataan di atas, maka saya bersedia menerima sanksi sebagaimana ketentuan yang berlaku, termasuk penghentian kegiatan penelitian, tanpa menuntut ganti rugi apapun.</b><br></font>
            <br /><br /><br /><br />
            
            <table border="0" width="100%">
                <tr>
                    <td width="80%"></td>
                    <td width="20%" align="center"><font size="3px" align="right"><b>Malang, 
                        <?php 
                            $month = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                            echo (date("d")+0)." ".$month[date("d")-1]." ".date("Y");?></b></font>
                    </td>
                </tr>
                <tr>
                    <td width="80%"></td>
                    <td width="20%" align="center"><font size="3px" align="right"><b>Pembuat Pernyataan,</b></font></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Materai 6.000</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="80%"></td>
                    <td width="20%" align="center"><font size="3px" align="right"><b><?= ucwords($nama);?></b></font></td>
                </tr>
            </table>
            
                        

                       

    </section>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url(); ?>pmp/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url(); ?>pmp/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo base_url(); ?>pmp/js/active.js"></script>
</body>

</html>