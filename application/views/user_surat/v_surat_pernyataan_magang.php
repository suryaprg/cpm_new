<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>PMP</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style.css">
    <link href="pmp/style.css" rel='stylesheet' type='text/css' media="all" />
    <link href="pmp/style1.css" rel='stylesheet' type='text/css' media="all" />
    <link href="pmp/style2.css" rel='stylesheet' type='text/css' media="all" />

     <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/inputcss.css">
     <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style1.css">
     <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style2.css">

</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>


    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcumb Area Start ##### -->

    <!-- ##### About Us Area Start ##### -->
    
                            <?php

                                $array_of_month = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
                                $tmp_tgl ="";

                                $nama_user = "";
                                $nik = "";
                                $tgl_lhr = "";
                                $alamat = "";
                                $email = "";
                                $tlp = "";
                                $alamat_dom = "";
                                $alamat_dom = "";
                                $instansi = "";
                                $pekerjaan = "";

                                $url_foto_profil =  "";

                                $status_pekerjaan = "";
                                $instansi_pekerjaan = "";

                                $tgl_lhr_fix = "";

                                $univ = "";
                                $fakultas = "";
                                $studi = "";
                                $jurusan = ""; 

                                $srt_perwakilan = "";    

                                if(isset($user)){
                                  if($user){
                                    $nama_user = $user["nama"];
                                    $email = $user["email"];
                                    $alamat_dom = $user["alamat_dom"];
                                    $alamat = $user["alamat"];
                                    $tgl_lhr = $user["tgl_lhr"];
                                    $nik = $user["nik"];
                                    $tlp = $user["tlp"];
                                    $instansi = $user["instansi"];
                                    $pekerjaan = $user["pekerjaan"];

                                    $tmp_tgl = explode("-", $tgl_lhr);
                                    $tgl_lhr_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $url_foto_profil =  $user["url_profil"];

                                    if($url_foto_profil != ""){
                                      $url_foto_profil_fix = base_url()."doc/foto_pemohon/".$url_foto_profil;  
                                    }else {
                                      $url_foto_profil_fix = base_url()."assets/core_img/icon_upload_152x277.png";  
                                    }

                                    $status_pekerjaan = "Mahasiswa, ";
                                    if($pekerjaan == 0){
                                        $status_pekerjaan = "Bekerja Pada, ";
                                    }

                                    if(strpos($instansi, ";")){
                                        $data_instansi = explode(";", $instansi);
                                        $instansi_pekerjaan = $status_pekerjaan.implode(", ", $data_instansi);
                                    }else{
                                        $instansi_pekerjaan = $status_pekerjaan.$instansi;
                                    }

                                    if(strpos($instansi, ";")){
                                        $data_instansi = explode(";", $instansi);
                                        $studi = $data_instansi[0];
                                        $jurusan = $data_instansi[1];    
                                        $fakultas = $data_instansi[2];
                                        $univ = $data_instansi[3];

                                        $srt_perwakilan = $fakultas.", ".$univ;
                                    }else{
                                        $srt_perwakilan = $instansi;
                                    }
                                    

                                  }
                                }

                                $id_pemohon = "";
                                $id_permohonan = "";
                                $keterangan_permohonan = "";
                                $no_register = "";
                                $tgl_start = "";
                                $tgl_selesai = "";
                                $judul = "";
                                $instansi_penerima = "";
                                $id_bidang = "";
                                $dosen_pembimbing = "";
                                $anggota = "";
                                $status_pendelegasian = "";
                                $url_ktp_pemohon = "";
                                $url_proposal_pemohon = "";
                                $url_sk_pemohon = "";
                                $jabatan_tdd = "";
                                $nama_tdd = "";
                                $no_surat_tdd = "";
                                $tgl_surat_tdd = "";
                                $alamat_dom_del = "";
                                $alamat_ktp_del = "";
                                $nama_del = "";
                                $nik_del = "";
                                $url_foto_del = "";
                                $url_ktp_del = "";
                                $url_srt_del = "";

                                $tgl_start_fix = "";
                                $tgl_selesai_fix = "";
                                $tgl_surat_tdd_fix = "";

                                $tgl_diterima_fix = "";
                                $tgl_acc_fix = "";

                                $distance_date = 0;

                                $t_anggota = 0;
                                                               

                                if(isset($pendaftaran)){
                                  if($pendaftaran){
                                    $id_user = $pendaftaran["id_user"];
                                    // $nama = $pendaftaran["nama"];
                                    $id_pemohon = $pendaftaran["id_pemohon"];
                                    $id_permohonan = $pendaftaran["id_permohonan"];
                                    $keterangan_permohonan = $pendaftaran["keterangan_permohonan"];
                                    $no_register = $pendaftaran["no_register"];
                                    $tgl_start = $pendaftaran["tgl_start"];
                                    $tgl_selesai = $pendaftaran["tgl_selesai"];
                                    $judul = $pendaftaran["judul"];
                                    $instansi_penerima = $pendaftaran["instansi_penerima"];
                                    $id_bidang = $pendaftaran["id_bidang"];
                                    $dosen_pembimbing = $pendaftaran["dosen_pembimbing"];
                                    $anggota = $pendaftaran["anggota"];
                                    $status_pendelegasian = $pendaftaran["status_pendelegasian"];
                                    $url_ktp_pemohon = $pendaftaran["url_ktp_pemohon"];
                                    $url_proposal_pemohon = $pendaftaran["url_proposal_pemohon"];
                                    $url_sk_pemohon = $pendaftaran["url_sk_pemohon"];
                                    $jabatan_tdd = $pendaftaran["jabatan_tdd"];
                                    $nama_tdd = $pendaftaran["nama_tdd"];
                                    $no_surat_tdd = $pendaftaran["no_surat_tdd"];
                                    $tgl_surat_tdd = $pendaftaran["tgl_surat_tdd"];
                                    $alamat_dom_del = $pendaftaran["alamat_dom_del"];
                                    $alamat_ktp_del = $pendaftaran["alamat_ktp_del"];
                                    $nama_del = $pendaftaran["nama_del"];
                                    $nik_del = $pendaftaran["nik_del"];
                                    $url_foto_del = $pendaftaran["url_foto_del"];
                                    $url_ktp_del = $pendaftaran["url_ktp_del"];
                                    $url_srt_del = $pendaftaran["url_srt_del"];

                                    $tgl_diterima = $pendaftaran["tgl_diterima"];
                                    $tgl_acc = $pendaftaran["tgl_acc"];

                                    $tmp_tgl = explode("-", $tgl_start);
                                    $tgl_start_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", $tgl_selesai);
                                    $tgl_selesai_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", $tgl_surat_tdd);
                                    $tgl_surat_tdd_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", explode(" ",$tgl_diterima)[0]);
                                    $tgl_diterima_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $tmp_tgl = explode("-", explode(" ",$tgl_acc)[0]);
                                    $tgl_acc_fix = $tmp_tgl[2]." ".$array_of_month[(int)$tmp_tgl[1]]." ".$tmp_tgl[0];

                                    $distance_date = round((strtotime($tgl_selesai) - strtotime($tgl_start)) / (60 * 60 * 24));

                                    $t_anggota = count(json_decode(str_replace("'", "\"", $anggota)));

                                    // print_r($anggota);

                                  }
                                }

                                
                              ?>
    <section class="about-us-area mt-50 section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="500ms">
                        <h3><u>SURAT PERNYATAAN</u></h3>
                     </div>
                </div>
            </div>
            <h6><font size "12px"> Saya bertanda tangan di bawah ini   :</font></h6>
            <table width=100%>
                <tr>
                    <font size="3px">
                    <td width="5%">1. </td>
                    <td width="15%">Nama</td>
                    <td align="center">:</td>
                    <td width="75%">
                        <?= $nama_user; ?>
                    </td>
                    </font>
                </tr>
                
                <tr>
                    <font size="3px">
                    <td>2.</td>
                    <td>Nomor Identitas</td>
                    <td align="center">:</td>
                    <td>
                        <?= $nik ?>
                    </td>
                    </font>
                </tr>
                <tr>
                    <font size="3px">
                    <td>3.</td>
                    <td>Jabatan</td>
                    <td align="center">:</td>
                    <td>
                        <div id="signindua-agile">
                            <?= "Koordinator"; ?>
                        </div>
                    </td>
                    </font>
                </tr>
            </table><br />
            <p align="justify">
                Selaku perwakilan/utusan dari <b><?php echo $srt_perwakilan;?></b> yang ditetapkan berdasarkan surat
                <b><?= $jabatan_tdd;?></b> No. Srt
                <b><?= $no_surat_tdd;?></b> menerangkan dengan sesungguhnya bahwa
                <b><?= $nama_user;?></b> bermaksud untuk melaksanakan Praktek Kerja Lapangan / Kegiatan di
                <b><?= $tmp_magang[$instansi_penerima];?></b> dengan judul / tema
                <b><?= $judul;?></b> dalam jangka waktu kegiatan selama
                <b><?= $distance_date;?></b> hari dan jumlah peserta kegiatan sebanyak
                <b><?= $t_anggota+1;?></b> orang. Adapun rincian data peserta kegiatan sebagaimana tercantum dalam rencana pelaksanaan kegiatan yang telah diserahkan kepada Bakesbangpol Kota Malang. Sehubungan dengan hal tersebut, saya bersedia menjamin bahwa peserta kegiatan Praktek Kerja Lapanagn sebagaimana dimaksud :
            </p>

            <table border="0">
                <b>                  
                <tr><td><font size="3px">1. </td><td>Tidak akan melakukan kegiatan yang tidak sesuai atau tidak ada kaitannya dengan maksud dan tujuan kegiatan; </td></tr></font>
                <tr><td><font size="3px">2. </td><td>Sanggup menjaga perilaku dan mentaati tata tertib yang berlaku pada Lokasi/ Instansi/ SKPD;</td></tr></font> 
                <tr><td><font size="3px">3  </td><td>Wajib melaporkan hasil Praktek Kerja Lapangan dan sejenisnya kepada Walikota Malang melalui Bakesbangpol Kota Malang;</td></tr></font>
                <tr><td><font size="3px">4. </td><td>Sanggup mentaati ketentuan peraturan perundang-undangan.</td></tr></font>
                </b>
            </table>
            <p align="justify"><font size="3px"><b>Demikian surat pernyataan ini saya buat dengan sesungguhnya dan 
                        apabila dalam pelaksanaan kegiatan peserta Praktek Kerja Lapangan saya terbukti melakukan pelanggaran terhadap pernyataan di atas, 
                        maka saya bersedia menerima sanksi sebagaimana ketentuan yang berlaku, 
                        termasuk penghentian kegiatan Praktek Kerja Lapangan, tanpa menuntut ganti rugi apapun.</b><br></font>  
            </p>

<br /><br /><br /><br />
            <table border="0" width="100%">
                <tr>
                    <td width="80%"></td>
                    <td width="20%" align="center"><font size="3px" align="right"><b>Malang, <?=$tgl_acc_fix;?></b></font></td>
                </tr>
                <tr>
                    <td width="80%"></td>
                    <td width="20%" align="center"><font size="3px" align="right"><b>Pembuat Pernyataan,</b></font></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>Materai 6.000</td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="80%"></td>
                    <td width="20%" align="center"><font size="3px" align="right"><b><?= ucwords($nama_user);?></b></font></td>
                </tr>
            </table>
                        
             
                    
              
      
          
             
    </section>
    <!-- ##### About Us Area End ##### -->


    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url(); ?>pmp/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url(); ?>pmp/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo base_url(); ?>pmp/js/active.js"></script>
</body>

</html>