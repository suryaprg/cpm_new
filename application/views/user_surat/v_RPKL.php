<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>PMP</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style.css">
    <link href="pmp/style.css" rel='stylesheet' type='text/css' media="all" />
    <link href="pmp/style1.css" rel='stylesheet' type='text/css' media="all" />
    <link href="pmp/style2.css" rel='stylesheet' type='text/css' media="all" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/inputcss.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style1.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style2.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

    <script type="text/javascript">
    </script>

</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <?php
    
    //print_r($pemohon);
        $judul = "";
        $tgl_start = "";
        $tgl_selesai = "";
        $periode = "";
        $no_register = "";
        $instansi_penerima = "";
        $jenis_kegiatan = "";
        $str_kegiatan = "";
        $keterangan_permohonan = "";
        $nama = "";
        $nik = "";
        $alamat = "";
        
        $id_bidang = "";

        $no_surat = "";
        $nama_tdd = "";
        $jabatan = "";
        
        $instansi = "";
        $tgl_surat="";

        $str_tgl_surat = "";

        $m = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"); 
                
        if(isset($pemohon)){
            if(!empty($pemohon)){
                //echo "ya";
                $judul = $pemohon["judul"];
                $tgl_start = $pemohon["tgl_start"];
                $tgl_selesai = $pemohon["tgl_selesai"];
                // $periode = $pemohon["periode"];
                $no_register = $pemohon["no_register"];
                $instansi_penerima = $pemohon["instansi_penerima"];
                $jenis_kegiatan = $pemohon["jenis_kegiatan"];
                $nama = $pemohon["nama"];
                $nik = $pemohon["nik"];
                $alamat = $pemohon["alamat"];
                
                $no_surat = $pemohon["no_surat_tdd"];
                $tgl_surat = $pemohon["tgl_surat_tdd"];
                $nama_tdd = $pemohon["nama_tdd"];
                $jabatan = $pemohon["jabatan_tdd"];
                $id_bidang = $pemohon["id_bidang"];

                $anggota = $pemohon["anggota"];
                $json_anggota = json_decode(str_replace("'", "\"", $anggota));

                $distance_date = round((strtotime($tgl_selesai) - strtotime($tgl_start)) / (60 * 60 * 24));
                
                if($pemohon["pekerjaan"] == 0){
                    $instansi = $pemohon["instansi"];
                }else{
                    $instansi = explode(";", $pemohon["instansi"])[3];
                }
                
                
                if($jenis_kegiatan == 0){
                    $str_kegiatan = "Penelitian ".$pemohon["keterangan_permohonan"]; 
                }else{
                    $str_kegiatan = "Magang ".$pemohon["keterangan_permohonan"];
                }
                
                $data_tgl = explode("-", $tgl_surat);
                $str_tgl_surat = ($data_tgl[2]+0)." ".$m[(int)$data_tgl[1]]." ".$data_tgl[0];
            }
        }
        
                                    
        $count_anggota = count($json_anggota);
        
        $admin_acc = $_SESSION["admin_lv_1"]["nama"];
    ?>
    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header">
            <div class="container h-100">
                <div class="row h-100">
                    <div class="col-12 h-100">
                        <br /><br /><br />
                        <font face="Times New Roman" color="black">
                        <table width="100%" border="0">
                            <tr>
                                <td width="65" align="center"><img src="<?php echo base_url(); ?>pmp/img/core-img/logo02.png" width="60%"></td>
                                <td width="20" align="center">
                                    <h3>PEMERINTAHAN KOTA MALANG <br>BADAN KESATUAN BANGSA DAN POLITIK </h3>
                                    <h6>Jl. Ahmad Yani No.98 Telp.(0341) 491180 Fax.474254 </h6>
                                    <strong><font size = "4px">MALANG </font></strong>
                                    <div align="right">Kode Pos 65125</div>
                                    <td width="50" align="center"></td>
                            </tr>
                        </table>
                        </font>
                        <hr class="style1">
                        <br>
                        <font face="Times New Roman" color="black">
                        <center>
                            <h4><u>REKOMENDASI PELAKSANAAN PRAKTEK KERJA LAPANGAN</u></h4>
                            <h5>NOMOR : <?= $no_register;?> </h5></center>
                        </center>
                        </br>
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                   
                                    <p align=justify> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font face="Times New Roman" size="4" color="black">
                                    Berdasarkan pemenuhan ketentuan persyaratan sebagaimana ditetapkan dalam 
                                    peraturan Walikota Malang, Nomor 24 Tahun 2011 tentang Pelayanan Pemberian Rekomendasi Pelaksanaan Penelitian dan Praktek Kerja Lapangan di Lingkungan Pemerintah Kota Malang 
                                    Oleh Badan Kesatuan Bangsa dan Politik Kota Malang serta menunjuk surat <b><?= $jabatan;?></b>,  <b><?= $instansi;?></b> No. <b><?= $no_surat;?></b> tgl. <b><?= $str_tgl_surat;?></b>
                                    perihal: pendalaman Materi Praktis, kepada pihak sebagaimana disebut dibawah ini:</font></p>
                                </td>
                            </tr>
                        </table>
                        <font size="4" color="black">
                        <table border="0" width="100%">
                                <tr>
                                    <td width="2%">a. </td>
                                    <td width="0%"></td>
                                    <td width="20%">Nama</td>
                                    <td width="2%" align="center">:</td>
                                    <td>
                                        <?= ucwords($nama);?> (peserta : <?= $count_anggota+1;?> orang terlampir)
                                    </td>
                                </tr>
                                <tr>
                                    <td>b. </td>
                                    <td>&nbsp</td>
                                    <td>Nomor Identitas <!-- <BR>(NIK/NIM/SIM) --></td>
                                    <td align="center">:</td>
                                    <td>
                                        <?= $nik;?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>c. </td>
                                    <td>&nbsp</td>
                                    <td>Tema/Judul</td>
                                    <td align="center">:</td>
                                    <td>
                                        <?= $judul;?>
                                    </td>
                                    </td>
                                </tr>

                        </table>
                        <br />
                        dinyatakan memenuhi persyaratan untuk melaksanakan kegiatan <b><?= $str_kegiatan;?></b> yang berlokasi di :
                        <table width="100%">
                            <tr>
                                <td width="2%">- </td>
                                <td ><?php
                                    if($instansi_penerima != "" and $instansi_penerima != "0"){
                                        echo $dinas[$instansi_penerima];
                                    }
                                    ?>
                                </td> 
                            </tr>
                            <?php 
                                /*$no = 1;
                                if(strpos($id_bidang,";")){
                                    $data_dinas = explode(";", $id_bidang);
                                    foreach($data_dinas as $val_data_dinas){
                                        echo "<tr>
                                                <td width=\"5%\">&nbsp;&nbsp;".$no.".</td>
                                                <td >".$dinas[$val_data_dinas]."</td> 
                                            </tr>";
                                        $no++;
                                    }
                                }else{
                                    echo "<tr>
                                                <td width=\"5%\">&nbsp;&nbsp;".$no.".</td>
                                                <td >".$dinas[$id_bidang]."</td> 
                                            </tr>";
                                }*/
                            ?>
                        </table>
                        <br />
                            <?php
                                $data_tgl_start = explode("-",$tgl_start);
                                $str_tgl_st = ($data_tgl_start[2]+0)." ".$m[$data_tgl_start[1]+0]." ".$data_tgl_start[0];
                                
                                $data_tgl_fn = explode("-",$tgl_selesai);
                                $str_tgl_fn = ($data_tgl_fn[2]+0)." ".$m[$data_tgl_fn[1]+0]." ".$data_tgl_fn[0];
                            
                            ?> 
						 
                         Dengan jangka waktu kegiatan mulai tanggal <b><?= $str_tgl_st; ?> s/d <?= $str_tgl_fn; ?>,</b> sepanjang yang bersangkutan memenuhi ketentuan sebagai berikut:<br /><br />
						 
                         <table width="100%" border="0">
                            <tr>
                                <td width="2%">a. </td>
                                <td> Tidak melakukan kegiatan yang tidak sesuai atau tidak ada kaitannya dengan maksud dan tujuan kegiatan; </td>
                            </tr>
                            <tr>
                                <td width="2%">b. </td>
                                <td> Menjaga perilaku dan mentaati tata tertib yang berlaku pada Lokasi tersebut diatas;</td>
                            </tr>
                            <tr>
                                <td width="2%">c. </td>
                                <td>Mentaati Ketentuan peraturan perundang-undangan.</td>
                            </tr>
                         </table>
                         <!--a.&nbsp;&nbsp;Tidak melakukan penelitian yang tidak sesuai atau tidak ada kaitannya dengan judul, maksud dan tujuan penelitian:</br>
						 b.&nbsp;&nbsp;Menjaga perilaku dan mentaati tata tertib yang berlaku pada Lokasi</br>
						 c.&nbsp;&nbsp;Mentaati Ketentuan peraturan perundang-undangan.<br />-->
						Demikian rekomendasi ini dibuat untuk dipergunakan sebagaimana mestinya.
                         </font>
                         <br /><br /><br /><br /><br />
                         <table width="100%" border="0">
                            <tr>
                                <td width="70%"></td>
                                <td width="30%" align="center"> <strong><font size = "3px">Malang, 
                                <?php echo date("d");?>&nbsp;
                                <?php
                                    echo $m[date("m")+0];
                                ?>&nbsp;
                                <?php echo date("Y");?>
                                </font></strong> </td>
                            </tr>
                            <tr>
                                <td width="70%"></td>
                                <td width="30%" align="center"> <strong><font size = "3px">An. KEPALA BAKESBANGPOL</font></strong> </td>
                            </tr>
                            <tr>
                                <td width="70%"></td>
                                <td width="30%" align="center"> <strong><font size = "3px">KOTA MALANG</font></strong> </td>
                            </tr>
                            <tr>
                                <td width="70%"></td>
                                <td width="30%" align="center"> <strong><font size = "3px">Sekretaris,</font></strong> </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="70%"></td>
                                <td width="30%" align="center"> <strong><font size = "3px"><u>HERU MULYONO, SIP.,MT.</u></font></strong> </td>
                            </tr>
                            <tr>
                                <td width="70%"></td>
                                <td width="30%" align="center"> <font size = "3px">Pembina Tingkat 1</font> </td>
                            </tr>
                            <tr>
                                <td width="70%"></td>
                                <td width="30%" align="center"> <font size = "3px">NIP. 19720420 199201 1 001</strong> </td>
                            </tr>
                         </table>
                         </font>
                         <br><br><br><br><br>
                         <font face="Times New Roman" size="3" color="black"><b>NB : Yang bersangkutan wajib melaporkan hasil penelitian dan sejenisnya kepada Bakesbangpol Kota Malang.</b></font>
	<!--<div align="right">
<strong><font size = "4px">An. KEPALA BAKESBANGPOL
<br>KOTA MALANG &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<br>Sekertaris, &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</font></strong>
<br><br><br><br><br><br><br>
<strong><font size = "4px"><u>HERU MULYONO, SIP.,MT.</u></font>
                                    </strong>
                                    <br>Pembina &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                    <br>NIP. 19720420 199201 1 001 &nbsp&nbsp-->
                    </div>

                </div>
            </div>
        </div>
    </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcumb Area Start ##### -->
    </div>
    <!-- ##### About Us Area Start ##### -->
    <section class="about-us-area mt-50 section-padding-100">
        <br /><br /><br />       
    </section>
    <!-- ##### About Us Area End ##### -->

        <!-- ##### Footer Area Start ##### -->

        <!-- ##### All Javascript Script ##### -->
        <!-- jQuery-2.2.4 js -->
        <script src="<?php echo base_url(); ?>pmp/js/jquery/jquery-2.2.4.min.js"></script>
        <!-- Popper js -->
        <script src="<?php echo base_url(); ?>pmp/js/bootstrap/popper.min.js"></script>
        <!-- Bootstrap js -->
        <script src="<?php echo base_url(); ?>pmp/js/bootstrap/bootstrap.min.js"></script>
        <!-- All Plugins js -->
        <script src="<?php echo base_url(); ?>pmp/js/plugins/plugins.js"></script>
        <!-- Active js -->
        <script src="<?php echo base_url(); ?>pmp/js/active.js"></script>
</body>

</html>