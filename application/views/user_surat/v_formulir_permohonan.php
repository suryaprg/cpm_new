<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>CMP</title>

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url(); ?>pmp/img/core-img/logopemkot.ico">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/style.css">
    <link href="pmp/style.css" rel='stylesheet' type='text/css' media="all" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>pmp/inputcss.css">

</head>

<body>
    <!-- ##### Preloader ##### -->
    <div id="preloader">
        <i class="circle-preloader"></i>
    </div>

    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcumb Area Start ##### -->

    <!-- ##### About Us Area Start ##### -->
    <section class="about-us-area mt-50 section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="5ms">
                        <h3><u>~ FORMULIR PERMOHONAN ~</u></h3>
                    </div>
                </div>
            </div>
            
            <?php
    
    //print_r($pemohon);
        $judul = "";
        $tgl_start = "";
        $tgl_selesai = "";
        $periode = "";
        $no_register = "";
        $tlp = "";
        
        $jenis_kegiatan = "";
        $str_kegiatan = "";
        $keterangan_permohonan = "";
        $nama = "";
        $nik = "";
        $alamat = "";
        $alamat_dom = "";
        
        $id_bidang = "";

        $no_surat = "";
        $nama_tdd = "";
        $jabatan = "";
        
        $instansi = "";
        $tgl_surat="";

        $str_tgl_surat = "";

        $str_dosen_pem = "";
        $str_instansi = "";

        $tempat_penelitian = "";


        $m = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"); 
                
        if(isset($pemohon)){
            if(!empty($pemohon)){
                // print_r("<pre>");
                // print_r($pemohon);
                $judul = $pemohon["judul"];
                $tgl_start = $pemohon["tgl_start"];
                $tgl_selesai = $pemohon["tgl_selesai"];
                // $periode = $pemohon["periode"];
                $no_register = $pemohon["no_register"];
                $tlp = $pemohon["tlp"];

                $jenis_kegiatan = $pemohon["jenis_kegiatan"];
                $nama = $pemohon["nama"];
                $nik = $pemohon["nik"];
                $alamat = $pemohon["alamat"];
                $alamat_dom = $pemohon["alamat_dom"];
                
                $no_surat = $pemohon["no_surat_tdd"];
                $tgl_surat = $pemohon["tgl_surat_tdd"];
                $nama_tdd = $pemohon["nama_tdd"];
                $jabatan = $pemohon["jabatan_tdd"];
                $id_bidang = $pemohon["id_bidang"];
                $anggota = $pemohon["anggota"];
                $json_anggota = json_decode(str_replace("'", "\"", $anggota));

                $keterangan_permohonan = $pemohon["keterangan_permohonan"];

                $tempat_penelitian = $pemohon["id_bidang"];
                
                if($pemohon["pekerjaan"] == 0){
                    $instansi = $pemohon["instansi"];
                }else{
                    $instansi = explode(";", $pemohon["instansi"])[3];
                }
                
                
                if($jenis_kegiatan == 0){
                    $str_kegiatan = "Penelitian ".$pemohon["keterangan_permohonan"]; 
                }else{
                    $str_kegiatan = "Magang ".$pemohon["keterangan_permohonan"];
                }
                
                $data_tgl = explode("-", $tgl_surat);
                $str_tgl_surat = ($data_tgl[2]+0)." ".$m[(int)$data_tgl[1]]." ".$data_tgl[0];

                if(strpos($pemohon["dosen_pembimbing"], ";")){
                    $data_dsn = explode(";", $pemohon["dosen_pembimbing"]);
                    $str_dosen_pem = implode(", ", $data_dsn);
                }else {
                    $str_dosen_pem = $pemohon["dosen_pembimbing"];
                }

                if(strpos($pemohon["instansi"], ";")){
                    $data_ins = explode(";", $pemohon["instansi"]);
                    $str_instansi = $data_ins[1].", ".$data_ins[2].", ".$data_ins[3];
                }else {
                    $str_instansi = $pemohon["instansi"];
                }
            }
        }
        
                                    
        $count_anggota = count($json_anggota);

        $admin_acc = $_SESSION["admin_lv_1"]["nama"];
        
        ?>
            
            <table width=0% border="0">
            <!-- 1 -->
              <tr>
                    <td width="2%"><b>1. </b></td>
                    <td width="20%"><font size="3px"><b>Keperluan permohonan</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;</b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= $keterangan_permohonan;?><b>
                    <!-- Rekomendasi penelitian dosen/ Skripsi/ Tesis/ Disertasi/ Mata Kuliah/ KTI/ PKM.<br>
                    Rekomendasi PKL/ PKN/Magang/ Pengabdian Masyarakat/ Prakerin.<br>
                    (lingkari sesuai keperluan) --></b></td>
                </tr>

            <!-- 2 -->
                  <tr>
                    <td width="2%"><b>2. </b></td>
                    <td width="0"><font size="3px"><b>Identitas pemohon</b></font></td>
                    <td width="1%"></td>
                    <td width="1%"></td>
                    <td width="50%"></td>
                </tr>
                <tr>
                    <td width="2%"><b>- </b></td>
                    <td width="0"><font size="3px"><b>Nama</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;</b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= ucwords($nama);?></td>
                </tr>
                <tr>
                    <td>- </td>
                    <td><font size="3px"><b>NIK / NIM / No. SIM</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;</b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= $nik;?></td>
                </tr>
                <tr>
                    <td>- </td>
                    <td><font size="3px"><b>Alamat KTP</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;</b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= $alamat;?></td>
                </tr>
                <tr>
                    <td>- </td>
                    <td><font size="3px"><b>Alamat Di Malang</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;</b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= $alamat_dom;?></td>
                </tr>
                <tr>
                    <td>- </td>
                    <td><font size="3px"><b>Telepon/ HP</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;</b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= $tlp;?></td>
                </tr>
                <tr>
                    <td>- </td>
                    <td><font size="3px"><b>Fak/ Univ/ Lembaga</b></font></td>
                    <td width="1"><b>&nbsp;:&nbsp;</b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= $str_instansi;?></td>
                </tr>

            <!-- 3 -->

                <?php
                    // print_r($tempat_penelitian);
                    // if(strpos($tempat_penelitian, ";")){
                        $no = 1;
                        $data_tmp_penelitian = json_decode(str_replace("'", "\"", $id_bidang));
                        foreach ($data_tmp_penelitian as $r_tmp =>$val_tmp) {
                            if($r_tmp == 0){
                                 echo "<tr>
                                        <td rowspan=\"".count($data_tmp_penelitian)."\" style=\"vertical-align: top;\"><b>3.</b> </td>
                                        <td rowspan=\"".count($data_tmp_penelitian)."\" style=\"vertical-align: top;\"><font size=\"3px\"><b>Lokasi / Tujuan Kegiatan</b></font></td>
                                        <td width=\"1%\"><b>&nbsp;:&nbsp;</b></td>
                                        <td width=\"1%\"><b>".$no."). </b></td>
                                        <td width=\"50%\">".$dinas[$val_tmp]."</td>
                                    </tr> ";
                            }else{
                                echo "<tr>
                                        <td width=\"1%\"><b>&nbsp;&nbsp;</b></td>
                                        <td width=\"1%\"><b>".$no."). </b></td>
                                        <td width=\"50%\">".$dinas[$val_tmp]."</td>
                                    </tr>";
                            }
                            $no++;
                        }
                    // }else{
                    //     echo "<tr>
                    //             <td><b>3.</b> </td>
                    //             <td><font size=\"3px\"><b>Lokasi / Tujuan Kegiatan</b></font></td>
                    //             <td width=\"1%\"><b>&nbsp;:&nbsp;</b></td>
                    //             <td width=\"1%\"><b>1)</b></td>
                    //             <td width=\"50%\">".$dinas[$tempat_penelitian]."</td>
                    //         </tr> ";
                    // }
                ?>
                
                            <?php
                                $data_tgl_start = explode("-",$tgl_start);
                                $str_tgl_st = ($data_tgl_start[2]+0)." ".$m[(int)$data_tgl_start[1]]." ".$data_tgl_start[0];
                                
                                $data_tgl_fn = explode("-",$tgl_selesai);
                                $str_tgl_fn = ($data_tgl_fn[2]+0)." ".$m[(int)$data_tgl_fn[1]]." ".$data_tgl_fn[0];
                            
                            ?> 

            <!-- 4 -->
                <tr>
                    <td><b>4.</b></td>
                    <td><font size="3px"><b>Waktu Kegiatan</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;<b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= $str_tgl_st; ?> s/d <?= $str_tgl_fn; ?></td>
                </tr>

            <!-- 5 -->
                <tr>
                    <td><b>5.</b></td>
                    <td><font size="3px"><b>Tema / Judul</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;<b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= $judul; ?></td>
                </tr>
            <!-- 6 -->
                 <tr>
                    <td><b>6.</b></td>
                    <td><font size="3px"><b>Jumlah Peserta</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;<b></td>
                    <td width="1%"></td>
                    <td width="50%"><?= $count_anggota+1; ?> Orang Anggota</td>
                </tr>
            <!-- 7 -->
                 <tr>
                    <td><b>7.</b> </td>
                    <td><font size="3px"><b>Dosen Pembimbing</b></font></td>
                    <td width="1%"><b>&nbsp;:&nbsp;</b></td>
                    <td width="1%"><b></b></td>
                    <td width="50%"><?php echo implode(", ", json_decode(str_replace("'","\"",$str_dosen_pem)));?></td>
                </tr> 
                    <!-- <tr>
                    <td></td>
                    <td></td>
                    <td width="1%"><b>&nbsp;&nbsp;</b></td>
                    <td width="1%"><b>2)</b></td>
                    <td width="50%"></td>
                    </tr> -->
            </table>
      
                <?php 
                // $tmp = strpos($lokasi_penelitan, ";");
                // if($tmp){
                //     $data_tmp = explode(";",$lokasi_penelitan);
                //     $no = 1;
                //     foreach($data_tmp as $val_tmp){
                //         echo "<tr>
                //                 <td width=\"3%\">".$no.".&nbsp;</td>
                //                 <td width=\"95%\">".$dinas[$val_tmp]."</td>
                //             </tr>";
                //         $no++;
                //     }
                // }else{
                //     $no = 1;
                //     echo "<tr>
                //                 <td width=\"3%\">".$no.".&nbsp;</td>
                //                 <td width=\"95%\">".$dinas[$lokasi_penelitan]."</td>
                //             </tr>";
                // }
                ?>
            <br>

            <div style="border-bottom:3px dotted #000;"></div>
           
            <br>
            
            <table border="0" width="100%">
            <tr>
                    <td width="50%" align="left"><font size="3px" align="right"><b>Berkas diterima,</b></font></td>
                    <td width="50%" align="right"><font size="3px" align="right"><b>No Register : <?= $no_register;?></b></font></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="50%" align="left"><font size="3px" align="right">
                        <b>Tanggal, &nbsp;<?php echo (date("d")+0)." ".$m[(int)date("m")]." ".date("Y");?>, 
                            Jam <?= date("h.i");?></b></font>
                    </td>
                    <td width="50%" align="center"><font size="3px" align="right"><b></b></font></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="50%" align="center"><font size="3px" align="right"><b>PETUGAS</b></font></td>
                    <td width="50%" align="center"><font size="3px" align="right"><b>PEMOHON</b></font></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
          
                <tr>
                    <td width="50%" align="center"><font size="3px" align="right"><b>(<?= ucwords($admin_acc);?>)</b></font></td>
                    <td width="50%" align="center"><font size="3px" align="right"><b>(<?= ucwords($nama);?>)</b></font></td>
                </tr>
            </table>
            <br>
            <br>
            <font size="3px" align="right"><b>Kelengkapan yang dibutuhkan : Fotocopy KTP/ Materai/ Proposal/ Lampiran Peserta/ Legalisir Kampus.</b></font>
            
                        

                       

    </section>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url(); ?>pmp/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>pmp/js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url(); ?>pmp/js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo base_url(); ?>pmp/js/active.js"></script>
</body>

</html>