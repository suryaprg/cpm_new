<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Superadmin extends CI_Controller {

    public function __construct(){
        parent::__construct();  
        $this->load->model('admin_super/super_admin', 'as');

        $this->load->model('admin/admin', 'ad');
        $this->load->model('admin/admin_new', 'adn');

        $this->load->model('user/main_data_user_new', 'mdun');

        $this->load->model('user/main_data_user', 'mdu');
        $this->load->model('user/main_data_doc', 'mdd');
        $this->load->model('user/main_data_prev_all', 'mdpa');
        
        $this->load->library("response_message");
        
        //print_r("<pre>");
        //print_r($_SESSION);
        $session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"] != 1 && $session["id_lv"] != 1 && $session["is_log"] != 1){
                redirect(base_url()."back-admin/login");
            }
        }else{
            redirect(base_url()."back-admin/login");
        }
    }

#=============================================================================#
#-------------------------------------------Varifikasi Data All---------------#
#=============================================================================#

    public function get_verifikation_data($id_user, $id_pemohon){
        $data_permohonan_where = array(
                                    "id_pemohon"=>$id_pemohon);
        $data_permohonan = $this->mdun->get_pemohon($data_permohonan_where);

        $data_user_where = array(
                                "id_user"=>$id_user);
        $data_user = $this->mdun->get_data_user($data_user_where);

    //set default value for verifikation

        $list_vert_user = array(
                            "email"=>"0",
                            "nama"=>"0",
                            "alamat"=>"0",
                            "alamat_dom"=>"0",
                            "nik"=>"0",
                            "tlp"=>"0",
                            "tgl_lhr"=>"0",
                            "url_profil"=>"0",
                            "pekerjaan"=>"0",
                            "instansi"=>"0"
                        );

        $list_vert_permohonan = array(
                        "judul" => "0",
                        "permohonan" => "0",
                        "tgl_st" => "0",
                        "tgl_fn" => "0",
                        "tmp" => "0",
                        "pengurusan" => "0",
                        "dosen" => "0",
                );

        $list_vert_delegasi = array(
                        "nik_del" => "0",
                        "nama_del" => "0",
                        "alamat_ktp_del" => "0",
                        "alamat_dom_del" => "0",
                        "url_foto_del" => "0",
                        "url_ktp_del" => "0",
                        "url_srt_del" => "0"
                );


        $list_vert_doc = array(
                        "nama_tdd" => "0",
                        "no_surat_tdd" => "0",
                        "tgl_surat_tdd" => "0",
                        "jabatan_tdd" => "0",
                        "url_ktp_pemohon" => "0",
                        "url_proposal_pemohon" => "0",
                        "url_sk_pemohon" => "0"
                );

        $list_vert_all = array(
                    "vert_user"=>"0",
                    "vert_permohonan"=>"0",
                    "vert_delegasi"=>"0",
                    "vert_doc"=>"0"
                );

    //vert user data in database
        $email = $data_user["email"];
        $nama = $data_user["nama"];
        $alamat = $data_user["alamat"];
        $alamat_dom = $data_user["alamat_dom"];
        $nik = $data_user["nik"];
        $tlp = $data_user["tlp"];
        $tgl_lhr = $data_user["tgl_lhr"];
        $url_profil = $data_user["url_profil"];
        $pekerjaan = $data_user["pekerjaan"];
        $instansi = $data_user["instansi"];

        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $list_vert_user["email"] = "1";
        }

        if (!empty($nama) && $nama != "") {
            $list_vert_user["nama"] = "1";
        }

        if (!empty($alamat) && $alamat != "") {
            $list_vert_user["alamat"] = "1";
        }

        if (!empty($alamat_dom) && $alamat_dom != "") {
            $list_vert_user["alamat_dom"] = "1";
        }

        if (!empty($nik) && $nik != "") {
            $list_vert_user["nik"] = "1";
        }

        if (!empty($tlp) && $tlp != "") {
            $list_vert_user["tlp"] = "1";
        }

        if (!empty($tgl_lhr) && $tgl_lhr != "") {
            $list_vert_user["tgl_lhr"] = "1";
        }

        if (!empty($url_profil) && $url_profil != "") {
            if(file_exists('./doc/foto_pemohon/'.$url_profil)){
                $list_vert_user["url_profil"] = "1";
            }
        }
        if ($pekerjaan != "") {
            $list_vert_user["pekerjaan"] = "1";
        }
        if (!empty($instansi) && $instansi != "") {
            $list_vert_user["instansi"] = "1";
        }

    //vert main pemohon in database
        $judul = $data_permohonan["judul"];
        $permohonan = $data_permohonan["id_permohonan"];
        $tgl_st = $data_permohonan["tgl_start"];
        $tgl_fn = $data_permohonan["tgl_selesai"];
        $tmp = $data_permohonan["id_bidang"];
        $pengurusan = $data_permohonan["status_pendelegasian"];
        $dosen = $data_permohonan["dosen_pembimbing"];
        

        if (!empty($judul) && $judul != "") {
            $list_vert_permohonan["judul"] = "1";
        }
        if (!empty($permohonan) && $permohonan != "") {
            $list_vert_permohonan["permohonan"] = "1";
        }
        if (!empty($tgl_st) && $tgl_st != "") {
            $list_vert_permohonan["tgl_st"] = "1";
        }
        if (!empty($tgl_fn) && $tgl_fn != "") {
            $list_vert_permohonan["tgl_fn"] = "1";
        }
        if (!empty($tmp) && $tmp != "") {
            $list_vert_permohonan["tmp"] = "1";
        }
        if ($pengurusan != "") {
            $list_vert_permohonan["pengurusan"] = "1";
        }
        if (!empty($dosen) && $dosen != "") {
            $list_vert_permohonan["dosen"] = "1";
        }
        
    //vert main pemohon delegasi in database
        $nik_del = $data_permohonan["nik_del"];
        $nama_del = $data_permohonan["nama_del"];
        $alamat_ktp_del = $data_permohonan["alamat_ktp_del"];
        $alamat_dom_del = $data_permohonan["alamat_dom_del"];
        $url_foto_del = $data_permohonan["url_foto_del"];
        $url_ktp_del = $data_permohonan["url_ktp_del"];
        $url_srt_del = $data_permohonan["url_srt_del"];
        

        if (!empty($nik_del) && $nik_del != "") {
            $list_vert_delegasi["nik_del"] = "1";
        }
        if (!empty($nama_del) && $nama_del != "") {
            $list_vert_delegasi["nama_del"] = "1";
        }
        if (!empty($alamat_ktp_del) && $alamat_ktp_del != "") {
            $list_vert_delegasi["alamat_ktp_del"] = "1";
        }
        if (!empty($alamat_dom_del) && $alamat_dom_del != "") {
            $list_vert_delegasi["alamat_dom_del"] = "1";
        }
        if (!empty($url_foto_del) && $url_foto_del != "") {
            if(file_exists('./doc/foto_delegasi/'.$url_foto_del)){
                $list_vert_delegasi["url_foto_del"] = "1";
            }             
        }
        if (!empty($url_ktp_del) && $url_ktp_del != "") {
            if(file_exists('./doc/ktp_delegasi/'.$url_ktp_del)){
                $list_vert_delegasi["url_ktp_del"] = "1";
            }            
        }
        if (!empty($url_srt_del) && $url_srt_del != "") {
            if(file_exists('./doc/srt_kuasa/'.$url_srt_del)){
                $list_vert_delegasi["url_srt_del"] = "1";
            }            
        }  
      
    //vert main pemohon document in database
        $nama_tdd = $data_permohonan["nama_tdd"];
        $no_surat_tdd = $data_permohonan["no_surat_tdd"];
        $tgl_surat_tdd = $data_permohonan["tgl_surat_tdd"];
        $jabatan_tdd = $data_permohonan["jabatan_tdd"];
        $url_ktp_pemohon = $data_permohonan["url_ktp_pemohon"];
        $url_proposal_pemohon = $data_permohonan["url_proposal_pemohon"];
        $url_sk_pemohon = $data_permohonan["url_sk_pemohon"];
        

        if (!empty($nama_tdd) && $nama_tdd != "") {
            $list_vert_doc["nama_tdd"] = "1";
        }
        if (!empty($no_surat_tdd) && $no_surat_tdd != "") {
            $list_vert_doc["no_surat_tdd"] = "1";
        }
        if (!empty($tgl_surat_tdd) && $tgl_surat_tdd != "") {
            $list_vert_doc["tgl_surat_tdd"] = "1";
        }
        if (!empty($jabatan_tdd) && $jabatan_tdd != "") {
            $list_vert_doc["jabatan_tdd"] = "1";
        }
        if (!empty($url_ktp_pemohon) && $url_ktp_pemohon != "") {
            if(file_exists('./doc/ktp/'.$url_ktp_pemohon)){
                $list_vert_doc["url_ktp_pemohon"] = "1";
            }            
        }
        if (!empty($url_proposal_pemohon) && $url_proposal_pemohon != "") {
            if(file_exists('./doc/proposal/'.$url_proposal_pemohon)){
                $list_vert_doc["url_proposal_pemohon"] = "1";
            }            
        }
        if (!empty($url_sk_pemohon) && $url_sk_pemohon != "") {
            if(file_exists('./doc/sk/'.$url_sk_pemohon)){
                $list_vert_doc["url_sk_pemohon"] = "1";
            }            
        }
        
    //vert all data in database

        if(!in_array("0", $list_vert_user)){
            $list_vert_all["vert_user"] = "1"; 
        }

        if(!in_array("0", $list_vert_permohonan)){
            $list_vert_all["vert_permohonan"] = "1"; 
        }

        if(!in_array("0", $list_vert_delegasi)){
            $list_vert_all["vert_delegasi"] = "1"; 
        }

        if(!in_array("0", $list_vert_doc)){
            $list_vert_all["vert_doc"] = "1"; 
        }

        if($pengurusan == "0"){
            $list_vert_all["vert_delegasi"] = "1";
        }

    //vert all data return database
        $main_vert_all = false;
        if(!in_array("0", $list_vert_all)){
            $main_vert_all = true;
        }

        return array(
                    "main_vert_all" => $main_vert_all,
                    "list_vert_all" => $list_vert_all
                );
        
    }
#=============================================================================#
#-------------------------------------------Varifikasi Data All---------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Home------------------------#
#=============================================================================#
    public function index(){
        $data["dinas"] = $this->as->get_dinas();
        $data["page"] = "super_home";

        $data["pemohon_req_p"] = count($this->adn->get_pemohon(array("1")));
        $data["pemohon_acc_p"] = count($this->adn->get_pemohon_acc_active(array("1")));
        $data["pemohon_rem_p"] = count($this->adn->get_pemohon_report(array("1")));

        $data["pemohon_all_p"] = count($this->adn->get_pemohon_all(array("1")));

        $data["pemohon_list_p"] = array(); 
        
        $th = date("Y");
        for ($i= 1; $i <= 12 ; $i++) { 
            $data["pemohon_list_p"][$i] = count($this->adn->get_pemohon_all_graph(array("1"), (string)$th, (string)$i));
        }


        $data["pemohon_req_m"] = count($this->adn->get_pemohon(array("0")));
        $data["pemohon_acc_m"] = count($this->adn->get_pemohon_acc_active(array("0")));
        $data["pemohon_rem_m"] = count($this->adn->get_pemohon_report(array("0")));

        $data["pemohon_all_m"] = count($this->adn->get_pemohon_all(array("0")));

        // print_r("<pre>");
        $data["pemohon_list_m"] = array(); 
        
        $th = date("Y");
        for ($i= 1; $i <= 12 ; $i++) { 
            $data["pemohon_list_m"][$i] = count($this->adn->get_pemohon_all_graph(array("0"), (string)$th, (string)$i));
        }

        $this->load->view('admin_super_new',$data);
    }
#=============================================================================#
#-------------------------------------------Index_Home------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#
    public function index_admin(){
        $data["admin"]  = $this->as->get_admin_all();
        $data["dinas"]  = $this->as->get_dinas();
        $data["lv"]     = $this->as->get_lv_admin(array("is_del"=>"0"));
        $data["page"]   = "super_admin";

        $this->load->view('admin_super_new',$data);
    }

    public function val_form(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails|is_unique[admin.email]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )
                       
                ),
                array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'nip',
                    'label'=>'Nomor Induk Pegawai',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'jbtn',
                    'label'=>'Jabatan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'dinas',
                    'label'=>'Nama Dinas',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'lv',
                    'label'=>'Lv. Admin',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'pass',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'repass',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "nama"=>"",
                    "nip"=>"",
                    "jabatan"=>"",
                    "id_bidang"=>"",
                    "lv"=>"",
                    "pass"=>"",
                    "repass"=>""
                );

        if($this->val_form()){
            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $nip = $this->input->post("nip");
            $jabatan = $this->input->post("jbtn");
            $id_bidang = $this->input->post("dinas");
            $lv = $this->input->post("lv");
            $pass = $this->input->post("pass");
            $repass = $this->input->post("repass");

            $admin_del = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update = date("Y-m-d h:i:s");

            if ($pass == $repass) {
                // print_r($_POST);
                $insert = $this->db->query("select func_insert_admin('".$nama."','".$email."','".$nip."','".$jabatan."','".$id_bidang."','".$lv."','".md5($pass)."','".$admin_del."','".$time_update."')");

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "email"=>strip_tags(form_error('email')),
                            "nama"=>strip_tags(form_error('nama')),
                            "nip"=>strip_tags(form_error('nip')),
                            "jabatan"=>strip_tags(form_error('jbtn')),
                            "id_bidang"=>strip_tags(form_error('dinas')),
                            "lv"=>strip_tags(form_error('lv')),
                            "pass"=>strip_tags(form_error('pass')),
                            "repass"=>strip_tags(form_error('repass'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
        // $this->session->set_flashdata("response_user", $res_msg);
        // redirect(base_url()."super/admin");
    }
    

    public function get_admin_update(){
        $id = $this->input->post("id_admin");
        // print_r($id);
        $data = $this->as->get_where_admin(array("id_admin"=>$id));
        // print_r($data);
        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }
    
    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'nip',
                    'label'=>'Nomor Induk Pegawai',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'jbtn',
                    'label'=>'Jabatan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'dinas',
                    'label'=>'Nama Dinas',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'lv',
                    'label'=>'Lv. Admin',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "nama"=>"",
                    "nip"=>"",
                    "jabatan"=>"",
                    "id_bidang"=>"",
                    "lv"=>""
                );

        if($this->val_form_update()){
            $id_admin = $this->input->post("id_admin");

            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $nip = $this->input->post("nip");
            $jabatan = $this->input->post("jbtn");
            $id_bidang = $this->input->post("dinas");
            $lv = $this->input->post("lv");

            $admin_del = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update = date("Y-m-d h:i:s");

            if($this->as->get_where_admin_ex(array("email"=>$email, "id_admin!="=>$id_admin))){
                $msg_detail["email"] = "email sudah terdaftar, silahkan gunakan email yang belum terdaftar";
            }else{
                $set = array(
                        "nama"=>$nama,
                        "email"=>$email,
                        "nip"=>$nip,
                        "jabatan"=>$jabatan,
                        "id_bidang"=>$id_bidang,
                        "id_lv"=>$lv,
                        "admin_del"=>$admin_del,
                        "time_update"=>$time_update
                    );

                $where = array(
                            "id_admin"=>$id_admin
                        );

                $update = $this->as->update_admin($set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }            
        }else{
            $msg_detail["email"] = strip_tags(form_error('email'));
            $msg_detail["nama"] = strip_tags(form_error('nama'));
            $msg_detail["nip"] = strip_tags(form_error('nip'));
            $msg_detail["jabatan"] = strip_tags(form_error('jbtn'));
            $msg_detail["id_bidang"] = strip_tags(form_error('dinas'));
            $msg_detail["lv"] = strip_tags(form_error('lv'));
                            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'id',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete()){
            $id_admin = $this->input->post("id_admin");

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_del"=>$is_del,
                    "time_del"=>$time_del
                );

            $where = array("id_admin"=>$id_admin);

            if($this->as->update_admin($set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }


    public function get_password(){
        $res_array = array(
                    "status"=>false,
                    "val_key"=>""
                );

        if(isset($_POST["id_admin"])){
            $id_admin = $this->input->post("id_admin");
            $data = $this->as->get_where_admin_ex(array("id_admin"=>$id_admin));

            $res_array = array(
                    "status"=>true,
                    "val_key"=>hash('sha256', $data[0]->password)
                );
        }
        print_r(json_encode($res_array));
    }

    private function validation_change_pass(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'id',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'key',
                    'label'=>'key',
                    'rules'=>'required|alpha_numeric|exact_length[64]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR"),
                        'exact_length[64]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    )
                       
                ),


                array(
                    'field'=>'pass',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'repass',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "pass"=>"",
                    "repass"=>""
                );

        if($this->validation_change_pass()){
            $id_admin = $this->input->post("id_admin");
            $key = $this->input->post("key");

            $pass = $this->input->post("pass");
            $repass = $this->input->post("repass");

            if($pass == $repass){
                $data_admin = $this->as->get_where_admin_ex(array("id_admin"=>$id_admin));
                if(hash('sha256', $data_admin[0]->password) == $key){
                    $set = array(
                            "password"=>md5($pass),
                        );

                    $where = array(
                                "id_admin"=>$id_admin
                            );

                    $update = $this->as->update_admin($set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }else{
                    // $msg_main = array("status"=>false, "msg"=>"key salah");
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
            }            
        }else{
            // $msg_detail["id_admin"] = strip_tags(form_error('id_admin'));
            // $msg_detail["key"] = strip_tags(form_error('key'));

            $msg_detail["pass"] = strip_tags(form_error('pass'));
            $msg_detail["repass"] = strip_tags(form_error('repass'));                            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Lv_Admin--------------------#
#=============================================================================#
    public function index_admin_lv(){
        $data["lv"]     = $this->as->get_lv_admin(array("is_del"=>"0"));
        $this->load->view('admin_super_new/super_admin_lv',$data);
    }

    public function validation_admin_lv(){
         $config_val_input = array(
                array(
                    'field'=>'ket_lv',
                    'label'=>'Keterangan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin_lv(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "ket_lv"=>""
                    );
        if($this->validation_admin_lv()){
            $ket_lv = $this->input->post("ket_lv");
            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];

            $data = array(
                        "id_lv"=>"",
                        "ket"=>$ket_lv,
                        "is_del"=>"0",
                        "time_del"=>"0000-00-00 00:00:00.000000",
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            if($this->as->insert_lv_admin($data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else{
            $msg_detail = array(
                        "ket_lv"=>strip_tags(form_error("ket_lv"))
                    );
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        $this->session->set_flashdata("response_lv", $res_msg);
        redirect(base_url()."admin_super/superadmin/index_admin_lv");
    }

    public function delete_admin_lv(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));

        $id_lv = $this->input->post("id_lv");
        $data = array(
                    "is_del"=>"1",
                    "time_del"=>date("Y-m-d h:i:s")
                );
        if($this->as->update_lv_admin($data, array("id_lv"=>$id_lv))){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }
        

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        $this->session->set_flashdata("response_lv", $res_msg);
        redirect(base_url()."admin_super/superadmin/index_admin_lv");
     }
#=============================================================================#
#-------------------------------------------Index_Lv_Admin--------------------#
#=============================================================================#    
    
#=============================================================================#
#-------------------------------------------Index_Dinas-----------------------#
#=============================================================================#
  
    public function index_dinas(){
        $data["dinas"] = $this->as->get_dinas_active();
        $data["page"] = "super_dinas";

        // print_r($data);
        $this->load->view('admin_super_new',$data);
    }
    
    public function validate_dinas(){
        $config_val_input = array(
                array(
                    'field'=>'nm_dinas',
                    'label'=>'Nama Dinas',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'alamat',
                    'label'=>'Alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_dinas(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "nm_dinas"=>"",
                        "alamat"=>""
                    );
        if($this->validate_dinas()){
            $nm_dinas = $this->input->post("nm_dinas");
            $alamat = $this->input->post("alamat");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];

            $data = array(
                        "nama_dinas"=>$nm_dinas,
                        "alamat"=>$alamat,
                        "is_del"=>"0",
                        "time_del"=>"0000-00-00 00:00:00.000000",
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );
            if($this->as->insert_dinas($data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else {
            $msg_detail["nm_dinas"] = strip_tags(form_error('nm_dinas'));
            $msg_detail["alamat"] = strip_tags(form_error('alamat'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get_dinas_update(){
        $id = $this->input->post("id_dinas");
        // print_r($id);
        $data = $this->as->get_where_dinas(array("id_dinas"=>$id));
        // print_r($data);
        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));

    }


    public function update_dinas(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                        "nm_dinas"=>"",
                        "alamat"=>""
                    );
        if($this->validate_dinas()){
            $nm_dinas = $this->input->post("nm_dinas");
            $alamat = $this->input->post("alamat");

            $id_dinas = $this->input->post("id_dinas");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"]; 

            $data = array(
                        "nama_dinas"=>$nm_dinas,
                        "alamat"=>$alamat,
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                    "id_dinas"=>$id_dinas
                );
            if($this->as->update_dinas($data, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else {
            $msg_detail["nm_dinas"] = strip_tags(form_error('nm_dinas'));
            $msg_detail["alamat"] = strip_tags(form_error('alamat'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function delete_dinas(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $res_msg = $this->response_message->default_mgs($msg_main, null);

        if(isset($_POST["id_dinas"])){

            $id_dinas = $this->input->post("id_dinas");
            $data = array(
                        "is_del"=>"1",
                        "time_del"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                        "id_dinas"=>$id_dinas
                    );

            $update = $this->as->update_dinas($data, $where);

            if($update){
               $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
               
            }else{
               $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
            }
            
        }
        
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Dinas-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Jenis_Permohonan------------#
#=============================================================================#
    public function index_permohonan(){
        $data["permohonan"] = $this->as->get_permohonan_active();
        $data["page"] = "super_permohonan";

        $this->load->view('admin_super_new',$data);
    }
    
    public function validate_permohonan(){
        $config_val_input = array(
                array(
                    'field'=>'jenis_kegiatan',
                    'label'=>'Jenis Kegiatan',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'keterangan_permohonan',
                    'label'=>'Keterangan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_permohonan(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "jenis_kegiatan"=>"",
                        "keterangan_permohonan"=>""
                    );
        if($this->validate_permohonan()){
            $jenis_kegiatan = $this->input->post("jenis_kegiatan");
            $keterangan_permohonan = $this->input->post("keterangan_permohonan");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];

            $data = array(
                        "id_permohonan"=>"",
                        "jenis_kegiatan"=>$jenis_kegiatan,
                        "keterangan_permohonan"=>$keterangan_permohonan,
                        "is_del"=>"0",
                        "time_del"=>"0000-00-00 00:00:00.000000",
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );
            if($this->as->insert_permohonan($data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else {
            $msg_detail["jenis_kegiatan"] = strip_tags(form_error('jenis_kegiatan'));
            $msg_detail["keterangan_permohonan"] = strip_tags(form_error('keterangan_permohonan'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get_permohonan_update(){
        $id = $this->input->post("id_permohonan");
        $data = $this->as->get_where_permohonan(array("id_permohonan"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));

    }


    public function update_permohonan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                        "jenis_kegiatan"=>"",
                        "keterangan_permohonan"=>""
                    );
        if($this->validate_permohonan()){
            $jenis_kegiatan = $this->input->post("jenis_kegiatan");
            $keterangan_permohonan = $this->input->post("keterangan_permohonan");

            $id_permohonan = $this->input->post("id_permohonan");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"]; 

            $data = array(
                        "jenis_kegiatan"=>$jenis_kegiatan,
                        "keterangan_permohonan"=>$keterangan_permohonan,
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                    "id_permohonan"=>$id_permohonan
                );
            if($this->as->update_permohonan($data, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else {
            $msg_detail["jenis_kegiatan"] = strip_tags(form_error('jenis_kegiatan'));
            $msg_detail["keterangan_permohonan"] = strip_tags(form_error('keterangan_permohonan'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function delete_permohonan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $res_msg = $this->response_message->default_mgs($msg_main, null);

        if(isset($_POST["id_permohonan"])){

            $id_permohonan = $this->input->post("id_permohonan");
            $data = array(
                        "is_del"=>"1",
                        "time_del"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                        "id_permohonan"=>$id_permohonan
                    );

            $update = $this->as->update_permohonan($data, $where);

            if($update){
               $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
               
            }else{
               $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
            }
            
        }
        
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Jenis_Permohonan------------#
#=============================================================================# 

#=============================================================================#
#-------------------------------------------Index_User------------------------#
#=============================================================================#
    public function index_user(){
        $data["user"] = $this->as->get_user_active();
        $data["page"] = "super_user";
        
        $this->load->view('admin_super_new',$data);
    }

    public function detail_user(){
        $id = $this->input->post("id_user");
        $data_user = $this->as->get_where_user(array("id_user"=>$id));
        
        $data["status"] = false;
        $data["val_response"] = null;

        if(!empty($data_user)){
            $data["status"] = true;
            $data["val_response"] = $data_user;
        }

        print_r(json_encode($data));
    }
    
    public function delete_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $res_msg = $this->response_message->default_mgs($msg_main, null);

        if(isset($_POST["id_user"])){
            $id_user = $this->input->post("id_user");
            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"]; 

            $data = array(
                        "is_del"=>"1",
                        "admin_del"=>$id_admin,
                        "time_del"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                        "id_user"=>$id_user
                    );

            $update = $this->as->update_user($data, $where);

            if($update){
               $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
               
            }else{
               $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
            }
            
        }
        
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_User------------------------#
#=============================================================================#
    
#=============================================================================#
#-------------------------------------------Index_Pemohon_Magang--------------#
#=============================================================================#
    public function index_magang(){
        $data["page"] = "super_magang";
        $data["pemohon"] = $this->as->get_pemohon_active(array("1"));

        $data["tmp_magang"] = "";
        $data_tmp_magang = $this->mdun->get_tmp_magang();

        foreach ($data_tmp_magang as $r_data => $v_data) {
            $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
        }
        
        $this->load->view('admin_super_new',$data);
    }
    
    public function delete_pemohon(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $res_msg = $this->response_message->default_mgs($msg_main, null);

        if(isset($_POST["id_pemohon"])){
            $id_pemohon = $this->input->post("id_pemohon");
            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"]; 

            $data = array(
                        "is_del"=>"1",
                        "admin_del"=>$id_admin,
                        "time_del"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                        "id_pemohon"=>$id_pemohon
                    );

            $update = $this->as->update_pemohon($data, $where);

            if($update){
               $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
               
            }else{
               $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
            }
            
        }
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Pemohon_Magang--------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Pemohon_Penelitian----------#
#=============================================================================#
    public function index_penelitian(){
        $data["page"] = "super_penelitian";
        $data["pemohon"] = $this->adn->get_pemohon_report(array("0"));

        $data["tmp_magang"] = "";
        $data_tmp_magang = $this->mdun->get_tmp_magang();

        foreach ($data_tmp_magang as $r_data => $v_data) {
            $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
        }
        
        $this->load->view('admin_super_new',$data);
    }
#=============================================================================#
#-------------------------------------------Index_Pemohon_Penelitian----------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Pemohon_Detail--------------------#
#=============================================================================#
    public function detail_pemohon_active(){
        if(isset($_POST["id_pemohon"])){
            $id_pemohon = $this->input->post("id_pemohon");
            $data_pendaftaran = $this->mdun->get_pemohon(array("id_pemohon"=>$id_pemohon));
            $data_vert_all = $this->get_verifikation_data($data_pendaftaran["id_user"], $id_pemohon);

            $data["pendaftaran"] = $data_pendaftaran;
            $data["permohonan"] = $this->mdun->get_keperluan(array("jenis_kegiatan"=>"1"));    

            $data["tmp_magang"] = "";
            $data_tmp_magang = $this->mdun->get_tmp_magang();

            foreach ($data_tmp_magang as $r_data => $v_data) {
                $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
            }

            $data["tmp_magang_select"] = $this->mdun->get_tmp_magang();

            $data["user"] = $this->mdun->get_data_user(array("id_user"=>$data_pendaftaran["id_user"]));
            $data["id_pemohon"] = $data_pendaftaran["id_pemohon"];
            $data["get_verifikation_data"] = $data_vert_all;

            // print_r(var, return)
            $this->load->view("admin_super_new/super_detail_active",$data);
        }
    }
#=============================================================================#
#-------------------------------------------Pemohon_Detail--------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------detail_pemohon_penelitian---------#
#=============================================================================#
    public function detail_pemohon_penelitian(){
        if(isset($_POST["id_pemohon"])){
            $id_pemohon = $this->input->post("id_pemohon");
            $data_pendaftaran = $this->mdun->get_pemohon(array("id_pemohon"=>$id_pemohon));
            $data_vert_all = $this->get_verifikation_data($data_pendaftaran["id_user"], $id_pemohon);

            $data["pendaftaran"] = $data_pendaftaran;
            $data["permohonan"] = $this->mdun->get_keperluan(array("jenis_kegiatan"=>"1"));    

            $data["tmp_magang"] = "";
            $data_tmp_magang = $this->mdun->get_tmp_magang();

            foreach ($data_tmp_magang as $r_data => $v_data) {
                $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
            }

            $data["tmp_magang_select"] = $this->mdun->get_tmp_magang();

            $data["user"] = $this->mdun->get_data_user(array("id_user"=>$data_pendaftaran["id_user"]));
            $data["id_pemohon"] = $data_pendaftaran["id_pemohon"];
            $data["get_verifikation_data"] = $data_vert_all;

            // print_r(var, return)
            $this->load->view("admin_super_new/super_detail_penelitian",$data);
        }
    }
#=============================================================================#
#-------------------------------------------detail_pemohon_penelitian---------#
#=============================================================================#
    
}
