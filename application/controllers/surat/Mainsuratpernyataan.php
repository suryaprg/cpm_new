<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainsuratpernyataan extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('user/main_data_user_new', 'mdun');
        $this->load->model("admin/admin_new", "ad");
        
        $this->load->library("response_message");
        $this->load->library("generate_token");
        $this->load->library("sendemail");

        //$this->load->model('Users_model');
        // $session = $this->session->userdata("admin_lv_1");
        // if(isset($session)){
        //     if($session["status_active"]== 1 and $session["is_log"] == 1){
        //         if($session["id_lv"] != 2){
        //             redirect(base_url()."admin/login");    
        //         }
                
        //     }
        // }else{
        //     redirect(base_url()."admin/login");
        // }
        
    }

    public function user_surat_pernyataan(){
        if($_POST["id_pemohon"]){
            $id_pemohon = $this->input->post("id_pemohon");
            $data["tmp_magang"] = "";
            $data_tmp_magang = $this->mdun->get_tmp_magang();

            foreach ($data_tmp_magang as $r_data => $v_data) {
                $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
            }

            $data["pendaftaran"] = $this->mdun->get_pemohon(array("id_pemohon"=>$id_pemohon));
            $data["user"] = $this->mdun->get_data_user(array("id_user"=>$data["pendaftaran"]["id_user"]));

            $this->load->view("user_surat/v_surat_pernyataan_magang", $data);
        }else {
            redirect(base_url()."pendaftaran/history");
        }
    }

    public function form_register(){
        if($_POST["id_pemohon"]){
            $id_pemohon = $this->input->post("id_pemohon");
            $data["pemohon"] = $this->ad->get_pemohon_surat(array("pe.id_pemohon"=>$id_pemohon));
            
            $dinas = $this->mdun->get_tmp_magang();
            foreach ($dinas as $val_dinas) {
                $data["dinas"][$val_dinas->id_dinas] = $val_dinas->nama_dinas;
            }

            $this->load->view("user_surat/v_formulir_permohonan", $data);
        }else {
            redirect(base_url()."bakesbangpol/monitor");
        }
    }

    public function surat_rujukan(){
        if($_POST["id_pemohon"]){
            $id_pemohon = $this->input->post("id_pemohon");
            $data["pemohon"] = $this->ad->get_pemohon_surat(array("pe.id_pemohon"=>$id_pemohon));
            
            $dinas = $this->mdun->get_tmp_magang();
            foreach ($dinas as $val_dinas) {
                $data["dinas"][$val_dinas->id_dinas] = $val_dinas->nama_dinas;
            }

            $this->load->view("user_surat/v_RPKL", $data);
        }else {
            redirect(base_url()."bakesbangpol/monitor");
        }
    }
}