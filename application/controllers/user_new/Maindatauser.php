<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindatauser extends CI_Controller {

    function __construct(){
		parent::__construct();
		$this->load->model('user/main_data_user', 'mdu');
        $this->load->model('user/Main_data_prev_all', 'mdpa');
        
        $this->load->model("user/register_user", "reg_user");
        
        $this->load->library("response_message");
        $this->load->library("generate_token");
        $this->load->library("sendemail");
        
        // if($this->session->userdata("user_bangkes")["is_log"] != 1){
        //     redirect(base_url()."home/login");
        // }
	}

#---------------------------------------------------------------------------------------------------Cek Kelengkapan Dokumen----------------------------------------------------------------------------
    
    private function cek_kelengkapan(){
        $id = $this->session->userdata("user_bangkes")["id_user"];

        $where = array(
                        "id_user"=>$id,
                        "no_register"=>"0",
                        "status_magang"=>"0",
                        "status_diterima"=>"0"
                    );

        $user = $this->session->userdata("user_bangkes");
        $pemohon = $this->mdpa->get_kelengkapan($where);

        $id_pemohon = $pemohon["id_pemohon"];

        $doc = $this->mdpa->get_kelengkapan_data(array("id_pemohon"=>$id_pemohon));

        $status_pemohon = 0;
        //cek pemohon
        if($pemohon["jenis_kegiatan"] == 0){
            // penelitian
            if(!empty($pemohon["judul"]) && !empty($pemohon["tgl_start"]) && !empty($pemohon["tgl_selesai"]) && !empty($pemohon["id_bidang"])){
                $status_pemohon = 1;
            }    
        }else {
            // magang

            if(!empty($pemohon["judul"]) && !empty($pemohon["periode"]) && !empty($pemohon["id_bidang"])){
                $status_pemohon = 1;
            }
        }


        $status_document = 0;
        if(!empty($doc["url_ktp"]) && !empty($doc["url_proposal"]) && !empty($doc["url_sk"])){
            $status_document = 1;
        }
        
        $status_daftar = 0;
        if($status_pemohon == true && $status_document == true){
            $status_daftar = 1;
        }



        return array("status_daftar"=>$status_daftar, "status_pemohon"=>$status_pemohon, "status_document"=>$status_document);

    }
    
#---------------------------------------------------------------------------------------------------Index Page----------------------------------------------------------------------------


    public function index(){
        $id = $this->session->userdata("user_bangkes")["id_user"];    
        $where = array(
                        "id_user"=>$id,
                        "no_register"=>"0",
                        "status_magang"=>"0",
                        "status_diterima"=>"0"
                    );    
        
        $data["page"] = "mainform";
        $data["user"] = $this->mdu->get_data_user(array("id_user"=>$id));
        $data["tmp_magang"] = $this->mdu->get_tmp_magang();
        $data["keperluan"] = $this->mdu->get_keperluan(array("jenis_kegiatan"=>1));
        $data["pemohon"] = $this->mdu->get_pemohon($where);
        $data["anggota"] = $this->mdu->get_anggota($data["pemohon"]["id_pemohon"]);
        $data["delegasi"] = $this->mdu->get_delegasi_where(array("id_pemohon"=>$data["pemohon"]["id_pemohon"]));
        
        
        
        $data["keperluan_string"] = "";
        $data["jenis_keperluan_string"] = "";
        foreach($data["keperluan"] as $val_pemohonan){
            $data["keperluan_string"] = $data["keperluan_string"]."'".$val_pemohonan->id_permohonan."', ";
            $data["jenis_keperluan_string"] = $data["jenis_keperluan_string"]."'".$val_pemohonan->jenis_kegiatan."', ";
        } 
        
        foreach($data["tmp_magang"] as $val_tmp){
            $data["tmp_magang"][$val_tmp->id_dinas]= $val_tmp;
        }
        $data["kelengkapan_pemohon"] = $this->cek_kelengkapan();
        print_r("<pre>");
        print_r($data);        
        //$this->load->view("user_data/input_profil", $data);
        // $this->load->view("v_index_mhs_new", $data);
    }
    
    public function index_home(){
        
        $data["magang"] = $this->mdu->get_data_count_pemohon(array("jenis_kegiatan"=>"1"));
        $data["penelitian"] = $this->mdu->get_data_count_pemohon(array("jenis_kegiatan"=>"0"));
        $data['page'] = 'home';
        $data["kelengkapan_pemohon"] = $this->cek_kelengkapan();

        // print_r("<pre>");
        // print_r($data);

        $this->load->view('v_index_mhs_new', $data);
        //echo base_url("uploads/");
    }
    
    public function index_lengkap_form(){
  	  	$data['page'] = 'isi_lengkap_form';
        $this->load->view('v_index_mhs', $data);
        $data["kelengkapan_pemohon"] = $this->cek_kelengkapan();
        //echo base_url("uploads/");
    }

    
    public function index_dokumen(){
        $data['page'] = 'dokumen';
        $data["kelengkapan_pemohon"] = $this->cek_kelengkapan();
        $this->load->view('v_index_mhs', $data);
        //echo base_url("uploads/");
    }

    public function index_cara_input(){
        $data['page'] = 'cara_input';
        $data["kelengkapan_pemohon"] = $this->cek_kelengkapan();
        $this->load->view('v_index_mhs', $data);
        //echo base_url("uploads/");
    }

    public function index_pengumuman(){
        $data['page'] = 'pengumuman';
        $data["kelengkapan_pemohon"] = $this->cek_kelengkapan();
        $this->load->view('v_index_mhs', $data);
        //echo base_url("uploads/");
    }
   
#---------------------------------------------------------------------------------------------------Logout----------------------------------------------------------------------------

    public function logout(){
	   $this->session->unset_userdata("user_bangkes");
	   redirect(base_url('home/login'));
    }
    
    public function get_ses(){
        print_r("<pre>");
        print_r($_SESSION);    
    }
    
    public function index_form_user(){
        $data['page'] = 'form_data_mhs';
        $this->load->view("v_index_mhs", $data);
    }

#---------------------------------------------------------------------------------------------------Insert Pemohon----------------------------------------------------------------------------

    
    private function validate_input_permohonan(){
        $config_val_input = array(
                array(
                    'field'=>'judul',
                    'label'=>'Judul Skripsi', 'alpha_numeric_spaces',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'permohonan',
                    'label'=>'Permohonan',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'tgl_st',
                    'label'=>'Tanggal Mulai',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'tgl_fn',
                    'label'=>'Tanggal Start',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'tmp',
                    'label'=>'Tempat Penelitian/Magang',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'pengurusan',
                    'label'=>'Cara Mengurus',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    private function validate_permohonan_peneitian(){
        $config_val_input = array(
                array(
                    'field'=>'tgl_st',
                    'label'=>'Tanggal Mulai',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'tgl_fn',
                    'label'=>'Tanggal Start',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    private function validate_permohonan_magang(){
        $config_val_input = array(
                array(
                    'field'=>'tgl_st',
                    'label'=>'Tanggal Mulai',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    public function insert_pemohon(){
        // print_r("<pre>");
        // print_r($_POST);
        
        //print_r($this->validate_input_permohonan());
        
        if($this->validate_input_permohonan()){
            $id_pemohon = $this->input->post("id_pemohon");
            
            $judul = $this->input->post("judul");
            $permohonan = $this->input->post("permohonan");
            
            $tmp = $this->input->post("tmp");
            $dosen = $this->input->post("dosen");
            $anggota = $this->input->post("anggota");
            $pengurusan = $this->input->post("pengurusan");
            
            $id = $this->session->userdata("user_bangkes")["id_user"];
            // print_r(json_encode($anggota));
            
            $tgl_st = "0000-00-00";
            $tgl_fn = "0000-00-00";
            $periode = "0";
            
            // echo $this->input->post("periode");
            
            
            $data_permohonan = $this->mdu->get_jenis_permohonan(array("id_permohonan"=>$permohonan));
            // print_r("<pre>");
            // print_r($data_permohonan);
            if($data_permohonan["jenis_kegiatan"] == "0"){
                $tgl_st = $this->input->post("tgl_st");
                $tgl_fn = $this->input->post("tgl_fn");
            }else{
                $periode = $this->input->post("periode")*30;   
            }
            // echo "insert into pemohon values (@id,'".$judul."','".$permohonan."','".$tgl_st."','".$tgl_fn."','".$periode."','".$tmp."','".$id."','".$dosen."','0','0','0','0','0')";
                       
            //surya hanggara 
            if($id_pemohon == ""){
                // echo "insert";
                $where = array(
                            "id_user"=>$id
                        );
                $cek_data_avail = $this->mdu->cek_data_aktif($where);
                // print_r($cek_data_avail);
                if($cek_data_avail <= 0){
                    if($tgl_st < $tgl_fn || $tgl_st == $tgl_fn){
                        // echo "insert into pemohon values (@id,'".$judul."','".$permohonan."','".$tgl_st."','".$tgl_fn."',".$tmp.",'".$id."',".$dosen.",'0','0')";
                        $this->db->trans_start();
                            $this->db->query("set @id = last_key_pemohon()");
                            $this->db->query("insert into pemohon values (@id,'".$judul."','".$permohonan."','".$tgl_st."','".$tgl_fn."','".$periode."','".$tmp."','".$id."','".$dosen."','".$pengurusan."','0','0','0','0','0')");
                            $id_pemohon_return = $this->db->query("select @id as last_id")->row_array();
                        $this->db->trans_complete();
                        // echo "insert into pemohon values (@id,'".$judul."','".$permohonan."','".$tgl_st."','".$tgl_fn."','".$periode."','".$tmp."','".$id."','".$dosen."','0','0','0','0','0')";
                        
                        if($this->db->trans_status()){
                            // echo "ins Suc";
                            //print_r($id_pemohon_return["last_id"]);
                            $list_anggota = json_decode($anggota);
                            $this->delete_anggota($id_pemohon_return["last_id"]);
                            $this->insert_anggota($id_pemohon_return["last_id"],$list_anggota);

                            
                            $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                            $msg_array = $this->response_message->default_mgs($main_msg,null);
                        }else{
                            //insert fail db insert fail
                           
                            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                            $msg_array = $this->response_message->default_mgs($main_msg,null);
                        }
                    }else{
                        //tgl start lebih kecil

                        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DATE_START_FAIL"));
                        $msg_array = $this->response_message->default_mgs($main_msg,null);
                    }    
                }else{
                    //ada data yang belum terpenuhi

                    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("AVAIL_DATA_PENELITIAN"));
                    $msg_array = $this->response_message->default_mgs($main_msg,null);
                }
                
            }else{
                // echo "update";
                    if($tgl_st < $tgl_fn || $tgl_st == $tgl_fn){
                        $where = array("id_pemohon"=>$id_pemohon);
                        $set = array("judul"=>$judul,
                                    "id_permohonan"=>$permohonan,
                                    "tgl_start"=>$tgl_st,
                                    "tgl_selesai"=>$tgl_fn,
                                    "periode"=>$periode,
                                    "id_bidang"=>$tmp,
                                    "dosen_pembimbing"=>$dosen,
                                    "pendelegasian"=>$pengurusan
                                    );
                        // if($this->mdu->delete_delegasi(array("id_permohonan"=>$permohonan))){

                        // }
                        if($this->mdu->update_pemohon($set,$where)){
                            $list_anggota = json_decode($anggota);
                            $this->delete_anggota($id_pemohon);
                            $this->insert_anggota($id_pemohon,$list_anggota);
                            
                            $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                            $msg_array = $this->response_message->default_mgs($main_msg,null);
                        }else{
                            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                            $msg_array = $this->response_message->default_mgs($main_msg,null);
                        }
                    }else{
                        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DATE_START_FAIL"));
                        $msg_array = $this->response_message->default_mgs($main_msg,null);
                    }
                
            }
            
                
            //print_r($list_anggota);         
        }else{
            // print_r(validation_errors());
            $msg_detail = array(
                        "judul" => strip_tags(form_error("judul")),
                        "permohonan" => strip_tags(form_error("permohonan")),
                        "tgl_st" => strip_tags(form_error("tgl_st")),
                        "tgl_fn" => strip_tags(form_error("tgl_fn")),
                        "tmp" => strip_tags(form_error("tmp")),
                        "pengurusan" => strip_tags(form_error("pengurusan"))
                        
                );
                            
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
        }

        print_r(json_encode($msg_array));
         
    }
    
#------------------------------------------------------------Edit Profil--------------------------------------------------------------------
    public function index_profil(){
        $data["user"] = $this->mdu->get_data_user(array("id_user"=>$this->session->userdata("user_bangkes")["id_user"]));
        $this->load->view("user_data/input_profil", $data);
    }
    
    public function update_profil(){

        if($this->validate_update_profil()){
            
            $email = $this->input->post("email");
            $tlp = $this->input->post("tlp");
            $alamat_dom = $this->input->post("alamat_dom");
            
            $email_old = $this->mdu->get_email_from_id(array("id_user"=>$this->session->userdata("user_bangkes")["id_user"]));
            
            if($this->mdu->cek_email_avail($email,$this->session->userdata("user_bangkes")["id_user"])){
                echo "update<br />";
                
                $data_update = array(
                                "email"=>$email,
                                "tlp"=>$tlp,
                                "alamat_dom"=>$alamat_dom
                            );
                if($email != $email_old){
                    $data_update["status_active"] = "0";
                }
                
                if($this->mdu->update_profil($data_update, array("id_user"=>$this->session->userdata("user_bangkes")["id_user"]))){
                    if($email != $email_old){
                        $link = $this->get_link();
                        if($link != null){
                            //update success && send email
                            // echo "update success && send email";
                            $this->sendemail->send_email_vert($email, $link, "Verifikasi Permintaan Perubahan Email");

                            // $msg_response["main_msg"] = $this->response_message->get_success_msg("UPDATE_PROF_SUC_EMAIL");
                            $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_PROF_SUC_EMAIL"));
                            $msg_detail = null;
                            // $this->session->set_userdata("user_bangkes")["email"] = $data_update["email"];

                            $this->session->unset_userdata("user_bangkes");
                        }
                    }else{
                        $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_PROF_SUC"));
                        $msg_detail = null;
                        // $msg_response["main_msg"] = $this->response_message->get_success_msg("UPDATE_PROF_SUC");
                    }       
                }else{
                    //update fail
                    // echo "update fail";
                    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                    $msg_detail = null;
                    // $msg_response["main_msg"] = $this->response_message->get_error_msg("UPDATE_FAIL");
                }
                
            }else{
                //email avail
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("EMAIL_AVAIL"));
                $msg_detail = null;
                // $msg_response["main_msg"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
            }          
        }else{
            //input not valid
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            // $msg_response["main_msg"] = $this->response_message->get_error_msg("INSERT_FAIL");
            $msg_detail = array(
                                "email"=>form_error("email"),
                                "alamat_dom"=>form_error("alamat_dom"),
                                "tlp"=>form_error("tlp"),
                            );
            
            //echo validation_errors();
            
            //surya hanggara
        }
        
        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);

        $this->session->set_flashdata("message_response", $msg_array);
        redirect(base_url()."user/maindatauser/index_profil");
    }
    
    private function validate_update_profil(){
        $config_val_input = array(
                array(
                    'field'=>'alamat_dom',
                    'label'=>'Alamat_Domisili',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_email',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),array(
                    'field'=>'tlp',
                    'label'=>'Nomor Telephon',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    private function get_link(){
        $id = $this->session->userdata("user_bangkes")["id_user"];
        $code = $this->generate_token->generate_link_email();
        
        $data = array(
                    "id_user"=>$id,
                    "time"=>date("Y-m-d H:i:s"),
                    "param"=>$code["param"],
                    "code"=>$code["code"]);
        
        if($this->reg_user->check_db_vert(array("id_user"=>$id), $data)){
            return base_url()."user/vertuser/activate/".$id."/?".$code["param"]."=".$code["code"];    
        }
        
        return null; 
    }
    
#-------------------------------------------------------------------instansi--------------------------------------------------------------------
    
    public function index_update_instansi(){
        
    }
    
    public function update_instansi(){
        $id_user = $this->session->userdata("user_bangkes")["id_user"];
        if($this->validate_update_ins()){
            $corp = $this->input->post("corp");
            if($corp == 0){
                // echo "0";
                if($this->validate_update_karyawan()){
                    $ins = $this->input->post("ins");
                    
                    $update = $this->mdu->update_ins(array("pekerjaan"=>$corp,"instansi"=>$ins),array("id_user"=>$id_user));                   
                    if($update){
                        // echo "yes";
                        $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                        $msg_array = $this->response_message->default_mgs($main_msg, null);
                    }else{
                        $main_msg = array("status" => true, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                        $msg_array = $this->response_message->default_mgs($main_msg, null);
                        // echo "no";
                    }
                    // $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    // $msg_array = $this->response_message->default_mgs($main_msg, null);
                    // $this->session->set_flashdata("response_update_ins", $msg_array);
                    
                }else{
                    
                    $msg_detail = array(
                                "nama_instansi" => strip_tags(form_error("ins"))
                            );
                            
                    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                    $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
                    // $this->session->set_flashdata("response_update_ins", $msg_array);
                        
                }
            }else{
                // echo "1";
                if($this->validate_update_mhs()){
                    $uni = $this->input->post("uni");
                    $prodi = $this->input->post("prodi");
                    $fak = $this->input->post("fak");
                    $jurusan = $this->input->post("jurusan");
                    
                    $ins_data_update = $prodi.";".$jurusan.";".$fak.";".$uni;
                    $this->mdu->update_ins(array("pekerjaan"=>$corp,"instansi"=>$ins_data_update),array("id_user"=>$id_user));                   
                    
                    $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    $msg_array = $this->response_message->default_mgs($main_msg, null);
                    // $this->session->set_flashdata("response_update_ins", $msg_array);
                    
                }else{
                    $msg_detail = array(
                                "nama_universitas" => strip_tags(form_error("uni")),
                                "jurusan" => strip_tags(form_error("jurusan")),
                                "fakultas" => strip_tags(form_error("fak"))    
                            );
                            
                    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                    $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
                    // $this->session->set_flashdata("response_update_ins", $msg_array);
                        
                }
            }
        }else{
            $msg_detail = array(
                "pekerjaan" => form_error("corp")
            );
                            
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
            $msg_array = $this->response_message->default_mgs($main_msg,null);
            // $this->session->set_flashdata("response_update_ins", $msg_array);
        }
        
        // print_r("<pre>");
        // print_r($_POST);
        // print_r($_SESSION["response_update_ins"]);

        print_r(json_encode($msg_array));
    }


    private function validate_update_ins(){
        $config_val_input = array(
                array(
                    'field'=>'corp',
                    'label'=>'Pekerjaan',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

#---------------------------------------------------------Valid Register Mhs---------------------------------------------------------------------    
    
    private function validate_update_mhs(){
        $config_val_input = array(
                array(
                    'field'=>'uni',
                    'label'=>'Nama Universitas',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'jurusan',
                    'label'=>'Jurusan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'fak',
                    'label'=>'Fakultas',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
        
    }

#---------------------------------------------------------Valid Register Corporasi---------------------------------------------------------------------

    private function validate_update_karyawan(){
        $config_val_input = array(
                array(
                    'field'=>'ins',
                    'label'=>'Nama Korporasi',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
#-------------------------------------------------------------------anggota---------------------------------------------------------------------
    
    private function insert_anggota($id, $list_anggota){
        if(!empty($list_anggota)){
                foreach($list_anggota as $val_ang){
                    //print_r($val_ang);
                    $insert = $this->mdu->insert_anggota($id, $val_ang[0], $val_ang[1]);
                }   
            }
    }
    
    private function delete_anggota($id_pemohon){
        $this->mdu->delete_anggota(array("id_pemohon"=>$id_pemohon));  
    }
    
    public function gat(){
        //$data["page"] = "c_mhs/form";
        $this->load->view("v_index_mhs");
    }

#-------------------------------------------------------------------history---------------------------------------------------------------------
    
    public function index_history(){
        $id_user = $this->session->userdata("user_bangkes")["id_user"];
        
        $tmp_magang = $this->mdu->get_tmp_magang();
        $dinas = array();
        foreach($tmp_magang as $val_tmp_magang){
            $dinas[$val_tmp_magang->id_dinas] = $val_tmp_magang->nama_dinas;
        }
        
        $data["dinas"] = $dinas;
        $data["page"] = "history";
        $data["pemohon"] = $this->mdu->get_pemohon_history(array("id_user"=>$id_user));
        $data["kelengkapan_pemohon"] = $this->cek_kelengkapan();
        // print_r("<pre>");
        // print_r($tmp_magang);
        $this->load->view("v_index_mhs", $data);
    }
    
    public function get_o(){
        $id = $this->session->userdata("user_bangkes")["id_user"];
            
        $where = array(
                "id_user"=>$id
        );
        $data = $this->mdu->cek_data_aktif($where);
        print_r($data);
    }
    
#-------------------------------------------------------------------------Surat Pernyataan---------------------------------------------------------------

    public function surper_pn($id_pemohon){
        $tmp_magang = $this->mdu->get_tmp_magang();
        $dinas = array();
        foreach($tmp_magang as $val_tmp_magang){
            $dinas[$val_tmp_magang->id_dinas] = $val_tmp_magang->nama_dinas;
        }
        
        $data["dinas"] = $dinas;
        $data["pemohon"] = $this->mdu->pernyataan(array("p.id_pemohon"=>$id_pemohon));
        //print_r("<pre>");
        //print_r($data);
        $this->load->view("user_surat/v_surat_pernyataan",$data);
    }
    
    public function surper_magang($id_pemohon){
        $tmp_magang = $this->mdu->get_tmp_magang();
        $dinas = array();
        foreach($tmp_magang as $val_tmp_magang){
            $dinas[$val_tmp_magang->id_dinas] = $val_tmp_magang->nama_dinas;
        }
        
        $data["dinas"] = $dinas;
        $data["pemohon"] = $this->mdu->pernyataan(array("p.id_pemohon"=>$id_pemohon));
        //print_r("<pre>");
        //print_r($data);
        $data["t_anggota"] = $this->mdpa->count_anggota($data["pemohon"]["id_pemohon"]);
        
        $this->load->view("user_surat/v_surat_pernyataan_magang",$data);
    }
    
    public function delete_pemohon($id_pemohon){
        if($this->mdu->delete(array("id_pemohon"=>$id_pemohon))){
            echo "sukses";
        }
            
    }

#-------------------------------------------------------------------------cek---------------------------------------------------

    public function cek_ses(){
        print_r("<pre>");
        print_r($_SESSION);
    }

#-------------------------------------------------------------------------update profil-------------------------------------------------------------

    public function input_profil(){
        print_r("<pre>");
        print_r($_FILES);
        print_r($_SESSION);
        $id_user = $this->session->userdata("user_bangkes")["id_user"];

        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        
        if(!empty($_FILES["ft_prof"]["name"])){
            $ret_foto = $this->upload_profil($id_user);

            // print_r($ret_foto);
            if($ret_foto["status"] == 1){
                $foto_name = $ret_foto["response"]["upload_data"]["file_name"];   
            }

            if($this->mdu->update_profil(array("url_profil"=>$foto_name), array("id_user"=>$id_user))){
                $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }

        $msg_array = $this->response_message->default_mgs($main_msg, null);
        $this->session->set_flashdata("msg_res_foto", $msg_array);
        redirect(base_url()."data/mainform");

    }

    private function upload_profil($id_user){
        $config['upload_path']          = './doc/foto_pemohon/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 200;
        
        $config['file_name']            = "foto_pemohon_".$id_user;
        
        if(file_exists($config['upload_path'].$config['file_name'].".jpg")){
            unlink($config['upload_path'].$config['file_name'].".jpg");    
        }
        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload('ft_prof')){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        //print_r($error);
            return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }

#---------------------------------------------------------------------------Delegasi-------------------------------------------------------------
    private function val_delegasi(){
        $config_val_input = array(
                array(
                    'field'=>'nik_del',
                    'label'=>'NIK',
                    'rules'=>'required|numeric|exact_length[16]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'is_unique'=>$this->response_message->get_error_msg("NIK_AVAIL"),
                        'exact_length[16]'=>"%s ".$this->response_message->get_error_msg("EXACT_LENGHT_NIK")
                    )
                       
                ),
                array(
                    'field'=>'nama_del',
                    'label'=>'Nama',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'alamat_ktp_del',
                    'label'=>'Alamat Sesuai KTP',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'alamat_dom_del',
                    'label'=>'Alamat Domisili',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function input_delegasi(){
        print_r("<pre>");
        print_r($_POST);
        print_r($_FILES);
        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = null;

        $id = $this->session->userdata("user_bangkes")["id_user"];    
        $where = array(
                        "id_user"=>$id,
                        "no_register"=>"0",
                        "status_magang"=>"0",
                        "status_diterima"=>"0"
                    );   
        $pemohon = $this->mdu->get_pemohon($where);
        
        if(!empty($pemohon)){
            $id_pemohon = $pemohon["id_pemohon"];

            if($this->val_delegasi()){
                // $id_pemohon = "";
                $nik_del = $this->input->post("nik_del");
                $nama_del = $this->input->post("nama_del");
                $alamat_ktp_del = $this->input->post("alamat_ktp_del");
                $alamat_dom_del = $this->input->post("alamat_dom_del");

                $ret_foto_del = $this->upload_foto_del($id_pemohon);
                if($ret_foto_del["status"]){
                    $foto_del_name = $ret_foto_del["response"]["upload_data"]["file_name"];   
                }

                $ret_ktp_del = $this->upload_ktp_del($id_pemohon);
                if($ret_ktp_del["status"]){
                    $ktp_del_name = $ret_ktp_del["response"]["upload_data"]["file_name"];   
                }

                $ret_kuasa_del = $this->upload_kuasa_del($id_pemohon);
                if($ret_kuasa_del["status"]){
                    $kuasa_del_name = $ret_kuasa_del["response"]["upload_data"]["file_name"];   
                }

                // cek delegasi dengan id_pemohon sudah ada atau belum
                $get_delegasi = $this->mdu->get_delegasi_where(array("id_pemohon" => $id_pemohon));
                if(empty($get_delegasi)){
                // jika tidak empty maka update
                    $data_insert = array(
                                    "id_pemohon"=>$id_pemohon,
                                    "nama"=>$nama_del,
                                    "nik"=>$nik_del,
                                    "alamat_ktp"=>$alamat_ktp_del,
                                    "alamat_dom"=>$alamat_dom_del,
                                    "url_ktp_del"=>$ktp_del_name,
                                    "url_foto_del"=>$foto_del_name,
                                    "url_srt_del"=>$kuasa_del_name
                                );
                    if($this->mdu->insert_delegasi($data_insert)){
                        $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }else {

                    $data_update = array(
                                    "nama"=>$nama_del,
                                    "nik"=>$nik_del,
                                    "alamat_ktp"=>$alamat_ktp_del,
                                    "alamat_dom"=>$alamat_dom_del,
                                    "url_ktp_del"=>$ktp_del_name,
                                    "url_foto_del"=>$foto_del_name,
                                    "url_srt_del"=>$kuasa_del_name
                                );
                    $where_update = array("id_pemohon"=>$id_pemohon);

                    if($this->mdu->update_delegasi($data_update, $where_update)){
                        $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                // jika empty maka insert

                }
                
                $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }else {
                $msg_detail["nik_del"] = strip_tags(form_error("nik_del"));
                $msg_detail["nama_del"] = strip_tags(form_error("nama_del"));
                $msg_detail["alamat_ktp_del"] = strip_tags(form_error("alamat_ktp_del"));
                $msg_detail["alamat_dom_del"] = strip_tags(form_error("alamat_dom_del"));                
            }
        }


        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
        $this->session->set_flashdata("msg_res_delegasi", $msg_array);
        redirect(base_url()."data/doc");
        print_r($msg_array);
    }

    private function upload_foto_del($id_pemohon){
        $config['upload_path']          = './doc/foto_delegasi/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 200;
        
        $config['file_name']            = "foto_delegasi_".$id_pemohon;
        
        if(file_exists($config['upload_path'].$config['file_name'].".jpg")){
            unlink($config['upload_path'].$config['file_name'].".jpg");    
        }
        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload('foto_del')){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        //print_r($error);
            return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }


    private function upload_ktp_del($id_pemohon){
        $config['upload_path']          = './doc/ktp_delegasi/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 512;
        
        $config['file_name']            = "ktp_delegasi_".$id_pemohon;
        
        if(file_exists($config['upload_path'].$config['file_name'].".jpg")){
            unlink($config['upload_path'].$config['file_name'].".jpg");    
        }
        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload('ktp_del')){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        //print_r($error);
            return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }

    private function upload_kuasa_del($id_pemohon){
        $config['upload_path']          = './doc/srt_kuasa/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 1024;
        
        $config['file_name']            = "kuasa_delegasi_".$id_pemohon;
        
        if(file_exists($config['upload_path'].$config['file_name'].".jpg")){
            unlink($config['upload_path'].$config['file_name'].".jpg");    
        }
        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload('surat_kuasa')){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        //print_r($error);
            return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }

    public function delete_delegasi($id_pemohon){
        $msg_detail = null;
        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));

        $data_deleagsi = $this->mdu->get_delegasi_where(array("id_pemohon"=>$id_pemohon));

        // print_r("<pre>");
        // print_r($data_deleagsi);
        if(file_exists('./doc/srt_kuasa/'.$data_deleagsi["url_srt_del"])){
            unlink('./doc/srt_kuasa/'.$data_deleagsi["url_srt_del"]);    
        }
        
        
        if(file_exists('./doc/foto_delegasi/'.$data_deleagsi["url_foto_del"])){
            unlink('./doc/foto_delegasi/'.$data_deleagsi["url_foto_del"]);    
        }
        
        if(file_exists('./doc/ktp_delegasi/'.$data_deleagsi["url_ktp_del"])){
            unlink('./doc/ktp_delegasi/'.$data_deleagsi["url_ktp_del"]);    
        }
        
        $delete = $this->mdu->delete_delegasi(array("id_pemohon"=>$id_pemohon));
        if($delete){
            $msg_detail = null;
            $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }

        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);

        print_r(json_encode($msg_array));
        
    }
}
