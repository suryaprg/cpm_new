<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vertuser extends CI_Controller {

    function __construct(){
		parent::__construct();
        $this->load->model("user/register_user", "reg_user");
        
        $this->load->library("response_message");        
	}
    
    public function activate($id){
	    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("ACTIVATION_FAIL"));
           
       $param_code = $this->reg_user->get_param_code(array("id_user"=>$id));
       if($param_code != null){
            if(isset($_GET[$param_code["param"]])){
                if($_GET[$param_code["param"]] == $param_code["code"]){
                    if($this->reg_user->update_activate_user(array("id_user"=>$id))){
                        if($this->reg_user->delete_vert(array("id_user"=>$id))){
                          $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("ACTIVATION_SUC"));
                        }
                    }
                }    
            }
       }

       $msg_array = $this->response_message->default_mgs($main_msg,null);
       $this->session->set_flashdata("response_activaiton",$msg_array);

       // print_r("<pre>");
       // print_r($msg_array);
       redirect(base_url()."home/login");
	}
}
