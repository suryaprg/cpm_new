<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindataprevall extends CI_Controller {
    
    function __construct(){
		parent::__construct();
		$this->load->model('user/main_data_user', 'mdu');
        $this->load->model('user/main_data_doc', 'mdd');
        $this->load->model('user/main_data_prev_all', 'mdpa');
        
        $this->load->library("response_message");
  
        if($this->session->userdata("user_bangkes")["is_log"] != 1){
            redirect(base_url()."home/login");
        }
	}

    private function cek_kelengkapan(){
        $id = $this->session->userdata("user_bangkes")["id_user"];

        $where = array(
                        "id_user"=>$id,
                        "no_register"=>"0",
                        "status_magang"=>"0",
                        "status_diterima"=>"0"
                    );

        $user = $this->session->userdata("user_bangkes");
        $pemohon = $this->mdpa->get_kelengkapan($where);

        $id_pemohon = $pemohon["id_pemohon"];

        $doc = $this->mdpa->get_kelengkapan_data(array("id_pemohon"=>$id_pemohon));

        $status_pemohon = 0;
        //cek pemohon
        if($pemohon["jenis_kegiatan"] == 0){
            // penelitian
            if(!empty($pemohon["judul"]) && !empty($pemohon["tgl_start"]) && !empty($pemohon["tgl_selesai"]) && !empty($pemohon["id_bidang"])){
                $status_pemohon = 1;
            }    
        }else {
            // magang

            if(!empty($pemohon["judul"]) && !empty($pemohon["periode"]) && !empty($pemohon["id_bidang"])){
                $status_pemohon = 1;
            }
        }


        $status_document = 0;
        if(!empty($doc["url_ktp"]) && !empty($doc["url_proposal"]) && !empty($doc["url_sk"])){
            $status_document = 1;
        }
        
        $status_daftar = 0;
        if($status_pemohon == true && $status_document == true){
            $status_daftar = 1;
        }



        return array("status_daftar"=>$status_daftar, "status_pemohon"=>$status_pemohon, "status_document"=>$status_document);

    }
    
    public function index(){
        $id = $this->session->userdata("user_bangkes")["id_user"];
        
        $data["user"] = $this->mdu->get_data_user(array("id_user"=>$id));
        $where = array(
                        "id_user"=>$id,
                        "no_register"=>"0",
                        "status_magang"=>"0",
                        "status_diterima"=>"0"
                    );
        $data["pemohon"] = $this->mdu->get_pemohon($where);
        
        $data_anggota = $this->mdu->get_anggota($data["pemohon"]["id_pemohon"]);
        $data["delegasi"] = $this->mdu->get_delegasi_where(array("id_pemohon"=> $data["pemohon"]["id_pemohon"]));

        $data_doc = $this->mdd->get_doc(array("id_pemohon"=>$data["pemohon"]["id_pemohon"]));
        
        $data_ktp = "./doc/ktp/".$data_doc["url_ktp"];
        $data_proposal = "./doc/proposal/".$data_doc["url_proposal"];
        $data_sk = "./doc/sk/".$data_doc["url_sk"];
        
        
        $data["doc"]["ktp"] =false;
        if(file_exists($data_ktp)){
            $data["doc"]["ktp"] = true;
        }
        
        $data["doc"]["proposal"] =false;
        if(file_exists($data_proposal)){
            $data["doc"]["proposal"] = true;
        }
        
        $data["doc"]["sk"] =false;
        if(file_exists($data_ktp)){
            $data["doc"]["sk"] = true;
        }
        
        $data["doc"]["doc"] = $data_doc;
        
        $tmp_magang = $this->mdu->get_tmp_magang();
        $dinas = array();
        foreach($tmp_magang as $val_tmp_magang){
            $dinas[$val_tmp_magang->id_dinas] = $val_tmp_magang->nama_dinas;
        }
        
        $data["dinas"] = $dinas;
        $data["t_anggota"] = $this->mdpa->count_anggota($data["pemohon"]["id_pemohon"]);
        $data["kelengkapan_pemohon"] = $this->cek_kelengkapan();
        
        //print_r("<pre>");
        //print_r($data);
        
        $data['page'] = 'kelengkapan_data';
        $this->load->view('v_index_mhs', $data);
    }
    
    public function get_no_register(){ 
       $response = $this->response_message->default_mgs(null,null);
       
       $id = $this->session->userdata("user_bangkes")["id_user"];
       $where = array(
                        "id_user"=>$id,
                        "no_register"=>"0",
                        "status_magang"=>"0",
                        "status_diterima"=>"0"
                    );
       $get_permohonan = $this->mdpa->get_permohonan($where);
       $permohonan = $this->mdpa->get_doc(array("id_pemohon"=>$get_permohonan["id_pemohon"]));
       if(!empty($permohonan)){
            $id_permohonan = $permohonan["id_pemohon"];
            $id_kepentingan = $get_permohonan["kepentingan"];
            $kepentingan = "P";
            if($id_kepentingan == 1){
                $kepentingan = "KL";
            }
            //print_r($kepentingan);
            if($this->cek_doc($id_permohonan)){
                print_r($id_permohonan);
                //$this->db->query("call update_no_reg('$id_permohonan','$kepentingan')");
                $update = $this->mdpa->update_status_magang(array("status_magang"=>"1"), array("id_pemohon"=>$id_permohonan));
                // echo $id_permohonan;
                
                if($update){
                
                    $main_msg = array("status" => true, "msg"=>"** .".$this->response_message->get_success_msg("PENDAFTARAN_SUC"));
                    $msg_array = $this->response_message->default_mgs($main_msg,null);
                    
                //     $this->session->set_flashdata("msg_pendaftaran", $main_msg);
                    // redirect(base_url()."data/history");
                }else{
                    // echo "fail";
                    $main_msg = array("status" => false, "msg"=>"** .".$this->response_message->get_error_msg("PENDAFTARAN_FAIL"));
                    $msg_array = $this->response_message->default_mgs($main_msg,null);   
                    // redirect(base_url()."data/ckdoc");
                }
                //redirect halaman sukses
            }else{
                // $main_res = "Mohon maaf proses pendaftaran di batalkan, Silahkan periksa kelengkapan dokumen saudara dan Lakukan pendafatran ulang..";
                //redirect maindataprevall
                // redirect(base_url()."data/ckdoc");
                $main_msg = array("status" => false, "msg"=>"** .".$this->response_message->get_error_msg("PENDAFTARAN_DOC_FAIL"));
                $msg_array = $this->response_message->default_mgs($main_msg,null);   
            }
       }else{
            $main_msg = array("status" => false, "msg"=>"** .".$this->response_message->get_error_msg("PENDAFTARAN_AVAIL_FAIL"));
            $msg_array = $this->response_message->default_mgs($main_msg,null);   
            // redirect(base_url()."data/ckdok");
       }
       $this->session->set_flashdata("msg_pendaftaran", $msg_array);
       redirect(base_url()."data/ckdoc");
       // print_r("<pre>");
       // print_r($msg_array);
       
    }
    
    private function cek_doc($id_permohonan){
        $get_doc = $this->mdpa->get_doc(array("id_pemohon"=>$id_permohonan));
        
        $data_ktp = "./doc/ktp/0";
        if(!empty($get_doc["url_ktp"])){
            $data_ktp = "./doc/ktp/".$get_doc["url_ktp"];
        }

        $data_proposal = "./doc/proposal/0";
        if (!empty($get_doc["url_proposal"])) {
            $data_proposal = "./doc/proposal/".$get_doc["url_proposal"];
        }
        
        $data_sk = "./doc/sk/0";
        if (!empty($get_doc["url_sk"])) {
            $data_sk = "./doc/sk/".$get_doc["url_sk"];
        }
        
        
        $status_ktp =false;
        if(file_exists($data_ktp)){
            $status_ktp = true;
        }
        
        $status_proposal =false;
        if(file_exists($data_proposal)){
            $status_proposal = true;
        }

        $status_sk =false;
        if(file_exists($data_sk)){
            $status_sk = true;
        }
        
        if($status_ktp==true && $status_proposal==true && $status_sk){
            return true;
        }
        
        //print_r($status_ktp);
        return false;
    }
    
    
}
