<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registeruser extends CI_Controller {

    function __construct(){
		parent::__construct();
		$this->load->library("response_message");
        $this->load->library("sendemail");
        $this->load->library("generate_token");
        
        $this->load->model("user/register_user", "reg_user");
        
        if($this->session->userdata("user_bangkes")["is_log"] == 1){
            redirect(base_url()."pendaftaran/home");
        }
	}
    
	public function index(){
	   $this->load->view('user_front/v_magang');
	}

#------------------------------------------------------------------------Valid Register---------------------------------------------------------------------------

    public function validate_register(){
        $config_val_input = array(
                array(
                    'field'=>'nama_pemohon',
                    'label'=>'Nama Pemohon',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'alamat_ktp',
                    'label'=>'Alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'alamat_dom',
                    'label'=>'Alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_email|is_unique[user.email]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric|min_length[5]|max_length[15]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR"),
                        'min_length[5]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT"),
                        'max_length[15]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    )   
                ),
                array(
                    'field'=>'password_confirm',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric|min_length[5]|max_length[15]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR"),
                        'min_length[5]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT"),
                        'max_length[15]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    ) 
                ),
                array(
                    'field'=>'no_telp',
                    'label'=>'Nomor Telephon',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                ),
                array(
                    'field'=>'tgl_lhr',
                    'label'=>'Tanggal Lahir',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),
                array(
                    'field'=>'corp',
                    'label'=>'Pekerjaan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

#------------------------------------------------------------------------Insert Register---------------------------------------------------------------------------

    public function get_post(){
        $this->response_message->default_mgs(null, null);
        // print_r("<pre>");
        // print_r($_POST);
        $msg_detail = array(    "identitas"=>"",

                                "nik"=>"",
                                "sim"=>"",
                                "nim"=>"",

                                "nama_pemohon"=>"",
                                "alamat_ktp"=>"",
                                "alamat_dom"=>"",
                                "email"=>"",
                                "password"=>"",
                                "password_confirm"=>"",
                                "no_telp"=>"",
                                "tgl_lhr"=>"",
                                "corp"=>"",
                                
                                "instansi"=>"",
                                "nama_universitas"=>"",
                                "program_studi"=>"",
                                "fakultas"=>"",
                            );

        if($this->validate_register_iden_main()){
            $jenis_iden = $this->input->post("identitas");
            $sts_fix_iden = false;

            $no_identitas = "";
            // echo $jenis_iden;
            if($jenis_iden == 0){
                // $nik = "";
                if($this->validate_register_iden_nik()){
                    $no_identitas = $this->input->post("nik");
                    $sts_fix_iden = true;
                }else {
                    $msg_detail["nik"] = strip_tags(form_error("nik"));
                }
            }elseif($jenis_iden == 1){
                // $sim = "";
                if($this->validate_register_iden_sim()){
                    $no_identitas = $this->input->post("sim");
                    $sts_fix_iden = true;
                }else {
                    $msg_detail["sim"] = strip_tags(form_error("sim"));
                }
            }else{
                // $nim = "";
                if($this->validate_register_iden_nim()){
                    $no_identitas = $this->input->post("nim");
                    $sts_fix_iden = true;
                }else {
                    $msg_detail["nim"] = strip_tags(form_error("nim"));
                }
            }
            
            if($sts_fix_iden){
                if($this->validate_register()){
                    $email = $this->input->post("email");
                    $pass = $this->input->post("password");
                    $repass = $this->input->post("password_confirm");
                    $alamat = $this->input->post("alamat_ktp");
                    $alamat_dom = $this->input->post("alamat_dom");
                    $nik = $no_identitas;
                    $tlp = $this->input->post("no_telp");
                    $nama = $this->input->post("nama_pemohon");
                    
                    $tgl_lhr = $this->input->post("tgl_lhr");
                    $corp = $this->input->post("corp");
                    
                    $instansi = "";
                    if($pass == $repass){
                        if($corp == 0){
                            if($this->validate_register_ins_corp()){
                                $instansi = $this->input->post("instansi");
                            }else{     
                                $msg_detail["instansi"] = strip_tags(form_error("instansi"));
                                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                            }
                        }else{
                            if($this->validate_register_ins_ak()){
                                $nm_ins = $this->input->post("nama_universitas");
                                $prodi_ins = $this->input->post("program_studi");
                                $fak_ins = $this->input->post("fakultas");
                                $jurusan = $this->input->post("jurusan");


                                
                                $instansi = $prodi_ins.";".$jurusan.";".$fak_ins.";".$nm_ins;
                            }else{
                                $msg_detail["nama_universitas"] = strip_tags(form_error("nama_universitas"));
                                $msg_detail["program_studi"] = strip_tags(form_error("program_studi"));
                                $msg_detail["fakultas"] = strip_tags(form_error("fakultas"));
                                    
                                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                            }
                        }
                        
                        if($instansi != ""){
                            // $this->db->trans_start();
                            // $this->db->query("set @id = last_key_user()");
                            // $this->db->query("insert into user values (@id,'".$email."','".md5($pass)."','".$nama."','".$alamat."','".$alamat_dom."','$jenis_iden','".$nik."','".$tlp."','0','".$tgl_lhr."','0','".$corp."','".$instansi."')");
                            // $this->db->trans_complete();
                            // if($this->db->trans_status()){
                            $insert_user_get_id = $this->db->query("SELECT func_insert_user('".$email."','".md5($pass)."','".$nama."','".$alamat."','".$alamat_dom."', '".$jenis_iden."', '".$nik."','".$tlp."', '".$tgl_lhr."','0','".$corp."','".$instansi."') AS func_insert_user;");
                            if($insert_user_get_id){
                                $link = $this->get_link($email);
                                $this->sendemail->send_email_vert($email,$link,"BANKESBANGPOL KOTA MALANG");
                                
                                
                                $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("REGISTER_SUC"));

                            }else{
                                
                                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                            }
                        }
                        
                    }else{
                        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                    }
                }else{
                    $msg_detail["nama_pemohon"] = strip_tags(form_error("nama_pemohon"));
                    $msg_detail["alamat_ktp"] = strip_tags(form_error("alamat_tkp"));
                    $msg_detail["alamat_dom"] = strip_tags(form_error("alamat_dom"));
                    $msg_detail["email"] = strip_tags(form_error("email"));
                    $msg_detail["password"] = strip_tags(form_error("password"));
                    $msg_detail["password_confirm"] = strip_tags(form_error("password_confirm"));
                    $msg_detail["no_telp"] = strip_tags(form_error("no_telp"));
                    $msg_detail["tgl_lhr"] = strip_tags(form_error("tgl_lhr"));
                    $msg_detail["corp"] = strip_tags(form_error("corp"));
                    
                                
                   $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("PENDAFTARAN_FAIL"));
                }
            }else {
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            }
        }else{
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            $msg_detail["identitas"] = strip_tags(form_error("identitas"));
        }

        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
        print_r(json_encode($msg_array));
        
    }
    
    public function su(){
        for($i = 0; $i<10; $i++){
            try{
                $this->db->trans_start();
                    $this->db->query("set @id = last_key_user()");
                    $this->db->query("insert into user values (@id,'suryahanggara@gmail.com','aff8fbcbf1363cd7edc85a1e11391173','surya hanggara','malang','3573011903940006','081230695774','SI, ILKOM, STIMATA','0')");
                $this->db->trans_complete();
                if($this->db->trans_status()){
                    echo "yeah";
                }
            }catch(exception $super){
                echo "sek".$super;
            }
                 
        }
    }
#------------------------------------------------------------------------Valid Register Akademisi---------------------------------------------------------------------------

    private function validate_register_ins_ak(){
        $config_val_input = array(
                array(
                    'field'=>'nama_universitas',
                    'label'=>'Nama Universitas',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'program_studi',
                    'label'=>'Program Studi',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'fakultas',
                    'label'=>'Fakultas',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
        
    }

#------------------------------------------------------------------------Valid Register Corporasi---------------------------------------------------------------------------

    private function validate_register_ins_corp(){
        $config_val_input = array(
                array(
                    'field'=>'instansi',
                    'label'=>'Nama Korporasi',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }


#------------------------------------------------------------------------Valid Main Identitas NIK---------------------------------------------------------------------------

    private function validate_register_iden_main(){
        $config_val_input = array(
                array(
                    'field'=>'identitas',
                    'label'=>'Identitas',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }


#------------------------------------------------------------------------Valid Identitas NIK---------------------------------------------------------------------------

    private function validate_register_iden_nik(){
        $config_val_input = array(
                array(
                    'field'=>'nik',
                    'label'=>'NIK',
                    'rules'=>'required|numeric|exact_length[16]|is_unique[user.nik]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'is_unique'=>$this->response_message->get_error_msg("NIK_AVAIL"),
                        'exact_length[16]'=>"%s ".$this->response_message->get_error_msg("EXACT_LENGHT_NIK")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }


#------------------------------------------------------------------------Valid Identitas NIM---------------------------------------------------------------------------

    private function validate_register_iden_nim(){
        $config_val_input = array(
                array(
                    'field'=>'nim',
                    'label'=>'NIM',
                    'rules'=>'required|numeric|is_unique[user.nik]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'is_unique'=>$this->response_message->get_error_msg("NIK_AVAIL")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    

#------------------------------------------------------------------------Valid Identitas SIM---------------------------------------------------------------------------

    private function validate_register_iden_sim(){
        $config_val_input = array(
                array(
                    'field'=>'sim',
                    'label'=>'No. SIM',
                    'rules'=>'required|numeric|exact_length[12]|is_unique[user.nik]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'is_unique'=>$this->response_message->get_error_msg("NIK_AVAIL"),
                        'exact_length[16]'=>"%s ".$this->response_message->get_error_msg("EXACT_LENGHT_NIK")
                    )
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    

#------------------------------------------------------------------Generate Actvation Link Register-----------------------------------------------------------------
 
 
    private function get_link($email){
        $id = $this->reg_user->get_id_from_email(array("email"=>$email));
        $code = $this->generate_token->generate_link_email();
        
        $data = array(
                    "id_user"=>$id,
                    "time"=>date("Y-m-d H:i:s"),
                    "param"=>$code["param"],
                    "code"=>$code["code"]);
        
        if($this->reg_user->check_db_vert(array("id_user"=>$id), $data)){
            return base_url()."user_new/vertuser/activate/".$id."/?".$code["param"]."=".$code["code"];    
        }
        
        return null; 
    }

#------------------------------------------------------------------------Actvation Register---------------------------------------------------------------------

	

    public function get_cek(){
        $this->sendemail->send_email_vert("suryahanggara@gmail.com","dsfsdf","BANKESBANGPOL KOTA MALANG");
    }
    
    public function logout(){
	   $this->session->unset_userdata("user_bangkes");
	   redirect(base_url('home/login'));
    }
}
