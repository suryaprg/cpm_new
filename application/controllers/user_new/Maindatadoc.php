<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindatadoc extends CI_Controller {

    function __construct(){
		parent::__construct();
		$this->load->model('user/main_data_user', 'mdu');
        $this->load->model('user/main_data_doc', 'mdd');
        $this->load->model('user/main_data_prev_all', 'mdpa');
        
        $this->load->library("response_message");
        
        if($this->session->userdata("user_bangkes")["is_log"] != 1){
            redirect(base_url()."home/login");
        }
	}

    private function cek_kelengkapan(){
        $id = $this->session->userdata("user_bangkes")["id_user"];

        $where = array(
                        "id_user"=>$id,
                        "no_register"=>"0",
                        "status_magang"=>"0",
                        "status_diterima"=>"0"
                    );

        $user = $this->session->userdata("user_bangkes");
        $pemohon = $this->mdpa->get_kelengkapan($where);

        $id_pemohon = $pemohon["id_pemohon"];

        $doc = $this->mdpa->get_kelengkapan_data(array("id_pemohon"=>$id_pemohon));

        $status_pemohon = 0;
        //cek pemohon
        if($pemohon["jenis_kegiatan"] == 0){
            // penelitian
            if(!empty($pemohon["judul"]) && !empty($pemohon["tgl_start"]) && !empty($pemohon["tgl_selesai"]) && !empty($pemohon["id_bidang"])){
                $status_pemohon = 1;
            }    
        }else {
            // magang

            if(!empty($pemohon["judul"]) && !empty($pemohon["periode"]) && !empty($pemohon["id_bidang"])){
                $status_pemohon = 1;
            }
        }


        $status_document = 0;
        if(!empty($doc["url_ktp"]) && !empty($doc["url_proposal"]) && !empty($doc["url_sk"])){
            $status_document = 1;
        }
        
        $status_daftar = 0;
        if($status_pemohon == true && $status_document == true){
            $status_daftar = 1;
        }



        return array("status_daftar"=>$status_daftar, "status_pemohon"=>$status_pemohon, "status_document"=>$status_document);

    }
// 
    public function index(){
        // print_r("<pre>");
        $id = $this->session->userdata("user_bangkes")["id_user"];
        $where = array(
                        "id_user"=>$id,
                        "no_register"=>"0",
                        "status_magang"=>"0",
                        "status_diterima"=>"0"
                    );   
        
        $data["pemohon"] = $this->mdu->get_pemohon($where);
        
        $id_pemohon = $data["pemohon"]["id_pemohon"];
        
        $data_anggota = $this->mdu->get_anggota($data["pemohon"]["id_pemohon"]);
        
        $data_doc = $this->mdd->get_doc(array("id_pemohon"=>$id_pemohon));
        // print_r($data_doc);

        $data["doc_avail"] = array(
                            "ktp"=>0,
                            "proposal"=>0,
                            "sk"=>0
                        );

        // print_r($data_doc);
        if(!empty($data_doc)){
            $url_ktp = "0";
            if(!empty($data_doc["url_ktp"])){
                $url_ktp = $data_doc["url_ktp"];
            }

            $url_proposal = "0";
            if(!empty($data_doc["url_proposal"])){
                $url_proposal = $data_doc["url_proposal"];
            }

            $url_sk = "0";
            if(!empty($data_doc["url_sk"])){
                $url_sk = $data_doc["url_sk"];
            }

            $data_ktp = "./doc/ktp/".$url_ktp;
            $data_proposal = "./doc/proposal/".$url_proposal;
            $data_sk = "./doc/sk/".$url_sk;
            
            // print_r($url_ktp);
            
            if(file_exists($data_ktp)){
                $data["doc_avail"]["ktp"] =1;
            }
            
            // $data["doc_avail"]["proposal"] =0;
            if(file_exists($data_proposal)){
                $data["doc_avail"]["proposal"] =1;
            }
            
            // $data["doc_avail"]["sk"] =0;
            if(file_exists($data_sk)){
                $data["doc_avail"]["sk"] =1;
            }
        }
                       
        $data["t_anggota"] = $this->mdpa->count_anggota($id_pemohon);
        
        $data["doc"] = $this->mdd->get_doc(array("id_pemohon"=>$id_pemohon));
        $data['page'] = 'dokumen';
        $data["kelengkapan_pemohon"] = $this->cek_kelengkapan();
        // print_r("<pre>");
        // print_r($data);
        
        $this->load->view('v_index_mhs', $data);
    }
    
    public function insert_doc(){
        print_r("<pre>");
        print_r($_FILES);
        $this->response_message->default_mgs(null, null);
        
                if($this->val_doc()){
                    $id_pemohon = $this->input->post("id_pemohon");
                    $no_surat = $this->input->post("no_surat");
                    $nama_tdd = $this->input->post("nama_tdd");
                    $jabatan = $this->input->post("jabatan");
                    $tgl_surat = $this->input->post("tgl_surat");
                    
                    $id_doc = $this->input->post("id_doc");
                    
                    $ktp_name = "";
                    $proposal_name = "";
                    $sk_name = "";
                    
                    //if(!empty($_FILES["ktp"]["name"])){
                        //if(!empty($_FILES["proposal"]["name"])){   
                        
                        if($id_doc == ""){
                            $ret_ktp = $this->upload_ktp($id_pemohon);
                            if($ret_ktp["status"]){
                                //print_r($ret_ktp);
                                $ktp_name = $ret_ktp["response"]["upload_data"]["file_name"];   
                            }
                            
                            $ret_proposal = $this->upload_proposal($id_pemohon);
                            if($ret_proposal["status"]){
                                $proposal_name = $ret_proposal["response"]["upload_data"]["file_name"];
                            }
                            
                            $ret_sk = $this->upload_sk($id_pemohon);
                            if($ret_sk["status"]){
                                $sk_name = $ret_sk["response"]["upload_data"]["file_name"];
                            }
                            
                            $data_insert = array(
                                            "id_doc"=>"",
                                            "id_pemohon"=>$id_pemohon,
                                            "url_ktp"=>$ktp_name,
                                            "url_proposal"=>$proposal_name,
                                            "url_sk"=>$sk_name,
                                            "no_surat"=>$no_surat,
                                            "nama_tdd"=>$nama_tdd,
                                            "jabatan"=>$jabatan
                                        );
                            
                            
                                $msg_detail = array(
                                        "no_surat" => form_error("no_surat"),
                                        "nama_tdd" => form_error("nama_tdd"),
                                        "jabatan" => form_error("jabatan")    
                                    );
                                $main_msg = array("status" => false, "msg"=>"** .".$this->response_message->get_error_msg("INSERT_FAIL"));
                                $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
                                
                            if($this->mdd->cek_pemohon(array("id_pemohon"=>$id_pemohon))<=0){
                                print_r($data_insert);
                                $this->db->query("call insert_doc('".$id_pemohon."','".$ktp_name."','".$proposal_name."','".$sk_name."','".$no_surat."','".$nama_tdd."','".$jabatan."','".$tgl_surat."')");
                                
                                
                                $main_msg = array("status" => true, "msg"=>"** .".$this->response_message->get_success_msg("INSERT_SUC"));
                                $msg_array = $this->response_message->default_mgs($main_msg,null);
                                
                                    $this->session->set_flashdata("msg_documen", $msg_array);
                                    redirect(base_url()."data/ckdoc");
                                                           
                            }
                            $this->session->set_flashdata("response_ins_doc", $msg_array);   
                        }else{
                            
                            $data_update = array(
                                            "no_surat"=>$no_surat,
                                            "nama_tdd"=>$nama_tdd,
                                            "jabatan"=>$jabatan,
                                            "tgl_surat"=>$tgl_surat
                                        );
                            

                            $msg_detail = array(
                                                "msg_ktp"=>"",
                                                "msg_proposal"=>"",
                                                "msg_sk"=>"",
                                            );

                            if(!empty($_FILES["ktp"]["name"])){
                                $ret_ktp = $this->upload_ktp($id_pemohon);
                                print_r($ret_ktp);
                                if($ret_ktp["status"] == 1){
                                    $data_update["url_ktp"] = $ktp_name;
                                }else {
                                    $msg_detail["msg_ktp"] = strip_tags($ret_ktp["response"]["error"]);
                                }
                            }
                            
                            if(!empty($_FILES["proposal"]["name"])){
                                $ret_proposal = $this->upload_proposal($id_pemohon);
                                print_r($ret_proposal);
                                if($ret_proposal["status"] == 1){
                                    $proposal_name = $ret_proposal["response"]["upload_data"]["file_name"];
                                    $data_update["url_proposal"] = $proposal_name;
                                }else {
                                    $msg_detail["msg_proposal"] = strip_tags($ret_ktp["response"]["error"]);
                                }
                            }
                            
                            if(!empty($_FILES["pengantar"]["name"])){
                                $ret_sk = $this->upload_sk($id_pemohon);
                                print_r($ret_sk);
                                if($ret_sk["status"] == 1){
                                    $sk_name = $ret_sk["response"]["upload_data"]["file_name"];
                                    $data_update["url_sk"] = $sk_name;
                                }else {
                                    $msg_detail["msg_sk"] = strip_tags($ret_ktp["response"]["error"]);
                                }
                            }
                            
                            $where_update = array(
                                            "id_doc"=>$id_doc
                                        );
                            
                            
                            if($this->mdd->cek_pemohon(array("id_doc"=>$id_doc))==1){
                                print_r($data_update);
                                $update = $this->mdd->update_doc($data_update,$where_update);
                                if($update){
                                    // echo "update";
                                    $main_msg = array("status" => true, "msg"=>"** .".$this->response_message->get_success_msg("INSERT_SUC"));
                                    $msg_array = $this->response_message->default_mgs($main_msg,null);
                                    $this->session->set_flashdata("msg_documen", $msg_array);
                                    redirect(base_url()."data/ckdoc");
                                    // $msg_detail = null;
                                }else{
                                    // echo "update fail";
                                    $main_msg = array("status" => false, "msg"=>"** .".$this->response_message->get_error_msg("INSERT_FAIL"));
                                    // $msg_detail = null;
                                }
                            }else {
                                // error pemohon null
                                $main_msg = array("status" => false, "msg"=>"** .".$this->response_message->get_error_msg("INSERT_FAIL"));
                                // $msg_detail = null;
                            }

                        }
                       
                }else{
                    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                    $msg_detail = array(
                                    "no_surat" => strip_tags(form_error("no_surat")),
                                    "nama_tdd" => strip_tags(form_error("nama_tdd")),
                                    "jabatan" => strip_tags(form_error("jabatan"))
                                );
                }
                $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
                $this->session->set_flashdata("msg_documen", $msg_array);
                
                redirect(base_url()."data/doc");
        
    }
    
    private function val_doc(){
        $config_val_input = array(
                array(
                    'field'=>'no_surat',
                    'label'=>'No Surat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'nama_tdd',
                    'label'=>'Persetujuan Atas Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'jabatan',
                    'label'=>'Jabatan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    private function upload_ktp($id_pemohon){
        $config['upload_path']          = './doc/ktp/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 512;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 0;
        //$config['encrypt_name']         = true;
        $config['file_name']            = "ktp_".$id_pemohon;
        
        if(file_exists($config['upload_path'].$config['file_name'].".jpg")){
            unlink($config['upload_path'].$config['file_name'].".jpg");    
        }
        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload('ktp')){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        //print_r($error);
            return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }

    private function upload_proposal($id_pemohon){
        $config['upload_path']          = './doc/proposal/';
        $config['allowed_types']        = "pdf";
        $config['max_size']             = 2048;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 0;
        //$config['encrypt_name']         = true;
        $config['file_name']            = "proposal_".$id_pemohon;
        //print_r($config);
        
        if(file_exists($config['upload_path'].$config['file_name'].".pdf")){
            unlink($config['upload_path'].$config['file_name'].".pdf");    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload('proposal')){
            $return_item = array('error' => $this->upload->display_errors());
            
            //print_r($error);
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        
        return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }
    
    private function upload_sk($id_pemohon){
        $config['upload_path']          = './doc/sk/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 1024;
        
        $config['file_name']            = "sk_".$id_pemohon;
        
        if(file_exists($config['upload_path'].$config['file_name'].".jpg")){
            unlink($config['upload_path'].$config['file_name'].".jpg");    
        }
        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload('pengantar')){
            $return_item = array('error' => $this->upload->display_errors());
            
            //print_r($error);
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }
    //public function
   
}
