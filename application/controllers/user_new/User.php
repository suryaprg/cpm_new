<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct(){
		parent::__construct();

		$this->load->model('admin/admin_new', 'adn');
	}

#------------------------------------------------------------front page------------------------------------------------------------------------
	
    public function index(){
    	$data["magang"] = count($this->adn->get_pemohon_all(array("1")));
        $data["penelitian"] =count($this->adn->get_pemohon_all(array("0")));

        // print_r($data);
	   	$this->load->view('user_front/v_home_pmp', $data);
	}
	
    public function hal_login(){
	   redirect(base_url()."home/login");
	}
	
    public function hal_persyaratan(){
	   $this->load->view('user_front/v_persyaratan');
	}
	
    public function hal_magang(){
	   redirect(base_url()."home/pendaftaran");
	}
	
	public function hal_kontak(){
	   $this->load->view('user_front/v_contacts');
	}
	public function isi_form(){
	   $this->load->view('user_front/v_cara_isi');
	}

#------------------------------------------------------------data page------------------------------------------------------------------------

}
