<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maindatausernew extends CI_Controller {

    function __construct(){
        parent::__construct();
        // $this->load->model('user/main_data_user', 'mdu');
        $this->load->model('user/main_data_user_new', 'mdun');
        $this->load->model('user/Main_data_prev_all', 'mdpa');
        
        $this->load->model("user/register_user", "reg_user");
        
        $this->load->library("response_message");
        $this->load->library("generate_token");
        $this->load->library("sendemail");

        if($this->session->userdata("user_bangkes")["is_log"] != 1){
            redirect(base_url()."home/login");
        }
        
    }

    public function get_sur(){
        $data_array = $this->mdun->cek_email_avail("roberthanggara19@gmail.com", $this->session->userdata("user_bangkes")["id_user"]);
        if($data_array){
            echo "ok";
        }else{
            echo "fail";
        }
    }

#=============================================================================#
#-------------------------------------------Varifikasi Data All---------------#
#=============================================================================#

    public function get_verifikation_data(){
        $id_user = $this->session->userdata("user_bangkes")["id_user"];
        $data_permohonan_where = array(
                                    "id_user"=>$id_user,
                                    "no_register"=>"0",
                                    "status_magang"=>"0",
                                    "status_diterima"=>"0",
                                    "pn.is_del"=>"0"
                                );
        $data_permohonan = $this->mdun->get_pemohon($data_permohonan_where);

        $data_user_where = array(
                                "id_user"=>$id_user,
                                "status_active"=>"1",
                                "is_del"=>"0"
                            );

        $data_user = $this->mdun->get_data_user($data_user_where);

    //set default value for verifikation

        $list_vert_user = array(
                            "email"=>"0",
                            "nama"=>"0",
                            "alamat"=>"0",
                            "alamat_dom"=>"0",
                            "nik"=>"0",
                            "tlp"=>"0",
                            "tgl_lhr"=>"0",
                            "url_profil"=>"0",
                            "pekerjaan"=>"0",
                            "instansi"=>"0"
                        );

        $list_vert_permohonan = array(
                        "judul" => "0",
                        "permohonan" => "0",
                        "tgl_st" => "0",
                        "tgl_fn" => "0",
                        "tmp" => "0",
                        "pengurusan" => "0",
                        "dosen" => "0",
                );

        $list_vert_delegasi = array(
                        "nik_del" => "0",
                        "nama_del" => "0",
                        "alamat_ktp_del" => "0",
                        "alamat_dom_del" => "0",
                        "url_foto_del" => "0",
                        "url_ktp_del" => "0",
                        "url_srt_del" => "0"
                );


        $list_vert_doc = array(
                        "nama_tdd" => "0",
                        "no_surat_tdd" => "0",
                        "tgl_surat_tdd" => "0",
                        "jabatan_tdd" => "0",
                        "url_ktp_pemohon" => "0",
                        "url_proposal_pemohon" => "0",
                        "url_sk_pemohon" => "0"
                );

        $list_vert_all = array(
                    "vert_user"=>"0",
                    "vert_permohonan"=>"0",
                    "vert_delegasi"=>"0",
                    "vert_doc"=>"0"
                );

    //vert user data in database
        $email = $data_user["email"];
        $nama = $data_user["nama"];
        $alamat = $data_user["alamat"];
        $alamat_dom = $data_user["alamat_dom"];
        $nik = $data_user["nik"];
        $tlp = $data_user["tlp"];
        $tgl_lhr = $data_user["tgl_lhr"];
        $url_profil = $data_user["url_profil"];
        $pekerjaan = $data_user["pekerjaan"];
        $instansi = $data_user["instansi"];

        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $list_vert_user["email"] = "1";
        }

        if (!empty($nama) && $nama != "") {
            $list_vert_user["nama"] = "1";
        }

        if (!empty($alamat) && $alamat != "") {
            $list_vert_user["alamat"] = "1";
        }

        if (!empty($alamat_dom) && $alamat_dom != "") {
            $list_vert_user["alamat_dom"] = "1";
        }

        if (!empty($nik) && $nik != "") {
            $list_vert_user["nik"] = "1";
        }

        if (!empty($tlp) && $tlp != "") {
            $list_vert_user["tlp"] = "1";
        }

        if (!empty($tgl_lhr) && $tgl_lhr != "") {
            $list_vert_user["tgl_lhr"] = "1";
        }

        if (!empty($url_profil) && $url_profil != "") {
            if(file_exists('./doc/foto_pemohon/'.$url_profil)){
                $list_vert_user["url_profil"] = "1";
            }
        }
        if ($pekerjaan != "") {
            $list_vert_user["pekerjaan"] = "1";
        }
        if (!empty($instansi) && $instansi != "") {
            $list_vert_user["instansi"] = "1";
        }

    //vert main pemohon in database
        $judul = $data_permohonan["judul"];
        $permohonan = $data_permohonan["id_permohonan"];
        $tgl_st = $data_permohonan["tgl_start"];
        $tgl_fn = $data_permohonan["tgl_selesai"];
        $tmp = $data_permohonan["id_bidang"];
        $pengurusan = $data_permohonan["status_pendelegasian"];
        $dosen = $data_permohonan["dosen_pembimbing"];
        

        if (!empty($judul) && $judul != "") {
            $list_vert_permohonan["judul"] = "1";
        }
        if (!empty($permohonan) && $permohonan != "") {
            $list_vert_permohonan["permohonan"] = "1";
        }
        if (!empty($tgl_st) && $tgl_st != "") {
            $list_vert_permohonan["tgl_st"] = "1";
        }
        if (!empty($tgl_fn) && $tgl_fn != "") {
            $list_vert_permohonan["tgl_fn"] = "1";
        }
        if (!empty($tmp) && $tmp != "") {
            $list_vert_permohonan["tmp"] = "1";
        }
        if ($pengurusan != "") {
            $list_vert_permohonan["pengurusan"] = "1";
        }
        if (!empty($dosen) && $dosen != "") {
            $list_vert_permohonan["dosen"] = "1";
        }
        
    //vert main pemohon delegasi in database
        $nik_del = $data_permohonan["nik_del"];
        $nama_del = $data_permohonan["nama_del"];
        $alamat_ktp_del = $data_permohonan["alamat_ktp_del"];
        $alamat_dom_del = $data_permohonan["alamat_dom_del"];
        $url_foto_del = $data_permohonan["url_foto_del"];
        $url_ktp_del = $data_permohonan["url_ktp_del"];
        $url_srt_del = $data_permohonan["url_srt_del"];
        

        if (!empty($nik_del) && $nik_del != "") {
            $list_vert_delegasi["nik_del"] = "1";
        }
        if (!empty($nama_del) && $nama_del != "") {
            $list_vert_delegasi["nama_del"] = "1";
        }
        if (!empty($alamat_ktp_del) && $alamat_ktp_del != "") {
            $list_vert_delegasi["alamat_ktp_del"] = "1";
        }
        if (!empty($alamat_dom_del) && $alamat_dom_del != "") {
            $list_vert_delegasi["alamat_dom_del"] = "1";
        }
        if (!empty($url_foto_del) && $url_foto_del != "") {
            if(file_exists('./doc/foto_delegasi/'.$url_foto_del)){
                $list_vert_delegasi["url_foto_del"] = "1";
            }             
        }
        if (!empty($url_ktp_del) && $url_ktp_del != "") {
            if(file_exists('./doc/ktp_delegasi/'.$url_ktp_del)){
                $list_vert_delegasi["url_ktp_del"] = "1";
            }            
        }
        if (!empty($url_srt_del) && $url_srt_del != "") {
            if(file_exists('./doc/srt_kuasa/'.$url_srt_del)){
                $list_vert_delegasi["url_srt_del"] = "1";
            }            
        }  
      
    //vert main pemohon document in database
        $nama_tdd = $data_permohonan["nama_tdd"];
        $no_surat_tdd = $data_permohonan["no_surat_tdd"];
        $tgl_surat_tdd = $data_permohonan["tgl_surat_tdd"];
        $jabatan_tdd = $data_permohonan["jabatan_tdd"];
        $url_ktp_pemohon = $data_permohonan["url_ktp_pemohon"];
        $url_proposal_pemohon = $data_permohonan["url_proposal_pemohon"];
        $url_sk_pemohon = $data_permohonan["url_sk_pemohon"];
        

        if (!empty($nama_tdd) && $nama_tdd != "") {
            $list_vert_doc["nama_tdd"] = "1";
        }
        if (!empty($no_surat_tdd) && $no_surat_tdd != "") {
            $list_vert_doc["no_surat_tdd"] = "1";
        }
        if (!empty($tgl_surat_tdd) && $tgl_surat_tdd != "") {
            $list_vert_doc["tgl_surat_tdd"] = "1";
        }
        if (!empty($jabatan_tdd) && $jabatan_tdd != "") {
            $list_vert_doc["jabatan_tdd"] = "1";
        }
        if (!empty($url_ktp_pemohon) && $url_ktp_pemohon != "") {
            if(file_exists('./doc/ktp/'.$url_ktp_pemohon)){
                $list_vert_doc["url_ktp_pemohon"] = "1";
            }            
        }
        if (!empty($url_proposal_pemohon) && $url_proposal_pemohon != "") {
            if(file_exists('./doc/proposal/'.$url_proposal_pemohon)){
                $list_vert_doc["url_proposal_pemohon"] = "1";
            }            
        }
        if (!empty($url_sk_pemohon) && $url_sk_pemohon != "") {
            if(file_exists('./doc/sk/'.$url_sk_pemohon)){
                $list_vert_doc["url_sk_pemohon"] = "1";
            }            
        }
        
    //vert all data in database

        if(!in_array("0", $list_vert_user)){
            $list_vert_all["vert_user"] = "1"; 
        }

        if(!in_array("0", $list_vert_permohonan)){
            $list_vert_all["vert_permohonan"] = "1"; 
        }

        if(!in_array("0", $list_vert_delegasi)){
            $list_vert_all["vert_delegasi"] = "1"; 
        }

        if(!in_array("0", $list_vert_doc)){
            $list_vert_all["vert_doc"] = "1"; 
        }

        if($pengurusan == "0"){
            $list_vert_all["vert_delegasi"] = "1";
        }

    //vert all data return database
        $main_vert_all = false;
        if(!in_array("0", $list_vert_all)){
            $main_vert_all = true;
        }

        return array(
                    "main_vert_all" => $main_vert_all,
                    "list_vert_all" => $list_vert_all
                );
        
    }
#=============================================================================#
#-------------------------------------------Varifikasi Data All---------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Verifikation Link-----------------#
#=============================================================================#

    private function get_link(){
        $id = $this->session->userdata("user_bangkes")["id_user"];
        $code = $this->generate_token->generate_link_email();
        
        $data = array(
                    "id_user"=>$id,
                    "time"=>date("Y-m-d H:i:s"),
                    "time_exp"=>date("Y-m-d H:i:s"),
                    "param"=>$code["param"],
                    "code"=>$code["code"]);
        
        if($this->reg_user->check_db_vert(array("id_user"=>$id), $data)){
            return base_url()."user_new/vertuser/activate/".$id."/?".$code["param"]."=".$code["code"];    
        }
        
        return null; 
    }
#=============================================================================#
#-------------------------------------------Verifikation Link-----------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Home------------------------------#
#=============================================================================#

    public function index(){
        $data["page"] = "home";
        $this->load->view("v_index_mhs_new",$data);
        // print_r($data);
    }
#=============================================================================#
#-------------------------------------------Home------------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Pendaftaran-----------------------#
#=============================================================================#

    public function index_pendaftaran(){
        $data["page"] = "pendaftaran";
        $id_user = $this->session->userdata("user_bangkes")["id_user"];

        $where = array(
                        "id_user"=>$id_user,
                        "no_register"=>"0",
                        "status_magang"=>"0",
                        "status_diterima"=>"0",
                        "status_active_mg"=>"0",
                        "pn.is_del"=>"0"
                    );
        // print_r($id_user);
        $data_pendaftaran = $this->mdun->get_pemohon($where);

        $data["pendaftaran"] = $data_pendaftaran;
        $data["permohonan"] = $this->mdun->get_keperluan_active();

        $data_tmp_magang = $this->mdun->get_tmp_magang();

        $data["tmp_magang"] = "";
        foreach ($data_tmp_magang as $r_data => $v_data) {
            $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
        }

        $data["tmp_magang_select"] = $this->mdun->get_tmp_magang_active();
        $data["user"] = $this->mdun->get_data_user(array("id_user"=>$id_user));
        $data["get_verifikation_data"] = $this->get_verifikation_data();

        $this->load->view("v_index_mhs_new",$data);
        // print_r("<pre>");
        // print_r($data);
    }
#=============================================================================#
#-------------------------------------------Pendaftaran-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Upload_Foto_Profil----------------#
#=============================================================================#
    private function upload_profil($id_user){
        $config['upload_path']          = './doc/foto_pemohon/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 200;
        $config['file_name']            = "foto_pemohon_".$id_user.".jpg";
        
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload('ft_prof')){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
        }

        return array(
            "status"=>0,
            "response"=>$return_item
        );
    }

    public function coba_hapus($id_user){
        $config['upload_path']          = "./doc/foto_pemohon/";
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 200;
        $config['file_name']            = "foto_pemohon_".$id_user.".jpg";

        unlink($config['upload_path'].$config['file_name']);
    }
#=============================================================================#
#-------------------------------------------Upload_Foto_Profil----------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Update Profil---------------------#
#=============================================================================#

    private function validate_update_profil(){
        $config_val_input = array(
                array(
                    'field'=>'alamat_dom',
                    'label'=>'Alamat_Domisili',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_email',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),array(
                    'field'=>'tlp',
                    'label'=>'Nomor Telephon',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
  
    private function validate_update_ins(){
        $config_val_input = array(
                array(
                    'field'=>'corp',
                    'label'=>'Pekerjaan',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
   
    private function validate_update_mhs(){
        $config_val_input = array(
                array(
                    'field'=>'uni',
                    'label'=>'Nama Universitas',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'jurusan',
                    'label'=>'Jurusan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'fak',
                    'label'=>'Fakultas',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
        
    }

    private function validate_update_karyawan(){
        $config_val_input = array(
                array(
                    'field'=>'ins',
                    'label'=>'Nama Korporasi',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_profil(){
        $msg_detail = array();

        $msg_detail["email"]            = "";
        $msg_detail["alamat_dom"]       = "";
        $msg_detail["tlp"]              = "";
        $msg_detail["nama_instansi"]    = "";
        $msg_detail["nama_universitas"] = "";
        $msg_detail["jurusan"]          = "";
        $msg_detail["fakultas"]         = "";
        $msg_detail["pekerjaan"]        = "";

        $msg_detail["foto_profil"]        = "";

    //cek valid input email, tlp, alamat
        if($this->validate_update_profil()){
            $email = $this->input->post("email");
            $tlp = $this->input->post("tlp");
            $alamat_dom = $this->input->post("alamat_dom");
            
            $email_old = $this->mdun->get_email_from_id(array("id_user"=>$this->session->userdata("user_bangkes")["id_user"]));
            $id_user = $this->session->userdata("user_bangkes")["id_user"];
            
    //cek name file available or not
                // if(!empty($_FILES["ft_prof"]["name"])){
                    // print_r("foto success");

                    $status_upload_profil = true;
                    if(!empty($_FILES["ft_prof"]["name"])){
                        $ret_foto = $this->upload_profil($id_user);

                        // print_r($ret_foto);

    //cek status return update foto
                        $status_upload_profil = false;
                        if($ret_foto["status"] == 1){
                            $foto_name = $ret_foto["response"]["upload_data"]["file_name"]; 
                            $status_upload_profil = true;  
                        }else{
                            // print_r(strip_tags($ret_foto["response"]["error"]));
                            $foto_name = "";
                            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                            $msg_detail["foto_profil"]        = strip_tags($ret_foto["response"]["error"]);
                            $status_upload_profil = true;
                        }
                    }
                    
    //cek instansi or akademisi
                    $status_ins = false;
                    if($this->validate_update_ins()){
                        // print_r("valid ins success");
                        $corp = $this->input->post("corp");
                        if($corp == 0){
                            if($this->validate_update_karyawan()){
                                $ins_data_update = $this->input->post("ins");
                                $status_ins = true;

                            }else{
                                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                                $msg_detail["nama_instansi"] = strip_tags(form_error("ins"));
                                $status_ins = false;
                            }
                        }else{
                            if($this->validate_update_mhs()){
                                $uni = $this->input->post("uni");
                                $prodi = $this->input->post("prodi");
                                $fak = $this->input->post("fak");
                                $jurusan = $this->input->post("jurusan");
                                
                                $ins_data_update = $prodi.";".$jurusan.";".$fak.";".$uni;

                                $status_ins = true;
                            }else{
                                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                                $msg_detail["nama_universitas"] = strip_tags(form_error("uni"));
                                $msg_detail["jurusan"]          = strip_tags(form_error("jurusan"));
                                $msg_detail["fakultas"]         = strip_tags(form_error("fak"));
                                $status_ins = false;
                            }
                        }
                    }else{
                        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                        $msg_detail["pekerjaan"] = strip_tags(form_error("corp"));
                        $status_ins = false;
                    }

                    
    //cek status instansi filter, if true update
                    if($this->mdun->cek_email_avail($email, $this->session->userdata("user_bangkes")["id_user"])){

                        if($status_ins && $status_upload_profil){
                            #-------------------------update---------------------------
                            $data_update = array(
                                        "email"=>$email,
                                        "tlp"=>$tlp,
                                        "alamat_dom"=>$alamat_dom,
                                        "pekerjaan" =>$corp,
                                        "instansi"=>$ins_data_update
                                    );

                            if(!empty($_FILES["ft_prof"]["name"])){
                                 $data_update["url_profil"] = $foto_name;
                                        
                            }
                            if($email != $email_old){
                                $data_update["status_active"] = "0";
                            }
                            
                            if($this->mdun->update_profil($data_update, array("id_user"=>$this->session->userdata("user_bangkes")["id_user"]))){
                                
                                if($email != $email_old){
                                    $link = $this->get_link();
                                    if($link != null){
                                        // update success && send email
                                        // echo "update success && send email";
                                        $this->sendemail->send_email_vert($email, $link, "Verifikasi Permintaan Perubahan Email");

                                        $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_PROF_SUC_EMAIL"));
                                            
                                        $this->session->unset_userdata("user_bangkes");
                                    }
                                }else{
                                    $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_PROF_SUC"));
                                }       
                            }else{
                                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                            }                   
                            #-------------------------update---------------------------
                        }
                    }else{
                        $msg_detail["email"] = "Email tersebut, telah digunakan oleh user lain sebagai email utama, silahkan gunakan email lain untuk mendaftar";
                    }
                    
                // }else{
                //     print_r("fail_foto");
                // }
                      
        }else{
            //input not valid
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            $msg_detail["email"]        =strip_tags(form_error("email"));
            $msg_detail["alamat_dom"]   =strip_tags(form_error("alamat_dom"));
            $msg_detail["tlp"]          =strip_tags(form_error("tlp"));
        }
        
        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
        // print_r($msg_array);
        $this->session->set_flashdata("response_profil", $msg_array);
        // print_r($_SESSION);
        redirect(base_url()."pendaftaran/mainform");
    }

    function oksk(){
        
    }
#=============================================================================#
#-------------------------------------------Update Profil---------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Insert_Permohonan-----------------#
#=============================================================================#
    private function validate_input_permohonan(){
        $config_val_input = array(
                array(
                    'field'=>'judul',
                    'label'=>'Judul Skripsi', 'alpha_numeric_spaces',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'permohonan',
                    'label'=>'Permohonan',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'tgl_st',
                    'label'=>'Tanggal Mulai',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'tgl_fn',
                    'label'=>'Tanggal Start',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'tmp',
                    'label'=>'Tempat Penelitian/Magang',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'pengurusan',
                    'label'=>'Cara Mengurus',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),

                

                array(
                    'field'=>'no_surat',
                    'label'=>'No Surat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'nama_tdd',
                    'label'=>'Persetujuan Atas Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'jabatan',
                    'label'=>'Jabatan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )

            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    private function validate_input_delegasi(){
        $config_val_input = array(
                array(
                    'field'=>'nik_del',
                    'label'=>'NIK',
                    'rules'=>'required|numeric|exact_length[16]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'is_unique'=>$this->response_message->get_error_msg("NIK_AVAIL"),
                        'exact_length[16]'=>"%s ".$this->response_message->get_error_msg("EXACT_LENGHT_NIK")
                    )
                       
                ),
                array(
                    'field'=>'nama_del',
                    'label'=>'Nama',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'alamat_ktp_del',
                    'label'=>'Alamat Sesuai KTP',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'alamat_dom_del',
                    'label'=>'Alamat Domisili',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),

            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }



    public function insert_pemohon(){

        $msg_detail = array(
                        "judul" => "",
                        "permohonan" => "",
                        "tgl_st" => "",
                        "tgl_fn" => "",
                        "tmp" => "",
                        "pengurusan" => "",
                        "dosen" => "",
                        "anggota" => "",


                        "nik_del" => "",
                        "nama_del" => "",
                        "alamat_ktp_del" => "",
                        "alamat_dom_del" => "",

                        "no_surat" => "",
                        "nama_tdd" => "",
                        "jabatan" => "",

                        "msg_url_foto_del" => "",
                        "msg_url_ktp_del" => "",
                        "msg_url_srt_del" => "",
                        "msg_url_ktp_pemohon" => "",
                        "msg_url_proposal_pemohon" => "",
                        "msg_url_sk_pemohon" => ""
                );
                   

    //check validate from view permohonan
        if($this->validate_input_permohonan()){
            $data_input = array();
    //get param insert from view
            $id_pemohon = $this->input->post("id_pemohon");

            $judul = $this->input->post("judul");
            $permohonan = $this->input->post("permohonan");
            $tgl_st = $this->input->post("tgl_st");
            $tgl_fn = $this->input->post("tgl_fn");
            $tmp = $this->input->post("tmp");
            $dosen = $this->input->post("dosen");
            $anggota = $this->input->post("anggota");
            $pengurusan = $this->input->post("pengurusan");

            $text_no_acc_opd = $this->input->post("text_no_acc_opd");
            
            $nik_del = $this->input->post("nik_del");
            $nama_del = $this->input->post("nama_del");
            $alamat_ktp_del = $this->input->post("alamat_ktp_del");
            $alamat_dom_del = $this->input->post("alamat_dom_del");

            $no_surat = $this->input->post("no_surat");
            $nama_tdd = $this->input->post("nama_tdd");
            $jabatan = $this->input->post("jabatan");
            $tgl_surat = $this->input->post("tgl_surat");

            $json_tmp = json_decode($tmp);
            $json_dosen = json_decode($dosen);

            $status_tmp = false;
            $msg_detail["tmp"] = "Tempat Magang/Penelitian tidak diperkenankan untuk kosong.";
            if(is_array($json_tmp)){
                if(count($json_tmp) != 0){
                    $status_tmp = true;
                    $msg_detail["tmp"] = "";
                }
            }

            $status_dosen = false;
            $msg_detail["dosen"] = "Dosen Pembimbing tidak diperkenankan untuk kosong.";
            if(is_array($json_dosen)){
                if(count($json_dosen) != 0){
                    $status_dosen = true;
                    $msg_detail["dosen"] = "";
                }
            }

            $url_ktp_pemohon = "";
            if(isset($_FILES["url_ktp_pemohon"])){
                $url_ktp_pemohon = $_FILES["url_ktp_pemohon"];
            }

            $url_sk_pemohon = "";
            if(isset($_FILES["url_sk_pemohon"])){
                $url_sk_pemohon = $_FILES["url_sk_pemohon"];
            }

            $url_proposal_pemohon = "";
            if(isset($_FILES["url_proposal_pemohon"])){
                $url_proposal_pemohon = $_FILES["url_proposal_pemohon"];
            }



            $url_foto_del = "";
            if(isset($_FILES["url_foto_del"])){
                $url_foto_del = $_FILES["url_foto_del"];
            }

            $url_ktp_del = "";
            if(isset($_FILES["url_ktp_del"])){
                $url_ktp_del = $_FILES["url_ktp_del"];
            }

            $url_srt_del = "";
            if(isset($_FILES["url_srt_del"])){
                $url_srt_del = $_FILES["url_srt_del"];
            }

            $id = $this->session->userdata("user_bangkes")["id_user"];
            $data_user = $this->mdun->get_data_user(array("id_user"=>$id));

            $instansi_pemohon = $data_user["instansi"];

            if($status_tmp == true && $status_dosen == true){
    //check date start and finish
            if($tgl_st < $tgl_fn || $tgl_st == $tgl_fn){

    //check data update or insert
                if($id_pemohon == "" or empty($id_pemohon)){
    //insert data
                    // print_r("insert");
                    $data_magang_aktiv = $this->mdun->get_pemohon(array("id_user"=>$id,
                                                                        "status_active_mg!="=>"2",
                                                                        "pn.is_del"=>"0"
                                                                    )
                                                                );
                    // print_r($data_magang_aktiv);
    //check aktive data in row pendafataram
                    if($data_magang_aktiv==0){

    //check delegasi status
                        if($pengurusan != "0"){
    //check delegasi verifikasi
                            if($this->validate_input_delegasi()){
                                $nik_del = $this->input->post("nik_del");
                                $nama_del = $this->input->post("nama_del");
                                $alamat_ktp_del = $this->input->post("alamat_ktp_del");
                                $alamat_dom_del = $this->input->post("alamat_dom_del");
                            }else {
                                $msg_detail["nik_del"] = strip_tags(form_error("nik_del"));
                                $msg_detail["nama_del"] = strip_tags(form_error("nama_del"));
                                $msg_detail["alamat_ktp_del"] = strip_tags(form_error("alamat_ktp_del"));
                                $msg_detail["alamat_dom_del"] = strip_tags(form_error("alamat_dom_del"));
                                // $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                                // $msg_array = $this->response_message->default_mgs($main_msg, $msg_detail);
                            }
                        }else{
                            $nik_del = "";
                            $nama_del = "";
                            $alamat_ktp_del = "";
                            $alamat_dom_del = "";
                        }

                        $query_insert = $this->db->query("SELECT last_key_pemohon('".$permohonan."', '0', '".$tgl_st."', '".$tgl_fn."', '".$judul."', '0', '".$id."', '".$instansi_pemohon."', \"".str_replace("\"", "'", $tmp)."\", \"".str_replace("\"", "'", $dosen)."\", \"".str_replace("\"", "'", $anggota)."\", '".$pengurusan."', '', '', '', '".$jabatan."', '".$nama_tdd."', '".$no_surat."', '".$tgl_surat."', '".$alamat_dom_del."', '".$alamat_ktp_del."', '".$nama_del."', '".$nik_del."', '', '', '', '0', '0', '0', '0000-00-00', '0000-00-00', '0', '0', '0000-00-00', '', \"".str_replace("\"", "'", $text_no_acc_opd)."\") AS last_key_pemohon;");

    // check insert data
                        if($query_insert->row_array()){
                            // print_r("update");
                            $id_pemohon_result = $query_insert->row_array()["last_key_pemohon"];
                            $where = array("id_pemohon"=>$id_pemohon_result);
                            $set = array();

    //document delegasi
                            if($url_foto_del != ""){
                                $ret_foto_del = $this->upload_foto_del($id_pemohon_result,"url_foto_del");
                                if($ret_foto_del["status"]){
                                    $foto_del_name = $ret_foto_del["response"]["upload_data"]["file_name"];
                                    $set["url_foto_del"] = $foto_del_name;   
                                }else{
                                    $msg_detail["msg_url_foto_del"] = strip_tags($ret_foto_del["response"]["error"]);
                                } 
                            }
                            if($url_ktp_del != ""){
                                $ret_ktp_del = $this->upload_ktp_del($id_pemohon_result, "url_ktp_del");
                                if($ret_ktp_del["status"]){
                                    $ktp_del_name = $ret_ktp_del["response"]["upload_data"]["file_name"];
                                    $set["url_ktp_del"] = $ktp_del_name;
                                }else{
                                    $msg_detail["msg_url_ktp_del"] = strip_tags($ret_ktp_del["response"]["error"]);
                                }
                            }
                            if($url_srt_del != ""){
                                $ret_kuasa_del = $this->upload_kuasa_del($id_pemohon_result, "url_srt_del");
                                if($ret_kuasa_del["status"]){
                                    $kuasa_del_name = $ret_kuasa_del["response"]["upload_data"]["file_name"];
                                    $set["url_srt_del"] = $kuasa_del_name;
                                }else{
                                    $msg_detail["msg_url_srt_del"] = strip_tags($ret_kuasa_del["response"]["error"]);
                                }                  
                            }

    //document pemohon
                            if($url_ktp_pemohon != ""){
                                $ret_ktp_pemohon = $this->upload_ktp($id_pemohon_result,"url_ktp_pemohon");
                                if($ret_ktp_pemohon["status"]){
                                    $ktp_name = $ret_ktp_pemohon["response"]["upload_data"]["file_name"];
                                    $set["url_ktp_pemohon"] = $ktp_name;   
                                }else{
                                    $msg_detail["msg_url_ktp_pemohon"] = strip_tags($ret_ktp_pemohon["response"]["error"]);
                                } 
                            }
                            if($url_proposal_pemohon != ""){
                                $ret_proposal_pemohon = $this->upload_proposal($id_pemohon_result, "url_proposal_pemohon");
                                if($ret_proposal_pemohon["status"]){
                                    $proposal_name = $ret_proposal_pemohon["response"]["upload_data"]["file_name"];
                                    $set["url_proposal_pemohon"] = $proposal_name;
                                }else{
                                    $msg_detail["msg_url_proposal_pemohon"] = strip_tags($ret_proposal_pemohon["response"]["error"]);
                                }
                            }
                            if($url_sk_pemohon != ""){
                                $ret_sk_pemohon = $this->upload_sk($id_pemohon_result, "url_sk_pemohon");
                                if($ret_sk_pemohon["status"]){
                                    $sk_name = $ret_sk_pemohon["response"]["upload_data"]["file_name"];
                                    $set["url_sk_pemohon"] = $sk_name;
                                }else{
                                    $msg_detail["msg_url_sk_pemohon"] = strip_tags($ret_sk_pemohon["response"]["error"]);
                                }                  
                            }

    //check update data insert profil
                            if($this->mdun->update_pemohon($set,$where)){ 
                                if(in_array(!"",$msg_detail)){
                                    // print_r("ada message_response");
                                    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                                    $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);    
                                }else{
                                    // print_r("no message_response");
                                    $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                                    $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
                                }
                                
                            }else{
                                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                                $msg_array = $this->response_message->default_mgs($main_msg, $msg_detail);
                            }
                        }else{
                            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                            $msg_array = $this->response_message->default_mgs($main_msg, $msg_detail);
                        }
                    }else {
                        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("AVAIL_DATA_PENELITIAN"));
                        $msg_array = $this->response_message->default_mgs($main_msg, $msg_detail);
                    }
                }else {
    //update data
                    // print_r("update");
                    $msg_detail == null;
                    $where = array("id_pemohon"=>$id_pemohon);

    //check delegasi status
                        if($pengurusan != "0"){
    //check delegasi verifikasi
                            if($this->validate_input_delegasi()){
                                $nik_del = $this->input->post("nik_del");
                                $nama_del = $this->input->post("nama_del");
                                $alamat_ktp_del = $this->input->post("alamat_ktp_del");
                                $alamat_dom_del = $this->input->post("alamat_dom_del");
                            }else {
                                $msg_detail["nik_del"] = strip_tags(form_error("nik_del"));
                                $msg_detail["nama_del"] = strip_tags(form_error("nama_del"));
                                $msg_detail["alamat_ktp_del"] = strip_tags(form_error("alamat_ktp_del"));
                                $msg_detail["alamat_dom_del"] = strip_tags(form_error("alamat_dom_del"));

                                // $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                                // $msg_array = $this->response_message->default_mgs($main_msg, $msg_detail);
                            }
                        }else{
                            $nik_del = "";
                            $nama_del = "";
                            $alamat_ktp_del = "";
                            $alamat_dom_del = "";
                        }


                    $set = array("id_permohonan"=>$permohonan,
                                    "tgl_start"=>$tgl_st,
                                    "tgl_selesai"=>$tgl_fn,
                                    "judul"=>$judul,
                                    "instansi_pendaftar"=>$instansi_pemohon,
                                    "id_bidang"=>str_replace("\"", "'",$tmp),
                                    "dosen_pembimbing"=>str_replace("\"", "'",$dosen),
                                    "anggota"=>str_replace("\"", "'",$anggota),
                                    "status_pendelegasian"=>$pengurusan,
                                    "text_no_acc_opd"=>str_replace("\"", "'",$text_no_acc_opd),

                                    "jabatan_tdd"=>$jabatan,
                                    "nama_tdd"=>$nama_tdd,
                                    "no_surat_tdd"=>$no_surat,
                                    "tgl_surat_tdd"=>$tgl_surat,

                                    "alamat_dom_del"=>$alamat_dom_del,
                                    "alamat_ktp_del"=>$alamat_ktp_del,
                                    "nama_del"=>$nama_del,
                                    "nik_del"=>$nik_del,

                                    "no_register"=>"0",
                                    "status_pendaftaran"=>"0",
                                    "status_magang"=>"0",
                                    "status_diterima"=>"0",

                                    "tgl_daftar"=>"0000-00-00 00:00:00.000000",
                                    "tgl_acc"=>"0000-00-00 00:00:00.000000",
                                    "tgl_diterima"=>"0000-00-00 00:00:00.000000"
                                );
                    
    //document delegasi
                    if($url_foto_del != ""){
                        $ret_foto_del = $this->upload_foto_del($id_pemohon,"url_foto_del");
                        if($ret_foto_del["status"]){
                            $foto_del_name = $ret_foto_del["response"]["upload_data"]["file_name"];
                            $set["url_foto_del"] = $foto_del_name;   
                        }else{
                            $msg_detail["msg_url_foto_del"] = strip_tags($ret_foto_del["response"]["error"]);
                        } 
                    }
                    if($url_ktp_del != ""){
                        $ret_ktp_del = $this->upload_ktp_del($id_pemohon, "url_ktp_del");
                        if($ret_ktp_del["status"]){
                            $ktp_del_name = $ret_ktp_del["response"]["upload_data"]["file_name"];
                            $set["url_ktp_del"] = $ktp_del_name;
                        }else{
                            $msg_detail["msg_url_ktp_del"] = strip_tags($ret_ktp_del["response"]["error"]);
                        }
                    }
                    if($url_srt_del != ""){
                        $ret_kuasa_del = $this->upload_kuasa_del($id_pemohon, "url_srt_del");
                        if($ret_kuasa_del["status"]){
                            $kuasa_del_name = $ret_kuasa_del["response"]["upload_data"]["file_name"];
                            $set["url_srt_del"] = $kuasa_del_name;
                        }else{
                            $msg_detail["msg_url_srt_del"] = strip_tags($ret_kuasa_del["response"]["error"]);
                        }                  
                    }

    //document pemohon
                    if($url_ktp_pemohon != ""){
                        $ret_ktp_pemohon = $this->upload_ktp($id_pemohon,"url_ktp_pemohon");
                        if($ret_ktp_pemohon["status"]){
                            $ktp_name = $ret_ktp_pemohon["response"]["upload_data"]["file_name"];
                            $set["url_ktp_pemohon"] = $ktp_name;   
                        }else{
                            $msg_detail["msg_url_ktp_pemohon"] = strip_tags($ret_ktp_pemohon["response"]["error"]);
                        } 
                    }
                    if($url_proposal_pemohon != ""){
                        $ret_proposal_pemohon = $this->upload_proposal($id_pemohon, "url_proposal_pemohon");
                        if($ret_proposal_pemohon["status"]){
                            $proposal_name = $ret_proposal_pemohon["response"]["upload_data"]["file_name"];
                            $set["url_proposal_pemohon"] = $proposal_name;
                        }else{
                            $msg_detail["msg_url_proposal_pemohon"] = strip_tags($ret_proposal_pemohon["response"]["error"]);
                        }
                    }
                    if($url_sk_pemohon != ""){
                        $ret_sk_pemohon = $this->upload_sk($id_pemohon, "url_sk_pemohon");
                        if($ret_sk_pemohon["status"]){
                            $sk_name = $ret_sk_pemohon["response"]["upload_data"]["file_name"];
                            $set["url_sk_pemohon"] = $sk_name;
                        }else{
                            $msg_detail["msg_url_sk_pemohon"] = strip_tags($ret_sk_pemohon["response"]["error"]);
                        }                  
                    }

    //check update data
                    if($this->mdun->update_pemohon($set,$where)){
                        if(in_array(!"",$msg_detail)){
                            // print_r("yes message_response");
                            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                            $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
                        }else{
                            // print_r("no message_response");
                            $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                            $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
                        }
                    }else{
                        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
                    }
                }  

            }else{
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DATE_START_FAIL"));
                $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
            }
            }else{
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
            }          
        }else{
            // print_r(validation_errors());
            $msg_detail = array(
                        "judul" => strip_tags(form_error("judul")),
                        "permohonan" => strip_tags(form_error("permohonan")),
                        "tgl_st" => strip_tags(form_error("tgl_st")),
                        "tgl_fn" => strip_tags(form_error("tgl_fn")),
                        "tmp" => strip_tags(form_error("tmp")),
                        "pengurusan" => strip_tags(form_error("pengurusan")),

                        "no_surat" => strip_tags(form_error("no_surat")),
                        "nama_tdd" => strip_tags(form_error("nama_tdd")),
                        "jabatan" => strip_tags(form_error("jabatan")),

                        "msg_url_foto_del" => "",
                        "msg_url_ktp_del" => "",
                        "msg_url_srt_del" => "",
                        "msg_url_ktp_pemohon" => "",
                        "msg_url_proposal_pemohon" => "",
                        "msg_url_sk_pemohon" => ""
                );
                            
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
        }

        print_r(json_encode($msg_array));
         
    }

    //delegasi upload document
    private function upload_foto_del($id_pemohon, $param_post){
        $config['upload_path']          = './doc/foto_delegasi/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 200;
        
        $config['file_name']            = "foto_delegasi_".$id_pemohon.".jpg";
        
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload($param_post)){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        //print_r($error);
            return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }


    private function upload_ktp_del($id_pemohon, $param_post){
        $config['upload_path']          = './doc/ktp_delegasi/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 512;
        
        $config['file_name']            = "ktp_delegasi_".$id_pemohon.".jpg";
        
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload($param_post)){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        //print_r($error);
            return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }

    private function upload_kuasa_del($id_pemohon, $param_post){
        $config['upload_path']          = './doc/srt_kuasa/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 1024;
        
        $config['file_name']            = "kuasa_delegasi_".$id_pemohon.".jpg";
        
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }

        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload($param_post)){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        //print_r($error);
            return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }


    //pemohon upload document
    private function upload_ktp($id_pemohon, $param_post){
        $config['upload_path']          = './doc/ktp/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 512;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 0;
        //$config['encrypt_name']         = true;
        $config['file_name']            = "ktp_".$id_pemohon.".jpg";
        
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload($param_post)){
            $return_item = array('error' => $this->upload->display_errors());
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        //print_r($error);
            return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }

    private function upload_proposal($id_pemohon, $param_post){
        $config['upload_path']          = './doc/proposal/';
        $config['allowed_types']        = "pdf";
        $config['max_size']             = 2048;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 0;
        //$config['encrypt_name']         = true;
        $config['file_name']            = "proposal_".$id_pemohon.".pdf";
        //print_r($config);
        
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload($param_post)){
            $return_item = array('error' => $this->upload->display_errors());
            
            //print_r($error);
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        
        return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }
    
    private function upload_sk($id_pemohon, $param_post){
        $config['upload_path']          = './doc/sk/';
        $config['allowed_types']        = "jpg|png";
        $config['max_size']             = 1024;
        
        $config['file_name']            = "sk_".$id_pemohon.".jpg";
        
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        //print_r($config);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        $return_item = null;
        if (!$this->upload->do_upload($param_post)){
            $return_item = array('error' => $this->upload->display_errors());
            
            //print_r($error);
        }else{
            $return_item = array('upload_data' => $this->upload->data());
            return array(
                        "status"=>1,
                        "response"=>$return_item
                    );
            //print_r($data);
        }
        return array(
                        "status"=>0,
                        "response"=>$return_item
                    );
    }
#=============================================================================#
#-------------------------------------------Insert_Permohonan-----------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------fixed_pendaftaran-----------------#
#=============================================================================#
    public function index_fixed_pendaftaran(){
        $data_vert_all = $this->get_verifikation_data();

        if($data_vert_all["main_vert_all"]){
            $data["page"] = "fixed_pendaftaran";

            $id_user = $this->session->userdata("user_bangkes")["id_user"];

            $where = array(
                            "id_user"=>$id_user,
                            "no_register"=>"0",
                            "status_magang"=>"0",
                            "status_diterima"=>"0",
                            "status_active_mg"=>"0",
                            "pn.is_del"=>"0"
                        );
            // print_r($id_user);
            $data_pendaftaran = $this->mdun->get_pemohon($where);

            $data["pendaftaran"] = $data_pendaftaran;
            $data["permohonan"] = $this->mdun->get_keperluan(array("jenis_kegiatan"=>"1"));
            

            $data["tmp_magang"] = "";
            $data_tmp_magang = $this->mdun->get_tmp_magang();

            foreach ($data_tmp_magang as $r_data => $v_data) {
                $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
            }

            $data["tmp_magang_select"] = $this->mdun->get_tmp_magang();

            $data["user"] = $this->mdun->get_data_user(array("id_user"=>$id_user));
            $data["get_verifikation_data"] = $data_vert_all;
            $data["id_pemohon"] = $data_pendaftaran["id_pemohon"];

            $this->load->view("v_index_mhs_new",$data);
        }else {
            redirect(base_url()."pendaftaran/mainform");
        }
        
    }
#=============================================================================#
#-------------------------------------------fixed_pendaftaran-----------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------update_status_pendaftaran---------#
#=============================================================================#
    public function validate_status_pendaftaran(){
        $config_val_input = array(
                array(
                    'field'=>'id_pemohon',
                    'label'=>'Id Permohonan',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_status_pendaftaran(){
        $id_pemohon = $this->input->post("id_pemohon");
        $cek_data = $this->input->post("cek_pendaftaran");

        $msg_detail = array("id_pemohon"=>"");

        if($cek_data){
            if($this->validate_status_pendaftaran()){
                if($this->mdun->update_pemohon(array("status_pendaftaran"=>"1","tgl_daftar"=>date("Y-m-d H:i:s")),array("status_pendaftaran"=>"0", "status_magang"=>"0", "status_diterima"=>"0", "id_pemohon"=>$id_pemohon))){
                    $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }else{
                    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                }
            }else{
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                $msg_detail["id_pemohon"]=form_error("id_pemohon");
            }
        }else{
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            $msg_detail["id_pemohon"]=form_error("id_pemohon");
        }
        

        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=============================================================================#
#-------------------------------------------update_status_pendaftaran---------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------History---------------------------#
#=============================================================================#
    public function index_history(){
        $id_user = $this->session->userdata("user_bangkes")["id_user"];
        $data["pemohon"] = $this->mdun->get_pemohon_complete(array("id_user"=>$id_user,
                                                                    "pn.is_del"=>"0")
                                                            );

        $data["tmp_magang"] = "";
        $data_tmp_magang = $this->mdun->get_tmp_magang();

        foreach ($data_tmp_magang as $r_data => $v_data) {
            $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
        }

        $data["page"] = "history";

        $this->load->view("v_index_mhs_new", $data);
    }
#=============================================================================#
#-------------------------------------------History---------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Action_Pemohon--------------------#
#=============================================================================#
    public function delete_pemohon(){
        $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = null;
        if($_POST["id_pemohon"]){
            $id_pemohon = $this->input->post("id_pemohon");
            if($this->mdun->delete_pemohon(array("id_pemohon"=>$id_pemohon))){
                $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
        print_r(json_encode($msg_array));
        
    }
#=============================================================================#
#-------------------------------------------Action_Pemohon--------------------#
#=============================================================================#

}
