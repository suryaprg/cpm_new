<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginuser extends CI_Controller {

    function __construct(){
		parent::__construct();
        $this->load->library("response_message");
        
        $this->load->model("user/login_user", "lu");
        
        if($this->session->userdata("user_bangkes")["is_log"] == 1){
            redirect(base_url()."pendaftaran/home");
        }
        //$this->data['users'] = $this->users_model->getAllUsers();
	}
    
	public function index(){
	   $this->load->view('user_front/v_login');
	}
	
    function aksi_login(){
        //$msg_array = $this->response_message->default_mgs(null, null);
        if($this->val_form_log()){
            $email = $this->input->post('email');
    		$password = $this->input->post('password');
    		$where = array(
    			'email' => $email,
    			'password' => md5($password),
                'status_active' => "1"
    			);
    		
            $cek = $this->lu->get_user_login($where);
    		if($cek != null){
                $cek["is_log"] = 1;
    			
                $this->session->set_userdata("user_bangkes",$cek);
    			
                $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC"));
                $msg_array = $this->response_message->default_mgs($main_msg,null);
                $this->session->set_flashdata("response_login", $msg_array);         
                
                redirect(base_url("pendaftaran/home"));
                echo "sukses Login";
                print_r($_SESSION);
                
    		}else{
                echo "gagal";
                print_r($_SESSION);
                
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
                $msg_array = $this->response_message->default_mgs($main_msg,null);
                $this->session->set_flashdata("response_login", $msg_array);         
                
    			redirect(base_url("home/login"));
    		}     
        }else{
            $msg_detail = array(
                                "email" => form_error("email"),
                                "password" => form_error("password")  
                            );
                            
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            $msg_array = $this->response_message->default_mgs($main_msg,$msq_detail);
            $this->session->set_flashdata("response_login", $msg_array);
        }
		
	}
    
    private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ". $this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
}
