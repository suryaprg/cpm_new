<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('user/main_data_user_new', 'mdun');
        
        $this->load->library("response_message");
        $this->load->library("generate_token");
        $this->load->library("sendemail");
        
    }

    public function index(){
        $this->session->unset_userdata("user_bangkes");
        // print_r("<pre>");
        // print_r($_SESSION);
        redirect(base_url('home/login'));
    }
}