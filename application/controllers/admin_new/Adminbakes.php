<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminbakes extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('admin/admin', 'ad');

        $this->load->model('admin/admin_new', 'adn');
        $this->load->model('user/main_data_user_new', 'mdun');

        $this->load->model('user/main_data_doc', 'mdd');
        $this->load->model('user/main_data_prev_all', 'mdpa');

        $this->load->library("response_message");
        $this->load->library("generate_token");
        $this->load->library("sendemail");

		//$this->load->model('Users_model');
        $session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"]== 1 and $session["is_log"] == 1){
                if($session["id_lv"] != 2){
                    redirect(base_url()."back-admin/login");    
                }
                
            }
        }else{
            redirect(base_url()."back-admin/login");
        }
	}

#=============================================================================#
#-------------------------------------------Varifikasi Data All---------------#
#=============================================================================#

    public function get_verifikation_data($id_user, $id_pemohon){
        $data_permohonan_where = array(
                                    "id_pemohon"=>$id_pemohon);
        $data_permohonan = $this->mdun->get_pemohon($data_permohonan_where);

        $data_user_where = array(
                                "id_user"=>$id_user);
        $data_user = $this->mdun->get_data_user($data_user_where);

    //set default value for verifikation

        $list_vert_user = array(
                            "email"=>"0",
                            "nama"=>"0",
                            "alamat"=>"0",
                            "alamat_dom"=>"0",
                            "nik"=>"0",
                            "tlp"=>"0",
                            "tgl_lhr"=>"0",
                            "url_profil"=>"0",
                            "pekerjaan"=>"0",
                            "instansi"=>"0"
                        );

        $list_vert_permohonan = array(
                        "judul" => "0",
                        "permohonan" => "0",
                        "tgl_st" => "0",
                        "tgl_fn" => "0",
                        "tmp" => "0",
                        "pengurusan" => "0",
                        "dosen" => "0",
                );

        $list_vert_delegasi = array(
                        "nik_del" => "0",
                        "nama_del" => "0",
                        "alamat_ktp_del" => "0",
                        "alamat_dom_del" => "0",
                        "url_foto_del" => "0",
                        "url_ktp_del" => "0",
                        "url_srt_del" => "0"
                );


        $list_vert_doc = array(
                        "nama_tdd" => "0",
                        "no_surat_tdd" => "0",
                        "tgl_surat_tdd" => "0",
                        "jabatan_tdd" => "0",
                        "url_ktp_pemohon" => "0",
                        "url_proposal_pemohon" => "0",
                        "url_sk_pemohon" => "0"
                );

        $list_vert_all = array(
                    "vert_user"=>"0",
                    "vert_permohonan"=>"0",
                    "vert_delegasi"=>"0",
                    "vert_doc"=>"0"
                );

    //vert user data in database
        $email = $data_user["email"];
        $nama = $data_user["nama"];
        $alamat = $data_user["alamat"];
        $alamat_dom = $data_user["alamat_dom"];
        $nik = $data_user["nik"];
        $tlp = $data_user["tlp"];
        $tgl_lhr = $data_user["tgl_lhr"];
        $url_profil = $data_user["url_profil"];
        $pekerjaan = $data_user["pekerjaan"];
        $instansi = $data_user["instansi"];

        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $list_vert_user["email"] = "1";
        }

        if (!empty($nama) && $nama != "") {
            $list_vert_user["nama"] = "1";
        }

        if (!empty($alamat) && $alamat != "") {
            $list_vert_user["alamat"] = "1";
        }

        if (!empty($alamat_dom) && $alamat_dom != "") {
            $list_vert_user["alamat_dom"] = "1";
        }

        if (!empty($nik) && $nik != "") {
            $list_vert_user["nik"] = "1";
        }

        if (!empty($tlp) && $tlp != "") {
            $list_vert_user["tlp"] = "1";
        }

        if (!empty($tgl_lhr) && $tgl_lhr != "") {
            $list_vert_user["tgl_lhr"] = "1";
        }

        if (!empty($url_profil) && $url_profil != "") {
            if(file_exists('./doc/foto_pemohon/'.$url_profil)){
                $list_vert_user["url_profil"] = "1";
            }
        }
        if ($pekerjaan != "") {
            $list_vert_user["pekerjaan"] = "1";
        }
        if (!empty($instansi) && $instansi != "") {
            $list_vert_user["instansi"] = "1";
        }

    //vert main pemohon in database
        $judul = $data_permohonan["judul"];
        $permohonan = $data_permohonan["id_permohonan"];
        $tgl_st = $data_permohonan["tgl_start"];
        $tgl_fn = $data_permohonan["tgl_selesai"];
        $tmp = $data_permohonan["id_bidang"];
        $pengurusan = $data_permohonan["status_pendelegasian"];
        $dosen = $data_permohonan["dosen_pembimbing"];
        

        if (!empty($judul) && $judul != "") {
            $list_vert_permohonan["judul"] = "1";
        }
        if (!empty($permohonan) && $permohonan != "") {
            $list_vert_permohonan["permohonan"] = "1";
        }
        if (!empty($tgl_st) && $tgl_st != "") {
            $list_vert_permohonan["tgl_st"] = "1";
        }
        if (!empty($tgl_fn) && $tgl_fn != "") {
            $list_vert_permohonan["tgl_fn"] = "1";
        }
        if (!empty($tmp) && $tmp != "") {
            $list_vert_permohonan["tmp"] = "1";
        }
        if ($pengurusan != "") {
            $list_vert_permohonan["pengurusan"] = "1";
        }
        if (!empty($dosen) && $dosen != "") {
            $list_vert_permohonan["dosen"] = "1";
        }
        
    //vert main pemohon delegasi in database
        $nik_del = $data_permohonan["nik_del"];
        $nama_del = $data_permohonan["nama_del"];
        $alamat_ktp_del = $data_permohonan["alamat_ktp_del"];
        $alamat_dom_del = $data_permohonan["alamat_dom_del"];
        $url_foto_del = $data_permohonan["url_foto_del"];
        $url_ktp_del = $data_permohonan["url_ktp_del"];
        $url_srt_del = $data_permohonan["url_srt_del"];
        

        if (!empty($nik_del) && $nik_del != "") {
            $list_vert_delegasi["nik_del"] = "1";
        }
        if (!empty($nama_del) && $nama_del != "") {
            $list_vert_delegasi["nama_del"] = "1";
        }
        if (!empty($alamat_ktp_del) && $alamat_ktp_del != "") {
            $list_vert_delegasi["alamat_ktp_del"] = "1";
        }
        if (!empty($alamat_dom_del) && $alamat_dom_del != "") {
            $list_vert_delegasi["alamat_dom_del"] = "1";
        }
        if (!empty($url_foto_del) && $url_foto_del != "") {
            if(file_exists('./doc/foto_delegasi/'.$url_foto_del)){
                $list_vert_delegasi["url_foto_del"] = "1";
            }             
        }
        if (!empty($url_ktp_del) && $url_ktp_del != "") {
            if(file_exists('./doc/ktp_delegasi/'.$url_ktp_del)){
                $list_vert_delegasi["url_ktp_del"] = "1";
            }            
        }
        if (!empty($url_srt_del) && $url_srt_del != "") {
            if(file_exists('./doc/srt_kuasa/'.$url_srt_del)){
                $list_vert_delegasi["url_srt_del"] = "1";
            }            
        }  
      
    //vert main pemohon document in database
        $nama_tdd = $data_permohonan["nama_tdd"];
        $no_surat_tdd = $data_permohonan["no_surat_tdd"];
        $tgl_surat_tdd = $data_permohonan["tgl_surat_tdd"];
        $jabatan_tdd = $data_permohonan["jabatan_tdd"];
        $url_ktp_pemohon = $data_permohonan["url_ktp_pemohon"];
        $url_proposal_pemohon = $data_permohonan["url_proposal_pemohon"];
        $url_sk_pemohon = $data_permohonan["url_sk_pemohon"];
        

        if (!empty($nama_tdd) && $nama_tdd != "") {
            $list_vert_doc["nama_tdd"] = "1";
        }
        if (!empty($no_surat_tdd) && $no_surat_tdd != "") {
            $list_vert_doc["no_surat_tdd"] = "1";
        }
        if (!empty($tgl_surat_tdd) && $tgl_surat_tdd != "") {
            $list_vert_doc["tgl_surat_tdd"] = "1";
        }
        if (!empty($jabatan_tdd) && $jabatan_tdd != "") {
            $list_vert_doc["jabatan_tdd"] = "1";
        }
        if (!empty($url_ktp_pemohon) && $url_ktp_pemohon != "") {
            if(file_exists('./doc/ktp/'.$url_ktp_pemohon)){
                $list_vert_doc["url_ktp_pemohon"] = "1";
            }            
        }
        if (!empty($url_proposal_pemohon) && $url_proposal_pemohon != "") {
            if(file_exists('./doc/proposal/'.$url_proposal_pemohon)){
                $list_vert_doc["url_proposal_pemohon"] = "1";
            }            
        }
        if (!empty($url_sk_pemohon) && $url_sk_pemohon != "") {
            if(file_exists('./doc/sk/'.$url_sk_pemohon)){
                $list_vert_doc["url_sk_pemohon"] = "1";
            }            
        }
        
    //vert all data in database

        if(!in_array("0", $list_vert_user)){
            $list_vert_all["vert_user"] = "1"; 
        }

        if(!in_array("0", $list_vert_permohonan)){
            $list_vert_all["vert_permohonan"] = "1"; 
        }

        if(!in_array("0", $list_vert_delegasi)){
            $list_vert_all["vert_delegasi"] = "1"; 
        }

        if(!in_array("0", $list_vert_doc)){
            $list_vert_all["vert_doc"] = "1"; 
        }

        if($pengurusan == "0"){
            $list_vert_all["vert_delegasi"] = "1";
        }

    //vert all data return database
        $main_vert_all = false;
        if(!in_array("0", $list_vert_all)){
            $main_vert_all = true;
        }

        return array(
                    "main_vert_all" => $main_vert_all,
                    "list_vert_all" => $list_vert_all
                );
        
    }
#=============================================================================#
#-------------------------------------------Varifikasi Data All---------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Bakesbangpol Home-----------------#
#=============================================================================#
    
    public function index(){
        $data['page'] = 'home_admin';

        $data["pemohon_req"] = count($this->adn->get_pemohon(array("1")));
        $data["pemohon_acc"] = count($this->adn->get_pemohon_acc_active(array("1")));
        $data["pemohon_rem"] = count($this->adn->get_pemohon_report(array("1")));

        $data["pemohon_all"] = count($this->adn->get_pemohon_all(array("1")));

        // print_r("<pre>");
        $data["pemohon_list"] = array(); 
        
        $th = date("Y");
        for ($i= 1; $i <= 12 ; $i++) { 
            $data["pemohon_list"][$i] = count($this->adn->get_pemohon_all_graph(array("1"), (string)$th, (string)$i));
        }

        $this->load->view('v_index_admin_new', $data);
    }
#=============================================================================#
#-------------------------------------------Bakesbangpol_Home-----------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Bakesbangpol_Penelitian-----------#
#=============================================================================#
    public function penelitian(){
        $data["page"] = "bakes_penelitian";
        $data["pemohon"] = $this->adn->get_pemohon(array("1"));

        $data["tmp_magang"] = "";
        $data_tmp_magang = $this->mdun->get_tmp_magang();

        foreach ($data_tmp_magang as $r_data => $v_data) {
            $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
        }
        
		$this->load->view('v_index_admin_new', $data); 
    }
#=============================================================================#
#-------------------------------------------Bakesbangpol_Penelitian-----------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Bakesbangpol_Detail---------------#
#=============================================================================#

    public function detail_pemohon(){
            if(isset($_POST["id_pemohon"])){
                $id_pemohon = $this->input->post("id_pemohon");
                $data_pendaftaran = $this->mdun->get_pemohon(array("id_pemohon"=>$id_pemohon));
                $data_vert_all = $this->get_verifikation_data($data_pendaftaran["id_user"], $id_pemohon);

                $data["pendaftaran"] = $data_pendaftaran;
                $data["permohonan"] = $this->mdun->get_keperluan(array("jenis_kegiatan"=>"1"));
                

                $data["tmp_magang"] = "";
                $data_tmp_magang = $this->mdun->get_tmp_magang();

                foreach ($data_tmp_magang as $r_data => $v_data) {
                    $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
                }

                $data["tmp_magang_select"] = $this->mdun->get_tmp_magang();

                $data["user"] = $this->mdun->get_data_user(array("id_user"=>$data_pendaftaran["id_user"]));
                $data["id_pemohon"] = $data_pendaftaran["id_pemohon"];
                $data["get_verifikation_data"] = $data_vert_all;

                // print_r("<pre>");
                // print_r($data);
                $this->load->view("admin_new/admin_data_detail",$data);
            }
            

            // $this->load->view("v_index_mhs_new",$data);
    }
#=============================================================================#
#-------------------------------------------Bakesbangpol_Detail---------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Permohonan_Accept-----------------#
#=============================================================================#
    public function validate_acc_pemohon(){
        $config_val_input = array(
                array(
                    'field'=>'id_pemohon',
                    'label'=>'Id Permohonan',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                ), array(
                    'field'=>'id_user',
                    'label'=>'Id User',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function prove(){

        // print_r($_SESSION);
        $msg_detail = array("id_pemohon"=>"","id_user"=>"");

        if($this->validate_acc_pemohon()){
            // print_r("ok");
            $id_pemohon = $this->input->post("id_pemohon");
            $id_user    = $this->input->post("id_user");
            $cek_data   = $this->input->post("cek_pendaftaran");
            $tgl_now    = date("Y-m-d H:i:s");
            $text_no_acc = "";
            $permohonan = "KL";
            $id_admin =  $this->session->userdata("admin_lv_1")["id_admin"];
            $status_magang = "1";
            $status_active = "1";

            if($cek_data){
                $no_register = $this->adn->update_register($id_pemohon, $permohonan, $tgl_now, $id_admin, $text_no_acc, $status_magang, $status_active);
                if($no_register){
                    $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }else{
                    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                }
            }else{
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                $msg_detail["cek_data"]="silahkan centan persyaratan untuk melanjutkan proses";
            }
        }else{
            // print_r("fail");
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            $msg_detail["id_pemohon"]=form_error("id_pemohon");
            $msg_detail["id_user"]=form_error("id_user");
        }

        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
        print_r(json_encode($msg_array));

    }
#=============================================================================#
#-------------------------------------------Permohonan_Accept-----------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Permohonan_Remove-----------------#
#=============================================================================#
    public function validate_remove_pemohon(){
        $config_val_input = array(
                array(
                    'field'=>'id_pemohon',
                    'label'=>'Id Permohonan',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                ), array(
                    'field'=>'id_user',
                    'label'=>'Id User',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                ), array(
                    'field'=>'keterangan',
                    'label'=>'Keterangan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }


    public function remove(){
        $msg_detail = array("id_pemohon"=>"","id_user"=>"","keterangan"=>"");

        if($this->validate_remove_pemohon()){
            // print_r("ok");
            $id_pemohon = $this->input->post("id_pemohon");
            $id_user    = $this->input->post("id_user");
            $cek_data   = $this->input->post("cek_pendaftaran");
            $tgl_now    = date("Y-m-d H:i:s");
            $text_no_acc = $this->input->post("keterangan");
            $permohonan = "KL";
            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];
            $status_magang = "2";
            $status_active = "2";

            if($cek_data){
                $set = array("status_magang"=>$status_magang,
                            "status_active_mg"=>$status_active,
                            "tgl_acc"=>$tgl_now,
                            "admin_acc"=>$id_admin,
                            "text_no_acc_bakes"=>trim(strip_tags($text_no_acc)));

                $where = array("id_pemohon"=>$id_pemohon);
                $no_register = $this->adn->remove_register($set, $where);
                if($no_register){
                    $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }else{
                    $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                }
            }else{
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                $msg_detail["cek_data"]="silahkan centan persyaratan untuk melanjutkan proses";
            }
        }else{
            // print_r("fail");
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            $msg_detail["id_pemohon"]=form_error("id_pemohon");
            $msg_detail["id_user"]=form_error("id_user");
            $msg_detail["keterangan"]=form_error("keterangan");
        }

        $msg_array = $this->response_message->default_mgs($main_msg,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=============================================================================#
#-------------------------------------------Permohonan_Remove-----------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Bakesbangpol_monitor--------------#
#=============================================================================#
 
    public function penelitian_active(){
        $data["page"] = "bakes_penelitian_active";
        $data["pemohon"] = $this->adn->get_pemohon_acc_active(array("1"));

        $data["tmp_magang"] = "";
        $data_tmp_magang = $this->mdun->get_tmp_magang();

        foreach ($data_tmp_magang as $r_data => $v_data) {
            $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
        }
        
        $this->load->view('v_index_admin_new', $data); 
    }
#=============================================================================#
#-------------------------------------------Bakesbangpol_monitor--------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Bakesbangpol_Detail---------------#
#=============================================================================#
    public function detail_pemohon_active(){
        if(isset($_POST["id_pemohon"])){
            $id_pemohon = $this->input->post("id_pemohon");
            $data_pendaftaran = $this->mdun->get_pemohon(array("id_pemohon"=>$id_pemohon));
            $data_vert_all = $this->get_verifikation_data($data_pendaftaran["id_user"], $id_pemohon);

            $data["pendaftaran"] = $data_pendaftaran;
            $data["permohonan"] = $this->mdun->get_keperluan(array("jenis_kegiatan"=>"1"));    

            $data["tmp_magang"] = "";
            $data_tmp_magang = $this->mdun->get_tmp_magang();

            foreach ($data_tmp_magang as $r_data => $v_data) {
                $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
            }

            $data["tmp_magang_select"] = $this->mdun->get_tmp_magang();

            $data["user"] = $this->mdun->get_data_user(array("id_user"=>$data_pendaftaran["id_user"]));
            $data["id_pemohon"] = $data_pendaftaran["id_pemohon"];
            $data["get_verifikation_data"] = $data_vert_all;

            $this->load->view("admin_new/admin_data_detail_active",$data);
        }
    }
#=============================================================================#
#-------------------------------------------Bakesbangpol_Detail---------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Bakesbangpol_report---------------#
#=============================================================================#
    public function penelitian_report(){
        $data["page"] = "bakes_report";
        $data["pemohon"] = $this->adn->get_pemohon_report(array("1"));

        $data["tmp_magang"] = "";
        $data_tmp_magang = $this->mdun->get_tmp_magang();

        foreach ($data_tmp_magang as $r_data => $v_data) {
            $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas; 
        }
        
        $this->load->view('v_index_admin_new', $data); 
    }
#=============================================================================#
#-------------------------------------------Bakesbangpol_report---------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Data_filter_all-------------------#
#=============================================================================#

    public function validation_penelitian_laporan(){
        $config_val_input = array(
                array(
                    'field'=>'kategori',
                    'label'=>'Kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'tgl_st',
                    'label'=>'Tanggal Awal',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'tgl_fn',
                    'label'=>'Tanggal Akhir',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function magang_laporan(){
        $data["page"] = "bakes_laporan_filter";
        $data["tmp_magang"] = "";
        $data_tmp_magang = $this->mdun->get_tmp_magang();
        $str_option = "";

        $data["pemohon"] = array();

        foreach ($data_tmp_magang as $r_data => $v_data) {
            $data["tmp_magang"][$v_data->id_dinas] = $v_data->nama_dinas;
            $str_option .= "<option value=\"".$v_data->id_dinas."\">".$v_data->nama_dinas."</option>";
        }

        $data["str_option"] = $str_option;
        $data["post"] = "";

        if(!empty($_POST)){
            $data["post"] = $_POST;

            $msg_detail = array(
                            "date_st"=>"",
                            "date_fn"=>""
                        );

            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

            if($this->validation_penelitian_laporan()){
                $in_kategori = $this->input->post("kategori");
                $in_date_st = $this->input->post("tgl_st");
                $in_date_fn = $this->input->post("tgl_fn");
                $in_opd = $this->input->post("opd");

                switch ($in_kategori) {
                    case '0':
                            $data_pemohon = $this->adn->get_pemohon_report_filter_base_on_date("tgl_acc", $in_date_st, $in_date_fn, "1");
                            $data["pemohon"] = $data_pemohon;
                        break;

                    case '1':
                            $data_pemohon = $this->adn->get_pemohon_report_filter_base_on_date_opd("tgl_diterima", $in_date_st, $in_date_fn, $in_opd, "1");
                            $data["pemohon"] = $data_pemohon;
                        break;

                    case '2':
                            $data_pemohon = $this->adn->get_pemohon_report_filter_base_on_active_user($in_date_st, $in_date_fn, "1");
                            $data["pemohon"] = $data_pemohon;
                        break;
                    
                    default:
                        break;
                }
                $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }else {
                $msg_detail = array(
                            "date_st"=>form_error("tgl_st"),
                            "date_fn"=>form_error("tgl_fn")
                        );
            }
        }else {

        }

        // print_r("<pre>");
        // print_r($data);

        $this->load->view('v_index_admin_new', $data); 
    }
#=============================================================================#
#-------------------------------------------Data_filter_all-------------------#
#=============================================================================#


}
?>