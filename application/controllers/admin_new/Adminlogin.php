<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlogin extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('admin/Admin_new', 'ad');

        $this->load->library("response_message");
        $session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            //echo "yes";
            if($session["status_active"] == "1" and $session["is_log"] == "1"){
                //echo "suip";
                if($session["id_lv"] == 2){
                    redirect(base_url("bakesbangpol/home"));
                }elseif($session["id_lv"] == 3){
                    redirect(base_url("opd/home"));
                }elseif($session["id_lv"] == 1){
                    redirect(base_url("super/home"));
                }elseif($session["id_lv"] == 4){
                    redirect(base_url("barenlitbang/home"));
                }
            }
        }else{

        }
	}
    
    public function index(){
        $this->load->view('admin_new/admin_login');
    }
    
    public function auth(){
        if($this->val_form_log()){
            $email = $this->input->post('email');
    		$password = $this->input->post('password');
    		$where = array(
    			'email' => $email,
    			'password' => md5($password),
                'status_active' => "1",
                "a.is_del" => "0"
    			);
    		
            $cek = $this->ad->get_admin($where);
    		if(!empty($cek)){
                $cek["is_log"] = 1;
    			
                $this->session->set_userdata("admin_lv_1",$cek);
               
                $main_msg = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC"));
                $msg_array = $this->response_message->default_mgs($main_msg,null);
                $this->session->set_flashdata("response_login", $msg_array);         
                
                if($cek["id_lv"] == 2){
                    redirect(base_url("bakesbangpol/home"));
                }elseif($cek["id_lv"] == 3){
                    redirect(base_url("opd/home"));
                }elseif($cek["id_lv"] == 1){
                    redirect(base_url("super/home"));
                }elseif($cek["id_lv"] == 4){
                    redirect(base_url("barenlitbang/home"));
                }
                
    		}else{
                //echo "gagal";
                // print_r($_SESSION);
                
                $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
                $msg_array = $this->response_message->default_mgs($main_msg,null);
                $this->session->set_flashdata("response_login", $msg_array);         
                
    			redirect(base_url("back-admin/login"));
    		}     
        }else{
            $msg_detail = array(
                                "email" => form_error("email"),
                                "password" => form_error("password")  
                            );
                            
            $main_msg = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
            $msg_array = $this->response_message->default_mgs($main_msg,$msq_detail);
            $this->session->set_flashdata("response_login", $msg_array);
            redirect(base_url("back-admin/login"));
        }
          
    }
    
    private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_email',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
}
?>