<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_user extends CI_Model {
    
    public function get_user_login($where){
        $this->db->select("id_user, email, nama, url_profil");
        $data = $this->db->get_where("user", $where)->row_array();
        if(count($data) > 0){
            return $data;
        }
        return null;
    }
}
