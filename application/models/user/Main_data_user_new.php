<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_data_user_new extends CI_Model{  
#-------------------------------------Daftar_Magang--------------------------------
    public function get_tmp_magang(){
        $data = $this->db->get("dinas")->result();
        return $data;
    }

    public function get_tmp_magang_active(){
        $this->db->where("is_del", "0");
        $data = $this->db->get("dinas")->result();
        return $data;
    }
    
    public function get_keperluan($where){
        $data = $this->db->get_where("permohonan", $where)->result();
        return $data;
    }

    public function get_keperluan_active(){
        $this->db->where("is_del", "0");
        $data = $this->db->get("permohonan")->result();
        return $data;
    }
    
    public function cek_data_aktif($where){
        $this->db->where("status_m!=", "2");
        $count_data = $this->db->get_where("pemohon", $where)->num_rows();
        return $count_data;
    }
    
    public function get_pemohon($where){
        $this->db->join("permohonan pr", "pn.id_permohonan=pr.id_permohonan");
        $data = $this->db->get_where("pemohon_new pn", $where)->row_array();
        return $data;
    }

    public function get_pemohon_complete($where){
        $this->db->join("permohonan pr", "pn.id_permohonan=pr.id_permohonan");
        $data = $this->db->get_where("pemohon_new pn", $where)->result();
        return $data;
    }
    
    #------ daftar hapus
    public function get_anggota($id_pemohon){
        $data = $this->db->get_where("anggota", array("id_pemohon"=>$id_pemohon))->result();
        return $data;
    }
#-------------------------------------Profil---------------------------------------

    public function get_data_user($where){
        $this->db->select("id_user, email, nama, nik, tlp, alamat, alamat_dom, instansi, pekerjaan, tgl_lhr, url_profil");
        $data = $this->db->get_where("user",$where)->row_array();
        if(!empty($data)){
            return $data;
        }
        
        return null;
    }
    
    public function update_pemohon($set, $where){
        $update = $this->db->update("pemohon_new", $set, $where);
        return $update;
    }

    public function cek_email_avail($email, $id){
        $this->db->where("id_user!=$id");
        $data = $this->db->get_where("user", array("email"=>$email))->result();
        if(count($data) == 0){
            return true;
        }
        return false;
    }
    
    public function get_email_from_id($where){
        $this->db->select("email");
        $data = $this->db->get_where("user", $where)->row_array();
        if(!empty($data)){
            return $data["email"];
        }
        return null;
    }
    
    public function get_jenis_permohonan($where){
        $data = $this->db->get_where("permohonan", $where)->row_array();
        return $data;
    }
    //public function get_nik($email){
        
    //}
#-------------------------------------Edit_Profil----------------------------------

    public function update_profil($set, $where){
        $update = $this->db->update("user", $set, $where);
        if($update){
            return true;
        }
        return false;
    }
#-------------------------------------instansi-------------------------------------

    public function update_ins($set, $where){
        $update = $this->db->update("user", $set, $where);
        return $update;
    }
#-------------------------------------history--------------------------------------
    public function get_pemohon_history($where){
        $this->db->join("permohonan pr", "pr.id_permohonan = p.id_permohonan");
        $data = $this->db->get_where("pemohon p",$where)->result();
        return $data;
    }
    
    public function delete_pemohon($where){
        $delete = $this->db->delete("pemohon_new", $where);
        return $delete;
    }

    // public function update_pemohon($where){
    //     $update = $this->db->update("pemohon", $where);
    //     return $update;
    // }
#-------------------------------------surat----------------------------------------
    public function pernyataan($where){
        $this->db->join("user us", "us.id_user = p.id_user");
        $this->db->join("doc_pemohon dp", "dp.id_pemohon = p.id_pemohon");
        $data = $this->db->get_where("pemohon p",$where)->row_array();
        return $data;
    }

    public function get_data_count_pemohon($where){
        $this->db->join("permohonan pr", "p.id_permohonan = pr.id_permohonan");
        $this->db->where("p.no_register!= ", "0");
        $this->db->where("p.status_magang!= ", "0");
        $data = $this->db->get_where("pemohon p", $where)->num_rows();
        return $data;

    }
}
