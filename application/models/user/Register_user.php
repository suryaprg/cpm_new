<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_user extends CI_Model {
    
    public function insert_register(){
        
    }


#------------------------------------------------------------------get verification---------------------------------------------------------------

    public function check_db_vert($where, $data){
        $count_select = $this->db->get_where("user_vert", $where)->num_rows();
        if($count_select > 0){
            if($this->update_vert($where,$data)){
                return true;
            }
        }else{
            if($this->insert_vert($data)){
                return true;
            }
        }
        return false;
    }
    
    private function insert_vert($data){
        $insert = $this->db->insert("user_vert", $data);
        if($insert){
            return true;
        }
        return false;
    }
    
    private function update_vert($where, $data){
        $delete = $this->db->delete("user_vert",$where);
        if($delete){
            if($this->insert_vert($data)){
                return true;
            }
        }return false;
    }


#------------------------------------------------------------------get id from email---------------------------------------------------------------
    
    public function get_id_from_email($data){
        $this->db->select("id_user");
        $data = $this->db->get_where("user",$data)->row_array();
        if(count($data)>0){
            return $data["id_user"];
            //print_r($data);
        }
        
        return null;
    }
    
#------------------------------------------------------------------get param && code---------------------------------------------------------------
    
    public function get_param_code($where){
        $data = $this->db->get_where("user_vert",$where)->row_array();
        if(!empty($data)){
            return $data;
        }
        
        return null;
    }
    
#------------------------------------------------------------------update aktivasi user---------------------------------------------------------------
    
    public function update_activate_user($where){
        $update = $this->db->update("user",array("status_active"=>"1"),$where);
        if($update){
            return true;
        }
        
        return false;
    }

#------------------------------------------------------------------delete verifikasi user---------------------------------------------------------------
    
    public function delete_vert($where){
        $delete = $this->db->delete("user_vert",$where);
        if($delete){
            return true;
        }
        
        return false;
    }
}
