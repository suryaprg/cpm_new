<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_data_user extends CI_Model{
    
#------------------------------------------------------------Daftar Magang--------------------------------------------------------------------
    public function get_tmp_magang(){
        $data = $this->db->get("dinas")->result();
        
        return $data;
    }
    
    public function get_keperluan($where){
        $data = $this->db->get_where("permohonan", $where)->result();
        return $data;
    }
    
    public function cek_data_aktif($where){
        $this->db->where("status_m!=", "2");
        $count_data = $this->db->get_where("pemohon", $where)->num_rows();
        return $count_data;
    }
    
    public function get_pemohon($where){
        $data = $this->db->get_where("pemohon", $where)->row_array();
        return $data;
    }
    
    #------ daftar hapus
    public function get_anggota($id_pemohon){
        $data = $this->db->get_where("anggota", array("id_pemohon"=>$id_pemohon))->result();
        return $data;
    }
#------------------------------------------------------------Profil--------------------------------------------------------------------

    public function get_data_user($where){
        $this->db->select("id_user, email, nama, nik, tlp, alamat, alamat_dom, instansi, pekerjaan, tgl_lhr, url_profil");
        $data = $this->db->get_where("user",$where)->row_array();
        if(!empty($data)){
            return $data;
        }
        
        return null;
    }
    
    public function update_pemohon($set, $where){
        $update = $this->db->update("pemohon", $set, $where);
        if($update){
            return true;
        }
        return false;
    }

    public function cek_email_avail($email, $id){
        $this->db->where("id_user!=$id");
        $data = $this->db->get_where("user", array("email"=>$email))->result();
        if(count($data) == 0){
            //echo count($data);
            return true;
        }//echo count($data);
        return false;
    }
    
    public function get_email_from_id($where){
        $this->db->select("email");
        $data = $this->db->get_where("user", $where)->row_array();
        if(!empty($data)){
            return $data["email"];
        }
        return null;
    }
    
    public function get_jenis_permohonan($where){
        $data = $this->db->get_where("permohonan", $where)->row_array();
        return $data;
    }
    //public function get_nik($email){
        
    //}
#---------------------------------------------------------Edit Profil--------------------------------------------------------------------

    public function update_profil($set, $where){
        $update = $this->db->update("user", $set, $where);
        if($update){
            return true;
        }
        return false;
    }

#-----------------------------------------------------------anggota--------------------------------------------------------------------
    
    #------ daftar hapus
    public function insert_anggota($id_user,$nama,$nim){
        $insert = $this->db->query("Call insert_anggota(\"".$id_user."\",\"".$nama."\",\"".$nim."\")");
        if($insert){
            return true;
        }
        return false;
    }
    
     #------ daftar hapus
    public function delete_anggota($where){
        $delete = $this->db->delete("anggota",$where);
        return $delete;
    }

#-----------------------------------------------------------instansi---------------------------------------------------------------------

    public function update_ins($set, $where){
        $update = $this->db->update("user", $set, $where);
        return $update;
    }
    
    

#------------------------------------------------------------history--------------------------------------------------------------------
    public function get_pemohon_history($where){
        $this->db->join("permohonan pr", "pr.id_permohonan = p.id_permohonan");
        $data = $this->db->get_where("pemohon p",$where)->result();
        return $data;
    }
    
    public function delete_pemohon($where){
        $delete = $this->db->delete("pemohon", $where);
        return $delete;
    }

#------------------------------------------------------------surat--------------------------------------------------------------------
    public function pernyataan($where){
        $this->db->join("user us", "us.id_user = p.id_user");
        $this->db->join("doc_pemohon dp", "dp.id_pemohon = p.id_pemohon");
        $data = $this->db->get_where("pemohon p",$where)->row_array();
        return $data;
    }

    public function get_data_count_pemohon($where){
        $this->db->join("permohonan pr", "p.id_permohonan = pr.id_permohonan");
        $this->db->where("p.no_register!= ", "0");
        $this->db->where("p.status_magang!= ", "0");
        $data = $this->db->get_where("pemohon p", $where)->num_rows();
        return $data;

    }


#------ daftar hapus
#------------------------------------------------------------delegasi--------------------------------------------------------------------
    public function get_delegasi_where($where){
        $data = $this->db->get_where("delegasi", $where)->row_array();
        return $data;
    }

    public function insert_delegasi($data){
        $insert = $this->db->insert("delegasi", $data);
        return $insert;
    }

    public function update_delegasi($set, $where){
        $update = $this->db->update("delegasi", $set, $where);
        return $update;
    }

    public function delete_delegasi($where){
        $delete = $this->db->delete("delegasi", $where);
        return $delete;
    }
}
