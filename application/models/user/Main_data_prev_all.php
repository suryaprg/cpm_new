<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_data_prev_all extends CI_Model {
    
    public function get_permohonan($where){
        $row_pemohon = $this->db->get_where("pemohon", $where)->row_array();
        if(!empty($row_pemohon)){
            return array("res" => true, "id_pemohon"=>$row_pemohon["id_pemohon"], "kepentingan"=>$row_pemohon["id_permohonan"]);
        }
        return array("res" => false, "id_pemohon"=>null, "kepentingan"=>null);
    }
    
    public function get_doc($where){
        $row_doc = $this->db->get_where("doc_pemohon", $where)->row_array();
        if(!empty($row_doc)){
            
            return $row_doc;
        }
        return null;
    }
    
    public function update_status_magang( $set, $where){
        $update = $this->db->update("pemohon", $set, $where);
        return $update;
    }
    
    public function count_anggota($id_pemohon){
        $count_data = $this->db->get_where("anggota", array("id_pemohon"=>$id_pemohon))->num_rows();
        return $count_data;
    }

    public function get_kelengkapan($where){
        $this->db->join("permohonan pr", "pm.id_permohonan = pr.id_permohonan");
        return $this->db->get_where("pemohon pm", $where)->row_array();
    }

    public function get_kelengkapan_data($where){
        return $this->db->get_where("doc_pemohon", $where)->row_array();
    }
    
    public function delete_kelengkapan_all($where){
        
        $del_anggota = $this->db->delete("anggota", $where);
        $del_doc = $this->db->delete("doc_pemohon", $where);
        $del_pemohon = $this->db->delete("pemohon", $where);
        if($del_pemohon == true && $del_doc == true && $del_anggota == true){
            return true;
        }

        return false;
    }
    
}
