<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_data_doc extends CI_Model{
    
#------------------------------------------------------------Insert Doc--------------------------------------------------------------------

    public function insert_doc(){
        
    }
    
    public function cek_pemohon($where){
        $count_data = $this->db->get_where("doc_pemohon", $where)->num_rows();
        return $count_data;
    }
    
    public function get_doc($where){
        $data = $this->db->get_where("doc_pemohon", $where)->row_array();
        return $data;
    }
    
    public function update_doc($data,$where){
        $update = $this->db->update("doc_pemohon",$data,$where);
        return $update;
    }
}
