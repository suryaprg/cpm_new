<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_new extends CI_Model{
    
    public function get_admin($where){
        $this->db->select("id_admin, a.id_lv, al.ket, email, status_active, nama, jabatan, a.id_bidang, dn.nama_dinas");
        $this->db->join("admin_lv al", "al.id_lv = a.id_lv");
        $this->db->join("dinas dn", "dn.id_dinas = a.id_bidang");
        $data = $this->db->get_where("admin a", $where)->row_array();
        return $data;
    }
    
    public function get_pemohon($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
	    $this->db->join('user us','pe.id_user=us.id_user');
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        $this->db->where("pe.status_pendaftaran", "1");
        $this->db->where("pe.status_diterima", "0");
        $this->db->where("pe.status_magang", "0");
        $this->db->where("pe.status_active_mg", "0");
        $this->db->where("pe.instansi_penerima", "0");

        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_pemohon_acc($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        $this->db->where("pe.status_pendaftaran", "1");
        $this->db->where("pe.status_diterima", "0");
        $this->db->where("pe.status_magang!=", "0");

        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_pemohon_acc_active($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        $this->db->where("pe.status_pendaftaran", "1");
        $this->db->where("pe.status_magang!=", "0");
        $this->db->where("pe.status_active_mg", "1");

        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_pemohon_surat($where){
        $this->db->select('*');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');

        $this->db->where("us.is_del", "0");

        $query = $this->db->get_where('pemohon_new pe', $where)->row_array();
        //$this->db->where("pe.no_register", "0");
        //$query = $this->db->get()->row_array();
        return $query;
    }
    
    public function get_pemohon_report($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
	    $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        
        $this->db->where("pe.status_pendaftaran", "1");
        $this->db->where("pe.status_magang!=", "0");
        $this->db->where("pe.status_active_mg!=", "0");

        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_pemohon_all($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        
        $this->db->where("pe.status_pendaftaran", "1");
        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }

     public function get_pemohon_all_graph($jenis_pemohon, $year, $month){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);

        $this->db->where("YEAR(pe.tgl_acc)", $year);
        $this->db->where("MONTH(pe.tgl_acc)", $month);
        $this->db->where("pe.status_pendaftaran", "1");

        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_pemohon_report_filter_base_on_date($base_field_time, $date_st, $date_fn, $jenis_kegiatan){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');

        // $this->db->join('admin ad','pe.admin_terima=ad.id_admin');
        // $this->db->join('dinas dn','ad.id_bidang=dn.id_dinas');
        
        $this->db->where("pe.".$base_field_time."<=", $date_fn);
        $this->db->where("pe.".$base_field_time.">=", $date_st);

        $this->db->where("pe.is_del", "0");
        $this->db->where('pr.jenis_kegiatan', $jenis_kegiatan);

        $query = $this->db->get()->result();
        return $query;
    }

    public function get_pemohon_report_filter_base_on_date_opd($base_field_time, $date_st, $date_fn, $opd, $jenis_kegiatan){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where("pe.".$base_field_time."<=", $date_fn);
        $this->db->where("pe.".$base_field_time.">=", $date_st);
        $this->db->where("pe.instansi_penerima", $opd);

        $this->db->where("pe.is_del", "0");
        $this->db->where('pr.jenis_kegiatan', $jenis_kegiatan);

        $query = $this->db->get()->result();
        return $query;
    }

     public function get_pemohon_report_filter_base_on_active_user($date_st, $date_fn, $jenis_kegiatan){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        
        // $this->db->where("pe.tgl_selesai>=", $date_st);
        // $this->db->where("pe.tgl_selesai<=", $date_fn);

        $this->db->where("pe.status_active_mg", "1");

        $this->db->where("pe.is_del", "0");
        $this->db->where('pr.jenis_kegiatan', $jenis_kegiatan);

        $query = $this->db->get()->result();
        return $query;
    }
    
    public function cek_no_reg($where){
        $this->db->join("permohonan pm", "pn.id_permohonan = pm.id_permohonan");
        $this->db->where("pn.is_del", "0");
        return $this->db->get_where("pemohon_new pn", $where)->row_array();
    }

	public function update_register($id_pemohon, $permohonan, $tgl_acc, $admin_acc, $text_no_acc, $status_magang, $status_active){
        $no_register = $this->db->query("select update_no_reg('".$id_pemohon."', '".$permohonan."','".$tgl_acc."','".$admin_acc."','".$text_no_acc."','".$status_magang."','".$status_active."') as no_register;");
        return $no_register;
    }

    public function remove_register($set, $where){
        $update = $this->db->update("pemohon_new", $set, $where);
        return $update;
    }

    
#-------------------------------------------------------------OPD----------------------------------------------------------

    public function list_opd_acc(){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
	    $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where("pe.status_pendaftaran", "1");
        $this->db->where("pe.status_magang", "1");
        $this->db->where("pe.status_diterima", "0");
        $this->db->where("pe.status_active_mg", "1");

        $this->db->where("pe.no_register!=", "0");
        $this->db->where("pe.no_register!=", "");

        $this->db->where("pe.admin_acc!=", "0");
        $this->db->where("pe.admin_acc!=", "");

        $this->db->where('pr.jenis_kegiatan','1');

        $this->db->where("pe.is_del", "0");

        //$this->db->where("pe.tgl_selesai >=", date("Y-m-d"));
        //$this->db->where("pe.no_register !=", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }

    public function list_opd_acc_look($id_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where("pe.status_pendaftaran", "1");
        $this->db->where("pe.status_magang", "1");
        $this->db->where("pe.status_diterima", "0");
        $this->db->where("pe.status_active_mg", "1");

        $this->db->where("pe.no_register!=", "0");
        $this->db->where("pe.no_register!=", "");

        $this->db->where("pe.admin_acc!=", "0");
        $this->db->where("pe.admin_acc!=", "");

        $this->db->where("pe.id_pemohon", $id_pemohon);

        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->row_array();
        return $query;
    }

    public function list_opd_active(){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where("pe.status_pendaftaran", "1");
        $this->db->where("pe.status_magang", "1");
        $this->db->where("pe.status_active_mg", "1");
        
        $this->db->where("pe.no_register!=", "0");
        $this->db->where("pe.no_register!=", "");

        $this->db->where("pe.admin_acc!=", "0");
        $this->db->where("pe.admin_acc!=", "");

        $this->db->where('pr.jenis_kegiatan','1');

        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }
    
    public function list_opd_report(){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where("pe.status_pendaftaran", "1");
        $this->db->where("pe.status_magang!=", "0");
        $this->db->where("pe.status_active_mg!=", "0");
        
        $this->db->where("pe.no_register!=", "0");
        $this->db->where("pe.no_register!=", "");

        $this->db->where("pe.admin_acc!=", "0");
        $this->db->where("pe.admin_acc!=", "");

        $this->db->where('pr.jenis_kegiatan','1');

        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_opd_report_filter_base_on_date($base_field_time, $date_st, $date_fn, $opd, $jenis_kegiatan){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where("pe.".$base_field_time."<=", $date_fn);
        $this->db->where("pe.".$base_field_time.">=", $date_st);
        $this->db->where("pe.instansi_penerima", $opd);

        $this->db->where("pe.is_del", "0");
        $this->db->where('pr.jenis_kegiatan', $jenis_kegiatan);

        $query = $this->db->get()->result();
        return $query;
    }

     public function get_opd_report_filter_base_on_active_user($date_st, $date_fn, $opd, $jenis_kegiatan){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where("pe.tgl_start<=", $date_fn);
        $this->db->where("pe.tgl_start>=", $date_st);

        $this->db->where("pe.tgl_selesai<=", $date_fn);

        $this->db->where("pe.instansi_penerima", $opd);

        $this->db->where("pe.is_del", "0");
        $this->db->where('pr.jenis_kegiatan', $jenis_kegiatan);

        $query = $this->db->get()->result();
        return $query;
    }
    
    
    public function update_pemohon($set,$where){
        $data = $this->db->update("pemohon_new", $set, $where);
        return $data;
    }

    
    public function get_pemohon_where($where){
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        $this->db->where("pe.is_del", "0");
        $pemohon = $this->db->get_where("pemohon_new pe", $where)->row_array();
        return $pemohon;
    }
}
?>