<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;

$route["data/mainform"]		= "user/maindatauser/index";
$route["data/doc"] 			= "user/maindatadoc/index";
$route["data/home"] 		= "user/maindatauser/index_home";
$route["data/tutorial"] 	= "user/maindatauser/index_cara_input";
$route["data/history"] 		= "user/maindatauser/index_history";
$route["data/notification"] = "user/maindatauser/index_pengumuman";
$route["data/ckdoc"]		= "user/maindataprevall/index";

// $route["admin/login"] = "admin/adminlogin/index";
// $route["admin/logout"] = "admin/adminall/logout";


//admin super
$route["super/home"] 		= "admin_super/superadmin/index";
$route["super/dinas"] 		= "admin_super/superadmin/index_dinas";
$route["super/permohonan"] 	= "admin_super/superadmin/index_permohonan";
$route["super/admin"] 		= "admin_super/superadmin/index_admin";
$route["super/user"] 		= "admin_super/superadmin/index_user";
$route["super/lv"] 			= "admin_super/superadmin/index_admin_lv";

$route["super/penelitian"] 	= "admin_super/superadmin/index_penelitian";
$route["super/magang"] 		= "admin_super/superadmin/index_magang";

$route["back-admin/login"] 			= "admin_new/adminlogin/index";
$route["back-admin/logout"] 		= "admin_new/adminall/logout";

$route["back-admin/bakesbangpol/accept"] = "admin_new/adminbakes/prove";
$route["back-admin/bakesbangpol/remove"] = "admin_new/adminbakes/remove";

$route["back-admin/barenlitbang/accept"] = "admin_new/Adminbaren/prove";
$route["back-admin/barenlitbang/remove"] = "admin_new/Adminbaren/remove";

$route["action/opd/accept"] = "admin_new/adminopd/prove";
$route["action/opd/remove"] = "admin_new/adminopd/remove";

$route["home"] 						= "user_new/user/index";
$route["home/form"] 				= "user_new/user/isi_form";
$route["home/persyaratan"] 			= "user_new/user/hal_persyaratan";
$route["home/kontak"] 				= "user_new/user/hal_kontak";
$route["home/pendaftaran"] 			= "user_new/registeruser/index";
$route["home/login"] 				= "user_new/loginuser/index";

$route["pendaftaran/mainform"]		= "user_new/maindatausernew/index_pendaftaran";
$route["pendaftaran/home"] 			= "user_new/maindatausernew/index";
$route["pendaftaran/tutorial"] 		= "user_new/maindatausernew/index_cara_input";
$route["pendaftaran/history"] 		= "user_new/maindatausernew/index_history";
$route["pendaftaran/accept"]		= "user_new/maindatausernew/index_fixed_pendaftaran";
$route["pendaftaran/logout"]		= "user_new/Logout/index";

//admin bakes
$route["bakesbangpol/home"] 		= "admin_new/adminbakes/index";
$route["bakesbangpol/penelitian"] 	= "admin_new/adminbakes/penelitian";
$route["bakesbangpol/monitor"] 		= "admin_new/adminbakes/penelitian_active";
$route["bakesbangpol/report"] 		= "admin_new/adminbakes/penelitian_report";
$route["bakesbangpol/magang_filter"] 		= "admin_new/adminbakes/magang_laporan";
// $route["bakesbangpol/penelitian_filter"] 	= "admin_new/adminbakes/magang_laporan";

//admin barenlitbang
$route["barenlitbang/home"] 		= "admin_new/Adminbaren/index";
$route["barenlitbang/penelitian"] 	= "admin_new/Adminbaren/penelitian";
$route["barenlitbang/monitor"] 		= "admin_new/Adminbaren/penelitian_active";
$route["barenlitbang/report"] 		= "admin_new/Adminbaren/penelitian_report";
$route["barenlitbang/penelitian_filter"] 	= "admin_new/Adminbaren/magang_laporan";

//admin opd
$route["opd/home"] 		= "admin_new/adminopd/index";
$route["opd/acc"] 		= "admin_new/adminopd/index_acc_opd";
$route["opd/monitor"] 	= "admin_new/adminopd/index_monitor_opd";
$route["opd/report"] 	= "admin_new/adminopd/index_report_opd";

$route["opd/magang_filter"] 		= "admin_new/adminopd/magang_laporan";
$route["opd/penelitian_filter"] 	= "admin_new/adminopd/penelitian_laporan";

$route["opd/penelitian"] 	= "admin_new/adminopd/penelitian_active";

//action super
$route["super/admin_lv/add_"]["post"]	= "admin_super/superadmin/insert_admin_lv";
$route["super/admin_lv/delete_"]["post"]	= "admin_super/superadmin/delete_admin_lv";

$route["super/putpass_"]["post"]	= "admin_super/superadmin/get_password";
$route["super/dontkillme_"]["post"]	= "admin_super/superadmin/change_pass";
