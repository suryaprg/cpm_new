-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2019 at 10:00 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bangkesdb_new`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_last_key` ()  SELECT id_pemohon
	FROM pemohon 
        order by id_pemohon DESC limit 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_anggota` (IN `id_pemohon` VARCHAR(12), IN `nama_anggota` VARCHAR(64), IN `nim` VARCHAR(32))  BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(14);
  
  select count(*) into count_row_user from anggota 
  	where left(id_anggota, 8) = left(NOW()+0, 8);
        
  select id_anggota into last_key_user from anggota
  	where left(id_anggota, 8) = left(NOW()+0, 8)
  	order by id_anggota desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat(left(NOW()+0, 8),"000001");
  else
      	set fix_key_user = last_key_user+1;
      
  END IF;
  
  insert into anggota values(fix_key_user, id_pemohon, nama_anggota, nim);
  
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_doc` (IN `id_pemohon` VARCHAR(12), IN `url_ktp` VARCHAR(64), IN `url_proposal` VARCHAR(64), IN `url_sk` VARCHAR(64), IN `no_surat` VARCHAR(64), IN `nama_tdd` VARCHAR(100), IN `jabatan` VARCHAR(32), IN `tgl_surat` DATE)  BEGIN
  declare last_key_doc varchar(20);
  declare count_row_doc int;
  declare fix_key_doc varchar(12);
  
  select count(*) into count_row_doc from doc_pemohon 
  	where left(id_doc, 8) = left(NOW()+0, 8);
        
  select id_doc into last_key_doc from doc_pemohon
  	where left(id_doc, 8) = left(NOW()+0, 8)
  	order by id_doc desc limit 1;
        
  if(count_row_doc <1) then
  	set fix_key_doc = concat(left(NOW()+0, 8),"0001");
  else
      	set fix_key_doc = last_key_doc+1;
  END IF;
  
  insert into doc_pemohon values(fix_key_doc, id_pemohon, url_ktp, url_proposal, url_sk, no_surat, tgl_surat, nama_tdd, jabatan);
  
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `tb_cek` (IN `sip` VARCHAR(32))  BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(20);
  
  select count(*) into count_row_user from tb_cek 
  	where left(id_cek, 8) = left(NOW()+0, 8);
        
  select id_cek into last_key_user from tb_cek
  	where left(id_cek, 8) = left(NOW()+0, 8)
  	order by id_cek desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat(left(NOW()+0, 8),"000001");
  else
      	set fix_key_user = last_key_user+1;
      
  END IF;
  
  insert into tb_cek values(fix_key_user, sip);
  
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `func_insert_admin` (`nama` VARCHAR(100), `email` VARCHAR(50), `nip` VARCHAR(25), `jabatan` VARCHAR(50), `id_bidang` VARCHAR(12), `lv` INT(2), `pass` VARCHAR(32), `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS VARCHAR(14) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, '0', lv, email, pass, '0', nama, nip, jabatan, id_bidang, '0', '0000-00-00 00:00:00', admin_del, time_update);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `func_insert_user` (`email` TEXT, `password_in` VARCHAR(32), `nama` VARCHAR(64), `alamat` TEXT, `alamat_dom` TEXT, `jenis_identitas` ENUM('0','1','2'), `nik` VARCHAR(16), `tlp` VARCHAR(16), `tgl_lhr` DATE, `url_profil` VARCHAR(64), `pekerjaan` VARCHAR(64), `instansi` TEXT) RETURNS VARCHAR(12) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from user 
  	where left(id_user, 8) = left(NOW()+0, 8);
        
  select id_user into last_key_user from user
  	where left(id_user, 8) = left(NOW()+0, 8)
  	order by id_user desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat(left(NOW()+0, 8),"0001");
  else
      	set fix_key_user = last_key_user+1;
      
  END IF;  
  
  insert into user values(fix_key_user, email, password_in, nama, alamat, alamat_dom, jenis_identitas, nik, tlp, "0", tgl_lhr, url_profil, pekerjaan, instansi, '0', '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00');
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `last_key_pemohon` (`id_permohonan` INT, `no_register` VARCHAR(28), `tgl_start` DATE, `tgl_selesai` DATE, `judul` TEXT, `instansi_penerima` TEXT, `id_user` VARCHAR(12), `instansi_pendaftar` TEXT, `id_bidang` VARCHAR(64), `dosen_pembimbing` TEXT, `anggota` TEXT, `status_pendelegasian` ENUM('0','1'), `url_ktp_pemohon` TEXT, `url_proposal_pemohon` TEXT, `url_sk_pemohon` TEXT, `jabatan_tdd` VARCHAR(32), `nama_tdd` VARCHAR(100), `no_surat_tdd` VARCHAR(64), `tgl_surat_tdd` DATE, `alamat_dom_del` TEXT, `alamat_ktp_del` TEXT, `nama_del` VARCHAR(64), `nik_del` VARCHAR(32), `url_foto_del` TEXT, `url_ktp_del` TEXT, `url_srt_del` TEXT, `status_pendaftaran` ENUM('0','1'), `status_magang` ENUM('0','1','2'), `status_diterima` ENUM('0','1'), `tgl_daftar` DATETIME, `tgl_diterima` DATETIME, `admin_terima` VARCHAR(12), `admin_acc` VARCHAR(12), `tgl_acc` DATETIME, `text_no_acc_bakes` LONGTEXT, `text_no_acc_opd` LONGTEXT) RETURNS VARCHAR(20) CHARSET latin1 BEGIN
  declare last_key varchar(20);
  declare count_row int;
  declare fix_key varchar(12);
  
  select count(*) into count_row from pemohon_new 
  	where left(id_pemohon, 8) = left(NOW()+0, 8);
    
  select right(id_pemohon,4) into last_key from pemohon_new
  	where left(id_pemohon, 8) = left(NOW()+0, 8)
  	order by id_pemohon desc limit 1;
        
  if(count_row = 0) then
  	set fix_key = concat(left(NOW()+0, 8),"0001");
  elseif(count_row > 0 and count_row < 10) then
  	set fix_key = concat(left(NOW()+0, 8),"000",last_key+1);
  elseif(count_row > 10 and count_row < 100) then
  	set fix_key = concat(left(NOW()+0, 8),"00",last_key+1);
  elseif(count_row > 100 and count_row < 1000) then
  	set fix_key = concat(left(NOW()+0, 8),"0",last_key+1);
  elseif(count_row > 1000 and count_row < 10000) then
  	set fix_key = concat(left(NOW()+0, 8),last_key+1);
  END IF;
  
  insert into pemohon_new VALUES(fix_key,id_permohonan, no_register, tgl_start, tgl_selesai, judul, instansi_penerima, id_user, instansi_pendaftar, id_bidang, dosen_pembimbing, anggota, status_pendelegasian, url_ktp_pemohon, url_proposal_pemohon, url_sk_pemohon, jabatan_tdd, nama_tdd, no_surat_tdd, tgl_surat_tdd, alamat_dom_del, alamat_ktp_del, nama_del, nik_del, url_foto_del, url_ktp_del, url_srt_del, status_pendaftaran, status_magang, status_diterima, tgl_daftar, tgl_acc, tgl_diterima, admin_terima, admin_acc,text_no_acc_bakes,text_no_acc_opd,'0','0','0000-00-00 00:00:00', '0');
  
  return fix_key;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `last_key_user` () RETURNS VARCHAR(12) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from user 
  	where left(id_user, 8) = left(NOW()+0, 8);
        
  select id_user into last_key_user from user
  	where left(id_user, 8) = left(NOW()+0, 8)
  	order by id_user desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat(left(NOW()+0, 8),"0001");
  else
      	set fix_key_user = last_key_user+1;
      
  END IF;
  
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `update_no_reg` (`id_pemohon_in` VARCHAR(12), `jenis_penelitian` VARCHAR(2), `tgl_acc` DATETIME, `admin_acc` VARCHAR(12), `text_no_acc` LONGTEXT, `status_magang` ENUM('0','1','2','3'), `status_active_in` ENUM('0','1','2')) RETURNS VARCHAR(30) CHARSET latin1 BEGIN
  declare last_no_reg varchar(30);
  declare m_now varchar(2);
  declare c_select int;
  declare fix_no_reg varchar(30);
  
  select count(*) into c_select from pemohon_new where right(no_register,4) = left(now()+0,4) and substr(no_register,9,2) = substr(now()+0,5,2);   
  select substr(no_register,5,3) into last_no_reg 
  	from pemohon_new 
        where right(no_register,4) = left(now()+0,4) 
        and substr(no_register,9,2) = substr(now()+0,5,2)
        order by no_register desc limit 1;   
 
   set m_now = substr(now()+0,5,2);
  if(c_select <= 0) then
  	set fix_no_reg = concat("072/001",".",m_now,".", jenis_penelitian,"/35.73.406/",left(now()+0,4));
  else
  	if(last_no_reg+1 > 1 and last_no_reg+1 < 10) then
  		set fix_no_reg = concat("072/00",last_no_reg+1,".",m_now,".", jenis_penelitian,"/35.73.406/",left(now()+0,4));
  	elseif(last_no_reg+1 >= 10 and last_no_reg+1 < 100) then
  		set fix_no_reg = concat("072/0",last_no_reg+1,".",m_now,".", jenis_penelitian,"/35.73.406/",left(now()+0,4));
  	elseif(last_no_reg+1 >=100 and last_no_reg+1 < 1000) then
  		set fix_no_reg = concat("072/0",last_no_reg+1,".",m_now,".", jenis_penelitian,"/35.73.406/",left(now()+0,4));
 	end if;	
  end if;
  
update pemohon_new set no_register=fix_no_reg, status_magang=status_magang, tgl_acc=tgl_acc, admin_acc=admin_acc, text_no_acc_bakes=text_no_acc, status_active_mg=status_active_in where id_pemohon = id_pemohon_in;
  
  RETURN fix_no_reg;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(12) NOT NULL,
  `del` enum('0','1') NOT NULL,
  `id_lv` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_bidang` char(12) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `del`, `id_lv`, `email`, `password`, `status_active`, `nama`, `nip`, `jabatan`, `id_bidang`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('AD2019020001', '0', 1, 'kominfo_super@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Suwardi Surya Ningrat', '19091999012130', 'staff bidang penelitian', '3', '0', '0000-00-00 00:00:00', '0', '2019-02-21 00:00:00'),
('AD2019020002', '0', 2, 'bakesbangpol@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Raditya Dika', '12097561409', 'Pelaksana Tugas', '20', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 04:23:13'),
('AD2019020003', '0', 3, 'diskominfo_admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Daniel Fernando', '19031994092011', 'Staff Umum', '3', '0', '2019-02-27 02:47:20', 'AD2019020001', '2019-02-27 02:45:23'),
('AD2019020004', '0', 4, 'barenlitbang@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Didi Kemput', '14091995022003', 'Staff Bagian Penelitian Publik', '31', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 02:53:52'),
('AD2019020005', '0', 2, 'dummy@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'dinda arumi', '12121990031999', 'Pelaksana Tugas', '3', '0', '2019-02-27 04:13:16', 'AD2019020001', '2019-02-27 04:24:31'),
('AD2019020006', '0', 3, 'disperin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Doni Andreas', '12031994051998', 'staff administrasi', '2', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 04:58:11'),
('AD2019020007', '0', 3, 'disbudpar@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Erick F', '12091985302000', 'staff tatausaha', '4', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 04:59:48');

-- --------------------------------------------------------

--
-- Table structure for table `admin_lv`
--

CREATE TABLE `admin_lv` (
  `id_lv` int(2) NOT NULL,
  `ket` varchar(32) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_lv`
--

INSERT INTO `admin_lv` (`id_lv`, `ket`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(1, 'Admin Super', '0', '0000-00-00 00:00:00', '0', '2019-02-21 00:00:00'),
(2, 'Admin BAKESBANGPOL', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 02:42:26'),
(3, 'Admin OPD', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 02:42:36'),
(4, 'Admin BARENLITBANG', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 02:42:48');

-- --------------------------------------------------------

--
-- Table structure for table `dinas`
--

CREATE TABLE `dinas` (
  `id_dinas` int(11) NOT NULL,
  `nama_dinas` varchar(500) NOT NULL,
  `alamat` text NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dinas`
--

INSERT INTO `dinas` (`id_dinas`, `nama_dinas`, `alamat`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(2, 'Dinas Perdagangan', 'Jl. Simp. Terusan Danau Sentani 3 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Dinas Komunikasi dan Informatika (DISKOMINFO)', 'Perkantoran Terpadu Gedung A Lt. 4 Malang, Jl. Mayjend. Sungkono Malang 65132', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, 'Dinas Kebudayaan dan Pariwisata (DISBUDPAR)', 'Museum Mpu Purwa, Jl. Sukarno Hatta B. 210 Malang 65142', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'Dinas Perindustrian (DISPERIN)', 'Perkantoran Terpadu Gedung A Lt.3, Jl. Mayjen Sungkono Malang 65132', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'Dinas Pendidikan (DISDIK)', 'Jl. Veteran 19 Malang 65145', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(8, 'Dinas Kesehatan (DINKES)', 'Jl. Simp. Laksda Adisucipto 45 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(9, 'Dinas Perhubungan (DISHUB)', 'Jl. Raden Intan 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, 'Dinas Pertanian dan Ketahanan Pangan', 'Jl. A. Yani Utara 202 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(11, 'Dinas Perumahan dan Kawasan Pemukiman (DISPERKIM)', 'Jl. Bingkil 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, 'Dinas Koperasi dan Usaha Mikro', 'Jl. Panji Suroso 18 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(15, 'Dinas Kepemudaan dan Olahraga (DISPORA)', 'Jl. Tenes Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(16, 'Dinas Kepedudukan dan Pencatatan Sipil (DISPENDUKCAPIL)', 'Perkantoran Terpadu Gedung A Lt. 2, Jl. Mayjen Sungkono Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(17, 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana (DP3AP2KB)', 'Jl. Ki Ageng Gribig Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(18, 'Dinas Lingkungan Hidup (DLH)', 'Jl. Mojopahit Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(19, 'Dinas Perpustakaan Umum dan Arsip Daerah', 'Jl. Besar Ijen No.30a Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(20, 'Badan Kesatuan Bangsa dan Politik (BAKESBANGPOL)', 'Jl. Jend. A. Yani 98 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(33, 'Badan Kepegawaian Daerah', 'Jl. Tugu 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPMPTSP)', 'Perkantoran Terpadu Gedung A Lt. 2 Malang,\r\nJl. Mayjend. Sungkono Malang 65132', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(27, 'Dinas Tenaga Kerja (DISNAKER)', 'Perkantoran Terpadu Gedung B Lt.3, Jl. Mayjen Sungkono Malang 65132', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(28, 'Dinas Pekerjaan Umum dan Penataan Ruang (DPUPR)', ' Jl. Bingkil 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(29, 'Dinas Sosial (DINSOS)', 'Jl. Sulfat No. 12 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(30, 'Inspektorat', 'Jl. Gajah Mada No. 2A Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(31, 'Badan Perencanaan, Penelitian dan Pengembangan (Barenlitbang)', 'Jl. Tugu No. 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(32, 'Badan Pelayanan Pajak Daerah (BP2D)', 'Perkantoran Terpadu Gedung B Lt.1 Jl. Mayjen Sungkono Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(34, 'Badan Pengelola Keuangan dan Aset Daerah (BPKAD)', 'Jl. Tugu 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(35, 'Badan Penanggulangan Bencana Daerah (BPBD)', 'Jl. Jend. A. Yani 98 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(36, 'Satuan Polisi Pamong Praja', 'Jl. Simpang Mojopahit No 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(37, 'Kecamatan Klojen', 'Jl. Surabaya 6 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(38, 'Kecamatan Blimbing', 'Jl. Raden Intan Kav. 14 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(39, 'Kecamatan Lowokwaru', 'Jl. Cengger Ayam I/12 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(40, 'Kecamatan Sukun', 'Jl. Keben I Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(41, 'Kecamatan Kedungkandang', 'Jl. Mayjen Sungkono 59 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(42, 'Badan Pengawas Pemilihan Umum (BAWASLU)', 'Jl. Teluk Cendrawasih No.206, Arjosari, Blimbing, Kota Malang, Jawa Timur 65126', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(43, 'Dewan Perwakilan Rakyat Daerah Kota Malang (DPRD)', 'Jl. Tugu No.1A, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(44, 'Rumah Potong Hewan Kota Malang', 'Jl. Kol. Sugiono No. 176, Malang, Jawa Timur 65148', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(45, 'PDAM Kota Malang ', 'Jl. Danau Sentani Raya No.100, Madyopuro, Kedungkandang, Kota Malang, Jawa Timur 65142', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(46, 'Komisi Pemilihan Umum Kota Malang (KPU) ', 'Jl. Bantaran No.6, Purwantoro, Blimbing, Kota Malang, Jawa Timur 65126', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(50, 'dinas perbuatan tidak menyenangkan', 'Malang', '1', '2019-02-27 03:35:41', 'AD2019020001', '2019-02-27 03:35:24');

-- --------------------------------------------------------

--
-- Table structure for table `pemohon_new`
--

CREATE TABLE `pemohon_new` (
  `id_pemohon` varchar(12) NOT NULL,
  `id_permohonan` int(11) NOT NULL,
  `no_register` varchar(28) NOT NULL,
  `tgl_start` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `judul` text NOT NULL,
  `instansi_penerima` int(3) NOT NULL,
  `id_user` varchar(12) NOT NULL,
  `instansi_pendaftar` text NOT NULL,
  `id_bidang` varchar(64) NOT NULL,
  `dosen_pembimbing` text NOT NULL,
  `anggota` text NOT NULL,
  `status_pendelegasian` enum('0','1') NOT NULL,
  `url_ktp_pemohon` text NOT NULL,
  `url_proposal_pemohon` text NOT NULL,
  `url_sk_pemohon` text NOT NULL,
  `jabatan_tdd` varchar(32) NOT NULL,
  `nama_tdd` varchar(100) NOT NULL,
  `no_surat_tdd` varchar(64) NOT NULL,
  `tgl_surat_tdd` date NOT NULL,
  `alamat_dom_del` text NOT NULL,
  `alamat_ktp_del` text NOT NULL,
  `nama_del` varchar(64) NOT NULL,
  `nik_del` varchar(32) NOT NULL,
  `url_foto_del` text NOT NULL,
  `url_ktp_del` text NOT NULL,
  `url_srt_del` text NOT NULL,
  `status_pendaftaran` enum('0','1') NOT NULL,
  `status_magang` enum('0','1','2','3') NOT NULL,
  `status_diterima` enum('0','1','2') NOT NULL,
  `tgl_daftar` datetime NOT NULL,
  `tgl_acc` datetime NOT NULL,
  `tgl_diterima` datetime NOT NULL,
  `admin_terima` varchar(12) NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_no_acc_bakes` longtext NOT NULL,
  `text_no_acc_opd` longtext NOT NULL,
  `status_active_mg` enum('0','1','2') NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemohon_new`
--

INSERT INTO `pemohon_new` (`id_pemohon`, `id_permohonan`, `no_register`, `tgl_start`, `tgl_selesai`, `judul`, `instansi_penerima`, `id_user`, `instansi_pendaftar`, `id_bidang`, `dosen_pembimbing`, `anggota`, `status_pendelegasian`, `url_ktp_pemohon`, `url_proposal_pemohon`, `url_sk_pemohon`, `jabatan_tdd`, `nama_tdd`, `no_surat_tdd`, `tgl_surat_tdd`, `alamat_dom_del`, `alamat_ktp_del`, `nama_del`, `nik_del`, `url_foto_del`, `url_ktp_del`, `url_srt_del`, `status_pendaftaran`, `status_magang`, `status_diterima`, `tgl_daftar`, `tgl_acc`, `tgl_diterima`, `admin_terima`, `admin_acc`, `text_no_acc_bakes`, `text_no_acc_opd`, `status_active_mg`, `is_del`, `time_del`, `admin_del`) VALUES
('201902260001', 5, '072/005.02.KL/35.73.406/2019', '2019-02-27', '2019-03-31', 'sistem informasi pengadaan barang di kota malang', 4, '201902260002', 'SARJANA;Sistem Informasi;Ilmu Komputer;STMIK PPKIA Pradnya Paramita Malang', '[\'2\',\'3\',\'4\']', '[\'Dr. Ali Mukti Raja\',\'Prof. Danu Setya Lubis\']', '[[\'Dimas Arya Nugraha\',\'12310005\'],[\'Donita Andreas\',\'15310054\']]', '1', 'ktp_201902260001.jpg', 'proposal_201902260001.pdf', 'sk_201902260001.jpg', 'Dekan', 'Rahmat Lie, S.Kom.,', 'PKI/12312/JKAS', '2019-02-26', 'Malang', 'Malang', 'Sarji Soeharto', '3573011903940007', 'foto_delegasi_201902260001.jpg', 'ktp_delegasi_201902260001.jpg', 'kuasa_delegasi_201902260001.jpg', '1', '1', '1', '2019-02-26 07:35:20', '2019-02-27 09:07:57', '2019-02-27 09:12:27', 'AD2019020007', 'AD2019020002', '', '[null,null,{\"status\":\"2\",\"date_action\":\"2019-02-27 09:11:43\",\"text\":\"judul tidak sesuai dengan instansi tempat mendaftar\",\"id_admin_accept\":\"AD2019020006\"},{\"status\":\"2\",\"date_action\":\"2019-02-27 09:08:32\",\"text\":\"judul tidak sesuai dengan tempat magang\",\"id_admin_accept\":\"AD2019020003\"},{\"status\":\"1\",\"date_action\":\"2019-02-27 09:12:27\",\"text\":\"-\",\"id_admin_accept\":\"AD2019020007\"}]', '1', '0', '2019-02-27 04:36:45', 'AD2019020001'),
('201902270001', 6, '072/004.02.KL/35.73.406/2019', '2019-02-28', '2019-03-28', 'Sistem Informasi Kependudukan Kota Malang', 3, '201902260001', 'NCC', '[\'2\',\'3\',\'4\']', '[\'Prof. Ali Al Basha\',\'Dr. Wijianto\']', '[[\'Dimas Arya Nugraha\',\'3573011903940006\'],[\'Doni Siregar\',\'3573044903560001\']]', '1', 'ktp_201902270001.jpg', 'proposal_201902270001.pdf', 'sk_201902270001.jpg', 'Dekan', 'Rahmat Lie, S.Kom.,', '10928/13/TIJ/2011', '2019-02-27', 'Malang', 'Malang', 'Ranggawarsita', '3573011903985404', 'foto_delegasi_201902270001.jpg', 'ktp_delegasi_201902270001.jpg', 'kuasa_delegasi_201902270001.jpg', '1', '1', '1', '2019-02-27 02:26:37', '2019-02-27 08:42:29', '2019-02-27 08:52:40', 'AD2019020003', 'AD2019020002', '', '[null,null,{\"status\":\"0\",\"date_action\":\"2019-02-27 05:02:11\",\"text\":\"judul tidak sesuai dengan instansi yang dipilih\",\"id_admin_accept\":\"AD2019020006\"},{\"status\":\"1\",\"date_action\":\"2019-02-27 08:52:40\",\"text\":\"-\",\"id_admin_accept\":\"AD2019020003\"},{\"status\":\"0\",\"date_action\":\"2019-02-27 05:07:41\",\"text\":\"-\",\"id_admin_accept\":\"AD2019020007\"}]', '1', '0', '2019-02-27 03:04:03', 'AD2019020001'),
('201902270002', 1, '0', '2019-03-01', '2019-04-30', 'Sistem Informasi Pembuangan Air Limbah', 0, '201902260001', 'NCC', '[\'4\',\'3\']', '[\'Prof. Aliudin Nor\',\'Dr. Radjiman\']', '[]', '1', 'ktp_201902270002.jpg', 'proposal_201902270002.pdf', 'sk_201902270002.jpg', 'Dekan', 'Rahmat Lie, S.Kom.,', 'PKI/12312/JKAS', '2019-02-27', 'Malang', 'Malang', 'Donal Trump', '3573022906940006', 'foto_delegasi_201902270002.jpg', 'ktp_delegasi_201902270002.jpg', 'kuasa_delegasi_201902270002.jpg', '1', '2', '0', '2019-02-27 06:24:11', '2019-02-27 09:31:03', '0000-00-00 00:00:00', '0', 'AD2019020004', '', 'aplikasi yang di jelaskan pada judul telah diimplementasikan di pemerintahan', '2', '0', '0000-00-00 00:00:00', '0'),
('201902270003', 2, '072/006.02.P/35.73.406/2019', '2019-02-28', '2019-03-31', 'Sistem Informasi Irigasi Online', 0, '201902260002', 'SARJANA;Sistem Informasi;Ilmu Komputer;STMIK PPKIA Pradnya Paramita Malang', '[\'2\',\'3\']', '[\'Prof. Suryadi Notonegoro\']', '[]', '0', 'ktp_201902270003.jpg', 'proposal_201902270003.pdf', 'sk_201902270003.jpg', 'Dekan', 'Rahmat Lie, S.Kom.,', '10928/13/TIJ/2011', '2019-02-27', '', '', '', '', '', '', '', '1', '1', '0', '2019-02-27 07:31:16', '2019-02-27 09:31:08', '0000-00-00 00:00:00', '0', 'AD2019020004', '', '[null,null,{\"status\":\"0\",\"date_action\":\"-\",\"text\":\"-\",\"id_admin_accept\":\"-\"},{\"status\":\"2\",\"date_action\":\"2019-02-27 08:52:57\",\"text\":\"judul tidak sesuai\",\"id_admin_accept\":\"AD2019020003\"}]', '1', '0', '0000-00-00 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `pemohon_penelitian`
--

CREATE TABLE `pemohon_penelitian` (
  `id_pemohon` varchar(12) NOT NULL,
  `id_permohonan` int(11) NOT NULL,
  `no_register` varchar(28) NOT NULL,
  `tgl_start` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `judul` text NOT NULL,
  `instansi_penerima` int(3) NOT NULL,
  `id_user` varchar(12) NOT NULL,
  `instansi_pendaftar` text NOT NULL,
  `id_bidang` varchar(64) NOT NULL,
  `dosen_pembimbing` text NOT NULL,
  `anggota` text NOT NULL,
  `status_pendelegasian` enum('0','1') NOT NULL,
  `url_ktp_pemohon` text NOT NULL,
  `url_proposal_pemohon` text NOT NULL,
  `url_sk_pemohon` text NOT NULL,
  `jabatan_tdd` varchar(32) NOT NULL,
  `nama_tdd` varchar(100) NOT NULL,
  `no_surat_tdd` varchar(64) NOT NULL,
  `tgl_surat_tdd` date NOT NULL,
  `alamat_dom_del` text NOT NULL,
  `alamat_ktp_del` text NOT NULL,
  `nama_del` varchar(64) NOT NULL,
  `nik_del` varchar(32) NOT NULL,
  `url_foto_del` text NOT NULL,
  `url_ktp_del` text NOT NULL,
  `url_srt_del` text NOT NULL,
  `status_pendaftaran` enum('0','1') NOT NULL,
  `status_magang` enum('0','1','2','3') NOT NULL,
  `status_diterima` enum('0','1','2') NOT NULL,
  `tgl_daftar` datetime NOT NULL,
  `tgl_acc` datetime NOT NULL,
  `tgl_diterima` datetime NOT NULL,
  `admin_terima` varchar(12) NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_no_acc_bakes` longtext NOT NULL,
  `text_no_acc_opd` longtext NOT NULL,
  `status_active_mg` enum('0','1','2') NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permohonan`
--

CREATE TABLE `permohonan` (
  `id_permohonan` int(11) NOT NULL,
  `keterangan_permohonan` varchar(64) NOT NULL,
  `jenis_kegiatan` enum('0','1') NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permohonan`
--

INSERT INTO `permohonan` (`id_permohonan`, `keterangan_permohonan`, `jenis_kegiatan`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(1, 'Skripsi', '0', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'Tesis', '0', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Disertasi', '0', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, 'Karya Tulis Ilmiah', '0', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(5, 'Program Kreativitas Mahasiswa', '1', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'Praktek Kerja Lapang / Magang', '1', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'Pengabdian Masyarakat', '1', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(8, 'Praktek Kerja industri', '1', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, 'Skripsick', '1', '1', '2019-02-27 03:34:55', 'AD2019020001', '2019-02-27 03:34:40');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` varchar(12) NOT NULL,
  `email` varchar(64) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `alamat` text NOT NULL,
  `alamat_dom` text NOT NULL,
  `jenis_identitas` enum('0','1','2') NOT NULL,
  `nik` varchar(16) NOT NULL,
  `tlp` varchar(16) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `tgl_lhr` date NOT NULL,
  `url_profil` varchar(64) NOT NULL,
  `pekerjaan` varchar(64) NOT NULL,
  `instansi` text NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`, `nama`, `alamat`, `alamat_dom`, `jenis_identitas`, `nik`, `tlp`, `status_active`, `tgl_lhr`, `url_profil`, `pekerjaan`, `instansi`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('201902260001', 'suryahanggara@gmail.com', 'aff8fbcbf1363cd7edc85a1e11391173', 'surya hanggara', 'malang', 'malang', '0', '3573011903940006', '0812310695774', '1', '1994-03-19', 'foto_pemohon_201902260001.jpg', '0', 'NCC', '0', '2019-02-27 03:03:08', 'AD2019020001', '0000-00-00 00:00:00'),
('201902260002', 'roberthanggara19@gmail.com', 'aff8fbcbf1363cd7edc85a1e11391173', 'Dimas arya nugraha', 'malang', 'malang selatan', '0', '3573011903940001', '0811230695775', '1', '1994-03-19', 'foto_pemohon_201902260002.jpg', '1', 'SARJANA;Sistem Informasi;Ilmu Komputer;STMIK PPKIA Pradnya Paramita Malang', '0', '2019-02-27 03:03:04', 'AD2019020001', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_vert`
--

CREATE TABLE `user_vert` (
  `id_user` varchar(12) NOT NULL,
  `time` datetime NOT NULL,
  `time_exp` datetime NOT NULL,
  `param` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_lv`
--
ALTER TABLE `admin_lv`
  ADD PRIMARY KEY (`id_lv`);

--
-- Indexes for table `dinas`
--
ALTER TABLE `dinas`
  ADD PRIMARY KEY (`id_dinas`);

--
-- Indexes for table `pemohon_new`
--
ALTER TABLE `pemohon_new`
  ADD PRIMARY KEY (`id_pemohon`);

--
-- Indexes for table `pemohon_penelitian`
--
ALTER TABLE `pemohon_penelitian`
  ADD PRIMARY KEY (`id_pemohon`);

--
-- Indexes for table `permohonan`
--
ALTER TABLE `permohonan`
  ADD PRIMARY KEY (`id_permohonan`),
  ADD UNIQUE KEY `id_permohonan` (`id_permohonan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_lv`
--
ALTER TABLE `admin_lv`
  MODIFY `id_lv` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dinas`
--
ALTER TABLE `dinas`
  MODIFY `id_dinas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `permohonan`
--
ALTER TABLE `permohonan`
  MODIFY `id_permohonan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
